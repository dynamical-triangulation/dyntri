use std::{cmp::Ordering, fmt::Display};

use crate::collections::Label as TypedLabel;

use super::{triangle::{Orientation, Triangle}, dynamic_triangulation::DynTriangulation, Triangulation};

type Label = TypedLabel<Triangle>;

struct LabelledLengthProfile {
    labels: Box<[Label]>,
    ups: Box<[usize]>,
}
impl LabelledLengthProfile {
    fn empty() -> Self {
        LabelledLengthProfile {
            labels: Box::new([]),
            ups: Box::new([]),
        }
    }

    fn from_triangulation(triangulation: &DynTriangulation) -> LabelledLengthProfile {
        let triangle_count = triangulation.size();
        let mut labels: Vec<Label> = Vec::with_capacity(triangle_count / 2);
        let mut ups: Vec<usize> = Vec::with_capacity(triangle_count / 2);

        // Pick random starting point
        let start = if let Some(label) = triangulation.labels().next() {
            label
        } else {
            return LabelledLengthProfile::empty();
        };

        let mut slice_start = start;
        let mut down_mark: Option<Label> = None;
        'time: for t in 0..triangle_count / 2 {
            labels.push(slice_start);
            ups.push(0);
            let mut mark = slice_start;
            'slice: for _ in 0..triangle_count {
                // Add triangle to the count
                match triangulation.get(mark).orientation {
                    Orientation::Up => ups[t] += 1,
                    Orientation::Down => {
                        down_mark.get_or_insert(mark);
                    }
                }
                // Move along the slice to the right
                mark = triangulation.get(mark).right;
                // Go to next slice if slice_start is reached again
                if mark == slice_start {
                    break 'slice;
                }
                // Stop if the original start is reached again
                if mark == start {
                    break 'time;
                }
            }
            slice_start = down_mark.expect(
                "No Down-triangle could be find in the timeslice, this should be impossible.",
            );
        }
        // Remove the last timeslice as it is the same as the first one
        ups.pop();

        LabelledLengthProfile {
            labels: labels.into_boxed_slice(),
            ups: ups.into_boxed_slice(),
        }
    }
}

const MAX_AMOUNT_TIMESLICES: usize = 10;

impl LabelledLengthProfile {
    fn find_minima(&self) -> Box<[Label]> {
        let mut min_count = usize::MAX;
        let mut min_labels = Vec::with_capacity(MAX_AMOUNT_TIMESLICES);
        for (&label, &up_count) in self.labels.iter().zip(self.ups.iter()) {
            match up_count.cmp(&min_count) {
                Ordering::Less => {
                    min_count = up_count;
                    min_labels.clear();
                    min_labels.push(label);
                }
                Ordering::Equal => {}
                _ => (),
            }
        }
        min_labels.into_boxed_slice()
    }
}

/// `Timeslice` is an `Iterator` that can walk through a slice given by any `Label` in the slice
/// in a rightward manner, starting at the given `Label`.
/// By returning the `Label`s along the way.
struct TimesliceIterator<'a> {
    triangulation: &'a DynTriangulation,
    slice_label: Label, // Some label marking a slice, taken as the 'start' of the slice
    current_label: Option<Label>,
}

impl DynTriangulation {
    /// Construct a `TimesliceIterator` from a `Triangulation` starting at `label`
    fn timeslice(&self, label: Label) -> TimesliceIterator {
        TimesliceIterator {
            triangulation: self,
            slice_label: label,
            current_label: None,
        }
    }
}

impl<'a> Iterator for TimesliceIterator<'a> {
    type Item = Label;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(label) = self.current_label {
            // Find right triangle
            let next = self.triangulation.get(label).right;
            // Check if triangle is not yet around
            if self.slice_label == next {
                return None;
            } else {
                self.current_label = Some(next);
            }
        } else {
            // Set label to right label
            self.current_label = Some(self.slice_label);
        }
        self.current_label
    }
}

/// Structure holding a unique ID for a `Triangulation`,
/// which can be used to compare `Triangulation`s.
/// 
/// `Triangulation`s with the same connectivity will have the same ID.
#[derive(Hash, PartialEq, PartialOrd, Eq, Ord)]
pub struct TriangulationID {
    id: Box<[Orientation]>,
    timeslice_indices: Box<[usize]>, // Indices of the starting points of the timeslices in `id`
    stitch_index: usize, // The index of the Up-Triangle mapping to the first Down-Triangle in the last slice
}

impl TriangulationID {
    fn empty() -> Self {
        TriangulationID {
            id: Box::new([]),
            timeslice_indices: Box::new([]),
            stitch_index: 0,
        }
    }
}

impl Display for TriangulationID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.id.len() == 0 {
            return writeln!(f);
        }
        // writeln!(f, "stitch index: {}", self.stitch_index)?;
        let mut indices = self.timeslice_indices.iter();
        let mut start = *indices.next().expect("timeslice_indices is empty");
        if let Some(&stop) = indices.next() {
            fmt_orientation_slice_marked(&self.id[start..stop], f, self.stitch_index)?;
            writeln!(f, ",")?;
            start = stop;
            for &stop in indices {
                fmt_orientation_slice(&self.id[start..stop], f)?;
                writeln!(f, ",")?;
                start = stop;
            }
            fmt_orientation_slice(&self.id[start..], f)
        } else {
            fmt_orientation_slice_marked(&self.id[start..], f, self.stitch_index)
        }
    }
}

fn fmt_orientation_slice(
    orientations: &[Orientation],
    f: &mut std::fmt::Formatter<'_>,
) -> std::fmt::Result {
    write!(
        f,
        "{}",
        orientations.iter().map(|o| o.char()).collect::<String>()
    )
}

fn fmt_orientation_slice_marked(
    orientations: &[Orientation],
    f: &mut std::fmt::Formatter<'_>,
    mark: usize,
) -> std::fmt::Result {
    write!(
        f,
        "{}",
        orientations
            .iter()
            .enumerate()
            .map(|(i, o)| if i == mark {
                o.char_special()
            } else {
                o.char()
            })
            .collect::<String>()
    )
}

// Do some crazy saving
impl DynTriangulation {
    /// Returns a unique ID for the `Triangulation`
    pub fn id(&self) -> TriangulationID {
        // Want to convert triangulation to list of binary numbers
        // Up -> 1, Down -> 0
        // Start each slice with the 1 that connects to the 0 of the previous slice
        // Have first slice be the one with the least amount of 1's
        // For any ambiguity pick the smallest one (a.k.a. the smallest starting number, if the same the smallest second number, etc.)
        //
        // The current solution is definitely not computationally optimal,
        // but this should not have a great impact as it is not expected to be required frequently.

        // Check if triangulation is empty
        if self.size() == 0 {
            return TriangulationID::empty();
        }

        let lengths = LabelledLengthProfile::from_triangulation(self);
        // First find the slice with the least amount of up triangles
        let min_labels = lengths.find_minima();
        // Then determine ID starting from all possible Up triangles and select first Lexicographic element
        min_labels
            .iter()
            .flat_map(|&label| self.timeslice(label))
            .filter(|&label| matches!(self.get(label).orientation, Orientation::Up))
            .map(|label| self.id_from_label(label))
            .min()
            .expect("No labels were found")
    }

    /// Create an ID starting from an `Up` `Triangle` given by `label`.
    ///
    /// Note that this will not work reliably if `label` does not point
    /// to an `Up` `Triangle`
    fn id_from_label(&self, start_label: Label) -> TriangulationID {
        let max_count = self.size() / 2;

        let mut slice_indices = Vec::with_capacity(max_count);
        let mut id = Vec::with_capacity(max_count);

        let mut slice_index = 0;
        let mut slice_label = start_label;
        'time: for t in 0..max_count {
            slice_indices.push(slice_index);

            let mut down_marker = None;
            for label in self.timeslice(slice_label) {
                if label == start_label && t > 0 {
                    break 'time;
                }

                let orientation = self.get(label).orientation;
                id.push(orientation);
                if down_marker.is_none() && matches!(orientation, Orientation::Down) {
                    down_marker = Some(label);
                }
            }
            slice_index = id.len();
            slice_label =
                self.get(down_marker.expect("No down triangle was found in the timeslice")).center;
        }
        // Remove unwanted last added slice index
        let id_size = slice_indices.pop().expect("Slice indices is empty");
        // Determine the stich index (the index of the up the first down in the last timeslice points to)
        let diff = id.len() - id_size;
        let length = if let Some(&index2) = slice_indices.get(1) {
            index2
        } else {
            id_size
        } - slice_indices
            .first()
            .expect("Somehow the slice indices are empty");
        let stitch_index = (length - diff) % length;
        // Remove unwanted parts of the id
        id.truncate(id_size);

        TriangulationID {
            id: id.into_boxed_slice(),
            timeslice_indices: slice_indices.into_boxed_slice(),
            stitch_index,
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    use crate::triangulation::dynamic_triangulation::DynTriangulation;

    #[test]
    fn create_id() {
        let mut triangulation = DynTriangulation::new(3, 3);
        println!("{}", triangulation.id());

        let mut rng = Xoshiro256StarStar::seed_from_u64(25);
        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..8 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..8 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        for _ in 0..30 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        println!("{}", triangulation.id());
    }
}
