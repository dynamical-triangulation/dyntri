//! Module implementing possible embeddings of graphs/triangulations
//! for visualisation purposes.

use argmin::core::{CostFunction, Executor, Gradient};
use argmin::solver::gradientdescent::SteepestDescent;
use argmin::solver::linesearch::MoreThuenteLineSearch;
use log::debug;
use ndarray::{Array1, ArrayView1};
use num_traits::Zero;

use std::f64::consts::{FRAC_PI_3, TAU};
use std::fmt::Display;

use crate::collections::{Label, LabelledArray};
use crate::graph::spacetime_graph;
use crate::io::embedding3d::{Edge, EdgeKind, Embedding3D, Embedding3DField, Triangle, Vertex};
use crate::triangulation::full_triangulation::vertex::Vertex as TriangulationVertex;
use crate::triangulation::{full_triangulation, FullTriangulation, Orientation};

use super::Point3D;

const RADIUS_RATIO: f64 = 0.8;

const ORIENTATION_COST_FACTOR: f64 = 0.24;
// const TIMESLICE_ROTATION_MAX_ITERATIONS: u64 = 100;
const EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION: u64 = 200;
const EDGEDISTANCE_DIFFERENCE_TOLERANCE: f64 = 1e-1;
// const TIMESLICE_WANTED_AVERAGE_LENGTH: f64 = 1.0;

type VertexLabel = Label<TriangulationVertex>;

/// Struct for holding the embedding of a [`FullTriangulation`]
pub struct TorusEmbedding<'a> {
    triangulation: &'a FullTriangulation,
    coordinates: LabelledArray<Point3D, VertexLabel>,
}

// impl TorusEmbedding<'_> {
//     fn get_time(&self, label: VertexLabel) -> usize {
//         self.triangulation.get_vertex(label).time()
//     }
// }

fn compute_coordinates(
    triangulation: &FullTriangulation,
    t: usize,
    i: usize,
    length: usize,
) -> Point3D {
    let tlen = triangulation.time_len() as f64;
    let average_ilen = triangulation.vertex_count() as f64 / tlen;

    let t = t as f64;
    let i = i as f64;
    let length = length as f64;

    // Choose outer_r such that the average distance between the timeslices
    // is sin(pi/3) such that the average edge length is close to 1 when flat
    let outer_r = tlen * FRAC_PI_3.sin() / TAU;
    // Choose inner_r such that the average radius is RADIUS_RATIO * outer_r
    let inner_r = length / average_ilen * RADIUS_RATIO * outer_r;

    let outer_angle = TAU * t / tlen;
    let inner_angle = TAU * i / length;

    let sradius = outer_r + inner_r * inner_angle.cos();
    Point3D {
        x: sradius * outer_angle.cos(),
        y: sradius * outer_angle.sin(),
        z: inner_r * inner_angle.sin(),
    }
}

impl<'a> TorusEmbedding<'a> {
    /// Generates an simple cylindrical embedding with independent timeslices
    /// kept in equidistantly spaced planes.
    pub fn initialize(triangulation: &'a FullTriangulation) -> Self {
        // Create empty list of coordinates
        let mut coordinates: LabelledArray<Option<Point3D>, VertexLabel> =
            LabelledArray::fill(None, triangulation.vertex_count());

        // Compute the coordinates of all vertices by walking through the timeslices
        let tmax = triangulation.time_len();
        for t in 0..tmax {
            let length = triangulation.timeslice_length(t);
            for (i, vertex) in triangulation.iter_time(t).enumerate() {
                coordinates[vertex] = Some(compute_coordinates(triangulation, t, i, length));
            }
        }

        let coordinates =
            coordinates.map(|element| element.expect("A vertex coordinate was not set"));
        TorusEmbedding {
            triangulation,
            coordinates,
        }
    }
}

impl TorusEmbedding<'_> {
    pub fn export(&self) -> Embedding3D {
        let vertices: Vec<Vertex> = self
            .coordinates
            .enumerate()
            .map(|(label, point)| {
                let coordinate = (point.x, point.y, point.z);
                let time = self.triangulation.get_vertex(label).time() as i32;
                Vertex { coordinate, time }
            })
            .collect();

        // let timelen = self.triangulation.time_len();
        // let mut final_mapping: LabelledArray<Option<usize>, VertexLabel> =
        //     LabelledArray::fill(None, self.triangulation.vertex_count());
        // for label in self.triangulation.iter_time(0) {
        //     final_mapping[label] = Some(vertices.len());
        //     vertices.push({
        //         let point0 = &self.coordinates[label];
        //         let coordinate = (
        //             point0.x,
        //             point0.y,
        //             point0.z + timelen as f64 * FRAC_PI_3.sin(),
        //         );
        //         let time = timelen as i32;
        //         Vertex { coordinate, time }
        //     })
        // }

        let mut triangles = Vec::with_capacity(self.triangulation.size());
        let mut edges = Vec::with_capacity(3 * self.triangulation.size() / 2);
        for triangle_label in self.triangulation.triangles.labels() {
            let triangle = self.triangulation.get_triangle(triangle_label);

            let vleft: usize = triangle.left_vertex.into();
            let vcenter: usize = triangle.center_vertex.into();
            let vright: usize = triangle.right_vertex.into();

            edges.push(Edge {
                start: vleft,
                end: vcenter,
                kind: EdgeKind::Timelike,
            });
            if matches!(triangle.orientation, Orientation::Up) {
                edges.push(Edge {
                    start: vleft,
                    end: vright,
                    kind: EdgeKind::Spacelike,
                });
            }

            let tvertices: (usize, usize, usize) = match triangle.orientation {
                Orientation::Up => (vleft, vcenter, vright),
                Orientation::Down => (vleft, vright, vcenter),
            };
            triangles.push(Triangle {
                vertices: tvertices,
                label: triangle_label.into(),
            });
        }

        Embedding3D {
            vertices,
            edges,
            triangles,
        }
    }
}

struct MinimalDistanceEmbedding<'a> {
    embedding: &'a TorusEmbedding<'a>,
    power: i32,
}

impl<'a> MinimalDistanceEmbedding<'a> {
    /// Returns iterator over every edge twice
    fn iter_edges(&'a self) -> impl Iterator<Item = (VertexLabel, VertexLabel)> + 'a {
        self.embedding
            .triangulation
            .iter_triangles()
            .flat_map(|triangle| {
                [
                    (triangle.left_vertex, triangle.right_vertex),
                    (triangle.left_vertex, triangle.center_vertex),
                    (triangle.right_vertex, triangle.center_vertex),
                ]
                .into_iter()
            })
    }

    // Return outward-pointing normal vector of triangle
    fn get_normal_vector(
        &'a self,
        label: Label<full_triangulation::triangle::Triangle>,
        coordinates: &LabelledArray<Point3D, VertexLabel>,
    ) -> Point3D {
        let triangle = self.embedding.triangulation.get_triangle(label);
        let (l0, l1, l2) = match triangle.orientation {
            Orientation::Up => (
                triangle.left_vertex,
                triangle.center_vertex,
                triangle.right_vertex,
            ),
            Orientation::Down => (
                triangle.left_vertex,
                triangle.right_vertex,
                triangle.center_vertex,
            ),
        };
        let v0 = coordinates[l0];
        let v1 = coordinates[l1];
        let v2 = coordinates[l2];

        let d0 = v1 - v0;
        let d1 = v2 - v0;
        d1.cross(&d0)
    }
}

impl CostFunction for MinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, param: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let vertex_count = self.embedding.triangulation.vertex_count();
        // let tlen = self.embedding.triangulation.time_len();
        let coordinates: LabelledArray<Point3D, VertexLabel> = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        let mut cost = 0.0;
        let mut count = 0;
        for (label0, label1) in self.iter_edges() {
            let v0: Point3D = coordinates[label0];
            let v1: Point3D = coordinates[label1];
            // let t0 = self.embedding.get_time(label0);
            // let t1 = self.embedding.get_time(label1);
            // if t0 == 0 && t1 == tlen - 1 {
            //     v0.z += tlen as f64 * FRAC_PI_3.sin();
            // } else if t0 == tlen - 1 && t1 == 0 {
            //     v1.z += tlen as f64 * FRAC_PI_3.sin();
            // }
            cost += (v0.distance2(&v1) - 1.0).powi(self.power);
            count += 1;
        }
        cost = 0.5 * cost / count as f64;

        let mut orientation_cost = 0.0;
        for vertex_label in self.embedding.triangulation.vertices.labels() {
            let v0 = coordinates[vertex_label];
            let vertex = self.embedding.triangulation.get_vertex(vertex_label);

            let triangle_label = vertex.triangle();
            let vnormal = self.get_normal_vector(triangle_label, &coordinates);

            let node = vertex.as_node();
            let mut vnbr_sum = Point3D::zero();
            let mut count = 0;
            for nbr in &mut spacetime_graph::IterNeighbours::new(&node) {
                let v1 = coordinates[nbr.convert()];
                vnbr_sum += v1;
                count += 1;
            }
            let vnbr = vnbr_sum / count as f64;
            let dv = vnbr - v0;
            if dv.dot(&vnormal) < 0.0 {
                orientation_cost += 1.0;
            }
        }
        orientation_cost /= self.embedding.triangulation.vertex_count() as f64;

        let total_cost = cost + orientation_cost * ORIENTATION_COST_FACTOR;

        Ok(total_cost)
    }
}

impl Gradient for MinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let nu = self.power;
        let vertex_count = self.embedding.triangulation.vertex_count();
        // let tlen = self.embedding.triangulation.time_len();
        let coordinates: LabelledArray<Point3D, VertexLabel> = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        let mut gradient = LabelledArray::fill_with(
            || Point3D {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            vertex_count,
        );

        let mut count = 0;
        for (label0, label1) in self.iter_edges() {
            let v0: Point3D = coordinates[label0];
            let v1: Point3D = coordinates[label1];
            // let t0 = self.embedding.get_time(label0);
            // let t1 = self.embedding.get_time(label1);
            // if t0 == 0 && t1 == tlen - 1 {
            //     v0.z += tlen as f64 * FRAC_PI_3.sin();
            // } else if t0 == tlen - 1 && t1 == 0 {
            //     v1.z += tlen as f64 * FRAC_PI_3.sin();
            // }
            let dv = v1 - v0;
            let prefactor = (nu as f64) * (dv.norm2() - 1.0).powi(nu - 1);
            gradient[label1] += dv * prefactor;
            gradient[label0] -= dv * prefactor;
            count += 1;
        }
        let norm = 0.5 / count as f64;
        let gradient = Array1::from_vec(gradient.into_vec());
        let grad_mean = gradient.sum() / vertex_count as f64;
        let gradient = Array1::from_iter(gradient.iter().flat_map(|v| {
            [
                norm * (v.x - grad_mean.x),
                norm * (v.y - grad_mean.y),
                norm * (v.z - grad_mean.z),
            ]
            .into_iter()
        }));
        Ok(gradient)
    }
}

impl TorusEmbedding<'_> {
    pub fn minimize_edge_lengths(&mut self) {
        let min_embedding = MinimalDistanceEmbedding {
            embedding: self,
            power: 2,
        };
        let vertex_count = self.triangulation.vertex_count();
        println!(
            "Initial Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        let initial_coordinates = Array1::from_iter(
            self.coordinates
                .iter()
                .flat_map(|v| [v.x, v.y, v.z].into_iter()),
        );
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        debug!(
            "Initial cost: {}",
            min_embedding
                .cost(&initial_coordinates)
                .expect("Initial cost should exist")
        );
        let result = Executor::new(min_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_coordinates)
                    .max_iters(EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION)
                    .target_cost(EDGEDISTANCE_DIFFERENCE_TOLERANCE)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        debug!(
            "Terminated contant edge length search with {}",
            result.state().termination_status
        );
        debug!("Best cost: {}", result.state.get_best_cost());
        let best_coordinates: Vec<Point3D> = result
            .state
            .best_param
            .expect("Optimal coordinates could not be determined")
            .into_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        println!(
            "Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        self.coordinates = LabelledArray::from(best_coordinates);
    }

    pub fn export_with_field<T: Display + Copy>(
        &self,
        field: ArrayView1<T>,
    ) -> Embedding3DField<T> {
        Embedding3DField {
            embedding: self.export(),
            field: self
                .coordinates
                .labels()
                .map(|label| field[usize::from(label)])
                .collect(),
        }
    }
}
