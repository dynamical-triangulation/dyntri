//! A random walker on a graph to determine the distance of a random walk
//!
//! The diffusion process of [diffusion](super::diffusion) can be used to determine the average
//! return probability of a random walk, but not the average deviation of a random walker.
//! This module provides a random walker that tracks the geodesic/shortest distance of the
//! current position to the origin.

use ndarray::Array2;
use rand::Rng;

use crate::{
    collections::LabelledArray,
    graph::{
        breadth_first_search::traverse_nodes::TraverseNodeDistance, GenericGraph, Graph,
        IterableGraph, SampleNeighbours, SizedGraph,
    },
};

#[derive(Debug, Clone, Copy)]
struct Walker<L> {
    label: L,
    distance: usize,
}

/// Returns the shortest distances of `samples` random walkers after `max_step` steps
///
/// The first index of the returned [`Array2`] indexes the walkers, the second index is the _step_
/// of the random walker.
///
/// The implementation first precomputes all distances up to the maximal reachable distance,
/// then performs the random walks.
pub fn measure<R: Rng + ?Sized>(
    graph: &GenericGraph,
    origin: <GenericGraph as Graph>::Label,
    max_step: usize,
    samples: usize,
    rng: &mut R,
) -> Array2<usize> {
    // Create distance list
    let mut distance_list = LabelledArray::fill(0, graph.len());
    for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
        if node.distance > max_step {
            break;
        }
        distance_list[node.label] = node.distance;
    }

    let start_walker = Walker {
        label: origin,
        distance: 0,
    };
    let mut walkers = vec![start_walker; samples];
    let mut distances = Array2::zeros([samples, max_step + 1]);
    for n in 1..max_step + 1 {
        for (i, walker) in walkers.iter_mut().enumerate() {
            let next_label = graph.sample_neighbours(walker.label, rng);
            let distance = distance_list[next_label];
            walker.label = next_label;
            walker.distance = distance;
            distances[(i, n)] = distance;
        }
    }

    distances
}
