//! This module is for generating simple PLY files
//! for 3D geometry meshes of triangulations

use std::fmt::Display;

const FORMAT: &str = "ascii 1.0";
const COMMENT: &str = "Mesh of CDT triangulation";
// const COLOR0: Oklab = Oklab {
//     l: 0.49232,
//     a: -0.03551,
//     b: -0.14633,
// }; // Blue
// const COLOR1: Oklab = Oklab {
//     l: 0.72246,
//     a: -0.14512,
//     b: 0.0594,
// }; // Light Green

/// Struct holding a list of vertex coordinates `(x, y, z)`
struct VertexList(Vec<(f32, f32, f32)>);

// type VertexColors = Vec<RGB<u8>>;

/// Struct holding a list of triangle vertex indices in clockwise order
/// in addition to the time coordinate
struct TriangleList(Vec<(usize, usize, usize)>, Vec<usize>);

/// Struct for holding PLY information
///
/// So here the vertex coordinates and triangle vertex lists
pub struct Ply {
    vertices: VertexList,
    // field: Option<Vec<f64>>,
    // colors: VertexColors,
    triangles: TriangleList,
}

// fn lerp<T, S>(scalar0: T, scalar1: T, ratio: S) -> T
// where
//     T: Sub<Output = T> + Add<Output = T> + Copy,
//     S: Mul<T, Output = T>,
// {
//     scalar0 + ratio * (scalar1 - scalar0)
// }

// fn interpolate_color(ratio: f32) -> RGB<u8> {
//     let color = Oklab {
//         l: lerp(COLOR0.l, COLOR1.l, ratio),
//         a: lerp(COLOR0.a, COLOR1.a, ratio),
//         b: lerp(COLOR0.b, COLOR1.b, ratio),
//     };
//     oklab_to_srgb(color)
// }

impl Display for Ply {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Write header
        writeln!(f, "ply")?;
        writeln!(f, "format {}", FORMAT)?;
        writeln!(f, "comment {}", COMMENT)?;

        writeln!(f, "element vertex {}", self.vertices.0.len())?;
        writeln!(f, "property float x")?;
        writeln!(f, "property float y")?;
        writeln!(f, "property float z")?;
        writeln!(f, "property uchar red")?;
        writeln!(f, "property uchar green")?;
        writeln!(f, "property uchar blue")?;
        // if self.field.is_some() {
        //     writeln!(f, "property double field")?;
        // }

        writeln!(f, "element face {}", self.triangles.0.len())?;
        writeln!(f, "property list uchar uint vertex_index")?;
        writeln!(f, "property uint time")?;

        writeln!(f, "end_header")?;

        // Write vertices
        // if let Some(field) = &self.field {
        //     for (i, vertex) in self.vertices.0.iter().enumerate() {
        //         let color = self.colors[i];
        //         let value = field[i];
        //         writeln!(
        //             f,
        //             "{} {} {} {} {} {} {}",
        //             vertex.0, vertex.1, vertex.2, color.r, color.g, color.b, value
        //         )?;
        //     }
        // } else {
        for (i, vertex) in self.vertices.0.iter().enumerate() {
            // let color = self.colors[i];
            writeln!(
                f,
                // "{} {} {} {} {} {}",
                "{} {} {}",
                vertex.0,
                vertex.1,
                vertex.2 //, color.r, color.g, color.b
            )?;
        }
        // }

        // Write triangles
        for (triangle, t) in self.triangles.0.iter().zip(self.triangles.1.iter()) {
            writeln!(f, "3 {} {} {} {t}", triangle.0, triangle.1, triangle.2)?;
        }

        Ok(())
    }
}
