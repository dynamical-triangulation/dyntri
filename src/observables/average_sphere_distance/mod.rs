//! Computing the average sphere distance of a graph or triangulation.
//!
//! The average sphere distance is a measure of distance between two geodesic spheres that is
//! designed to be computationally efficient on graphs.
//! This notion of sphere distance is used to define a notation of curvature called the
//! _quantum Ricci curvature_, a generalisation of _Ricci curvature_ which works on simplicial
//! manifolds like the triangulations we consider here.
//! This quantum Ricci curvature was introduced by _Renate Loll_ and _Nilas Klitgaard_ in
//! [arXiv:1712.08847](https://arxiv.org/abs/1712.08847) for Dynamical Triangulation to
//! measure curvature in these very irregular spaces.
//!
//! Two variants of the average sphere distance have been implemented. [`double_sphere`] implements
//! the ASD where the spheres are separated with the same distance as their radius, and
//! [`single_sphere`] implements the ASD where the spheres are separated by distance 0, i.e. they
//! are fully overlapping spheres.

use std::fmt::Display;

use serde::Serialize;

pub mod double_sphere;
pub mod geodesic_spheres;
mod marked_list;
pub mod single_sphere;

/// Struct holding the [AverageSphereDistance] including the [`violation_ratio`](AverageSphereDistance::violation_ratio),
/// meaning the ratio of geodesic distance measurements that would have wrapped around in
/// a torus topology, compared to the used graph topology.
/// For a more detailed discussion of the meaning of the `violation_ratio` see
/// [Master thesis of J. Duin](https://www.ru.nl/publish/pages/913395/jduin-master-thesis.pdf).
///
/// Note: the `violation_ratio` only gives useful results when used with
/// [periodic graphs](crate::graph::periodic_graph::PeriodicGraph), and it should indeed only be
/// returned by functions that operate on such graphs.
#[derive(Debug, Copy, Clone, Serialize)]
pub struct AverageSphereDistance {
    /// The value of the average sphere distance
    pub asd: f64,
    /// The part of the sphere distance measurements that would have wrapping compared to a graph
    /// with toroidal topology.
    /// For a more detailed discussion of the meaning of the `violation_ratio` see
    /// [Master thesis of J. Duin](https://www.ru.nl/publish/pages/913395/jduin-master-thesis.pdf).
    pub violation_ratio: f64,
}

/// Struct holding the average sphere distance profile for different length scales
#[derive(Debug, Clone, Serialize)]
pub struct AverageSphereDistanceProfile {
    /// The average sphere distance values over the length scale
    pub profile: Box<[f64]>,
    /// The violation ratios over the length scale, see
    /// [`violation_ratio`](AverageSphereDistance::violation_ratio)
    pub violation_ratios: Box<[f64]>,
}

impl Display for AverageSphereDistance {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}; {}", self.asd, self.violation_ratio)
    }
}

impl Display for AverageSphereDistanceProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "                         ")?;
        for asd in 1..self.profile.len() + 1 {
            write!(f, "{:^8} ", asd)?;
        }
        writeln!(f)?;
        write!(f, "Average sphere distance: ")?;
        for asd in self.profile.iter() {
            write!(f, "{:>8.5} ", asd)?;
        }
        writeln!(f)?;
        write!(f, "Violation ratios:        ")?;
        for ratio in self.violation_ratios.iter() {
            write!(f, "{:>8.5} ", ratio)?;
        }
        writeln!(f)
    }
}
