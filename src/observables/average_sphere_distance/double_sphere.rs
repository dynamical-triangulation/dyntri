//! The average sphere distance of two spheres, whose origins are separated by the same distance
//! as the radius of the spheres.
//!
//! This variant of the average sphere distance is the original variant as first introduced and
//! requires the sampling of two points separated by the given length scale.

use std::any::type_name;
use std::marker::PhantomData;
use std::time;

use super::geodesic_spheres::{get_geodesic_sphere, GeodesicSphere};
use super::marked_list::MarkedList;
use super::{AverageSphereDistance, AverageSphereDistanceProfile};
use crate::graph::breadth_first_search::traverse_nodes::TraverseNodeDistance;
use crate::graph::breadth_first_search::IterNeighbours;
use crate::graph::field::PeriodicField;
use crate::graph::periodic_graph::{PeriodicGraph, PeriodicLabel, PeriodicLabelMatch};
use crate::graph::{FieldGraph, Graph, IterableGraph, SampleGraph, SizedGraph};
use crate::observables::full_graph::distance_matrix::{
    DistanceMatrixMasks, DISTANCE_MATRIX_DATASET_NAME, MASK_GROUP_BASE_NAME,
};
use crate::observables::full_graph::sparse::{mask_product_disk, mask_product_elem, BoolMatrix};
use hdf5::{Dataset, Group, H5Type};
use humantime::format_duration;
use log::debug;
use ndarray::prelude::*;
use rand::Rng;

/// Returns the [`AverageSphereDistance`] of a `graph` computed from a given `origin` at length
/// scale `delta`.
///
/// This function does the same as [`measure_sized_graph`], but also computes the violation ratio
/// of the average sphere distance, hence it returns [`AverageSphereDistance`].
/// The [`violation_ratio`](AverageSphereDistance::violation_ratio) is the ratio of geodesic
/// distance measurements that would have wrapped around if the measurement was performed on a graph
/// that was not periodic but sized with a toroidal topology.
pub fn measure<'a, I, F, G, R>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
    rng: &mut R,
) -> AverageSphereDistance
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G::Label: PeriodicLabel,
    G: IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    R: Rng + ?Sized,
{
    average_sphere_distance(graph, origin, delta, rng)
}

/// Returns the [`AverageSphereDistance`] of a `graph` computed from a given `origin` at length
/// scale `delta`.
///
/// Note that graph has to be sized, for an infinite size graph use [`measure`].
///
/// Note that the second point of the average sphere distance is sampled uniformly from the sphere
/// of points at distance `delta` from the `origin`. This does not give a uniform sampling of point
/// pairs if the first point is sampled uniformly from all points.
pub fn measure_sized_graph<'a, I, G, R>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
    rng: &mut R,
) -> f64
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
    R: Rng + ?Sized,
{
    average_sphere_distance_sized_graph(graph, origin, delta, rng)
}

/// Computes the Average Sphere Distance from an origin
fn average_sphere_distance<'a, I, F, G, R>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
    rng: &mut R,
) -> AverageSphereDistance
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G::Label: PeriodicLabel,
    G: IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    R: Rng + ?Sized,
{
    let spheres = SpheresASD::from_origin(graph, origin, delta, rng);
    spheres.average_sphere_distance()
}

/// Computes the Average Sphere Distance from an origin
fn average_sphere_distance_sized_graph<'a, I, G, R>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
    rng: &mut R,
) -> f64
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
    R: Rng + ?Sized,
{
    let spheres = SpheresASD::from_origin(graph, origin, delta, rng);
    spheres.average_sphere_distance_sized_graph()
}

/// Compute the Average sphere distance for different deltas
pub fn profile<'a, I, F, G, R>(
    graph: &'a G,
    origin: G::Label,
    deltas: impl Iterator<Item = usize>,
    rng: &mut R,
) -> AverageSphereDistanceProfile
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G: SampleGraph + IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    G::Label: PeriodicLabel,
    R: Rng + ?Sized,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let AverageSphereDistance {
            asd,
            violation_ratio,
        } = average_sphere_distance(graph, origin, delta, rng);
        profile.push(asd);
        violation_ratios.push(violation_ratio);
    }

    AverageSphereDistanceProfile {
        profile: profile.into_boxed_slice(),
        violation_ratios: violation_ratios.into_boxed_slice(),
    }
}

/// Returns the average sphere distance profile at several `deltas` from the same `origin`
/// for a [sized graph](crate::graph::SizedGraph)
pub fn profile_sized_graph<'a, I, G, R>(
    graph: &'a G,
    origin: G::Label,
    deltas: impl Iterator<Item = usize>,
    rng: &mut R,
) -> Array1<f64>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
    R: Rng + ?Sized,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let asd = average_sphere_distance_sized_graph(graph, origin, delta, rng);
        profile.push(asd);
    }

    Array1::from_vec(profile)
}

/// Struct for bundling the precomputed spheres and origin for ASD
#[derive(Debug, Clone)]
pub struct SpheresASD<'a, G: IterableGraph<'a, I>, I> {
    graph: &'a G,
    delta: usize,
    // `origin0` is supposed to be the origin of `sphere0`
    origin0: G::Label,
    // `origin1` is supposed to be the origin of `sphere1`
    origin1: G::Label,
    sphere0: GeodesicSphere<G::Label>,
    sphere1: GeodesicSphere<G::Label>,
    _neighbour_iterator_phantom: PhantomData<I>,
}

impl<'a, I, G> SpheresASD<'a, G, I>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I>,
{
    /// Compute spheres from the origin of one of the spheres
    pub fn from_origin<R>(graph: &'a G, origin: G::Label, delta: usize, rng: &mut R) -> Self
    where
        R: Rng + ?Sized,
    {
        // Get sphere around the origin with radius `delta`
        let sphere = get_geodesic_sphere(graph, origin, delta);
        Self::from_origin_sphere(graph, origin, sphere, delta, rng)
    }

    /// Compute spheres with a single sphere already computed
    ///
    /// NOTE: This function assumes the given `sphere` has `origin`
    /// asits origin, if this is NOT the case the function gives
    /// incorrect results.
    pub fn from_origin_sphere<R>(
        graph: &'a G,
        origin: G::Label,
        sphere: GeodesicSphere<G::Label>,
        delta: usize,
        rng: &mut R,
    ) -> Self
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        // Choose random second origin from sphere nodes
        let origin1 = sphere[rng.gen_range(0..sphere.len())];
        let sphere1 = get_geodesic_sphere(graph, origin1, delta);

        Self::from_spheres(graph, delta, origin, origin1, sphere, sphere1)
    }

    /// Get SphereASD from precomputed spheres
    pub fn from_spheres(
        graph: &'a G,
        delta: usize,
        origin0: G::Label,
        origin1: G::Label,
        sphere0: GeodesicSphere<G::Label>,
        sphere1: GeodesicSphere<G::Label>,
    ) -> Self {
        SpheresASD {
            graph,
            delta,
            origin0,
            origin1,
            sphere0,
            sphere1,
            _neighbour_iterator_phantom: PhantomData,
        }
    }
}

impl<'a, I, F, G> SpheresASD<'a, G, I>
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G::Label: PeriodicLabel,
    G: IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
{
    /// Compute the average sphere distance given the two necessary spheres and the origin of one of them
    ///
    /// NOTE: Important to note is that this function assumes that `origin` lies on `sphere1` for a slight
    /// optimisation, and DOES NOT give the correct answer when this is not the case! So make sure that
    /// `origin` is indeed part of `sphere1`.
    pub fn average_sphere_distance(&self) -> AverageSphereDistance {
        let SpheresASD {
            graph,
            origin0,
            origin1,
            delta,
            sphere0,
            sphere1,
            _neighbour_iterator_phantom,
        } = self;

        // Track amount of times a violation would have occured
        let mut violation_count = 0;

        // Set sphere0 and sphere1 such that the main loop has the fewest iterations (least BFSs)
        let (origin, sphere0, sphere1) = if sphere0.len() < sphere1.len() {
            (origin1, sphere1, sphere0)
        } else {
            (origin0, sphere0, sphere1)
        };

        // Compute average distance
        let mut count = sphere0.len() as f64;
        // Use a continously updating average such that the total does not need to become large
        let mut average = *delta as f64;
        // Reuse marked list because it SHOULD BE emptied every node
        let mut marked = MarkedList::new(*graph);
        // Filter out the origin distance calculation as all of those are `delta` and already added
        for &start_node in sphere1.iter().filter(|&label| label != origin) {
            // This could break if sphere0 contained duplicate labels, but the BFS for getting the
            // geodesic sphere should guarantee this never occurs.
            marked.add_multiple(sphere0.iter().copied());
            // OPTIMIZABLE: BFS iterator is the other hot function
            for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(start_node) {
                // Mark the point as visisted
                // assert!(
                //     // This assert should be able to be removed (but performance impact is
                //     // minimal)
                //     node.distance <= 3 * delta,
                //     "Something went wrong, not all sphere points were found within 3*delta"
                // );

                match marked.try_remove_periodic(node.label) {
                    PeriodicLabelMatch::None => continue,
                    PeriodicLabelMatch::Equal => {
                        count += 1.0;
                        average += (node.distance as f64 - average) / count;
                        // Stop BFS if all nodes are visited
                        if marked.is_empty() {
                            break;
                        }
                    }
                    PeriodicLabelMatch::Patch => {
                        violation_count += 1;
                    }
                }
            }
        }

        AverageSphereDistance {
            asd: average,
            violation_ratio: violation_count as f64 / count,
        }
    }
}

impl<'a, I, G> SpheresASD<'a, G, I>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
{
    /// Compute the average sphere distance given the two necessary spheres and the origin of one of them
    ///
    /// NOTE: Important to note is that this function assumes that `origin` lies on `sphere1` for a slight
    /// optimisation, and DOES NOT give the correct answer when this is not the case! So make sure that
    /// `origin` is indeed part of `sphere1`.
    pub fn average_sphere_distance_sized_graph(&self) -> f64 {
        let SpheresASD {
            graph,
            origin0,
            origin1,
            delta,
            sphere0,
            sphere1,
            _neighbour_iterator_phantom,
        } = self;

        // Set sphere0 and sphere1 such that the main loop has the fewest iterations (least BFSs)
        let (origin, sphere0, sphere1) = if sphere0.len() < sphere1.len() {
            (*origin1, sphere1, sphere0)
        } else {
            (*origin0, sphere0, sphere1)
        };

        // Compute average distance
        let mut count = sphere0.len() as f64;
        // Use a continously updating average such that the total does not need to become large
        let mut average = *delta as f64;
        // Reuse marked list because it SHOULD BE emptied every node
        let mut marked = MarkedList::new(*graph);
        // Filter out the origin distance calculation as all of those are `delta` and already added
        for &start_node in sphere1.iter().filter(|&&label| label != origin) {
            // for &start_node in sphere1.iter() {
            // This could break if sphere0 contained duplicate labels, but the BFS for getting the
            // geodesic sphere should guarantee this never occurs.
            marked.add_multiple(sphere0.iter().copied());
            // OPTIMIZABLE: BFS iterator is the other hot function
            for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(start_node) {
                // Mark the point as visisted
                // assert!(
                //     // This assert should be able to be removed (but performance impact is
                //     // minimal)
                //     node.distance <= 3 * delta + 1,
                //     "Something went wrong, not all sphere points were found within 3*delta"
                // );

                // Mark the point as visisted
                if marked.try_remove(node.label) {
                    count += 1.0;
                    average += (node.distance as f64 - average) / count;
                    // Stop BFS if all nodes are visited
                    if marked.is_empty() {
                        break;
                    }
                }
            }
        }

        let difference = (sphere0.len() * sphere1.len()) as f64 - count;
        assert!(
            difference.abs() < 1e-4,
            "The total amount of pairs is not correct: delta={difference}"
        );

        average
    }
}

/// Measured the average sphere distance profile at every point pair given by `point_pairs`
/// in the graph given by the distance matrix stored in the `group` file.
///
/// The first array of the point pairs signifies the first point for the average sphere ditance
/// the second array the second point. Both arrays thus need to be the same size.
/// The typical usage would be to get a list of point seperated by a given distance `epsilon` to
/// get the double sphere distance with given delta and epsilon.
///
/// Note: this function needs masks of the distance profile to be present in the `group` to
/// work, other wise it will return an error. E.g. using
/// [compute_distance_matrix_with_mask_disk()](crate::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk())
/// gives the correct group.
pub fn measure_points_full_graph_from_disk<T, U>(
    group: &Group,
    point_pairs: (ArrayView1<usize>, ArrayView1<usize>),
    delta: usize,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    assert_eq!(point_pairs.0.len(), point_pairs.1.len());

    debug!("Loading data from HDF5 file");
    let dmatrix_dataset = group
        .dataset(DISTANCE_MATRIX_DATASET_NAME)
        .unwrap_or_else(|err| {
            panic!(
                "Dataset `{}` should exist: {err}",
                DISTANCE_MATRIX_DATASET_NAME
            )
        });
    // // Temporary check if points are correct
    // for (&point0, &point1) in point_pairs.0.iter().zip(point_pairs.1.iter()) {
    //     let r: u8 = dmatrix_dataset
    //         .read_slice(s![point0, point1])
    //         .expect("Could not read from Dataset")
    //         .into_scalar();
    //     assert_eq!(
    //         r as usize, delta,
    //         "Given points are not delta={delta} apart"
    //     );
    // }

    debug!("Retrieving mask for delta {delta} from HDF5 file");
    let mask_group = group
        .group(&format!("{MASK_GROUP_BASE_NAME}{delta}"))
        .expect("Mask for delta does not exist");
    let mask =
        BoolMatrix::from_disk(&mask_group).expect("BoolMatrix could not be formed from disk");

    // Temporary check if points are in mask
    for (&point0, &point1) in point_pairs.0.iter().zip(point_pairs.1.iter()) {
        assert!(
            mask.view().get(point0, point1),
            "Given points are not in mask {mask_group:?}"
        );
    }

    debug!(
        "Computing average sphere distances for {} points at delta={delta}",
        point_pairs.0.len()
    );
    let start = time::Instant::now();
    let asd_field =
        measure_points_full_graph_from_disk_with_mask::<T, U>(&dmatrix_dataset, point_pairs, &mask);
    debug!(
        "Finished ASD computation at delta={delta} in {}",
        format_duration(start.elapsed())
    );

    asd_field
}

/// Measure the average sphere distance of many point pairs from disk.
///
/// This performs the same measurement as [`measure_points_full_graph`] but with a distance matrix
/// from disk.
///
/// `dmatrix` should be a [`Dataset`] representing the distance matrix of a graph,
/// and `mask` should be a [`BoolMatrix`] of this distance matrix at the desired length scale
/// `delta` as distance.
/// Note that for the calculation to reflect the `double_sphere` average sphere distance the
/// distance between the points in the `point_pairs` should be the same `delta` as used for the
/// mask.
/// However, this function could also be used to compute more general average sphere distances
/// at different length scale combinations.
pub fn measure_points_full_graph_from_disk_with_mask<T, U>(
    dmatrix: &Dataset,
    point_pairs: (ArrayView1<usize>, ArrayView1<usize>),
    mask: &BoolMatrix,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    let sphere_vols = mask.view().row_sum();
    let indices = Array1::from_iter(
        point_pairs
            .0
            .iter()
            .zip(point_pairs.1)
            .map(|(&point0, &point1)| Dim([point0, point1])),
    );
    let norm: Array1<f64> = indices
        .iter()
        .map(|&idx| (sphere_vols[idx[0]] as f64) * (sphere_vols[idx[1]] as f64))
        .collect();
    let unnorm_asd: Array1<U> =
        mask_product_disk::<T, U>(mask.view(), dmatrix, mask.transpose(), indices.view());
    unnorm_asd.mapv(|v| num_traits::cast::<_, f64>(v).expect("U should be castable to f64")) / norm
}

/// Measure the average sphere distance of many point pairs at scale `delta`.
///
/// This function does the same calculation as [`measure_points_full_graph_from_disk_with_mask`],
/// but with a distance matrix and mask from memory instead of disk.
/// Requires a distance matrix with mask given by `dmatrix`.
pub fn measure_points_full_graph<T, U>(
    dmatrix: DistanceMatrixMasks<T>,
    point_pairs: (ArrayView1<usize>, ArrayView1<usize>),
    delta: usize,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    assert_eq!(point_pairs.0.len(), point_pairs.1.len());

    let mask =
        dmatrix
            .get_mask(T::from(delta).unwrap_or_else(|| {
                panic!("`delta` cannot be convered to type {}", type_name::<T>())
            }))
            .unwrap_or_else(|| panic!("Mask has not yet been computed for {delta}"));

    // Temporary check if points are in mask
    for (&point0, &point1) in point_pairs.0.iter().zip(point_pairs.1.iter()) {
        assert!(
            mask.view().get(point0, point1),
            "Given points are not in mask"
        );
    }

    debug!(
        "Computing average sphere distances for {} points at delta={delta}",
        point_pairs.0.len()
    );
    let start = time::Instant::now();

    let sphere_vols = mask.view().row_sum();
    let pairs_iter = point_pairs.0.iter().zip(point_pairs.1.iter());
    let asd_iter = pairs_iter.map(|(&point0, &point1)| {
        let norm = (sphere_vols[point0] as f64) * (sphere_vols[point1] as f64);
        let index = Dim([point0, point1]);
        let asd_unnorm = mask_product_elem::<T, U>(
            mask.view(),
            dmatrix.distance_matrix.view(),
            mask.transpose(),
            index,
        );
        num_traits::cast::<_, f64>(asd_unnorm).expect("U should be castable to f64") / norm
    });
    let asd: Array1<f64> = Array1::from_iter(asd_iter);

    debug!(
        "Finished ASD computation at delta={delta} in {}",
        format_duration(start.elapsed())
    );

    asd
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::graph::spacetime_graph::SpacetimeGraph;
    use crate::observables::two_point_function::sample_from_origin_full_graph;
    use crate::{collections::Label, graph::GenericGraph, io::graph};
    use crate::{
        monte_carlo::fixed_universe,
        observables::{
            full_graph::distance_matrix::{self, compute_distance_matrix_with_masks_disk},
            two_point_function::{sample_uniform_full_graph, sample_uniform_full_graph_from_disk},
        },
        triangulation::FullTriangulation,
    };
    use indoc::indoc;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256PlusPlus;
    use std::io::Cursor;

    const EXAMPLE_GENERIC_GRAPH: &str = indoc! {"
        8, 5, 9, 1, 2, 10
        0, 9, 2, 4, 3, 6, 5, 2
        9, 10, 0, 1, 5, 4, 1
        1, 4, 7, 6
        1, 2, 5, 8, 10, 7, 3
        2, 1, 6, 9, 0, 8, 4
        1, 3, 7, 9, 5
        3, 4, 10, 9, 6
        4, 5, 0, 10
        6, 7, 10, 2, 1, 0, 5
        7, 4, 8, 0, 2, 9
    "};

    fn parse_graph() -> Result<GenericGraph, graph::AdjacencyListParseError> {
        let graph = graph::import_adjacency_list(Cursor::new(EXAMPLE_GENERIC_GRAPH))
            .map(GenericGraph::new)?;
        Ok(graph)
    }

    #[test]
    fn parsing_test() {
        parse_graph().unwrap();
    }

    #[test]
    fn asd_generic_graph_single() {
        let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
        let graph = parse_graph().unwrap();

        let delta = 1;
        let origin0 = graph.sample_node(&mut rng);
        let sphere0 = get_geodesic_sphere(&graph, origin0, delta);
        let origin1 = sphere0[rng.gen_range(0..sphere0.len())];
        let sphere1 = get_geodesic_sphere(&graph, origin1, delta);
        let spheres = SpheresASD::from_spheres(&graph, delta, origin0, origin1, sphere0, sphere1);
        eprintln!("Computing ASD for p:{origin0} and q:{origin1}");

        let asd_normal = spheres.average_sphere_distance_sized_graph();
        eprintln!("ASD normal {asd_normal:.3}");

        let distances = vec![0, 1, 2, 3, 4];
        let masked_dmatrix =
            distance_matrix::compute_distance_matrix_with_masks::<u8>(&graph, distances, 12);
        let dmatrix = &masked_dmatrix.distance_matrix;
        eprintln!("{}", dmatrix);
        assert_eq!(
            0,
            (&dmatrix.view() - &dmatrix.t()).sum(),
            "The distance matrix is not symmetric somehow"
        );
        let mask0 = masked_dmatrix
            .get_mask(0)
            .unwrap()
            .to_dense()
            .map(|&v| v as u8);
        let mask1 = masked_dmatrix
            .get_mask(1)
            .unwrap()
            .to_dense()
            .map(|&v| v as u8);
        let mask2 = masked_dmatrix
            .get_mask(2)
            .unwrap()
            .to_dense()
            .map(|&v| v as u8);
        assert_eq!((&mask0 + &mask1 + &mask2).sum(), graph.len().pow(2) as u8);
        assert_eq!(1 * mask1 + 2 * mask2, dmatrix);
        let pairs0 = arr1(&[usize::from(origin0)]);
        let pairs1 = arr1(&[usize::from(origin1)]);

        let asd_dmatrix = measure_points_full_graph::<_, u16>(
            masked_dmatrix,
            (pairs0.view(), pairs1.view()),
            delta,
        );
        eprintln!("ASD dmatrix {asd_dmatrix}");
    }

    #[test]
    fn asd_generic_graph() {
        let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
        let graph = parse_graph().unwrap();

        let delta = 1;

        let distances = vec![0, 1, 2, 3, 4];
        let masked_dmatrix =
            distance_matrix::compute_distance_matrix_with_masks::<u8>(&graph, distances, 12);
        let dmatrix = &masked_dmatrix.distance_matrix;
        let (pairs0, pairs1) =
            sample_uniform_full_graph(dmatrix.view(), std::iter::once(delta), 500, &mut rng)
                .unwrap();

        let mut asd_normal = Vec::new();
        for (&point0, &point1) in pairs0
            .slice(s![0, ..])
            .iter()
            .zip(pairs1.slice(s![0, ..]).iter())
        {
            let origin0 = Label::from(point0);
            let sphere0 = get_geodesic_sphere(&graph, origin0, delta);
            let origin1 = Label::from(point1);
            let sphere1 = get_geodesic_sphere(&graph, origin1, delta);
            let spheres =
                SpheresASD::from_spheres(&graph, delta, origin0, origin1, sphere0, sphere1);

            let asd = spheres.average_sphere_distance_sized_graph();
            asd_normal.push(asd);
        }
        let asd_normal = Array1::from_vec(asd_normal);
        eprintln!("ASD normal  {:.5}", asd_normal.slice(s![..5]));

        let point_pairs = (pairs0.slice(s![0, ..]), pairs1.slice(s![0, ..]));
        let asd_dmatrix = measure_points_full_graph::<_, u16>(masked_dmatrix, point_pairs, delta);
        eprintln!("ASD dmatrix {:.5}", asd_dmatrix.slice(s![..5]));
        assert!((asd_normal - asd_dmatrix).sum().abs() < 1e-9);
    }

    #[test]
    fn asd_generic_graph_disk() {
        let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
        let graph = parse_graph().unwrap();

        let delta = 1;
        let max_distance = 12;

        let file = hdf5::File::create(".temp_dmatrix.h5").unwrap();
        let dmatrix_group =
            compute_distance_matrix_with_masks_disk::<u8>(&graph, max_distance, &file).unwrap();
        let test_mask_group = dmatrix_group.group("mask1").unwrap();
        let test_mask = BoolMatrix::from_disk(&test_mask_group).unwrap();
        eprintln!("{test_mask:?}");

        let (pairs0, pairs1) =
            sample_uniform_full_graph_from_disk(&dmatrix_group, std::iter::once(1), 50, &mut rng)
                .unwrap();

        let mut asd_normal = Vec::new();
        for (&point0, &point1) in pairs0
            .slice(s![0, ..])
            .iter()
            .zip(pairs1.slice(s![0, ..]).iter())
        {
            let origin0 = Label::from(point0);
            let sphere0 = get_geodesic_sphere(&graph, origin0, delta);
            let origin1 = Label::from(point1);
            let sphere1 = get_geodesic_sphere(&graph, origin1, delta);
            let spheres =
                SpheresASD::from_spheres(&graph, delta, origin0, origin1, sphere0, sphere1);

            let asd = spheres.average_sphere_distance_sized_graph();
            asd_normal.push(asd);
        }
        let asd_normal = Array1::from_vec(asd_normal);
        eprintln!("ASD normal  {:.5}", asd_normal.slice(s![..5usize]));
        eprintln!("ASD normal mean: {}", asd_normal.mean().unwrap());

        let point_pairs = (pairs0.slice(s![0, ..]), pairs1.slice(s![0, ..]));
        let asd_dmatrix =
            measure_points_full_graph_from_disk::<u8, u16>(&dmatrix_group, point_pairs, delta);
        eprintln!("ASD dmatrix {:.5}", asd_dmatrix.slice(s![..5]));
        eprintln!("ASD dmatrix mean: {}", asd_dmatrix.mean().unwrap());
        assert!((asd_normal - asd_dmatrix).sum().abs() < 1e-9);
    }

    fn generate_triangulation<R: Rng + ?Sized>(rng: &mut R) -> FullTriangulation {
        let mut universe = fixed_universe::Universe::default(300, 15);
        for _ in 0..10_000 {
            universe.step(rng);
        }
        FullTriangulation::from_triangulation(&universe.triangulation)
    }

    fn generate_graph<R: Rng + ?Sized>(rng: &mut R) -> GenericGraph {
        let triangulation = generate_triangulation(rng);
        let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&triangulation);
        GenericGraph::from_spacetime_graph(&spacetime_graph)
    }

    #[test]
    fn check_generated_graph() {
        let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
        let graph = generate_graph(&mut rng);

        let distances = vec![0, 1, 2, 3];
        let masked_dmatrix =
            distance_matrix::compute_distance_matrix_with_masks::<u8>(&graph, distances, 3);
        let dmatrix = &masked_dmatrix.distance_matrix;
        eprintln!("{}", dmatrix);
        assert_eq!(
            0,
            (&dmatrix.view() - &dmatrix.t()).sum(),
            "The distance matrix is not symmetric somehow"
        );
    }

    #[test]
    fn asd_large() {
        let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
        let graph = generate_graph(&mut rng);

        let delta = 1;

        let distances = vec![0, 1];
        let max_distance = 3;
        let masked_dmatrix = distance_matrix::compute_distance_matrix_with_masks::<u8>(
            &graph,
            distances,
            max_distance,
        );
        let dmatrix = &masked_dmatrix.distance_matrix;
        eprintln!("{}", dmatrix.slice(s![50..56, 50..56]));

        let sample_count = 500;
        let mut asd_standard = Vec::new();
        for _ in 0..sample_count {
            let origin = graph.sample_node(&mut rng);
            let asd = measure_sized_graph(&graph, origin, delta, &mut rng);
            asd_standard.push(asd);
        }
        let asd_standard = Array1::from_vec(asd_standard);
        eprintln!("ASD standard  {:.5}", asd_standard.slice(s![..5]));
        let asd_mean = asd_standard.mean().unwrap();
        let asd_std = ((&asd_standard * &asd_standard).mean().unwrap() - asd_mean.powi(2)).sqrt();
        eprintln!(
            "ASD standard mean: {:.3} ± {:.3}",
            asd_mean,
            asd_std / 500.0f64.sqrt()
        );

        let (pairs0, pairs1) = sample_from_origin_full_graph(
            dmatrix.view(),
            std::iter::once(delta),
            sample_count,
            &mut rng,
        )
        .unwrap();

        let mut asd_normal = Vec::new();
        for (&point0, &point1) in pairs0
            .slice(s![0, ..])
            .iter()
            .zip(pairs1.slice(s![0, ..]).iter())
        {
            let origin0 = Label::from(point0);
            let sphere0 = get_geodesic_sphere(&graph, origin0, delta);
            let origin1 = Label::from(point1);
            let sphere1 = get_geodesic_sphere(&graph, origin1, delta);
            let spheres =
                SpheresASD::from_spheres(&graph, delta, origin0, origin1, sphere0, sphere1);

            let asd = spheres.average_sphere_distance_sized_graph();
            asd_normal.push(asd);
        }
        let asd_normal = Array1::from_vec(asd_normal);
        eprintln!("ASD normal  {:.5}", asd_normal.slice(s![..5]));
        let asd_mean = asd_normal.mean().unwrap();
        let asd_std = ((&asd_normal * &asd_normal).mean().unwrap() - asd_mean.powi(2)).sqrt();
        eprintln!(
            "ASD normal mean: {:.3} ± {:.3}",
            asd_mean,
            asd_std / 500.0f64.sqrt()
        );

        let point_pairs = (pairs0.slice(s![0, ..]), pairs1.slice(s![0, ..]));
        let asd_dmatrix = measure_points_full_graph::<_, u16>(masked_dmatrix, point_pairs, delta);
        eprintln!("ASD dmatrix {:.5}", asd_dmatrix.slice(s![..5]));
        let asd_mean = asd_dmatrix.mean().unwrap();
        let asd_std = ((&asd_dmatrix * &asd_dmatrix).mean().unwrap() - asd_mean.powi(2)).sqrt();
        eprintln!(
            "ASD dmatrix mean: {:.3} ± {:.3}",
            asd_mean,
            asd_std / 500.0f64.sqrt()
        );

        assert!((asd_normal - asd_dmatrix).sum().abs() < 1e-9);
    }
}
