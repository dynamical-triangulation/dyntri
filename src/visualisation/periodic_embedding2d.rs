//! A two-dimensional embedding of a toroidal graph by means of a Tutte embedding,
//! by minimizing the distance of each node to the center of mass of its neighbours.

use std::cmp::Ordering;
use std::fmt::Display;

use argmin::core::observers::ObserverMode;
use argmin::core::{CostFunction, Executor, Gradient};
use argmin::solver::gradientdescent::SteepestDescent;
use argmin::solver::linesearch::MoreThuenteLineSearch;
use argmin_observer_slog::SlogLogger;
use ndarray::prelude::*;

use super::Point2D;
use crate::collections::{Label, LabelledArray};
use crate::graph::periodic1d_generic_graph::{i8usize_central_mapping, usizei8_central_mapping};
use crate::graph::periodic_graph::{IterPeriodicGraph, PeriodicGraph};
use crate::graph::periodic_spacetime_graph::{
    PeriodicSpacetimeGraph, PeriodicSpacetimeLabel, SpacetimeBoundary, SpacetimePatch,
};
use crate::graph::spacetime_graph::{LinkLabel, Node};
use crate::graph::IterableGraph;
use crate::io::embedding::{
    self, Edge, EdgeKind, Embedding, EmbeddingDualField, EmbeddingField, Vertex,
};
use crate::triangulation::FullTriangulation;

const MAX_SLICE_SHIFT_ITERATIONS: u64 = 50;
const MAX_HARMONIC_ITERATIONS: u64 = 500;
const HARMONIC_TARGET_COST: f64 = 1e-3; // 1 represents being a full vertex off

pub struct PeriodicEmbedding2D<'a, 'b> {
    triangulation: &'a FullTriangulation,
    periodic_graph: &'b PeriodicSpacetimeGraph,
    time_extension: f64,
    space_extension: f64,
    coordinates: LabelledArray<Point2D, Label<Node>>,
}

struct SlicedEmbedding<'a> {
    embedding: &'a PeriodicEmbedding2D<'a, 'a>,
}

impl<'a> CostFunction for SlicedEmbedding<'a> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, shifts: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let tlen = self.embedding.triangulation.time_len() as i32;
        let mut counter = 0.0;
        let mut total_cost = 0.0;
        for label in self.embedding.periodic_graph.iter_base() {
            let (x0, y0) = self.embedding.get_coordinate(label);
            let t0 = usize::try_from(self.embedding.get_time(label))
                .expect("Time coorindate should not be negative in the base patch");
            for nbr in self.embedding.periodic_graph.iter_neighbours(label) {
                let t1 = ((tlen + self.embedding.get_time(nbr)) % tlen) as usize;
                if t0 <= t1 {
                    continue;
                }
                let (x1, y1) = self.embedding.get_coordinate(nbr);
                let cost = (x1 + shifts[t1] - x0 - shifts[t0]).powi(2) + (y1 - y0).powi(2);

                counter += 1.0;
                // total_cost += (cost - total_cost) / counter;
                total_cost += cost;
            }
        }

        Ok(total_cost / counter)
    }
}

impl<'a> Gradient for SlicedEmbedding<'a> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, shifts: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let tlen = self.embedding.triangulation.time_len() as i32;
        let mut counter = 0.0;
        let mut grad = Array1::zeros(shifts.dim());

        for label in self.embedding.periodic_graph.iter_base() {
            let (x0, _) = self.embedding.get_coordinate(label);
            let t0 = usize::try_from(self.embedding.get_time(label))
                .expect("Time coorindate should not be negative in the base patch");
            for nbr in self.embedding.periodic_graph.iter_neighbours(label) {
                let t1 = ((tlen + self.embedding.get_time(nbr)) % tlen) as usize;
                if t0 <= t1 {
                    continue;
                }
                let (x1, _) = self.embedding.get_coordinate(nbr);
                let diff = 2.0 * (x1 + shifts[t1] - x0 - shifts[t0]);

                counter += 1.0;
                grad[t1] += diff;
                grad[t0] -= diff;
            }
        }

        grad -= grad.mean().expect("Grad should never be empty");
        grad /= counter;
        Ok(grad)
    }
}

struct HarmonicEmbedding<'a> {
    embedding: &'a PeriodicEmbedding2D<'a, 'a>,
}

impl<'a> HarmonicEmbedding<'a> {
    fn get_coordinate(
        &self,
        base_coordinates: ArrayView2<f64>,
        label: PeriodicSpacetimeLabel,
    ) -> Array1<f64> {
        let mut point = base_coordinates.row(usize::from(label.label)).to_owned();
        point[0] += label.patch.x as f64 * self.embedding.space_extension;
        point[1] += label.patch.t as f64 * self.embedding.time_extension;
        point
    }

    fn get_neighbour_center(
        &self,
        base_coordinates: ArrayView2<f64>,
        label: PeriodicSpacetimeLabel,
    ) -> Array1<f64> {
        let mut counter = 0;
        let mut center: Array1<f64> = Array1::zeros(2);
        for nbr in self.embedding.periodic_graph.iter_neighbours(label) {
            counter += 1;
            center += &self.get_coordinate(base_coordinates, nbr);
        }
        center /= counter as f64;
        center
    }
}

impl<'a> CostFunction for HarmonicEmbedding<'a> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, flat_coordinates: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let vertex_count = self.embedding.periodic_graph.base_len();
        let coordinates = flat_coordinates
            .to_shape([vertex_count, 2])
            .expect("The coordinates should be `vertex_count`*2 by construction");
        let mut counter = 0;
        let mut total_cost = 0.0;
        for label in self.embedding.periodic_graph.iter_base() {
            let point = self.get_coordinate(coordinates.view(), label);
            let center = self.get_neighbour_center(coordinates.view(), label);
            let diff = &point - &center;
            let cost = diff[0].powi(2) + diff[1].powi(2);

            counter += 1;
            total_cost += cost;
        }

        Ok(total_cost / counter as f64)
    }
}

impl<'a> Gradient for HarmonicEmbedding<'a> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(
        &self,
        flat_coordinates: &Self::Param,
    ) -> Result<Self::Gradient, argmin::core::Error> {
        let vertex_count = self.embedding.periodic_graph.base_len();
        let coordinates = flat_coordinates
            .to_shape([vertex_count, 2])
            .expect("The coordinates should be `vertex_count`*2 by construction");

        let norm = 2.0 / vertex_count as f64;
        let mut grad = Array2::zeros([vertex_count, 2]);
        for label in self.embedding.periodic_graph.iter_base() {
            let point = self.get_coordinate(coordinates.view(), label);
            let center = self.get_neighbour_center(coordinates.view(), label);
            let diff = &point - &center;

            let mut counter = 0;
            let mut nbr_diff = Array1::zeros(2);
            for nbr in self.embedding.periodic_graph.iter_neighbours(label) {
                counter += 1;
                let point = self.get_coordinate(coordinates.view(), nbr);
                let center = self.get_neighbour_center(coordinates.view(), nbr);
                let diff = &point - &center;
                nbr_diff += &diff;
            }
            let diff_tot = &diff - (&nbr_diff / counter as f64);
            grad.row_mut(usize::from(label.label)).assign(&diff_tot);
        }
        let grad_mean = grad
            .mean_axis(Axis(0))
            .expect("Coordinates should not be empty");
        grad -= &grad_mean
            .to_shape([1, 2])
            .expect("mean should have length 2");
        grad *= norm;

        Ok(grad
            .to_shape([vertex_count * 2])
            .expect(
                "The gradient should be `vertex_count`*2\
            length by construction",
            )
            .to_owned())
    }
}

impl<'a, 'b> PeriodicEmbedding2D<'a, 'b> {
    /// Returns a basic embedding to initialize for the PeriodicEmbedding2D
    ///
    /// This embedding spreads out the vertices in a rectangle in timeslices,
    /// this rectangle has a size of the average amount of vertices in the average slice
    /// by the the amount of timeslices
    pub fn initialize(
        triangulation: &'a FullTriangulation,
        graph: &'b PeriodicSpacetimeGraph,
    ) -> Self {
        let tlen = triangulation.time_len();
        let vertex_count = triangulation.size() / 2;
        let time_extension = triangulation.time_len() as f64;
        let average_space_extension = vertex_count as f64 / time_extension;

        let mut coordinates: LabelledArray<Option<Point2D>, Label<Node>> =
            LabelledArray::fill_with(|| None, vertex_count);

        // Identify lower boundary
        let mut tstart = None;
        for t in 0..tlen {
            let istart = triangulation.timeslices[t].start;
            let bound = graph.get_bound(istart.convert(), LinkLabel::Down(0));
            match bound {
                SpacetimeBoundary::Backward
                | SpacetimeBoundary::BackwardLeft
                | SpacetimeBoundary::BackwardRight => {
                    if tstart.is_some() {
                        panic!(
                            "The given graph is not constructed correctly,\
                            more than one lower boundary exists"
                        );
                    }
                    tstart = Some(t);
                }
                _ => continue,
            }
        }
        let tstart = tstart.expect("No lower boundary could be found");

        for t in 0..tlen {
            let space_extension = triangulation.timeslice_length(t);

            // Identify left boundary
            let mut xstart = None;
            for vertex_label in triangulation.iter_time(t) {
                let bound = graph.get_bound(vertex_label.convert(), LinkLabel::Left);
                match bound {
                    SpacetimeBoundary::Left
                    | SpacetimeBoundary::BackwardLeft
                    | SpacetimeBoundary::ForwardLeft => {
                        if xstart.is_some() {
                            panic!(
                                "The given graph is not constructed correctly,\
                                more than one left boundary exists"
                            );
                        }
                        xstart = Some(vertex_label);
                    }
                    _ => continue,
                }
            }
            let xstart = xstart.unwrap_or_else(|| {
                panic!(
                    "The periodic graph does not have a left boundary, timeslice length: {}",
                    triangulation.timeslice_length(t)
                )
            });

            // Set initial coordinates
            for (i, vertex_label) in triangulation.iter_time_vertex(xstart).enumerate() {
                coordinates[vertex_label.convert()] = Some(Point2D {
                    t: ((tlen + t - tstart) % tlen) as f64,
                    x: (i as f64 / space_extension as f64) * average_space_extension,
                });
            }
        }

        let coordinates =
            coordinates.map_into(|opt| opt.expect("A node has not been given a coordinate"));

        Self {
            triangulation,
            periodic_graph: graph,
            time_extension,
            space_extension: average_space_extension,
            coordinates,
        }
    }

    fn get_coordinate(&self, label: PeriodicSpacetimeLabel) -> (f64, f64) {
        let base = self.coordinates[label.label];
        let x = base.x + label.patch.x as f64 * self.space_extension;
        let y = base.t + label.patch.t as f64 * self.time_extension;
        (x, y)
    }

    fn get_time(&self, label: PeriodicSpacetimeLabel) -> i32 {
        let tbase = self.triangulation.get_vertex(label.label.convert()).time();
        tbase as i32 + label.patch.t as i32 * self.triangulation.time_len() as i32
    }

    /// Shift the timeslices such that the total timelike seperation is minimized
    pub fn shift_slices(&mut self) {
        let sliced_embedding = SlicedEmbedding { embedding: self };
        let initial_shifts = Array1::from_elem(self.triangulation.time_len(), 0.0);
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        let result = Executor::new(sliced_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_shifts)
                    .max_iters(MAX_SLICE_SHIFT_ITERATIONS)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        let best_shifts = result
            .state
            .best_param
            .expect("Optimal shifts could not be determined");
        for label in self.periodic_graph.iter_base() {
            let t = usize::try_from(self.get_time(label))
                .expect("A base label should not have negative time");
            self.coordinates[label.label].x += best_shifts[t]
        }
    }

    /// Shift the vertices to get an harmonic embedding, that is: every vertex should be at
    /// (or in this case) as close as possible to the center of mass of its neighbours
    pub fn make_harmonic(&mut self) {
        let harmonic_embedding = HarmonicEmbedding { embedding: self };
        let initial_coordinates = Array1::from_iter(
            self.coordinates
                .iter()
                .flat_map(|point| [point.x, point.t].into_iter()),
        );
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        let result = Executor::new(harmonic_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_coordinates)
                    .max_iters(MAX_HARMONIC_ITERATIONS)
                    .target_cost(HARMONIC_TARGET_COST)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        println!(
            "Optimized with final cost: {}, in {} steps",
            result.state.best_cost, result.state.iter
        );
        let best_coordinates_flat = result
            .state
            .best_param
            .expect("Optimal coordinates could not be determined");

        let mut best_coordinates: LabelledArray<Option<Point2D>, Label<Node>> =
            LabelledArray::fill_with(|| None, self.coordinates.len());
        for label in best_coordinates.labels() {
            let i = usize::from(label);
            best_coordinates[label] = Some(Point2D {
                t: best_coordinates_flat[2 * i + 1],
                x: best_coordinates_flat[2 * i],
            });
        }
        self.coordinates =
            best_coordinates.map_into(|elem| elem.expect("All elements should be set"));
    }

    pub fn make_harmonic_with_observer(&mut self) {
        let harmonic_embedding = HarmonicEmbedding { embedding: self };
        let initial_coordinates = Array1::from_iter(
            self.coordinates
                .iter()
                .flat_map(|point| [point.x, point.t].into_iter()),
        );
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        let result = Executor::new(harmonic_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_coordinates)
                    .max_iters(MAX_HARMONIC_ITERATIONS)
                    .target_cost(HARMONIC_TARGET_COST)
            })
            .add_observer(SlogLogger::term(), ObserverMode::Always)
            .run()
            .expect("The gradient decent could not run correctly");
        println!(
            "Optimized with final cost: {}, in {} steps",
            result.state.best_cost, result.state.iter
        );
        let best_coordinates_flat = result
            .state
            .best_param
            .expect("Optimal coordinates could not be determined");

        let mut best_coordinates: LabelledArray<Option<Point2D>, Label<Node>> =
            LabelledArray::fill_with(|| None, self.coordinates.len());
        for label in best_coordinates.labels() {
            let i = usize::from(label);
            best_coordinates[label] = Some(Point2D {
                t: best_coordinates_flat[2 * i + 1],
                x: best_coordinates_flat[2 * i],
            });
        }
        self.coordinates =
            best_coordinates.map_into(|elem| elem.expect("All elements should be set"));
    }
}

/// Strange mapping of patch to number based on the ElegantPairing
/// If t is the vertical axis up and x the horizontal axis right it looks like:
/// 23 21 20 22 24
/// 11  7  6  8 18
///  9  1  0  4 16
/// 10  3  2  5 17
/// 15 13 12 14 19
/// This is done is such a way that the closer squares appear earlier in their
/// completeness.
fn patch_to_usize(patch: SpacetimePatch) -> usize {
    let ut = i8usize_central_mapping(patch.t);
    let ux = i8usize_central_mapping(patch.x);

    if ut < ux {
        ux * ux + ut
    } else {
        ut * (ut + 1) + ux
    }
}

/// Inverse of [`patch_to_usize()`]
fn usize_to_patch(index: usize) -> SpacetimePatch {
    let isqrt = (index as f64).sqrt() as usize;
    let (ut, ux) = if index - isqrt * isqrt < isqrt {
        (index - isqrt * isqrt, isqrt)
    } else {
        (isqrt, index - isqrt * (isqrt + 1))
    };

    SpacetimePatch {
        t: usizei8_central_mapping(ut),
        x: usizei8_central_mapping(ux),
    }
}

impl<'a, 'b> PeriodicEmbedding2D<'a, 'b> {
    pub fn export(&self, patches: usize) -> Embedding {
        let vertex_count = self.periodic_graph.base_len();
        let mut coordinates = Vec::new();
        for ipatch in 0..patches {
            assert_eq!(coordinates.len(), ipatch * vertex_count);
            let patch = usize_to_patch(ipatch);
            for baselabel in self.periodic_graph.iter_base() {
                let label = PeriodicSpacetimeLabel {
                    label: baselabel.label,
                    patch,
                };
                let (x, y) = self.get_coordinate(label);
                let t = self.get_time(label);
                coordinates.push(Vertex {
                    coordinate: (x, y),
                    time: t,
                });
            }
        }

        let mut edges = Vec::new();
        let triangle_count = self.triangulation.size();
        let mut triangles: Vec<Option<embedding::Triangle>> = vec![None; triangle_count * patches];
        for node in self.periodic_graph.iter_base() {
            for ipatch in 0..patches {
                let node = PeriodicSpacetimeLabel {
                    label: node.label,
                    patch: usize_to_patch(ipatch),
                };
                let inode = ipatch * vertex_count + usize::from(node.label);

                // Add edges
                for nbr in self.periodic_graph.iter_neighbours(node) {
                    let kind = match self.get_time(node).cmp(&self.get_time(nbr)) {
                        Ordering::Equal => {
                            if node.label > nbr.label {
                                continue;
                            }
                            EdgeKind::Spacelike
                        }
                        Ordering::Less => EdgeKind::Timelike,
                        Ordering::Greater => continue,
                    };
                    let ipatch = patch_to_usize(nbr.patch);
                    let inbr = ipatch * vertex_count + usize::from(nbr.label);
                    if inbr >= coordinates.len() {
                        continue;
                    }
                    edges.push(Edge {
                        start: inode,
                        end: inbr,
                        kind,
                    })
                }

                // Add triangles
                let up_label = self
                    .triangulation
                    .get_vertex(node.label.convert())
                    .triangle();
                let up_triangle = self.triangulation.get_triangle(up_label);
                assert_eq!(
                    usize::from(up_triangle.left_vertex),
                    usize::from(node.label)
                );

                let origin_node = self.periodic_graph.get_node_unchecked(node);
                let center_up_node = self
                    .periodic_graph
                    .get_neighbour(node, LinkLabel::Up(origin_node.up_count() - 1))
                    .expect("The last up link should exist by construction");
                assert_eq!(
                    usize::from(up_triangle.center_vertex),
                    usize::from(center_up_node.label),
                    "Triangle nodes should be neighbours of one other"
                );
                let icenter_up = patch_to_usize(center_up_node.patch) * vertex_count
                    + usize::from(center_up_node.label);
                let right_node = self
                    .periodic_graph
                    .get_neighbour_unchecked(node, LinkLabel::Right);
                assert_eq!(
                    usize::from(up_triangle.right_vertex),
                    usize::from(right_node.label),
                    "Triangle nodes should be neighbours of one other"
                );
                let iright =
                    patch_to_usize(right_node.patch) * vertex_count + usize::from(right_node.label);

                if icenter_up < coordinates.len() && iright < coordinates.len() {
                    let itriangle = ipatch * triangle_count + usize::from(up_label);
                    triangles[itriangle] = Some(embedding::Triangle {
                        vertices: (inode, icenter_up, iright),
                        label: usize::from(up_label),
                    });
                }

                let down_label = up_triangle.center;
                let down_triangle = self.triangulation.get_triangle(down_label);
                assert_eq!(
                    usize::from(down_triangle.left_vertex),
                    usize::from(node.label)
                );

                let center_down_node = self
                    .periodic_graph
                    .get_neighbour(node, LinkLabel::Down(0))
                    .expect("The first down link should exist by construction");
                assert_eq!(
                    usize::from(down_triangle.center_vertex),
                    usize::from(center_down_node.label),
                    "Triangle nodes should be neighbours of one other"
                );
                let icenter_down = patch_to_usize(center_down_node.patch) * vertex_count
                    + usize::from(center_down_node.label);

                if icenter_down < coordinates.len() && iright < coordinates.len() {
                    let itriangle = ipatch * triangle_count + usize::from(down_label);
                    triangles[itriangle] = Some(embedding::Triangle {
                        vertices: (inode, iright, icenter_down),
                        label: usize::from(down_label),
                    });
                }
            }
        }

        let triangles = triangles.into_iter().flatten().collect();

        Embedding {
            vertices: coordinates,
            edges,
            triangles,
        }
    }

    pub fn export_with_field<T: Display + Copy>(
        &self,
        patches: usize,
        field: ArrayView1<T>,
    ) -> EmbeddingField<T> {
        let mut unpacked_field = Vec::new();
        for _ in 0..patches {
            for baselabel in self.periodic_graph.iter_base() {
                unpacked_field.push(field[usize::from(baselabel.label)])
            }
        }

        EmbeddingField {
            embedding: self.export(patches),
            field: unpacked_field,
        }
    }

    pub fn export_with_dual_field<T: Display + Copy>(
        &self,
        patches: usize,
        field: ArrayView1<T>,
    ) -> EmbeddingDualField<T> {
        let base_embedding = self.export(patches);
        let unpacked_field: Vec<T> = base_embedding
            .triangles
            .iter()
            .map(|triangle| field[triangle.label])
            .collect();

        EmbeddingDualField {
            embedding: base_embedding,
            field: unpacked_field,
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    use crate::monte_carlo::fixed_universe::Universe;

    use super::*;

    #[test]
    fn simple_embedding() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);
        let mut universe = Universe::default(200, 10);
        for _ in 0..10 {
            universe.step(&mut rng);
        }
        let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        let embedding = PeriodicEmbedding2D::initialize(&triangulation, &graph);
        embedding.export(9);
    }

    #[test]
    fn shifted_embedding() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);
        let mut universe = Universe::default(200, 10);
        for _ in 0..10 {
            universe.step(&mut rng);
        }
        let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        let mut embedding = PeriodicEmbedding2D::initialize(&triangulation, &graph);
        embedding.shift_slices();
        embedding.export(9);
    }

    #[test]
    fn harmonic_embedding() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);
        let mut universe = Universe::default(200, 10);
        for _ in 0..10 {
            universe.step(&mut rng);
        }
        let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        let mut embedding = PeriodicEmbedding2D::initialize(&triangulation, &graph);
        embedding.shift_slices();
        embedding.make_harmonic();
        embedding.export(9);
    }

    #[test]
    fn patch_mapping() {
        assert_eq!(patch_to_usize(SpacetimePatch { t: 0, x: 0 }), 0);
        assert_eq!(patch_to_usize(SpacetimePatch { t: 0, x: -1 }), 1);
        assert_eq!(patch_to_usize(SpacetimePatch { t: -1, x: 0 }), 2);
        assert_eq!(patch_to_usize(SpacetimePatch { t: -1, x: -1 }), 3);
        assert_eq!(patch_to_usize(SpacetimePatch { t: 0, x: 1 }), 4);
        assert_eq!(patch_to_usize(SpacetimePatch { t: -3, x: 2 }), 34);

        for i in 0..144 {
            let patch = usize_to_patch(i);
            assert_eq!(patch_to_usize(patch), i);
        }
    }
}
