use std::{
    error::Error,
    io::{LineWriter, Write},
};

use crate::graph::{spacetime_graph::SpacetimeGraph, GenericGraph, IterableGraph};
use crate::triangulation::FullTriangulation;

/// Export [FullTriangulation] to text file including vertex and triangle connectivity
///
/// # Format
/// First line `N0 N2` contains the number of vertices and triangles respectively, which are
/// also the lengths of the lists on the following lines
///
/// The next `N0` lines contain the vertex information. Each of these lines has the format
/// `t(v0) nbr(v0)..`, where the first element is the time coordinate, followed by a list of
/// the vertex labels of the neighbours in clockwise order
///
/// The next `N2` lines contain the triangle information. Each of these lines has the format
/// ``u`/`d` v0(s0) v1(s0) v2(s0) nbr0(s0) nbr1(s0) nbr2(s0)`. Here the first element denotes
/// the type of triangle (up-pointing `u` or down-pointing `d`); the next three elements are the
/// vertex labels of the corners in the order: left, center, right; the final three elements are
/// the neighbouring triangles, where they correspond to the triangle opposite the respective vertex
pub fn export_full_triangulation<W>(
    buffer: W,
    triangulation: &FullTriangulation,
) -> Result<(), Box<dyn Error>>
where
    W: Write,
{
    let mut writer = LineWriter::new(buffer);

    // Write help header
    writeln!(writer, "# N0 N2",)?;
    writeln!(writer, "# t(v0) nbr(v0)..",)?;
    writeln!(writer, "# t(v1) nbr(v1)..",)?;
    writeln!(writer, "# ...",)?;
    writeln!(
        writer,
        "# `u`/`d` v0(s0) v1(s0) v2(s0) nbr0(s0) nbr1(s0) nbr2(s0)",
    )?;
    writeln!(writer, "# ...",)?;

    // Write data
    writeln!(
        writer,
        "{} {}",
        triangulation.vertex_count(),
        triangulation.triangles.len(),
    )?;

    let graph = GenericGraph::from_spacetime_graph(&SpacetimeGraph::from_triangulation_vertex(
        triangulation,
    ));
    for (vlabel, vertex) in triangulation.vertices.enumerate() {
        write!(writer, "{} ", vertex.time)?;
        let nbr_strs: Vec<String> = graph
            .iter_neighbours(vlabel.convert())
            .map(|nbr| usize::from(nbr).to_string())
            .collect();
        writeln!(writer, "{}", nbr_strs.join(" "))?;
    }

    for triangle in triangulation.triangles.iter() {
        write!(writer, "{} ", triangle.orientation.char_ascii())?;

        write!(writer, "{} ", triangle.left_vertex)?;
        write!(writer, "{} ", triangle.center_vertex)?;
        write!(writer, "{} ", triangle.right_vertex)?;

        write!(writer, "{} ", triangle.right)?;
        write!(writer, "{} ", triangle.center)?;
        writeln!(writer, "{}", triangle.left)?;
    }

    Ok(())
}
