//! Visualisation module for creating 2D and 3D graph embeddings
//!
//! The embeddings are focussed on toroidal graph, which are created from CDT triangulation

// #![warn(missing_docs)]

use std::{
    fmt::Display,
    ops::{Add, AddAssign, Div, Mul, Sub, SubAssign},
};

use ndarray::ArrayView2;
use num_traits::{real::Real, Float, FloatConst, NumAssignOps, Zero};

pub mod cylinder_embedding;
pub mod periodic_embedding2d;
pub mod torus_embedding;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Point2D {
    pub t: f64,
    pub x: f64,
}

type Point3D = Vector3D<f64>;

/// A standard 3D vector with many of the standard vector operations implemented
/// for calculations for 3D visualisations
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(missing_docs)]
pub struct Vector3D<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<T: Real> Vector3D<T> {
    /// Rotates the vector around the z-axis with a given `angle`
    pub fn rotate_z(&mut self, angle: T) {
        let x = self.x;
        let y = self.y;
        self.x = angle.cos() * x - angle.sin() * y;
        self.y = angle.sin() * x + angle.cos() * y;
    }

    /// Returns vector rotated around the z-axis with a given `angle`
    pub fn rotated_z(&self, angle: T) -> Self {
        Self {
            x: angle.cos() * self.x - angle.sin() * self.y,
            y: angle.sin() * self.x + angle.cos() * self.y,
            z: self.z,
        }
    }

    /// Rotates the vector around the x-axis with a given `angle`
    pub fn rotate_x(&mut self, angle: T) {
        let y = self.y;
        let z = self.z;
        self.y = angle.cos() * y - angle.sin() * z;
        self.z = angle.sin() * y + angle.cos() * z;
    }

    /// Returns vector rotated around the x-axis with a given `angle`
    pub fn rotated_x(&self, angle: T) -> Self {
        Self {
            x: self.x,
            y: angle.cos() * self.y - angle.sin() * self.z,
            z: angle.sin() * self.y + angle.cos() * self.z,
        }
    }

    /// Rotates the vector around the y-axis with a given `angle`
    pub fn rotate_y(&mut self, angle: T) {
        let x = self.x;
        let z = self.z;
        self.x = angle.cos() * x + angle.sin() * z;
        self.z = -angle.sin() * x + angle.cos() * z;
    }

    /// Returns vector rotated around the y-axis with a given `angle`
    pub fn rotated_y(&self, angle: T) -> Self {
        Self {
            x: angle.cos() * self.x + angle.sin() * self.z,
            y: self.y,
            z: -angle.sin() * self.x + angle.cos() * self.z,
        }
    }

    /// Returns the dot product between `self` and `other`
    pub fn dot(&self, other: &Self) -> T {
        let x2 = self.x * other.x;
        let y2 = self.y * other.y;
        let z2 = self.z * other.z;
        x2 + y2 + z2
    }

    /// Returns the cross product between `self` and `other`
    pub fn cross(&self, other: &Self) -> Self {
        Self {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.z,
            z: self.x * other.y - self.y * other.x,
        }
    }

    /// Compute the square norm of the vector
    pub fn norm2(&self) -> T {
        self.dot(self)
    }

    /// Compute the norm of the vector
    pub fn norm(&self) -> T {
        self.norm2().sqrt()
    }

    /// Compute the square distance between two vectors,
    /// i.e. the square norm of their difference.
    pub fn distance2(&self, other: &Self) -> T {
        let dx2 = (other.x - self.x).powi(2);
        let dy2 = (other.y - self.y).powi(2);
        let dz2 = (other.z - self.z).powi(2);
        dx2 + dy2 + dz2
    }

    /// Compute the distance between two vectors,
    /// i.e. the norm of their difference.
    pub fn distance(&self, other: &Self) -> T {
        self.distance2(other).sqrt()
    }

    /// Returns the `matrix` tranformed vector
    ///
    /// Matrix must be a 3x3 matrix otherwise this may return unexpected results
    pub fn transformed(&self, matrix: ArrayView2<T>) -> Self {
        let x = matrix[(0, 0)] * self.x + matrix[(0, 1)] * self.y + matrix[(0, 2)] * self.x;
        let y = matrix[(1, 0)] * self.x + matrix[(1, 1)] * self.y + matrix[(1, 2)] * self.x;
        let z = matrix[(2, 0)] * self.x + matrix[(2, 1)] * self.y + matrix[(2, 2)] * self.x;

        Self { x, y, z }
    }

    /// Transforms the with with `matrix`
    ///
    /// Matrix must be a 3x3 matrix otherwise this may return unexpected results
    pub fn transform(&mut self, matrix: ArrayView2<T>) {
        *self = self.transformed(matrix);
    }
}

/// Returns the modulus of an angle
///
/// This mean it returns the equivalent angle to `angle` in the range [0, 2pi]
pub fn modulus_angle<T: Float + FloatConst + NumAssignOps>(mut angle: T) -> T {
    let n = (angle / T::TAU()).floor();
    angle -= n * T::TAU();
    if angle.is_sign_negative() {
        angle += T::TAU();
    }
    angle
}

/// Returns the modulus of an angle around 0
///
/// This mean it returns the equivalent angle to `angle` in the range [-pi, pi]
pub fn modulus_central_angle<T: Float + FloatConst + NumAssignOps>(angle: T) -> T {
    let angle = modulus_angle(angle);
    let n = (angle / T::PI()).floor();
    angle - n * T::TAU()
}

impl<T: Real> Sub for Vector3D<T> {
    type Output = Vector3D<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector3D {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl<T: Real> Add for Vector3D<T> {
    type Output = Vector3D<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Vector3D {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl<T: Real + AddAssign> AddAssign for Vector3D<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl<T: Real + SubAssign> SubAssign for Vector3D<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl<T: Display> Display for Vector3D<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({},{},{})", self.x, self.y, self.z)
    }
}

impl<T: Real> Mul<T> for Vector3D<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Vector3D {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl Mul<Vector3D<f64>> for f64 {
    type Output = Vector3D<f64>;

    fn mul(self, rhs: Vector3D<f64>) -> Self::Output {
        Vector3D {
            x: rhs.x * self,
            y: rhs.y * self,
            z: rhs.z * self,
        }
    }
}

impl Mul<Vector3D<f32>> for f32 {
    type Output = Vector3D<f32>;

    fn mul(self, rhs: Vector3D<f32>) -> Self::Output {
        Vector3D {
            x: rhs.x * self,
            y: rhs.y * self,
            z: rhs.z * self,
        }
    }
}

impl<T: Real> Div<T> for Vector3D<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Vector3D {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs,
        }
    }
}

impl<T: Zero + Real> Zero for Vector3D<T> {
    fn zero() -> Self {
        Self {
            x: T::zero(),
            y: T::zero(),
            z: T::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self.x.is_zero() && self.y.is_zero() && self.z.is_zero()
    }
}

// impl<T: Real> Mul<Vector3D<T>> for T {
//     type Output = Self;

//     fn mul(self, rhs: T) -> Self::Output {
//         Vector3D {
//             x: self.x * rhs,
//             y: self.y * rhs,
//             z: self.z * rhs,
//         }
//     }
// }

#[cfg(test)]
mod tests {
    use std::{f32::consts::FRAC_PI_2, f64::consts::TAU};

    use super::*;

    #[test]
    fn rotate_vector() {
        let mut v = Vector3D {
            x: 1.0,
            y: 0.0,
            z: 0.0,
        };
        let v2 = Vector3D {
            x: 0.0,
            y: 1.0,
            z: 0.0,
        };
        v.rotate_z(FRAC_PI_2);
        assert!(v.distance2(&v2) < 1e-6);
        assert!(v2.rotated_z(0.0).distance2(&v2) < 1e-6);
    }

    #[test]
    fn modulus_angles() {
        assert!(modulus_angle(0.1) - 0.1 < 1e-6);
        assert!(modulus_angle(-0.1) - (TAU - 0.1) < 1e-6);
        assert!(modulus_angle(4.0) - 4.0 < 1e-6);
        assert!(modulus_angle(TAU + 0.3) - 0.3 < 1e-6);

        assert!(modulus_central_angle(0.1) - 0.1 < 1e-6);
        assert!(modulus_central_angle(-0.1) - (-0.1) < 1e-6);
        assert!(modulus_central_angle(4.0) - (4.0 - TAU) < 1e-6);
        assert!(modulus_central_angle(5.0 * TAU - 0.1) - (0.1) < 1e-6);
    }

    #[test]
    fn cross_product() {
        let x = Vector3D {
            x: 1.0,
            y: 0.0,
            z: 0.0,
        };
        let y = Vector3D {
            x: 0.0,
            y: 1.0,
            z: 0.0,
        };
        assert_eq!(
            x.cross(&y),
            Vector3D {
                x: 0.0,
                y: 0.0,
                z: 1.0
            }
        );
    }
}
