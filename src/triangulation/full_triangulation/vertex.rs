use super::triangle::Triangle;
use crate::{collections::Label, graph::spacetime_graph};

/// Struct representing a vertex with it's connectivity
///
/// The vertices are ordered in such a way that from `left`,
/// through `up`, to `right`, through `down` is a clockwise
/// listing of the graph link, starting from `left` being
/// the left spacelike edge.
#[derive(Debug, Clone)]
pub struct Vertex {
    pub(crate) time: usize, // Marks the timeslice
    // Marks the upper triangle of the shard the vertex is associated to,
    // such that the associated vertex is the left most vertex of the shard.
    pub(crate) triangle: Label<Triangle>,
    pub(crate) left: Label<Vertex>,
    pub(crate) up: Box<[Label<Vertex>]>,
    pub(crate) right: Label<Vertex>,
    pub(crate) down: Box<[Label<Vertex>]>,
}

impl Vertex {
    /// Return the vertex degree of the [`Vertex`]
    pub fn degree(&self) -> usize {
        self.up.len() + self.down.len() + 2
    }

    /// Return the time coordinate of the [`Vertex`]
    pub fn time(&self) -> usize {
        self.time
    }

    /// Return the triangle associated with the [`Vertex`]
    ///
    /// This is the upper triangle of the shard the vertex
    /// is the left most vertex of.
    pub fn triangle(&self) -> Label<Triangle> {
        self.triangle
    }
}

impl Vertex {
    pub fn as_node(&self) -> spacetime_graph::Node {
        let left = self.left.convert();
        let up = self.up.iter().map(|&label| label.convert()).collect();
        let right = self.right.convert();
        let down = self.down.iter().map(|&label| label.convert()).collect();
        spacetime_graph::Node::new(left, up, right, down)
    }
}
