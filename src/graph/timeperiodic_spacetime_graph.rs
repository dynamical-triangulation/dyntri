//! Implementation of graph traits for [`TimePeriodicSpacetimeGraph`], see that for details on use

use std::char::ParseCharError;
use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

use super::field::{Field as FieldTrait, FieldCollection, FieldValue};
use super::periodic1d_generic_graph::i8usize_central_mapping;
use super::spacetime_graph::{LinkLabel, Node};
use super::{FieldGraph, Graph, IterableGraph};
use crate::collections::{Label, LabelledArray};

mod from_triangulation;
mod iter_neighbours;
mod iter_neighbours_forward;
pub(super) mod path;

pub use iter_neighbours::IterNeighbours;

#[derive(Debug, Clone, Copy)]
pub enum TimeBoundary {
    Center,
    Forward,
    Backward,
}

#[derive(Debug, Clone)]
pub enum ParseTimeBoundaryError {
    ParseCharError(ParseCharError),
    IncorrectCharError,
}

impl Display for ParseTimeBoundaryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ParseTimeBoundaryError::ParseCharError(err) => err.fmt(f),
            ParseTimeBoundaryError::IncorrectCharError => write!(
                f,
                "Only the characters 'C'/'U'/'D', 'C'/'N'/'S' or 'C'/'F'/'B' can be used."
            ),
        }
    }
}

impl Error for ParseTimeBoundaryError {}

impl FromStr for TimeBoundary {
    type Err = ParseTimeBoundaryError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let symbol: char = s.parse().map_err(ParseTimeBoundaryError::ParseCharError)?;
        let bound = match symbol.to_ascii_uppercase() {
            'C' => TimeBoundary::Center,
            'U' => TimeBoundary::Forward,
            'N' => TimeBoundary::Forward,
            'F' => TimeBoundary::Forward,
            'D' => TimeBoundary::Backward,
            'S' => TimeBoundary::Backward,
            'B' => TimeBoundary::Backward,
            _ => return Err(ParseTimeBoundaryError::IncorrectCharError),
        };
        Ok(bound)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct TimePeriodicLabel {
    label: Label<Node>,
    patch: i8,
}

impl super::Label for TimePeriodicLabel {}

impl TimePeriodicLabel {
    pub fn new(label: Label<Node>, patch: i8) -> Self {
        Self { label, patch }
    }

    pub fn new_center(label: Label<Node>) -> Self {
        Self::new(label, 0)
    }
}

impl From<usize> for TimePeriodicLabel {
    fn from(index: usize) -> Self {
        Self::new_center(Label::from(index))
    }
}

impl Display for TimePeriodicLabel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}({})", self.label, self.patch)
    }
}

#[derive(Debug, Clone)]
pub struct NodeLinks<T> {
    pub(super) left: T,
    pub(super) up: Box<[T]>,
    pub(super) right: T,
    pub(super) down: Box<[T]>,
}

impl<T: std::fmt::Debug> NodeLinks<T> {
    /// Return the [`Label`] of the [`Node`] the [`LinkLabel`] is pointing to
    pub(super) fn get_neighbour(&self, link: LinkLabel) -> Option<&T> {
        match link {
            LinkLabel::Left => Some(&self.left),
            LinkLabel::Up(i) => self.up.get(i),
            LinkLabel::Right => Some(&self.right),
            LinkLabel::Down(i) => self.down.get(i),
        }
    }

    /// Return the [`Label`] of the [`Node`] the [`LinkLabel`] is pointing to,
    /// assuming `link` actually points to an existing link.
    pub(super) fn get_neighbour_unchecked(&self, link: LinkLabel) -> &T {
        match link {
            LinkLabel::Left => &self.left,
            LinkLabel::Up(i) => &self.up[i],
            LinkLabel::Right => &self.right,
            LinkLabel::Down(i) => &self.down[i],
        }
    }

    /// Set the [`Label`] of the [`Node`] the [`LinkLabel`] is pointing to
    pub(super) fn set_neighbour(&mut self, link: LinkLabel, value: T) {
        let nbr = match link {
            LinkLabel::Left => &mut self.left,
            LinkLabel::Up(i) => &mut self.up[i],
            LinkLabel::Right => &mut self.right,
            LinkLabel::Down(i) => &mut self.down[i],
        };
        *nbr = value;
    }

    pub(super) fn fill_like_node<F: Fn() -> T>(fill: F, node: &Node) -> Self {
        Self {
            left: fill(),
            up: std::iter::repeat_with(&fill).take(node.up.len()).collect(),
            right: fill(),
            down: std::iter::repeat_with(&fill)
                .take(node.down.len())
                .collect(),
        }
    }

    pub(super) fn combine<U, F>(&mut self, other: &NodeLinks<U>, mut combine: F)
    where
        F: FnMut(&mut T, U),
        U: Copy + std::fmt::Debug,
    {
        if self.up.len() != other.up.len() || self.down.len() != other.down.len() {
            panic!(
                "Cannot combine NodeLinks with different connectivity.\nself: {:?}, other: {:?}",
                self, other
            );
        }

        combine(&mut self.left, other.left);
        self.up
            .iter_mut()
            .enumerate()
            .for_each(|(i, up_elem)| combine(up_elem, other.up[i]));
        combine(&mut self.right, other.right);
        self.down
            .iter_mut()
            .enumerate()
            .for_each(|(i, down_elem)| combine(down_elem, other.down[i]));
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ParseNodeLinksError<E> {
    ItemParseError(E),
    IncorrectNodeLinksFormat,
}

use ParseNodeLinksError::IncorrectNodeLinksFormat as IncorrectFormat;
use ParseNodeLinksError::ItemParseError;

impl<E: Display> Display for ParseNodeLinksError<E> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IncorrectNodeLinksFormat => {
                write!(f, "Node string could not be parsed, use specified format.")
            }
            Self::ItemParseError(err) => write!(
                f,
                "An item in the Node string could not be parsed, error: {}",
                err
            ),
        }
    }
}

impl<E: Error> Error for ParseNodeLinksError<E> {}

impl<E, T: FromStr<Err = E>> FromStr for NodeLinks<T> {
    type Err = ParseNodeLinksError<E>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut links = s.split(';');
        let left_str = links.next().ok_or(IncorrectFormat)?;
        let left: T = left_str.trim().parse().map_err(|err| ItemParseError(err))?;
        let up_str = links.next().ok_or(IncorrectFormat)?;
        let mut up: Vec<T> = Vec::new();
        for up_elem in up_str.trim().split(',') {
            up.push(up_elem.trim().parse().map_err(|err| ItemParseError(err))?);
        }
        let right_str = links.next().ok_or(IncorrectFormat)?;
        let right: T = right_str
            .trim()
            .parse()
            .map_err(|err| ItemParseError(err))?;
        let down_str = links.next().ok_or(IncorrectFormat)?;
        let mut down: Vec<T> = Vec::new();
        for down_elem in down_str.trim().split(',') {
            down.push(
                down_elem
                    .trim()
                    .parse()
                    .map_err(|err| ItemParseError(err))?,
            );
        }

        Ok(Self {
            left,
            up: up.into_boxed_slice(),
            right,
            down: down.into_boxed_slice(),
        })
    }
}

type BoundaryList = LabelledArray<Option<NodeLinks<TimeBoundary>>, Label<Node>>;
#[derive(Debug, Clone)]
pub struct TimePeriodicSpacetimeGraph {
    pub(super) nodes: LabelledArray<Node>,
    pub(super) bounds: BoundaryList,
}

impl TimePeriodicSpacetimeGraph {
    pub fn new(
        nodes: LabelledArray<Node>,
        bounds: LabelledArray<Option<NodeLinks<TimeBoundary>>, Label<Node>>,
    ) -> Self {
        Self { nodes, bounds }
    }
}

impl Graph for TimePeriodicSpacetimeGraph {
    type Label = TimePeriodicLabel;

    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.nodes[label.label].vertex_degree()
    }
}

// Implementation is achieved by mapping `i8` to `usize` in such a way that:
// (0, -1, 1, -2, 2, -3, ...) -> (0, 1, 2, 3, 4, 5, 6, ...)
// This results in the internal vector being able to be small since patches will usually be
// only -1, 0, or 1; and it will grow in both directions.
pub type Field<T> = FieldCollection<T, LabelledArray<Vec<FieldValue<T>>, Label<Node>>>;

impl<T: Display> Display for Field<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut values = self
            .0
            .enumerate()
            .flat_map(|(label, values)| std::iter::repeat(label).zip(values.iter()))
            .filter_map(|(label, fieldvalue)| {
                fieldvalue.as_ref().some().map(|value| (label, value))
            });
        let Some((first_label, first_value)) = values.next() else {
            return Ok(());
        };
        if let Some(precision) = f.precision() {
            write!(f, "{}: {:.2$}", first_label, first_value, precision)?;
            for (label, value) in values {
                write!(f, ", {}: {:.2$}", label, value, precision)?;
            }
        } else {
            write!(f, "{}: {}", first_label, first_value,)?;
            for (label, value) in values {
                write!(f, ", {}: {}", label, value)?;
            }
        }
        Ok(())
    }
}

impl<T> FieldTrait<T, TimePeriodicSpacetimeGraph> for Field<T> {
    fn get(&self, label: TimePeriodicLabel) -> &FieldValue<T> {
        match self.0[label.label].get(i8usize_central_mapping(label.patch)) {
            None => self.none(),
            Some(value) => value,
        }
    }

    fn get_mut(&mut self, label: TimePeriodicLabel) -> &mut FieldValue<T> {
        let index = i8usize_central_mapping(label.patch);
        let list = &mut self.0[label.label];

        // Resize the list if there are too little elements
        if index >= list.len() {
            list.resize_with(index + 1, || FieldValue::None);
        }

        &mut list[index]
    }
}

impl FieldGraph for TimePeriodicSpacetimeGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        FieldCollection::new(LabelledArray::fill_with(Vec::new, self.nodes.len()))
    }
}

impl<'a> IterableGraph<'a, IterNeighbours<'a>> for TimePeriodicSpacetimeGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighbours<'a> {
        IterNeighbours::from_graph(self, label)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        graph::breadth_first_search::{
            traverse_nodes::{TraverseNodeDistance, TraverseNodeLabel},
            IterBreadthFirst,
        },
        io::graph,
        observables::{diffusion, distance_profile},
        triangulation::{FixedTriangulation, FullTriangulation},
    };

    use super::*;
    use indoc::indoc;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;
    use std::io::Cursor;

    const EXAMPLE_NODE: &str = "3; 1, 4, 6; 5; 4";
    const EXAMPLE_BOUND: &str = "C; N, N, N; C; S";
    const EXAMPLE_GRAPH: &str = indoc! {"
        2; 6, 3, 4; 1; 11, 10, 14, 13
        0; 4, 5, 6; 2; 11
        1; 6; 0; 13, 12, 11
        6; 7; 4; 0
        3; 7; 5; 1, 0
        4; 7, 8, 9; 6; 1
        5; 9, 7; 3; 0, 2, 1
        9; 14, 10, 11; 8; 5, 4, 3, 6
        7; 11; 9; 5
        8; 11, 12, 13, 14; 7; 6, 5
        14; 0; 11; 7
        10; 0, 1, 2; 12; 9, 8, 7
        11; 2; 13; 9
        12; 2, 0; 14; 9
        13; 0; 10; 7, 9
    "};
    const EXAMPLE_BOUNDS: [(usize, &str); 8] = [
        (0, "C; C, C, C; C; D, D, D, D"),
        (1, "C; C, C, C; C; D"),
        (2, "C; C; C; D, D, D"),
        (10, "C; U; C; C"),
        (11, "C; U, U, U; C; C, C, C"),
        (12, "C; U; C; C"),
        (13, "C; U, U; C; C"),
        (14, "C; U; C; C, C"),
    ];
    // Expected results for starting label 8
    const EXPECTED_EXAMPLE_SEARCH: [(usize, usize, i8); 30] = [
        (8, 0, 0),
        (7, 1, 0),
        (11, 1, 0),
        (9, 1, 0),
        (5, 1, 0),
        (14, 2, 0),
        (10, 2, 0),
        (4, 2, 0),
        (3, 2, 0),
        (6, 2, 0),
        (0, 2, 1),
        (1, 2, 1),
        (2, 2, 1),
        (12, 2, 0),
        (13, 2, 0),
        (1, 2, 0),
        (0, 3, 0),
        (2, 3, 0),
        (6, 3, 1),
        (3, 3, 1),
        (4, 3, 1),
        (5, 3, 1),
        (11, 3, -1),
        (10, 4, -1),
        (14, 4, -1),
        (13, 4, -1),
        (12, 4, -1),
        (9, 4, 1),
        (7, 4, 1),
        (8, 4, 1),
    ];

    #[test]
    fn parse_example_node() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();
        println!("{:?}", node)
    }

    #[test]
    fn parse_example_bound() {
        let bound: NodeLinks<TimeBoundary> = EXAMPLE_BOUND.parse().unwrap();
        println!("{:?}", bound)
    }

    #[test]
    fn node_neighbour_iterator() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();
        let bound: NodeLinks<TimeBoundary> = EXAMPLE_BOUND.parse().unwrap();

        println!("Neigbours in center");
        for node in IterNeighbours::new(&node, None, 0) {
            print!("{:} ", node)
        }
        println!();
        let expected_result: [(usize, i8); 6] = [(3, 0), (1, 0), (4, 0), (6, 0), (5, 0), (4, 0)];
        assert!(
            IterNeighbours::new(&node, None, 0).eq(expected_result.into_iter().map(|l| {
                TimePeriodicLabel {
                    label: Label::from(l.0),
                    patch: l.1,
                }
            }))
        );

        println!("Neigbours on edge");
        for node in IterNeighbours::new(&node, Some(&bound), 0) {
            print!("{:} ", node)
        }
        println!();
        let expected_result: [(usize, i8); 6] = [(3, 0), (1, 1), (4, 1), (6, 1), (5, 0), (4, -1)];
        assert!(
            IterNeighbours::new(&node, Some(&bound), 0).eq(expected_result.into_iter().map(|l| {
                TimePeriodicLabel {
                    label: Label::from(l.0),
                    patch: l.1,
                }
            }))
        );
    }

    fn example_graph() -> TimePeriodicSpacetimeGraph {
        let nodes = graph::import_spacetime_adjacency_list(Cursor::new(EXAMPLE_GRAPH)).unwrap();
        let mut bounds = LabelledArray::fill_with(|| None, nodes.len());
        for (node_label, bound_str) in EXAMPLE_BOUNDS {
            let label = Label::from(node_label);
            let node_bounds: NodeLinks<TimeBoundary> = bound_str.parse().unwrap();
            bounds[label] = Some(node_bounds);
        }
        TimePeriodicSpacetimeGraph::new(nodes, bounds)
    }

    #[test]
    fn import_example_graph() {
        example_graph();
    }

    #[test]
    fn check_graph_undirected() {
        let graph = example_graph();
        assert_graph_undirected(&graph);
        assert_graph_node_bound_connectivity(&graph);
    }

    fn assert_graph_undirected(graph: &TimePeriodicSpacetimeGraph) {
        for label in graph.nodes.labels() {
            let node = &graph.nodes[label];
            let label_left = node.left;
            let labels_up = &node.up;
            let label_right = node.right;
            let labels_down = &node.down;
            assert_eq!(graph.nodes[label_left].right, label);
            for &label_up in labels_up.iter() {
                assert!(graph.nodes[label_up].down.contains(&label));
            }
            assert_eq!(graph.nodes[label_right].left, label);
            for &label_down in labels_down.iter() {
                assert!(graph.nodes[label_down].up.contains(&label));
            }
        }
    }

    #[test]
    fn bfs_plain() {
        let graph = example_graph();
        let origin = TimePeriodicLabel {
            label: Label::from(8),
            patch: 0,
        };

        // Display search results
        println!("Plain breadth first search");
        for node in graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .take(40)
        {
            print!("{} ", node.label)
        }
        println!();

        // Assert search
        graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .map(|node| (usize::from(node.label.label), node.label.patch))
            .zip(EXPECTED_EXAMPLE_SEARCH.into_iter().map(|x| (x.0, x.2)))
            .for_each(|(left, right)| assert_eq!(left, right));
    }

    #[test]
    fn bfs_distance() {
        let graph = example_graph();
        let origin = TimePeriodicLabel {
            label: Label::from(8),
            patch: 0,
        };

        // Display search results
        println!("Distanced breadth first search");
        let mut current_distance = 0;
        print!("{}: ", current_distance);
        let bfs: IterBreadthFirst<_, IterNeighbours, TraverseNodeDistance<_>> =
            graph.iter_breadth_first(origin);

        for node in bfs.take(40) {
            if node.distance > current_distance {
                println!();
                current_distance = node.distance;
                print!("{}: ", current_distance);
            }
            print!("{} ", node.label)
        }
        println!();

        // Assert search
        graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .map(|node| {
                (
                    usize::from(node.label.label),
                    node.distance,
                    node.label.patch,
                )
            })
            .zip(EXPECTED_EXAMPLE_SEARCH)
            .for_each(|(left, right)| assert_eq!(left, right));
    }

    #[test]
    fn sphere_area_growth_test() {
        let graph = example_graph();
        let origin = TimePeriodicLabel {
            label: Label::from(4),
            patch: 0,
        };
        let profile = distance_profile::measure(&graph, origin, 10);
        println!("{}", profile);

        let mut manual_profile = Vec::new();
        manual_profile.push(0usize);
        let mut current_distance = 0;
        for node in graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .take(50)
        {
            if node.distance > current_distance {
                current_distance = node.distance;
                manual_profile.push(0usize);
            }
            let last_index = manual_profile.len() - 1;
            manual_profile[last_index] += 1;
        }
        manual_profile.pop();
        manual_profile
            .into_iter()
            .enumerate()
            .for_each(|(r, count)| assert_eq!(profile.get_count(r), count));
    }

    #[test]
    fn diffusion_test() {
        let graph = example_graph();
        let origin = TimePeriodicLabel::from(4);
        println!("Diffusion with D=1.0");
        let mut diff = diffusion::iterable(&graph, origin, 1.0);
        for _ in 0..3 {
            let rprob = diff.next().unwrap();
            println!(
                "sigma: {}, rprob: {:.5}\n {:.3}",
                diff.current_sigma().unwrap(),
                rprob,
                diff.current_state()
            );
        }
        println!("Diffusion with D=0.9");
        let mut diff = diffusion::iterable(&graph, origin, 0.9);
        for _ in 0..3 {
            let rprob = diff.next().unwrap();
            let total_prob: f64 = diff
                .current_state()
                .0
                .iter()
                .flat_map(|values| values.iter())
                .filter_map(|value| value.some())
                .sum();
            println!(
                "sigma: {}, rprob: {:.5}, totalprob: {:}\n {:.3}",
                diff.current_sigma().unwrap(),
                rprob,
                total_prob,
                diff.current_state()
            );
            assert!((total_prob - 1.0).abs() < 1e-9)
        }
    }

    fn random_triangulation() -> FullTriangulation {
        let mut triangulation = FixedTriangulation::new(10, 5);
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..28 {
            triangulation.relocate(
                triangulation.sample_order_four(&mut rng),
                triangulation.sample(&mut rng),
            );
        }
        for _ in 0..300 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn from_triangulation_test() {
        let triangulation = random_triangulation();
        let vertex_graph = TimePeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
        assert_graph_node_bound_connectivity(&vertex_graph);
        assert_graph_undirected(&vertex_graph);
        let dual_graph = TimePeriodicSpacetimeGraph::from_triangulation_dual(&triangulation);
        assert_graph_node_bound_connectivity(&dual_graph);
        assert_graph_undirected(&dual_graph);
    }

    /// Check if nodes and bounds have same connectivity
    fn assert_graph_node_bound_connectivity(graph: &TimePeriodicSpacetimeGraph) {
        for (label, node) in graph.nodes.enumerate() {
            let Some(bound) = &graph.bounds[label] else {
                continue;
            };
            assert_eq!(
                bound.up.len(),
                node.up.len(),
                "Node {}: {:?} did not match up with bound {:?}",
                label,
                node,
                bound
            );
            assert_eq!(
                bound.down.len(),
                node.down.len(),
                "Node {}: {:?} did not match up with bound {:?}",
                label,
                node,
                bound
            );
        }
    }
}
