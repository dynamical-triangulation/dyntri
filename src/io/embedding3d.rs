//! Custom inspired format to store graph with embedding and additional decorations

use std::{fmt::Display, io::Write};

#[derive(Debug, Clone)]
pub struct Embedding3D {
    pub vertices: Vec<Vertex>,
    pub edges: Vec<Edge>,
    pub triangles: Vec<Triangle>,
}

#[derive(Debug, Clone)]
pub struct Embedding3DField<T> {
    pub embedding: Embedding3D,
    pub field: Vec<T>,
}

#[derive(Debug, Clone)]
pub struct Embedding3DDualField<T> {
    pub embedding: Embedding3D,
    pub field: Vec<T>,
}

#[derive(Debug, Clone)]
pub struct Vertex {
    pub coordinate: (f64, f64, f64),
    pub time: i32,
}

#[derive(Debug, Clone)]
pub struct Edge {
    pub start: usize,
    pub end: usize,
    pub kind: EdgeKind,
}

/// Designed to hold the vertex labels of the triangle in clockwise order
#[derive(Debug, Clone)]
pub struct Triangle {
    pub vertices: (usize, usize, usize),
    pub label: usize,
}

#[derive(Debug, Clone, Copy)]
pub enum EdgeKind {
    Spacelike,
    Timelike,
}

impl Display for EdgeKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EdgeKind::Spacelike => write!(f, "0"),
            EdgeKind::Timelike => write!(f, "1"),
        }
    }
}

impl Embedding3D {
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.vertices.len())?;
        writeln!(buffer, "{}", self.edges.len())?;
        writeln!(buffer, "{}", self.triangles.len())?;

        for vertex in self.vertices.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.coordinate.2, vertex.time
            )?;
        }

        for edge in self.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for triangle in self.triangles.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                triangle.vertices.0, triangle.vertices.1, triangle.vertices.2, triangle.label
            )?;
        }

        Ok(())
    }
}

impl<T: Display> Embedding3DField<T> {
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.embedding.vertices.len())?;
        writeln!(buffer, "{}", self.embedding.edges.len())?;
        writeln!(buffer, "{}", self.embedding.triangles.len())?;

        for (i, vertex) in self.embedding.vertices.iter().enumerate() {
            let value = self
                .field
                .get(i)
                .expect("Not enough field values are given");
            writeln!(
                buffer,
                "{} {} {} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.coordinate.2, vertex.time, value
            )?;
        }

        for edge in self.embedding.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for triangle in self.embedding.triangles.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                triangle.vertices.0, triangle.vertices.1, triangle.vertices.2, triangle.label
            )?;
        }

        Ok(())
    }
}

impl<T: Display> Embedding3DDualField<T> {
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.embedding.vertices.len())?;
        writeln!(buffer, "{}", self.embedding.edges.len())?;
        writeln!(buffer, "{}", self.embedding.triangles.len())?;

        for vertex in self.embedding.vertices.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.coordinate.1, vertex.time
            )?;
        }

        for edge in self.embedding.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for (triangle, value) in self.embedding.triangles.iter().zip(self.field.iter()) {
            writeln!(
                buffer,
                "{} {} {} {} {}",
                triangle.vertices.0,
                triangle.vertices.1,
                triangle.vertices.2,
                triangle.label,
                value
            )?;
        }

        Ok(())
    }
}
