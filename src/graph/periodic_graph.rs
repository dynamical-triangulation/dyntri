//! Graph traits for graphs that are periodic, that is infinite and with a repeating structure.
use super::{field::Field, FieldGraph, Graph, Label};

/// Enum representing in what way to node labels match one another
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum PeriodicLabelMatch {
    /// Two labels are identical, i.e. they refer to the same node
    Equal = 2,
    /// Two labels refer to equivalent nodes in different patches,
    /// i.e. the nodes are copies of oneanother in different patches.
    Patch = 1,
    /// The two labels refer to two entirely different nodes
    None = 0,
}

/// A type of labels that refers to nodes in a periodic graph
///
/// These should keep track of some sort of patches within which each node has a label, which
/// we call the [`BaseLabel`](PeriodicLabel::BaseLabel)
pub trait PeriodicLabel: Label {
    /// The label of the node within a patch
    type BaseLabel: Into<usize>;

    /// Compare to labels to check whether they refer to the same node or nodes with the same
    /// base label
    fn cmp(self, other: Self) -> PeriodicLabelMatch;

    /// Returns the base label of a [`PeriodicLabel`]
    fn base_label(self) -> Self::BaseLabel;
}

/// A field on a periodic graph
pub trait PeriodicField<T, G>: Field<T, G>
where
    G: FieldGraph<FieldType<T> = Self> + PeriodicGraph,
    G::Label: PeriodicLabel,
{
    /// Returns if the field value at `label` was already set in given or other patch.
    ///
    /// If the `label` itself was set [`PeriodicLabelMatch::Equal`] is returned, if it was only
    /// set in another patch [`PeriodicLabelMatch::Patch`], else [`PeriodicLabelMatch::None`].
    fn has(&self, label: G::Label) -> PeriodicLabelMatch;

    /// Returns the amount of patches the `label` appears in
    fn has_in_patches(&self, label: <G::Label as PeriodicLabel>::BaseLabel) -> usize;

    /// Returns if the value at another patch of `label` is already set
    fn has_in_other_patch(&self, label: G::Label) -> bool {
        let patch_count = self.has_in_patches(label.base_label());
        if Field::has(self, label) {
            patch_count > 1
        } else {
            patch_count > 0
        }
    }

    /// Returns whether any label in `patch` is already defined
    fn has_patch(&self, patch: G::Patch) -> bool;
}

/// A graph with periodicity that can be infinite
///
/// This means the graph has a certain structure that repeats itself
pub trait PeriodicGraph: Graph {
    /// The patch label used to denote each patch
    type Patch: Copy + std::fmt::Debug;

    /// The number of nodes in a single patch of the periodic graph
    fn base_len(&self) -> usize;
}

/// A graph with periodicity but a limited number of copies of each patch,
/// meaning it is not infinite
pub trait BoundedPeriodicGraph<I>: PeriodicGraph
where
    I: Iterator<Item = Self::Patch>,
{
    /// Returns an iterator over patches of the bounded periodic graph
    ///
    /// This means the iterator is finite
    fn iter_patches(&self) -> I;
}

/// A graph over whose nodes one can iterate
pub trait IterPeriodicGraph<I>: PeriodicGraph
where
    I: Iterator<Item = Self::Label>,
{
    /// Iterate over labels in the base copy; do `iter_patch()` on the base patch.
    fn iter_base(&self) -> I;

    /// Iterate over all labels in the a copy given by patch of a periodic
    /// graph in order, i.e. it should be implemented such that the iteration
    /// starts at the label representing 0 and each consecutive element should it's succesor.
    fn iter_patch(&self, patch: Self::Patch) -> I;
}

impl PeriodicLabelMatch {
    /// Return combination of [`PeriodicLabelMatch`] with `self`
    ///
    /// This combination is done in a way such that the strongest match condition is used.
    /// From strong to weak, the match conditions are:
    ///  - [`PeriodicLabelMatch::Equal`]
    ///  - [`PeriodicLabelMatch::Patch`]
    ///  - [`PeriodicLabelMatch::None`]
    /// So for example combining `Equal` with `Patch` gives `Equal`
    ///
    #[inline]
    pub fn combine(self, other: Self) -> Self {
        std::cmp::max(self, other)
    }

    /// Combine `other` with `self` using the rules of [`PeriodicLabelMatch::combine`]
    #[inline]
    pub fn update(&mut self, other: Self) {
        *self = self.combine(other);
    }
}
