//! Graph implementation for a standard graph without additional structure, see [`GenericGraph`]

use super::field::{Field as FieldTrait, FieldValue};
use super::spacetime_graph::SpacetimeGraph;
use super::{
    FieldGraph, Graph, IterSizedGraph, IterableGraph, SampleGraph, SampleNeighbours, SizedGraph,
};
use crate::collections::{Label, LabelledArray};
use std::cmp::Ordering;
use std::iter;
use std::{iter::Copied, slice::Iter};

/// Structure representing a node in a general graph, containing the labels
/// of the neighbouring nodes in clockwise-order.
#[derive(Debug, Clone)]
pub struct Node {
    neighbours: Vec<Label<Node>>,
}

impl Node {
    /// Create a new [`Node`] from a [`Vec`] of [Label]s
    pub fn new(neighbours: Vec<Label<Node>>) -> Self {
        Self { neighbours }
    }

    /// Return the neighbour of the [`Node`] connected via the link labelled by `link`
    #[inline]
    pub fn get_neighbour(&self, link: usize) -> Option<Label<Node>> {
        self.neighbours.get(link).copied()
    }

    /// Return the vertex degree or the number of (not necessarily unique) neighbours of a node.
    #[inline]
    pub fn vertex_degree(&self) -> usize {
        self.neighbours.len()
    }
}

/// Struct representing a standard, generic graph.
///
/// This graph is simply a list of nodes which each have a list of their neighbours, no
/// additional structure.
#[derive(Debug, Clone)]
pub struct GenericGraph {
    nodes: LabelledArray<Node>,
}

impl Graph for GenericGraph {
    type Label = Label<Node>;

    #[inline]
    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.nodes[label].vertex_degree()
    }
}

impl SizedGraph for GenericGraph {
    fn len(&self) -> usize {
        self.nodes.len()
    }

    fn is_empty(&self) -> bool {
        self.nodes.is_empty()
    }
}

impl SampleGraph for GenericGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        self.nodes.sample_label(rng)
    }
}

impl SampleNeighbours for GenericGraph {
    fn sample_neighbours<R: rand::Rng + ?Sized>(
        &self,
        label: Self::Label,
        rng: &mut R,
    ) -> Self::Label {
        let nbrs = &self.nodes[label].neighbours[..];
        let i = rng.gen_range(0..nbrs.len());
        nbrs[i]
    }
}

type IterNeighbours<'a, L> = Copied<Iter<'a, L>>;

impl<'a> IterableGraph<'a, IterNeighbours<'a, <Self as Graph>::Label>> for GenericGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighbours<'a, <Self as Graph>::Label> {
        self.nodes[label].neighbours.iter().copied()
    }
}

/// The [`Field`](super::field::Field) implementation for [`GenericGraph`]
#[derive(Debug, Clone)]
pub struct Field<F>(pub LabelledArray<FieldValue<F>, Label<Node>>);

impl<F> FieldTrait<F, GenericGraph> for Field<F> {
    #[inline]
    fn get(&self, label: Label<Node>) -> &FieldValue<F> {
        &self.0[label]
    }

    #[inline]
    fn get_mut(&mut self, label: Label<Node>) -> &mut FieldValue<F> {
        &mut self.0[label]
    }
}

impl FieldGraph for GenericGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        Field(LabelledArray::fill_with(|| FieldValue::None, self.len()))
    }
}

impl GenericGraph {
    /// Create a new [`GenericGraph`] from a [`LabelledArray`] of [Node]s
    pub fn new(nodes: LabelledArray<Node>) -> Self {
        Self { nodes }
    }

    /// Create a [`GenericGraph`] from  a [`SpacetimeGraph`], removing the additional
    /// spacetime structure
    pub fn from_spacetime_graph(graph: &SpacetimeGraph) -> Self {
        let nodes = graph.get_nodes().map(|node| Node {
            neighbours: iter::once_with(|| node.left)
                .chain(node.up.iter().copied())
                .chain(iter::once_with(|| node.right))
                .chain(node.down.iter().copied())
                .map(|label| label.convert())
                .collect(),
        });
        Self { nodes }
    }

    /// Return a reference to the underlying [`LabelledArray`] of [Node]s of the graph
    pub fn get_nodes(&self) -> &LabelledArray<Node> {
        &self.nodes
    }
}

/// Plain iterator over all [Node]s in the [`GenericGraph`] in the order in which the [`Node`]s
/// are stored internally.
///
/// The order of this iterator has no physical meaning and can just be considered an
/// arbitrary order. Use this iterator to iterate over all nodes in the fastest way possible.
///
/// This is the iterator returned by the [`GenericGraph`] implementation of [`IterSizedGraph`]
pub struct BaseIterator {
    current: Label<Node>,
    max: Label<Node>,
}

impl Iterator for BaseIterator {
    type Item = Label<Node>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current.cmp(&self.max) {
            Ordering::Less => (),
            Ordering::Equal => return None,
            Ordering::Greater => panic!("Somehow the iterator went past max"),
        }

        let next_label = self.current.succ();
        Some(std::mem::replace(&mut self.current, next_label))
    }
}

impl IterSizedGraph<BaseIterator> for GenericGraph {
    fn iter(&self) -> BaseIterator {
        BaseIterator {
            current: Label::initial(),
            max: Label::from(self.len()),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{error::Error, io::Cursor};

    use crate::graph::breadth_first_search::traverse_nodes::{
        TraverseNodeDistance, TraverseNodeLabel,
    };
    use crate::io::graph;
    use crate::observables::vertex_degree_distribution;
    use ndarray::prelude::*;

    use super::*;
    use indoc::indoc;

    const EXAMPLE_GENERIC_GRAPH: &str = indoc! {"
        8, 5, 9, 1, 2, 10
        0, 9, 2, 4, 3, 6, 5, 2
        9, 10, 0, 1, 5, 4, 1
        1, 4, 7, 6
        1, 2, 5, 8, 10, 7, 3
        2, 1, 6, 9, 0, 8, 4
        1, 3, 7, 9, 5
        3, 4, 10, 9, 6
        4, 5, 0, 10
        6, 7, 10, 2, 1, 0, 5
        7, 4, 8, 0, 2, 9
    "};
    const EXPECTED_VERTEX_DEGREE: [usize; 11] = [6, 8, 7, 4, 7, 7, 5, 5, 4, 7, 6];

    #[test]
    fn check_example_graph() {
        let graph = parse_graph().unwrap();
        let vdegree = vertex_degree_distribution::field_from_graph(&graph);
        assert_eq!(vdegree, arr1(&EXPECTED_VERTEX_DEGREE));
        check_graph_undirectedness(&graph);
    }

    fn check_graph_undirectedness(graph: &GenericGraph) {
        for label in graph.nodes.labels() {
            for nbr in graph.iter_neighbours(label) {
                graph
                    .iter_neighbours(nbr)
                    .find(|&node| node == label)
                    .unwrap_or_else(|| {
                        panic!(
                            "The neighbour {nbr} should have a connection to the original node {label}"
                        )
                    });
            }
        }
    }

    fn parse_graph() -> Result<GenericGraph, Box<dyn Error>> {
        let graph = graph::import_adjacency_list(Cursor::new(EXAMPLE_GENERIC_GRAPH))
            .map(GenericGraph::new)?;
        Ok(graph)
    }

    #[test]
    fn bfs_plain() {
        let graph = parse_graph().unwrap();
        let origin = Label::from(5);

        // Display search results
        println!("Plain breadth first search");
        for node in graph.iter_breadth_first::<TraverseNodeLabel<_>>(origin) {
            print!("{} ", node.label)
        }
        println!();
        // Assert search
        let expected_search = [5usize, 2, 1, 6, 9, 0, 8, 4, 10, 3, 7];
        assert!(graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .map(|node| usize::from(node.label))
            .eq(expected_search.into_iter()));
    }

    #[test]
    fn bfs_distance() {
        let graph = parse_graph().unwrap();
        let origin = Label::from(4);

        // Display search results
        println!("Distanced breadth first search");
        let mut current_distance = 0;
        print!("{}: ", current_distance);
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
            if node.distance > current_distance {
                println!();
                current_distance = node.distance;
                print!("{}: ", current_distance);
            }
            print!("{} ", node.label)
        }
        println!();

        // Check search results
        let check = [
            (4, 0),
            (1, 1),
            (2, 1),
            (5, 1),
            (8, 1),
            (10, 1),
            (7, 1),
            (3, 1),
            (0, 2),
            (9, 2),
            (6, 2),
        ];
        assert!(graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .map(|node| (usize::from(node.label), node.distance))
            .eq(check.into_iter()));
    }
}
