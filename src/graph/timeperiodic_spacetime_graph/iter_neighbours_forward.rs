use super::*;

pub struct IterNeighboursForward<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<TimeBoundary>>,
    patch: i8,
    cursor: LinkLabel,
}

impl<'a> IterNeighboursForward<'a> {
    /// Create new iterator, starting at Left going around clockwise stopping at Right.
    ///
    /// Thus this iterator only iterators over non-backward pointing neighbouring nodes
    pub fn new(node: &'a Node, bound: Option<&'a NodeLinks<TimeBoundary>>, patch: i8) -> Self {
        IterNeighboursForward {
            node,
            bound,
            patch,
            cursor: LinkLabel::Left,
        }
    }

    pub fn from_graph(graph: &'a TimePeriodicSpacetimeGraph, label: TimePeriodicLabel) -> Self {
        Self::new(
            &graph.nodes[label.label],
            graph.bounds[label.label].as_ref(),
            label.patch,
        )
    }

    fn get_bound(&self, link: LinkLabel) -> TimeBoundary {
        if let Some(bounds) = self.bound {
            *bounds
                .get_neighbour(link)
                .expect("Boundary should be defined for each existing link on boundary node.")
        } else {
            TimeBoundary::Center
        }
    }
}

impl<'a> Iterator for IterNeighboursForward<'a> {
    type Item = TimePeriodicLabel;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = self.node.get_neighbour(self.cursor);
        let (new_cursor, bound) = match self.cursor {
            LinkLabel::Left => (LinkLabel::Up(0), self.get_bound(self.cursor)),
            LinkLabel::Up(i) => {
                if result.is_some() {
                    (LinkLabel::Up(i + 1), self.get_bound(self.cursor))
                } else {
                    result = self.node.get_neighbour(LinkLabel::Right);
                    (LinkLabel::Down(0), self.get_bound(LinkLabel::Right))
                }
            }
            LinkLabel::Right => panic!("Right should be skipped by construction."),
            LinkLabel::Down(_) => return None,
        };
        self.cursor = new_cursor;
        result.map(|label| {
            let patch = match bound {
                TimeBoundary::Center => self.patch,
                TimeBoundary::Forward => self.patch + 1,
                TimeBoundary::Backward => self.patch - 1,
            };
            TimePeriodicLabel { label, patch }
        })
    }
}
