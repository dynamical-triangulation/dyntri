//! Module describing the type specific label and array indexed by this label

use super::Label;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};
use std::{iter, slice};

use rand::Rng;

/// Structure for storing a constant size list of items
/// indexed by [`Label`](crate::collections::Label).
/// This is designed to be used as a constant size variant of
/// [`LabelledList`](crate::collections::LabelledList)
///
/// This is simply implemented as an abstraction over a boxed array
#[derive(Clone)]
pub struct LabelledArray<T, L: ?Sized = Label<T>>(Box<[T]>, PhantomData<L>);

impl<T: Debug, L> Debug for LabelledArray<T, L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl<T, L: Into<usize>> Index<L> for LabelledArray<T, L> {
    type Output = T;
    fn index(&self, index: L) -> &Self::Output {
        &self.0[index.into()]
    }
}

impl<T> Index<Label<T>> for LabelledArray<Option<T>> {
    type Output = Option<T>;
    fn index(&self, index: Label<T>) -> &Self::Output {
        &self.0[usize::from(index)]
    }
}

impl<T, L: Into<usize>> IndexMut<L> for LabelledArray<T, L> {
    fn index_mut(&mut self, index: L) -> &mut Self::Output {
        &mut self.0[index.into()]
    }
}

impl<T> IndexMut<Label<T>> for LabelledArray<Option<T>> {
    fn index_mut(&mut self, index: Label<T>) -> &mut Self::Output {
        &mut self.0[usize::from(index)]
    }
}

impl<T, L> From<Vec<T>> for LabelledArray<T, L> {
    fn from(list: Vec<T>) -> Self {
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L> LabelledArray<T, L> {
    /// Returns length of the array
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether the [[LabelledArray]] is empty,
    /// e.g. contains no elements
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns iterator over the items in the [`LabelledArray`]
    pub fn iter(&self) -> Iter<T> {
        Iter {
            iter: self.0.iter(),
        }
    }
}

impl<T, L: Into<usize>> LabelledArray<T, L> {
    /// Returns whether the array has an item at the given `label`
    pub fn has(&self, label: L) -> bool {
        self.len() > label.into()
    }

    /// Returns `Some(&T)` it the label is present in the array,
    /// else return `None`
    pub fn get(&self, label: L) -> Option<&T> {
        self.0.get(label.into())
    }

    /// Returns `Some(&mut T)` it the label is present in the array,
    /// else return `None`
    pub fn get_mut(&mut self, label: L) -> Option<&mut T> {
        self.0.get_mut(label.into())
    }
}

impl<T, L: From<usize>> LabelledArray<T, L> {
    /// Returns the last label in the [LabelledArray]
    pub fn last_label(&self) -> L {
        (self.len() - 1).into()
    }

    /// Returns iterator of the `Label`s in the array.
    pub fn labels(&self) -> impl Iterator<Item = L> {
        (0..self.len()).map(|i| i.into())
    }

    /// Returns iterator over the tuples `(label, item)` in the [`LabelledArray`]
    pub fn enumerate(&self) -> impl Iterator<Item = (L, &T)> {
        self.iter().enumerate().map(|(i, item)| (i.into(), item))
    }

    /// Map `LabelledArray` to other `LabelledArray`
    pub fn map<U, F, M>(&self, f: F) -> LabelledArray<U, M>
    where
        F: FnMut(&T) -> U,
    {
        let list: Vec<U> = self.0.iter().map(f).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }

    /// Map `LabelledArray` to other `LabelledArray`, consuming the original
    pub fn map_into<U, F, M>(self, f: F) -> LabelledArray<U, M>
    where
        F: FnMut(T) -> U,
    {
        let list: Vec<U> = self.0.into_vec().into_iter().map(f).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L: From<usize> + Into<usize>> LabelledArray<T, L> {
    /// Sample an item from the [`LabelledArray`]
    pub fn sample<'a, R: Rng + ?Sized>(&'a self, rng: &mut R) -> &'a T {
        &self[self.sample_label(rng)]
    }

    /// Sample a label present in the [`LabelledArray`]
    pub fn sample_label<R: Rng + ?Sized>(&self, rng: &mut R) -> L {
        L::from(rng.gen_range(0..self.0.len()))
    }
}

impl<T, L> LabelledArray<T, L> {
    /// Create empty `LabelledArray`
    pub fn empty() -> Self {
        LabelledArray(Box::new([]), PhantomData)
    }

    /// Create new `LabelledArray` filled with `filler`
    pub fn fill_with(filler: impl FnMut() -> T, len: usize) -> Self {
        let list: Vec<T> = iter::repeat_with(filler).take(len).collect();
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T: num_traits::Zero + Clone, L> LabelledArray<T, L> {
    /// Create new `LabelledArray` filled with 0
    pub fn zeros(len: usize) -> Self {
        Self::fill(T::zero(), len)
    }
}

impl<T: num_traits::One + Clone, L> LabelledArray<T, L> {
    /// Create new `LabelledArray` filled with 1
    pub fn ones(len: usize) -> Self {
        Self::fill(T::one(), len)
    }
}

impl<T: Clone, L> LabelledArray<T, L> {
    /// Create new `LabelledArray` filled with `filler`
    pub fn fill(filler: T, len: usize) -> Self {
        let list: Vec<T> = vec![filler; len];
        LabelledArray(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L> FromIterator<T> for LabelledArray<T, L> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let list: Vec<T> = iter.into_iter().collect();
        Self(list.into_boxed_slice(), PhantomData)
    }
}

impl<T, L> LabelledArray<T, L> {
    /// Convert [`LabelledArray`] into a boxed slice
    pub fn into_boxed_slice(self) -> Box<[T]> {
        self.0
    }

    /// Convert [`LabelledArray`] into a [Vec]
    pub fn into_vec(self) -> Vec<T> {
        self.0.into_vec()
    }
}

/// Iterator over the [`LabelledArray`]
pub struct Iter<'a, T: 'a> {
    iter: slice::Iter<'a, T>,
}

impl<'a, T: 'a> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_array() {
        let test = vec![1, 2, 3, 4, 5];
        let array: LabelledArray<_> = test.into();
        println!("{:?}", array.0)
    }

    #[test]
    fn option_array() {
        let mut array: LabelledArray<Option<f64>> = LabelledArray::fill_with(|| None, 50);
        for label in array.labels() {
            array[label] = Some(2.3)
        }
        let _ = array.map_into::<_, _, Label<Option<f64>>>(|item| item.unwrap());
    }
}
