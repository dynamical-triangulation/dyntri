//! Generic functions for the mapping of signed and unsigned labels used in the implementation
//! of the graph data structures.

/// Map of `i8` to `usize` in such a way that:
/// (0, -1, 1, -2, 2, 3, ...) -> (0, 1, 2, 3, 4, 5, 6, ...)
pub fn i8usize_central_mapping(int: i8) -> usize {
    let abs_int = int.unsigned_abs();
    if int.is_negative() {
        2 * (abs_int as usize) - 1
    } else {
        2 * (abs_int as usize)
    }
}

/// Map of `usize` to `i8` in such a way that:
/// (0, 1, 2, 3, 4, 5, 6, ...) -> (0, -1, 1, -2, 2, 3, ...)
/// This is the inverse of [`i8usize_central_mapping()`]
pub fn usizei8_central_mapping(uint: usize) -> i8 {
    if uint % 2 == 0 {
        (uint / 2) as i8
    } else {
        -(((uint + 1) / 2) as i8)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn assert_mapping() {
        assert_eq!(i8usize_central_mapping(0), 0);
        assert_eq!(i8usize_central_mapping(-1), 1);
        assert_eq!(i8usize_central_mapping(1), 2);
        assert_eq!(i8usize_central_mapping(-7), 13);
        assert_eq!(i8usize_central_mapping(7), 14);
        assert_eq!(i8usize_central_mapping(127), 254);
        assert_eq!(i8usize_central_mapping(-128), 255);
    }
}
