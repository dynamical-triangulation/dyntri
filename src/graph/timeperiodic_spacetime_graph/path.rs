use super::{iter_neighbours_forward::IterNeighboursForward, *};
use std::collections::VecDeque;
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct Path<L>(Vec<PathNode<L>>);

#[derive(Debug, Clone, Copy)]
pub enum PathNode<L> {
    Start {
        node: L,
        out_link: LinkLabel,
    },
    Middle {
        node: L,
        in_link: LinkLabel,
        out_link: LinkLabel,
    },
    End {
        node: L,
        in_link: LinkLabel,
    },
    Single {
        #[allow(dead_code)]
        node: L,
    },
}

#[derive(Debug, Clone)]
pub struct ClosedPath<L>(Vec<ClosedPathNode<L>>);

#[derive(Debug, Clone, Copy)]
pub struct ClosedPathNode<L> {
    pub label: L,
    pub in_link: LinkLabel,
    pub out_link: LinkLabel,
}

struct ForwardBfs<'a, G>
where
    G: FieldGraph,
{
    graph: &'a G,
    to_visit: VecDeque<G::Label>,
    explored: G::FieldType<Option<G::Label>>,
}

#[derive(Debug, Error, Clone, Copy)]
pub enum ClosedPathFromPathError {
    #[error("{:}", .0)]
    MiddleError(#[from] PathNodeToClosedError),
    #[error("The path does not start with Start")]
    StartError,
    #[error("The path does not start with End")]
    EndError,
    #[error("The path is not closed")]
    NotClosed,
}

impl ClosedPath<Label<Node>> {
    pub fn from_path(path: &Path<TimePeriodicLabel>) -> Result<Self, ClosedPathFromPathError> {
        let PathNode::Start {
            node: start_node,
            out_link,
        } = path.start()
        else {
            return Err(ClosedPathFromPathError::StartError);
        };
        let PathNode::End {
            node: end_node,
            in_link,
        } = path.end()
        else {
            return Err(ClosedPathFromPathError::EndError);
        };
        if start_node.label != end_node.label {
            return Err(ClosedPathFromPathError::NotClosed);
        }

        let mut closed_path: Vec<ClosedPathNode<Label<Node>>> = path.0[1..path.0.len() - 1]
            .iter()
            .map(|&elem| ClosedPathNode::try_from(elem))
            .collect::<Result<_, _>>()?;
        closed_path.push(ClosedPathNode {
            label: start_node.label,
            in_link,
            out_link,
        });
        Ok(Self(closed_path))
    }

    pub fn iter(&self) -> std::slice::Iter<ClosedPathNode<Label<Node>>> {
        self.0.iter()
    }
}

#[derive(Debug, Clone, Copy, Error)]
#[error("ClosedPathNode can only be created from Middle PathNode pieces")]
pub struct PathNodeToClosedError;

impl TryFrom<PathNode<TimePeriodicLabel>> for ClosedPathNode<Label<Node>> {
    type Error = PathNodeToClosedError;

    fn try_from(value: PathNode<TimePeriodicLabel>) -> Result<Self, Self::Error> {
        let PathNode::Middle {
            node,
            in_link,
            out_link,
        } = value
        else {
            return Err(PathNodeToClosedError);
        };
        Ok(ClosedPathNode {
            label: node.label,
            in_link,
            out_link,
        })
    }
}

impl<'a, G: FieldGraph + 'a> ForwardBfs<'a, G> {
    pub fn new(graph: &'a G, origin: G::Label) -> Self {
        // OPTIMIZABLE: Pre-allocate space with some good estimation
        let mut to_visit = VecDeque::new();
        let mut explored = graph.new_field();
        to_visit.push_back(origin);
        explored.get_or_insert(origin, None);
        Self {
            graph,
            to_visit,
            explored,
        }
    }
}

impl<'a> Iterator for ForwardBfs<'a, TimePeriodicSpacetimeGraph> {
    type Item = TimePeriodicLabel;

    fn next(&mut self) -> Option<Self::Item> {
        // Get next node with info to visit
        let label = self.to_visit.pop_front()?;
        // Find all new neighbours and add them to list `to_vist`
        self.to_visit.extend(
            IterNeighboursForward::from_graph(self.graph, label)
                // Filter out the neighbours that are already explored
                .filter(|&nbr| {
                    // Add neighbour to `explored` list and check whether
                    // it was previously unexplored
                    self.explored.set(nbr, Some(label))
                }),
        );

        Some(label)
    }
}

impl<L: Copy> Path<L> {
    pub fn start(&self) -> PathNode<L> {
        *self
            .0
            .first()
            .expect("Path has at least a single element by construction")
    }

    pub fn end(&self) -> PathNode<L> {
        *self
            .0
            .last()
            .expect("Path has at least a single element by construction")
    }
}

pub fn shortest_forward_path(
    graph: &TimePeriodicSpacetimeGraph,
    start_label: TimePeriodicLabel,
    end_label: TimePeriodicLabel,
) -> Path<TimePeriodicLabel> {
    // Search all paths
    let mut bfs = ForwardBfs::new(graph, start_label);
    for node in &mut bfs {
        if node == end_label {
            break;
        }
    }
    // Reconstruct path backwards from the end point
    let mut path: Vec<PathNode<TimePeriodicLabel>> = Vec::new();
    let nodes = bfs.explored;
    // First add the end point
    let Some(previous_label) = nodes
        .get(end_label)
        .expect("The path should be fully explored to get to the end")
    else {
        // If the end label has no predecessor, and it thus the start label
        // just return a path of 0 length
        return Path(vec![PathNode::Single { node: start_label }]);
    };
    path.push(PathNode::End {
        node: end_label,
        in_link: get_link(graph, end_label.label, previous_label.label)
            .expect("These labels should be connected by construction"),
    });
    // Next add the middle points
    let mut current_label = previous_label;
    let mut next_label = end_label;
    loop {
        let Some(previous_label) = nodes
            .get(current_label)
            .expect("The path should be fully explored to get to the current label")
        else {
            // Break if None, meaning the initial point
            break;
        };
        path.push(PathNode::Middle {
            node: current_label,
            in_link: get_link(graph, current_label.label, previous_label.label)
                .expect("These labels should be connected by construction"),
            out_link: get_link(graph, current_label.label, next_label.label)
                .expect("These labels should be connected by construction"),
        });

        next_label = current_label;
        current_label = previous_label;
    }
    // Add the initial point
    path.push(PathNode::Start {
        node: current_label,
        out_link: get_link(graph, current_label.label, next_label.label)
            .expect("These labels should be connected by construction"),
    });

    // Reverse path to go from start_label to end_label
    path.reverse();
    Path(path)
}

#[allow(clippy::manual_map)]
fn get_link(
    graph: &TimePeriodicSpacetimeGraph,
    from: Label<Node>,
    to: Label<Node>,
) -> Option<LinkLabel> {
    let node = &graph.nodes[from];
    if node.left == to {
        Some(LinkLabel::Left)
    } else if node.right == to {
        Some(LinkLabel::Right)
    } else if let Some(i) = node.up.iter().position(|&label| label == to) {
        Some(LinkLabel::Up(i))
    } else if let Some(i) = node.down.iter().position(|&label| label == to) {
        Some(LinkLabel::Down(i))
    } else {
        None
    }
}
