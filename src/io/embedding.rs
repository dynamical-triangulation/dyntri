//! Custom format to store graph with embedding and additional decorations.
//!
//! To use the format create an [`Embedding`] and call [`Embedding::write`]
//!
//!
//! The format consists of 3 initial lines denoting the number of: _vertices_, _edges_,
//! and _triangles_ respectively.
//! These first lines are followed by the vertices which have 2 coordinates, an
//! additional time coordinate, and an optional field value.
//! Then the edges with the indices of the start and end vertices follow, with the type of the edge
//! marked by an [`EdgeKind`] 0 or 1.
//! Then the triangles with the indices of the three vertices in clockwise order, the label of the
//! triangle itself, and an optional field value of the triangle.
//!
//! Note that all the elements are space-separated.
//! An example with a field on the vertices:
//! ```csv
//! 3
//! 3
//! 1
//! 0.0 0.0 3.125
//! 1.0 0.0 2.334
//! 0.0 1.0 6.234
//! 0 1 0
//! 1 2 1
//! 2 0 1
//! 0 1 2 0
//! ```

use std::{fmt::Display, io::Write};

/// Struct storing all simplices of the embedding
#[derive(Debug, Clone)]
#[allow(missing_docs)]
pub struct Embedding {
    pub vertices: Vec<Vertex>,
    pub edges: Vec<Edge>,
    pub triangles: Vec<Triangle>,
}

/// Struct storing the [`Embedding`] along with a field on the vertices
#[derive(Debug, Clone)]
pub struct EmbeddingField<T> {
    /// Underlying embedding
    pub embedding: Embedding,
    /// Field values on the vertices
    ///
    /// Should have exactly as many elements as the vertices of the embedding
    pub field: Vec<T>,
}

/// Struct storing the [`Embedding`] along with a field on the triangles
#[derive(Debug, Clone)]
pub struct EmbeddingDualField<T> {
    /// Underlying embedding
    pub embedding: Embedding,
    /// Field values on the triangles
    ///
    /// Should have exactly as many elements as the triangles of the embedding
    pub field: Vec<T>,
}

/// Vertex of triangulation in 2D embedding with associated time coordinate
#[derive(Debug, Clone)]
pub struct Vertex {
    /// 2D vertex coordinate
    pub coordinate: (f64, f64),
    /// Time associated with vertex in CDT triangulatio
    pub time: i32,
}

/// Edge of triangulation in 2D embedding of spacelike or timelike type
#[derive(Debug, Clone)]
pub struct Edge {
    /// Label of starting vertex
    pub start: usize,
    /// Label of final vertex
    pub end: usize,
    /// Type of edge: spacelike or timelike
    pub kind: EdgeKind,
}

/// Struct representing a triangle in the embedding
/// Designed to hold the vertex labels of the triangle in clockwise order
#[derive(Debug, Clone)]
pub struct Triangle {
    /// Labels of the vertices in clockwise order
    pub vertices: (usize, usize, usize),
    /// Label of the triangle itself as it is used in the simulation
    pub label: usize,
}

/// Type of edge in CDT triangulation: spacelike or timelike
#[derive(Debug, Clone, Copy)]
pub enum EdgeKind {
    /// Spacelike means the edge connects two vertices with equal time
    Spacelike = 0,
    /// Timelike means the edge connects two vertices with times differing by exactly 1
    Timelike = 1,
}

impl Display for EdgeKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EdgeKind::Spacelike => write!(f, "0"),
            EdgeKind::Timelike => write!(f, "1"),
        }
    }
}

impl Embedding {
    /// Write the embedding to a text buffer to export the embedding to a file for example.
    ///
    /// For the used format read the module docs [`embedding`](super::embedding)
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.vertices.len())?;
        writeln!(buffer, "{}", self.edges.len())?;
        writeln!(buffer, "{}", self.triangles.len())?;

        for vertex in self.vertices.iter() {
            writeln!(
                buffer,
                "{} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.time
            )?;
        }

        for edge in self.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for triangle in self.triangles.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                triangle.vertices.0, triangle.vertices.1, triangle.vertices.2, triangle.label
            )?;
        }

        Ok(())
    }
}

impl<T: Display> EmbeddingField<T> {
    /// Write the embedding to a text buffer to export the embedding to a file for example.
    ///
    /// For the used format read the module docs [`embedding`](super::embedding)
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.embedding.vertices.len())?;
        writeln!(buffer, "{}", self.embedding.edges.len())?;
        writeln!(buffer, "{}", self.embedding.triangles.len())?;

        for (vertex, value) in self.embedding.vertices.iter().zip(self.field.iter()) {
            writeln!(
                buffer,
                "{} {} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.time, value
            )?;
        }

        for edge in self.embedding.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for triangle in self.embedding.triangles.iter() {
            writeln!(
                buffer,
                "{} {} {} {}",
                triangle.vertices.0, triangle.vertices.1, triangle.vertices.2, triangle.label
            )?;
        }

        Ok(())
    }
}

impl<T: Display> EmbeddingDualField<T> {
    /// Write the embedding to a text buffer to export the embedding to a file for example.
    ///
    /// For the used format read the module docs [`embedding`](super::embedding)
    pub fn write<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        writeln!(buffer, "{}", self.embedding.vertices.len())?;
        writeln!(buffer, "{}", self.embedding.edges.len())?;
        writeln!(buffer, "{}", self.embedding.triangles.len())?;

        for vertex in self.embedding.vertices.iter() {
            writeln!(
                buffer,
                "{} {} {}",
                vertex.coordinate.0, vertex.coordinate.1, vertex.time
            )?;
        }

        for edge in self.embedding.edges.iter() {
            writeln!(buffer, "{} {} {}", edge.start, edge.end, edge.kind)?;
        }

        for (i, triangle) in self.embedding.triangles.iter().enumerate() {
            let value = self
                .field
                .get(i)
                .expect("Not all triangle field values were set");
            writeln!(
                buffer,
                "{} {} {} {} {}",
                triangle.vertices.0,
                triangle.vertices.1,
                triangle.vertices.2,
                triangle.label,
                value
            )?;
        }

        Ok(())
    }
}
