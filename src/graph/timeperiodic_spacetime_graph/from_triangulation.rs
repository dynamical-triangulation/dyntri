use super::{BoundaryList, NodeLinks, TimeBoundary, TimePeriodicSpacetimeGraph};
use crate::{
    collections::{Label, LabelledArray},
    graph::spacetime_graph::Node,
    triangulation::{
        full_triangulation::{triangle::Triangle, vertex::Vertex},
        FullTriangulation,
    },
};

/// Timeslice to use for construction of the [TimePeriodicSpacetimeGraph],
/// this is completely arbitrary and only changes where the cut is made.
const CUTTING_TIMESLICE: usize = 0;

impl TimePeriodicSpacetimeGraph {
    /// Construct a [`TimePeriodicSpacetimeGraph`] representing the vertex
    /// connections of the [`FullTriangulation`]
    ///
    /// The [`TimePeriodicSpacetimeGraph`] represents the triangulation by cutting
    /// the triangulation open in time, and stacking it on top of oneother,
    /// creating an infinitly repeating graph in the time direction.
    ///
    /// Note that the `usize` numbers the labels correspond to are the same between the vertices
    /// of the [`FullTriangulation`] and the nodes of the resulting graph
    pub fn from_triangulation_vertex(triangulation: &FullTriangulation) -> Self {
        construct_stacked_vertex_graph(triangulation)
    }

    /// Construct a [`TimePeriodicSpacetimeGraph`] representing the triangle
    /// connections of the [`FullTriangulation`]
    ///
    /// The [`TimePeriodicSpacetimeGraph`] represents the triangulation by cutting
    /// the triangulation open in time, and stacking it on top of oneother,
    /// creating an infinitly repeating graph in the time direction.
    pub fn from_triangulation_dual(triangulation: &FullTriangulation) -> Self {
        construct_stacked_dual_graph(triangulation)
    }

    pub fn from_triangulation_vertex_cut_at(
        triangulation: &FullTriangulation,
        cutting_label: Label<Vertex>,
    ) -> Self {
        let timeslice = triangulation.get_vertex(cutting_label).time();
        construct_stacked_vertex_graph_cut_at(triangulation, timeslice)
    }

    pub fn from_triangulation_dual_cut_at(
        triangulation: &FullTriangulation,
        cutting_label: Label<Triangle>,
    ) -> Self {
        let timeslice = triangulation.get_triangle(cutting_label).time();
        construct_stacked_vertex_graph_cut_at(triangulation, timeslice)
    }
}

fn construct_stacked_vertex_graph(triangulation: &FullTriangulation) -> TimePeriodicSpacetimeGraph {
    construct_stacked_vertex_graph_cut_at(triangulation, CUTTING_TIMESLICE)
}

fn construct_stacked_dual_graph(triangulation: &FullTriangulation) -> TimePeriodicSpacetimeGraph {
    construct_stacked_dual_graph_cut_at(triangulation, CUTTING_TIMESLICE)
}

fn construct_stacked_vertex_graph_cut_at(
    triangulation: &FullTriangulation,
    cutting_timeslice: usize,
) -> TimePeriodicSpacetimeGraph {
    let nodes: LabelledArray<Node> = triangulation.vertices.map(|vertex| vertex.as_node());
    let t_max = triangulation.time_len();

    let mut bounds: BoundaryList = LabelledArray::fill_with(|| None, triangulation.vertex_count());
    // Set the top boundary at the cutting timeslice (so we are cutting above the slice)
    for vertex_label in triangulation.iter_time(cutting_timeslice % t_max) {
        let node_label: Label<Node> = vertex_label.convert();
        let up_bounds: Box<[TimeBoundary]> = std::iter::repeat(TimeBoundary::Forward)
            .take(nodes[node_label].up.len())
            .collect();
        let down_bounds: Box<[TimeBoundary]> = std::iter::repeat(TimeBoundary::Center)
            .take(nodes[node_label].down.len())
            .collect();
        let boundary_links = NodeLinks {
            left: TimeBoundary::Center,
            up: up_bounds,
            right: TimeBoundary::Center,
            down: down_bounds,
        };
        bounds[node_label] = Some(boundary_links);
    }
    // Set the bottom boundary at the next timeslice
    for vertex_label in triangulation.iter_time((cutting_timeslice + 1) % t_max) {
        let node_label: Label<Node> = vertex_label.convert();
        let up_bounds: Box<[TimeBoundary]> = std::iter::repeat(TimeBoundary::Center)
            .take(nodes[node_label].up.len())
            .collect();
        let down_bounds: Box<[TimeBoundary]> = std::iter::repeat(TimeBoundary::Backward)
            .take(nodes[node_label].down.len())
            .collect();
        let boundary_links = NodeLinks {
            left: TimeBoundary::Center,
            up: up_bounds,
            right: TimeBoundary::Center,
            down: down_bounds,
        };
        bounds[node_label] = Some(boundary_links);
    }

    TimePeriodicSpacetimeGraph { nodes, bounds }
}

fn construct_stacked_dual_graph_cut_at(
    triangulation: &FullTriangulation,
    cutting_timeslice: usize,
) -> TimePeriodicSpacetimeGraph {
    // Convert vertices to nodes
    let triangles = &triangulation.triangles;
    let vertices = &triangulation.vertices;
    let nodes: LabelledArray<Node> = triangles.map(|triangle| triangle.as_node());
    let t_max = triangulation.time_len();

    let mut bounds: BoundaryList = LabelledArray::fill_with(|| None, triangulation.size());
    for vertex_label in triangulation.iter_time(cutting_timeslice % t_max) {
        // Set lower boundary on upper part of the triangle shard
        let upper_label = vertices[vertex_label].triangle();
        let backward_bound = NodeLinks {
            left: TimeBoundary::Center,
            up: Box::new([]),
            right: TimeBoundary::Center,
            down: Box::new([TimeBoundary::Backward]),
        };
        bounds[upper_label.convert()] = Some(backward_bound);
        // Set upper boundary on the lower part of the triangle shard
        let lower_label = triangles[upper_label].center;
        let forward_bound = NodeLinks {
            left: TimeBoundary::Center,
            up: Box::new([TimeBoundary::Forward]),
            right: TimeBoundary::Center,
            down: Box::new([]),
        };
        bounds[lower_label.convert()] = Some(forward_bound);
    }

    TimePeriodicSpacetimeGraph { nodes, bounds }
}
