use super::vertex::Vertex;
use crate::collections::Label;
use crate::graph::spacetime_graph;
use crate::triangulation::triangle::Orientation;
use std::fmt::Debug;

/// Struct representing a triangle with it's connectivity,
/// and associated vertices
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Triangle {
    pub orientation: Orientation,
    pub time: usize, // Marks the lower timeslices
    pub left: Label<Triangle>,
    pub center: Label<Triangle>,
    pub right: Label<Triangle>,
    pub left_vertex: Label<Vertex>,
    pub center_vertex: Label<Vertex>,
    pub right_vertex: Label<Vertex>,
}

impl Debug for Triangle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.orientation {
            Orientation::Down => write!(
                f,
                "Triangle{{ {}, t: {}, triangles: {}\\{}/{}, vertices: {}\\{}/{} }}",
                self.orientation,
                self.time,
                self.left,
                self.center,
                self.right,
                self.left_vertex,
                self.center_vertex,
                self.right_vertex
            ),
            Orientation::Up => write!(
                f,
                "Triangle{{ {}, t: {}, triangles: {}/{}\\{}, vertices: {}/{}\\{} }}",
                self.orientation,
                self.time,
                self.left,
                self.center,
                self.right,
                self.left_vertex,
                self.center_vertex,
                self.right_vertex
            ),
        }
    }
}

impl Triangle {
    /// Return the vertex associated with the [`Triangle`]
    ///
    /// This is the left most vertex of the triangle.
    pub fn vertex(&self) -> Label<Vertex> {
        self.left_vertex
    }

    /// Returns the vertices of the [`Triangle`] in counterclockwise order,
    /// starting with the left most vertex.
    ///
    /// Note: counterclockwise is the standard used for 3D graphics to have the
    /// normal vectors pointing outward.
    pub fn vertices(&self) -> (Label<Vertex>, Label<Vertex>, Label<Vertex>) {
        match self.orientation {
            Orientation::Down => (self.left_vertex, self.center_vertex, self.right_vertex),
            Orientation::Up => (self.left_vertex, self.right_vertex, self.center_vertex),
        }
    }

    /// Return the time coordinate of the [`Triangle`]
    ///
    /// This is the same as the time coordinate of the lower vertices
    /// of this [Triangle].
    pub fn time(&self) -> usize {
        self.time
    }
}

impl Triangle {
    pub fn as_node(&self) -> spacetime_graph::Node {
        let left = self.left.convert();
        let right = self.right.convert();
        let center = self.center.convert();
        let mut up = Vec::with_capacity(1);
        let mut down = Vec::with_capacity(1);
        match self.orientation {
            // If pointing up the center neighbour is down, and visa versa
            Orientation::Up => down.push(center),
            Orientation::Down => up.push(center),
        }

        spacetime_graph::Node::new(left, up.into_boxed_slice(), right, down.into_boxed_slice())
    }
}
