use super::*;

pub struct IterNeighbours<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<TimeBoundary>>,
    patch: i8,
    cursor: LinkLabel,
}

impl<'a> IterNeighbours<'a> {
    /// Start iterator at Left going around clockwise
    pub fn new(node: &'a Node, bound: Option<&'a NodeLinks<TimeBoundary>>, patch: i8) -> Self {
        IterNeighbours {
            node,
            bound,
            patch,
            cursor: LinkLabel::Left,
        }
    }

    pub fn from_graph(graph: &'a TimePeriodicSpacetimeGraph, label: TimePeriodicLabel) -> Self {
        Self::new(
            &graph.nodes[label.label],
            graph.bounds[label.label].as_ref(),
            label.patch,
        )
    }

    fn get_bound(&self, link: LinkLabel) -> TimeBoundary {
        if let Some(bounds) = self.bound {
            *bounds
                .get_neighbour(link)
                .expect("Boundary should be defined for each existing link on boundary node.")
        } else {
            TimeBoundary::Center
        }
    }
}

impl<'a> Iterator for IterNeighbours<'a> {
    type Item = TimePeriodicLabel;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = self.node.get_neighbour(self.cursor);
        let (new_cursor, bound) = match self.cursor {
            LinkLabel::Left => (LinkLabel::Up(0), self.get_bound(self.cursor)),
            LinkLabel::Up(i) => {
                if result.is_some() {
                    (LinkLabel::Up(i + 1), self.get_bound(self.cursor))
                } else {
                    result = self.node.get_neighbour(LinkLabel::Right);
                    (LinkLabel::Down(0), self.get_bound(LinkLabel::Right))
                }
            }
            LinkLabel::Right => panic!("Right should be skipped by construction."),
            // If Down just increase, it will stop here in any case after the last
            LinkLabel::Down(i) => {
                result?;
                (LinkLabel::Down(i + 1), self.get_bound(self.cursor))
            }
        };
        self.cursor = new_cursor;
        result.map(|label| {
            let patch = match bound {
                TimeBoundary::Center => self.patch,
                TimeBoundary::Forward => self.patch + 1,
                TimeBoundary::Backward => self.patch - 1,
            };
            TimePeriodicLabel { label, patch }
        })
    }
}
