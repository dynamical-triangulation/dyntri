//! Numerical simulation and observable measurements for 2D CDT (causal dynamical triangulation),
//! using Markov chain Monte Carlo.
//!
//! # Features
//! - Performing a Markov chain Monte Carlo simulation to generate random triangulations of 2D CDT.
//! See module [`monte_carlo`].
//! - Data structures for handeling dynamically and statically sized causal triangulations.
//! See module [`triangulation`].
//! - Working with graphs of different types, which will generally be generated as the vertex or
//! dual graph of a triangulation. Implementing _breadth first search_, repeating periodic graphs,
//! and more features that are useful to triangulations. See [`graph`] module.
//! - Performing measurements on the triangulation or graphs to analyse them. Here most of the
//! common measurements are implemented to determine observables like the Hausdorf and spectral
//! dimension as well as quantum Ricci curvature. All graph measurements are generic over graph
//! types so they can also be performed on graphs that do not come from 2D CDT by importing them
//! from other models. See module [`observables`].
//! - Importing and exporting graphs to allow for sharing graphs with other programs. For example
//! allowing importing of EDT triangulation to measure the implemented observables on.
//! See module [`io`].
//! - Making graph embeddings in 2D and 3D for visualisation of the underlying triangulations.
//! See module [`visualisation`] for more details.
//!
//! # Example
//! A short and simple example of use of the library by generating a small CDT triangulation and
//! measuring the volume profile distribution and average sphere distance on the vertex graph of
//! the triangulation.
//! ```rust
//! use dyntri::graph::{SampleGraph, periodic_spacetime_graph::PeriodicSpacetimeGraph};
//! use dyntri::monte_carlo::fixed_universe::Universe;
//! use dyntri::observables::{average_sphere_distance::double_sphere, volume_profile};
//! use dyntri::triangulation::FullTriangulation;
//! use rand::thread_rng;
//!
//! // Create a new universe of 100 triangles with 10 timeslices
//! let mut universe = Universe::default(100, 10);
//! // Initialize a random number generator of choice
//! let mut rng = thread_rng();
//! // Perform 10_000 Monte Carlo moves on the triangulation
//! for _ in 0..10_000 {
//!     universe.step(&mut rng);
//! }
//! // Create a trianglulation with all connectivity information
//! // Not just the triangle connectivity stored in `universe.triangulation`
//! let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
//!
//! // Measure the volume profile of the triangulation
//! let volume_profile = volume_profile::measure(&triangulation);
//! println!("Volume profile: {:?}", volume_profile.volumes());
//!
//! // Create the (cut-open) periodic vertex graph of the triangulation
//! let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
//! // Randomly select a label to use as an origin
//! let label = graph.sample_node(&mut rng);
//! // Measure the average sphere distance with delta = 3
//! let delta = 3;
//! let asd = double_sphere::measure(&graph, label, delta, &mut rng);
//! println!("Average sphere distance (delta = {delta}) is {asd}");
//! ```
//!
//!
//! # Collections
//! Note that some clever data structures are used to effectively store a dynamically sized
//! triangulation. These data structures could be useful for other applications outside CDT.
//! If you are interested see the [`collections`] module.

#![allow(clippy::uninlined_format_args)]
// #![warn(missing_docs)]

pub mod collections;
pub mod graph;
pub mod io;
pub mod observables;
pub mod triangulation;
pub mod visualisation;

#[allow(dead_code)]
pub mod monte_carlo;

#[cfg(test)]
mod tests {
    use crate::graph::periodic_spacetime_graph::PeriodicSpacetimeGraph;
    use crate::graph::SampleGraph;
    use crate::monte_carlo::fixed_universe::Universe;
    use crate::observables::average_sphere_distance::double_sphere;
    use crate::observables::volume_profile;
    use crate::triangulation::FullTriangulation;
    use rand::thread_rng;

    #[test]
    fn test() {
        let mut universe = Universe::default(100, 10);
        let mut rng = thread_rng();
        for _ in 0..10_000 {
            universe.step(&mut rng);
        }
        let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
        let volume_profile = volume_profile::measure(&triangulation);
        println!("Volume profile: {:?}", volume_profile.volumes());

        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
        let label = graph.sample_node(&mut rng);
        let delta = 3;
        let asd = double_sphere::measure(&graph, label, delta, &mut rng);
        println!("Average sphere distance (delta = {delta}) is {asd}");
    }
}
