//! Traits for fields on top of the graph structures defined in [`graph`](crate::graph)

pub use super::periodic_graph::PeriodicField;
use super::FieldGraph;

/// Enum representing the field value of a graph node
///
/// To allow for infinite graphs where no all nodes can be set the field value can also be
/// [`None`](FieldValue::None).
/// This enum is very similar to the Rust default [`Option`]
#[derive(Debug, Clone, Copy)]
pub enum FieldValue<T> {
    /// (Yet) undefined field value
    None,
    /// Defined field value
    Some(T),
}

impl<T> FieldValue<T> {
    /// See [`Option::is_some`]
    pub fn is_some(&self) -> bool {
        match self {
            Self::Some(_) => true,
            Self::None => false,
        }
    }

    /// See [`Option::is_none`]
    pub fn is_none(&self) -> bool {
        match self {
            Self::None => true,
            Self::Some(_) => false,
        }
    }

    /// Convert [`FieldValue`] to [`Option`]
    pub fn some(self) -> Option<T> {
        match self {
            Self::None => None,
            Self::Some(value) => Some(value),
        }
    }

    /// See [`Option::unwrap()`]
    pub fn unwrap(self) -> T {
        if let Self::Some(value) = self {
            value
        } else {
            panic!();
        }
    }

    /// See [`Option::unwrap_or_else()`]
    pub fn unwrap_or_else<F>(self, f: F) -> T
    where
        F: FnOnce() -> T,
    {
        match self {
            FieldValue::None => f(),
            FieldValue::Some(value) => value,
        }
    }

    /// See [`Option::expect()`]
    pub fn expect(self, msg: &str) -> T {
        if let Self::Some(value) = self {
            value
        } else {
            panic!("{}", msg);
        }
    }

    /// See [`Option::as_ref()`]
    pub fn as_ref(&self) -> FieldValue<&T> {
        match self {
            FieldValue::None => FieldValue::None,
            FieldValue::Some(value) => FieldValue::Some(value),
        }
    }

    /// See [`Option::as_mut()`]
    pub fn as_mut(&mut self) -> FieldValue<&mut T> {
        match self {
            FieldValue::None => FieldValue::None,
            FieldValue::Some(value) => FieldValue::Some(value),
        }
    }

    /// Sets the field value to [`FieldValue::Some`] containing `value` if it was
    /// [`FieldValue::None`] and returns `true`. If the field value was already set
    /// nothing is changed and `false` is returned.
    pub fn set(&mut self, value: T) -> bool {
        if self.is_none() {
            *self = Self::Some(value);
            true
        } else {
            false
        }
    }

    /// Takes out the value if the [`FieldValue`] returning the taken value and leaving
    /// [`FieldValue::None`] in its place.
    pub fn take(&mut self) -> FieldValue<T> {
        std::mem::replace(self, FieldValue::None)
    }

    /// See [`Option::get_or_insert()`]
    pub fn get_or_insert(&mut self, value: T) -> &mut T {
        if self.is_none() {
            *self = Self::Some(value);
        }
        self.as_mut().unwrap()
    }

    /// See [`Option::get_or_insert_with()`]
    pub fn get_or_insert_with<F: FnOnce() -> T>(&mut self, f: F) -> &mut T {
        if self.is_none() {
            *self = Self::Some(f())
        }
        self.as_mut().unwrap()
    }

    /// Set [`FieldValue`] to given `value` and return a mutable reference to it
    pub fn insert(&mut self, value: T) -> &mut T {
        *self = FieldValue::Some(value);
        self.as_mut().unwrap()
    }

    /// See [`Option::map()`]
    pub fn map<U, F>(self, f: F) -> FieldValue<U>
    where
        F: FnOnce(T) -> U,
    {
        match self {
            FieldValue::None => FieldValue::None,
            FieldValue::Some(e) => FieldValue::Some(f(e)),
        }
    }
}

impl<T: Copy> FieldValue<&T> {
    /// See [`Option::copied()`]
    pub fn copied(self) -> FieldValue<T> {
        match self {
            FieldValue::None => FieldValue::None,
            FieldValue::Some(&value) => FieldValue::Some(value),
        }
    }
}

impl<T: Clone> FieldValue<&T> {
    /// See [`Option::cloned()`]
    pub fn cloned(self) -> FieldValue<T> {
        match self {
            FieldValue::None => FieldValue::None,
            FieldValue::Some(value) => FieldValue::Some(value.clone()),
        }
    }
}

/// Trait representing a field of type `T` on a graph `G`
pub trait Field<T, G: FieldGraph<FieldType<T> = Self>> {
    /// Get a reference to the [`FieldValue`] at the given `label`
    fn get(&self, label: G::Label) -> &FieldValue<T>;

    /// Get a mutable reference to the [`FieldValue`] at the given `label`
    fn get_mut(&mut self, label: G::Label) -> &mut FieldValue<T>;

    /// Get a mutable reference to the value at the given `label` if it is not [`FieldValue::None`],
    /// in which case `value` is inserted and mutable reference is returned to that value.
    #[inline]
    fn get_or_insert(&mut self, label: G::Label, value: T) -> &mut T {
        self.get_mut(label).get_or_insert(value)
    }

    /// Get a mutable reference to the value at the given `label` if it is not [`FieldValue::None`],
    /// in which case a new value is inserted according to the function `f`, and a mutable
    /// reference is returned to the new value.
    #[inline]
    fn get_or_insert_with<C>(&mut self, label: G::Label, f: C) -> &mut T
    where
        C: FnOnce() -> T,
    {
        self.get_mut(label).get_or_insert_with(f)
    }

    /// Returns if the field value at `label` is already defined
    #[inline]
    fn has(&self, label: G::Label) -> bool {
        self.get(label).is_some()
    }

    /// Sets the field value if it was unset, returning if it was.
    ///
    /// Specifically it sets the field value at `label` to [`FieldValue::Some`]
    /// containing `value` if it was [`FieldValue::None`] and returns `true`.
    /// If the field value was already set nothing is changed and `false` is returned.
    fn set(&mut self, label: G::Label, new_value: T) -> bool {
        let value = self.get_mut(label);
        if value.is_none() {
            *value = FieldValue::Some(new_value);
            return true;
        }
        false
        // let mut unset = false;
        // self.get_or_insert_with(label, || {
        //     unset = true;
        //     value
        // });
        // unset
    }

    /// Takes the field value from the `label` returning the value and leaving
    /// [`FieldValue::None`] in it's place.
    #[inline]
    fn take(&mut self, label: G::Label) -> FieldValue<T> {
        self.get_mut(label).take()
    }
}

/// Generic field implementation wrapper which allows me to return a reference
/// to `FieldValue::None`. This should probably not be public and needs refactoring
// TODO
#[derive(Debug, Clone)]
pub struct FieldCollection<T, C>(pub C, FieldValue<T>);

impl<T, C> FieldCollection<T, C> {
    pub(super) fn new(field: C) -> Self {
        Self(field, FieldValue::None)
    }

    pub(super) fn none(&self) -> &FieldValue<T> {
        &self.1
    }
}
