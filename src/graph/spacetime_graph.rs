//! Implementation of the graph traits for [`SpacetimeGraph`], see that for use details.

use super::field::{Field as FieldTrait, FieldValue};
use super::{FieldGraph, Graph, IterSizedGraph, IterableGraph, SampleGraph, SizedGraph};
use crate::collections::{Label, LabelledArray};
use crate::triangulation::FullTriangulation;
use std::cmp::Ordering;
use std::{error::Error, fmt::Display, str::FromStr};

/// Struct representing a Node in Minkowski type graph,
/// containing all links to other nodes
///
/// This Node is designed for a graph where a local sense of direction is present,
/// in this case a space- and time-direction for a Lorentzian spacetime.
/// Convention is here chosen with `left` and `right` in space,
/// and `up` and `down` as forward and backward in time respectively.
#[derive(Debug, Clone)]
pub struct Node {
    // The links stored in the [Node], should be ordered clockwise;
    // thus the links in `up` are from *left* to *right*; the links
    // in `down` are from *right* to *left*.
    // With this ordering one can easily identify a link connections
    // by origin's `Label<Node>` and some index
    // Note: the up links correspond to time-forward and the down links to
    // time-backward
    pub(super) left: Label<Node>,
    pub(super) up: Box<[Label<Node>]>,
    pub(super) right: Label<Node>,
    pub(super) down: Box<[Label<Node>]>,
}

#[derive(Debug, Clone, Copy)]
pub struct ParseNodeError;

impl Display for ParseNodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Node string could not be parsed, use specified format.")
    }
}

impl Error for ParseNodeError {}

impl FromStr for Node {
    type Err = ParseNodeError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut links = s.split(';');
        let left_str = links.next().ok_or(ParseNodeError)?;
        let left: Label<Node> = Label::from(
            left_str
                .trim()
                .parse::<usize>()
                .map_err(|_| ParseNodeError)?,
        );
        let up_str = links.next().ok_or(ParseNodeError)?;
        let mut up: Vec<Label<Node>> = Vec::new();
        for up_elem in up_str.trim().split(',') {
            up.push(Label::from(
                up_elem
                    .trim()
                    .parse::<usize>()
                    .map_err(|_| ParseNodeError)?,
            ));
        }
        let right_str = links.next().ok_or(ParseNodeError)?;
        let right: Label<Node> = Label::from(
            right_str
                .trim()
                .parse::<usize>()
                .map_err(|_| ParseNodeError)?,
        );
        let down_str = links.next().ok_or(ParseNodeError)?;
        let mut down: Vec<Label<Node>> = Vec::new();
        for down_elem in down_str.trim().split(',') {
            down.push(Label::from(
                down_elem
                    .trim()
                    .parse::<usize>()
                    .map_err(|_| ParseNodeError)?,
            ));
        }

        Ok(Node {
            left,
            up: up.into_boxed_slice(),
            right,
            down: down.into_boxed_slice(),
        })
    }
}

impl Node {
    pub fn new(
        left: Label<Node>,
        up: Box<[Label<Node>]>,
        right: Label<Node>,
        down: Box<[Label<Node>]>,
    ) -> Self {
        Self {
            left,
            up,
            right,
            down,
        }
    }

    pub fn vertex_degree(&self) -> usize {
        self.up.len() + self.down.len() + 2
    }

    /// Return the amount of links in the `up` or forward time direction
    pub fn up_count(&self) -> usize {
        self.up.len()
    }

    /// Return the amount of links in the `down` or backward time direction
    pub fn down_count(&self) -> usize {
        self.down.len()
    }

    /// Return the [`Label`] of the [`Node`] the [`LinkLabel`] is pointing to
    pub fn get_neighbour(&self, link: LinkLabel) -> Option<Label<Node>> {
        match link {
            LinkLabel::Left => Some(self.left),
            LinkLabel::Up(i) => self.up.get(i).copied(),
            LinkLabel::Right => Some(self.right),
            LinkLabel::Down(i) => self.down.get(i).copied(),
        }
    }

    /// Return the [`Label`] of the [`Node`] the [`LinkLabel`] is pointing to
    ///
    /// Will panic if the `link` points to a link that is not part of the connections
    /// of the node, use [`Node::get_neighbour()`] instead for a checked variant.
    pub fn get_neighbour_unchecked(&self, link: LinkLabel) -> Label<Node> {
        match link {
            LinkLabel::Left => self.left,
            LinkLabel::Up(i) => self.up[i],
            LinkLabel::Right => self.right,
            LinkLabel::Down(i) => self.down[i],
        }
    }

    /// Search for the link that points to the neighbour given by `label`
    pub fn get_link_of_neighbour(&self, label: Label<Node>) -> Option<LinkLabel> {
        IterLinks::new(self).find(|&link| self.get_neighbour_unchecked(link) == label)
    }
}

/// Graph structure holding all connectivity information [SpacetimeGraph].
///
/// This Graph is designed to model a triangulation, representing a closed 1+1D
/// spacetime, thus having the topology of a torus. And an indentifyable time-like
/// and space-like direction; with the edges between nodes labelled as such.
///
/// Likely this graph structure is created from a triangulation by calling
/// [`from_triangulation_vertex`](SpacetimeGraph::from_triangulation_vertex) or
/// [`from_triangulation_dual`](SpacetimeGraph::from_triangulation_dual)
#[derive(Debug, Clone)]
pub struct SpacetimeGraph {
    nodes: LabelledArray<Node>,
}

impl SpacetimeGraph {
    pub fn new(nodes: LabelledArray<Node>) -> Self {
        Self { nodes }
    }

    /// Create a new [`SpacetimeGraph`] from a [`FullTriangulation`]
    pub fn from_triangulation_vertex(triangulation: &FullTriangulation) -> Self {
        let nodes = triangulation.vertices.map(|vertex| vertex.as_node());
        Self { nodes }
    }

    pub fn from_triangulation_dual(triangulation: &FullTriangulation) -> Self {
        let nodes = triangulation.triangles.map(|triangle| triangle.as_node());
        Self { nodes }
    }

    pub fn get_nodes(&self) -> &LabelledArray<Node> {
        &self.nodes
    }

    pub fn get_node(&self, label: Label<Node>) -> Option<&Node> {
        self.nodes.get(label)
    }

    pub fn get_node_unchecked(&self, label: Label<Node>) -> &Node {
        &self.nodes[label]
    }
}

impl Graph for SpacetimeGraph {
    type Label = Label<Node>;

    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.nodes[label].vertex_degree()
    }
}

impl SizedGraph for SpacetimeGraph {
    fn is_empty(&self) -> bool {
        self.nodes.is_empty()
    }

    fn len(&self) -> usize {
        self.nodes.len()
    }
}

impl SampleGraph for SpacetimeGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        self.nodes.sample_label(rng)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LinkLabel {
    Left,
    Up(usize),
    Right,
    Down(usize),
}

pub struct IterNeighbours<'a> {
    node: &'a Node,
    cursor: LinkLabel,
}

impl<'a> IterNeighbours<'a> {
    /// Start iterator at Left going around clockwise
    pub fn new(node: &'a Node) -> Self {
        IterNeighbours {
            node,
            cursor: LinkLabel::Left,
        }
    }
}

impl<'a> Iterator for IterNeighbours<'a> {
    type Item = Label<Node>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = self.node.get_neighbour(self.cursor);
        self.cursor = match self.cursor {
            LinkLabel::Left => LinkLabel::Up(0),
            LinkLabel::Up(i) => {
                if result.is_some() {
                    LinkLabel::Up(i + 1)
                } else {
                    result = self.node.get_neighbour(LinkLabel::Right);
                    LinkLabel::Down(0)
                }
            }
            LinkLabel::Right => panic!("Right should be skipped by construction."),
            // If Down just increase, it will stop here in any case after the last
            LinkLabel::Down(i) => LinkLabel::Down(i + 1),
        };
        result
    }
}

impl<'a> IterableGraph<'a, IterNeighbours<'a>> for SpacetimeGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighbours<'a> {
        IterNeighbours::new(&self.nodes[label])
    }
}

pub struct Field<F>(LabelledArray<FieldValue<F>, Label<Node>>);

impl<F> FieldTrait<F, SpacetimeGraph> for Field<F> {
    #[inline]
    fn get(&self, label: Label<Node>) -> &FieldValue<F> {
        &self.0[label]
    }

    #[inline]
    fn get_mut(&mut self, label: Label<Node>) -> &mut FieldValue<F> {
        &mut self.0[label]
    }
}

impl FieldGraph for SpacetimeGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        Field(LabelledArray::fill_with(
            || FieldValue::None,
            self.nodes.len(),
        ))
    }
}

pub(super) struct IterLinksFromTo {
    cursor: LinkLabel,
    to: LinkLabel,
    up_count: usize,
    down_count: usize,
}

impl IterLinksFromTo {
    /// Start new iterator going around clockwise
    ///
    /// Note that `from` must be a link of the node, otherwise the iteration will panic,
    /// and `to` must be link as well, else the iteration will continue indefinitely.
    /// The range is exclusive of the end point, i.e. [`from`, `to`)
    pub(super) fn new(node: &Node, from: LinkLabel, to: LinkLabel) -> Self {
        Self {
            cursor: from,
            to,
            up_count: node.up.len(),
            down_count: node.down.len(),
        }
    }
}

impl Iterator for IterLinksFromTo {
    type Item = LinkLabel;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cursor == self.to {
            return None;
        }

        let old_cursor = self.cursor;
        self.cursor = match old_cursor {
            LinkLabel::Left => {
                if self.up_count > 0 {
                    LinkLabel::Up(0)
                } else {
                    LinkLabel::Right
                }
            }
            LinkLabel::Up(i) => {
                if i + 1 < self.up_count {
                    LinkLabel::Up(i + 1)
                } else {
                    LinkLabel::Right
                }
            }
            LinkLabel::Right => {
                if self.down_count > 0 {
                    LinkLabel::Down(0)
                } else {
                    LinkLabel::Left
                }
            }
            LinkLabel::Down(i) => {
                if i + 1 < self.down_count {
                    LinkLabel::Down(i + 1)
                } else {
                    LinkLabel::Left
                }
            }
        };
        Some(old_cursor)
    }
}

pub(super) struct IterLinks {
    cursor: LinkLabel,
    up_count: usize,
    down_count: usize,
    started: bool,
}

impl IterLinks {
    /// Start new iterator going around clockwise starting at LinkLabel::Left
    pub(super) fn new(node: &Node) -> Self {
        Self {
            cursor: LinkLabel::Left,
            up_count: node.up.len(),
            down_count: node.down.len(),
            started: false,
        }
    }
}

impl Iterator for IterLinks {
    type Item = LinkLabel;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cursor == LinkLabel::Left {
            if self.started {
                return None;
            }
            self.started = true;
        }

        let old_cursor = self.cursor;
        self.cursor = match old_cursor {
            LinkLabel::Left => {
                if self.up_count > 0 {
                    LinkLabel::Up(0)
                } else {
                    LinkLabel::Right
                }
            }
            LinkLabel::Up(i) => {
                if i + 1 < self.up_count {
                    LinkLabel::Up(i + 1)
                } else {
                    LinkLabel::Right
                }
            }
            LinkLabel::Right => {
                if self.down_count > 0 {
                    LinkLabel::Down(0)
                } else {
                    LinkLabel::Right
                }
            }
            LinkLabel::Down(i) => {
                if i + 1 < self.down_count {
                    LinkLabel::Down(i + 1)
                } else {
                    LinkLabel::Left
                }
            }
        };
        Some(old_cursor)
    }
}

pub struct BaseIterator {
    current: Label<Node>,
    max: Label<Node>,
}

impl Iterator for BaseIterator {
    type Item = Label<Node>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current.cmp(&self.max) {
            Ordering::Less => (),
            Ordering::Equal => return None,
            Ordering::Greater => panic!("Somehow the iterator went past max"),
        }

        let next_label = self.current.succ();
        Some(std::mem::replace(&mut self.current, next_label))
    }
}

impl IterSizedGraph<BaseIterator> for SpacetimeGraph {
    fn iter(&self) -> BaseIterator {
        BaseIterator {
            current: Label::initial(),
            max: Label::from(self.len()),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        graph::breadth_first_search::traverse_nodes::{TraverseNodeDistance, TraverseNodeLabel},
        io::graph,
        observables::{distance_profile, vertex_degree_distribution},
        triangulation::FixedTriangulation,
    };

    use super::*;
    use indoc::indoc;
    use std::io::Cursor;

    const EXAMPLE_NODE: &str = "3; 1, 4, 6; 5; 4";
    const EXAMPLE_GRAPH: &str = indoc! {"
        2; 6, 3, 4; 1; 11, 10, 14, 13
        0; 4, 5, 6; 2; 11
        1; 6; 0; 13, 12, 11
        6; 7; 4; 0
        3; 7; 5; 1, 0
        4; 7, 8, 9; 6; 1
        5; 9, 7; 3; 0, 2, 1
        9; 14, 10, 11; 8; 5, 4, 3, 6
        7; 11; 9; 5
        8; 11, 12, 13, 14; 7; 6, 5
        14; 0; 11; 7
        10; 0, 1, 2; 12; 9, 8, 7
        11; 2; 13; 9
        12; 2, 0; 14; 9
        13; 0; 10; 7, 9
    "};

    #[test]
    fn parse_example_node() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();
        println!("{:?}", node)
    }

    #[test]
    fn node_neighbour_iterator() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();

        let expected_result = [3usize, 1, 4, 6, 5, 4];
        assert!(IterNeighbours::new(&node)
            .map(usize::from)
            .eq(expected_result.into_iter()))
    }

    fn example_graph() -> SpacetimeGraph {
        graph::import_spacetime_adjacency_list(Cursor::new(EXAMPLE_GRAPH))
            .map(SpacetimeGraph::new)
            .unwrap()
    }

    #[test]
    fn import_example_graph() {
        example_graph();
    }

    #[test]
    fn bfs_plain() {
        let graph = example_graph();
        let origin = Label::from(8);

        // Display search results
        println!("Plain breadth first search");
        for node in graph.iter_breadth_first::<TraverseNodeLabel<_>>(origin) {
            print!("{} ", node.label)
        }
        println!();
        // Assert search

        let expected_search = [8, 7, 11, 9, 5, 14, 10, 4, 3, 6, 0, 1, 2, 12, 13];
        assert!(graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .map(|node| usize::from(node.label))
            .eq(expected_search.into_iter()));
    }

    #[test]
    fn bfs_distance() {
        let graph = example_graph();
        let origin = Label::from(4);

        // Display search results
        println!("Distanced breadth first search");
        let mut current_distance = 0;
        print!("{}: ", current_distance);
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
            if node.distance > current_distance {
                println!();
                current_distance = node.distance;
                print!("{}: ", current_distance);
            }
            print!("{} ", node.label)
        }
        println!();

        // Check search results
        let check = [
            (4, 0),
            (3, 1),
            (7, 1),
            (5, 1),
            (1, 1),
            (0, 1),
            (6, 2),
            (9, 2),
            (14, 2),
            (10, 2),
            (11, 2),
            (8, 2),
            (2, 2),
            (13, 2),
            (12, 3),
        ];
        assert!(graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .map(|node| (usize::from(node.label), node.distance))
            .eq(check.into_iter()));
    }

    #[test]
    fn sphere_volume_test() {
        let triangulation = FixedTriangulation::new(20, 20);
        let triangulation = FullTriangulation::from_triangulation(&triangulation);
        let vdegree = vertex_degree_distribution::measure_triangulation(&triangulation).0;
        assert!(vdegree[6] != 0);
        assert!(vdegree
            .iter()
            .copied()
            .enumerate()
            .filter(|&(degree, _)| degree != 6)
            .all(|(_, count)| count == 0));
        let graph = SpacetimeGraph::from_triangulation_vertex(&triangulation);
        let vdegree = vertex_degree_distribution::measure_graph(&graph).0;
        assert!(vdegree[6] != 0);
        assert!(vdegree
            .iter()
            .copied()
            .enumerate()
            .filter(|&(degree, _)| degree != 6)
            .all(|(_, count)| count == 0));

        let sphere_volumes = distance_profile::measure(&graph, Label::from(0), 50).profile;
        println!("{sphere_volumes:?}");
    }
}
