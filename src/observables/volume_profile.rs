//! The volume profile a causal triangulation, also called the length profile in 2D CDT.
//!
//! The volume profile is a measure of the number of vertices in each time-slice, that is a
//! spatial slice of constant time.
//! The distribution of this volume profile is frequently measured observable and is known
//! analytically for 2D CDT.

use serde::Serialize;

use crate::triangulation::FullTriangulation;

/// Volume profile collection
///
/// Stores the number of up triangles in a 'thick' slice or slab of the triangulation,
/// which is equivalent to the number of vertices in the time-slice below the slab.
#[derive(Debug, Clone, Serialize)]
pub struct VolumeProfile {
    // Stores the number of triangles per slice in a simple array
    ups: Box<[usize]>, // Number of up triangles in every slice
}

impl VolumeProfile {
    /// Create a new [`VolumeProfile`] without any time-slices.
    pub fn empty() -> Self {
        Self { ups: Box::new([]) }
    }
}

impl VolumeProfile {
    /// Returns the lengths of the [`VolumeProfile`] as a slice,
    ///
    /// The length here is the number of vertices in the time-slices,
    /// or equivalently the number of `Up` `Triangle`s
    /// in the upper thick time-slice or slab.
    pub fn volumes(&self) -> &[usize] {
        &self.ups
    }

    /// Return the number of time-slices in the [`VolumeProfile`]
    pub fn timeslices_count(&self) -> usize {
        self.ups.len()
    }
}

/// Returns the [`VolumeProfile`] of a [`FullTriangulation`]
pub fn measure(triangulation: &FullTriangulation) -> VolumeProfile {
    VolumeProfile {
        ups: triangulation
            .timeslices
            .iter()
            .map(|timeslice| timeslice.len())
            .collect(),
    }
}
