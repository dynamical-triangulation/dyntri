//! Distance matrix handling, both on memory and on disk

use super::bufwriter::BufWriter;
use super::chunking::chunk_iter;
use super::sparse::BoolMatrix;
use crate::graph::breadth_first_search::traverse_nodes::TraverseNodeDistance;
use crate::graph::{GenericGraph, IterableGraph, SizedGraph};

use hdf5::{Dataset, File, Group, H5Type, Result};
use humantime::format_duration;
use log::{debug, info, log_enabled, trace};
use ndarray::prelude::*;
use std::any::type_name;
use std::time::{Duration, Instant};

/// The default number of bytes that should be in each chunk of the distance matrix on disk
pub const DEFAULT_CHUNK_BYTE_SIZE: usize = 10_485_760; // in bytes
/// The default number of bytes that should be in each chunk of the distance matrix masks on disk
pub const DEFAULT_CHUNK_BYTE_SIZE_MASK: usize = 1_048_576; // in bytes
/// The name of the HDF5 dataset holding the distance matrix
pub const DISTANCE_MATRIX_DATASET_NAME: &str = "distance-matrix";
/// The name of the HDF5 dataset holding the distibution of the geodesic distances of point pairs
/// over the graph
pub const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
/// The name of the HDF5 dataset holding the sphere volumes of the graph
pub const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
/// The base name of the HDF5 group storing the masks
pub const MASK_GROUP_BASE_NAME: &str = "mask";
/// The name of the HDF5 dataset holding the span of a sparse matrix representing a mask
pub const MASK_SPAN_DATASET_NAME: &str = "span";
/// The name of the HDF5 dataset holding the indices of a sparse matrix representing a mask
pub const MASK_INDICES_DATASET_NAME: &str = "indices";

/// Struct holding the distance matrix and its masks
pub struct DistanceMatrixMasks<T> {
    /// The distance matrix as an [`Array2`]
    pub distance_matrix: Array2<T>,
    /// The masks as a list of [`BoolMatrix`] with indices corresponding to `distances`
    pub masks: Vec<BoolMatrix>,
    /// The distances of the masks
    pub distances: Vec<T>,
}

impl<T> DistanceMatrixMasks<T> {
    fn new(distance_matrix: Array2<T>, masks: Vec<BoolMatrix>, distances: Vec<T>) -> Self {
        DistanceMatrixMasks {
            distance_matrix,
            masks,
            distances,
        }
    }
}

impl<T: Eq> DistanceMatrixMasks<T> {
    /// Returns the mask corresponding to `distance`, or None if this mask is not present
    pub fn get_mask(&self, distance: T) -> Option<&BoolMatrix> {
        let idx = self.distances.iter().position(|r| r == &distance);
        idx.map(|i| &self.masks[i])
    }
}

/// Returns a HDF5 group containing the distance matrix of `graph`,
/// stored in type `T` in dataset `"distance-matrix"`.
///
/// Additionally, it stores the amount of pairs at each distance in dataset
/// `"distance-distribution"`, i.e. the amount of occurences of a distance in `distance-matrix`.
/// As well as the amount of nodes at each distance from each nodes, i.e. the volume of
/// geodesic spheres or the area of geodesic balls, in dataset `"sphere-volumes"`.
///
/// Type `T` should probably be a type like u8 or u64, best chosen as small as possible as this
/// reduces the size of the matrix on disk as well as the reading/writing speeds. If the matrix
/// is small enough to fit in memory, it is most likely faster to use [`compute_distance_matrix()`].
/// The default chunk size is `DEFAULT_CHUNK_BYTE_SIZE`, use
/// [compute_distance_matrix_disk_with_chunk_size()] to set a custom value.
pub fn compute_distance_matrix_disk<T>(
    graph: &GenericGraph,
    max_distance: usize,
    file: &hdf5::File,
) -> Result<Group>
where
    T: H5Type + Copy + num_traits::Num + num_traits::NumCast,
{
    compute_distance_matrix_disk_with_chunk_size::<T>(
        graph,
        max_distance,
        file,
        DEFAULT_CHUNK_BYTE_SIZE,
    )
}

/// Returns a HDF5 group containing the distance matrix of `graph` with a given `chunk_memory_size`
/// used to determine the size of the chunks in the dataset.
///
/// See [compute_distance_matrix_disk()] for more information on usage.
pub fn compute_distance_matrix_disk_with_chunk_size<T>(
    graph: &GenericGraph,
    max_distance: usize,
    file: &hdf5::File,
    chunk_memory_size: usize,
) -> Result<Group>
where
    T: H5Type + Copy + num_traits::Num + num_traits::NumCast,
{
    // Determine the size the chunks should be to get CHUNK_MEMORY_SIZE sized chunks
    let n = graph.len();
    let size_of_type = std::mem::size_of::<T>();
    let chunk_row_size = (chunk_memory_size / n / size_of_type).min(n);

    info!(
        "Initializing distance matrix in HDF5 file \
        with chunk size ({chunk_row_size}, {n})"
    );
    // let file = File::create(&filename)?;
    let dmatrix = file
        .new_dataset::<T>()
        .chunk((chunk_row_size, n))
        .chunk_cache(1, 8, 0.75) // Should disable cache, doesn't seem to work in this create
        .blosc_lz4(9, true) // Use LZ4 compression algorithm at highest compression level
        .shape((n, n))
        .fill_value(T::from(255).unwrap())
        .create(DISTANCE_MATRIX_DATASET_NAME)?;

    info!("Filling the distance matrix");
    let mut counter = 0;
    let mut start_row = 0;
    let mut current_chunk_size = chunk_row_size.min(n);
    let mut chunk: Array2<T> = Array2::zeros((current_chunk_size, n));
    let mut sphere_volumes: Array2<u64> = Array2::zeros([max_distance + 1, n]);

    let start_timer = Instant::now();
    let mut writing_time = 0; // in us
    let mut prev_part_finished = 0.0;
    for label in graph.get_nodes().labels() {
        // Uses the fact that label iterator simply iterates from 0 to n
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(label) {
            if node.distance > max_distance {
                break;
            }
            chunk[[counter, usize::from(node.label)]] = num_traits::cast(node.distance)
                .unwrap_or_else(|| {
                    panic!(
                        "Could not fit {} in type {}",
                        node.distance,
                        type_name::<T>()
                    )
                });
            sphere_volumes[(node.distance, usize::from(label))] += 1;
        }
        counter += 1;

        if counter == current_chunk_size {
            trace!("Writing chunk ({current_chunk_size},{n}) to disk");
            let start_inner_timer = Instant::now();
            dmatrix.write_slice(&chunk, s![start_row..start_row + current_chunk_size, ..])?;
            writing_time += start_inner_timer.elapsed().as_micros();
            // Reset variables
            counter = 0;
            start_row += current_chunk_size;
            if log_enabled!(log::Level::Info) {
                let part_finished = start_row as f32 / n as f32;
                if part_finished - prev_part_finished > 1e-2 {
                    prev_part_finished = part_finished;
                    let time_passed = start_timer.elapsed().as_millis();
                    let expected_total_time = time_passed as f32 / part_finished;
                    let eta =
                        Duration::from_secs_f32((expected_total_time - time_passed as f32) / 1e3);
                    info!(
                        "Constructing distance matrix at {:.0}%, expected to finish in {}",
                        part_finished * 100.0,
                        format_duration(Duration::from_secs(eta.as_secs()))
                    );
                }
            }
            current_chunk_size = chunk_row_size.min(n - start_row);
            chunk = Array2::zeros((current_chunk_size, n));
        }
    }
    debug!(
        "Finished creating the distance matrix in {:.1}s, {:.1}s of which was time writing to disk",
        start_timer.elapsed().as_millis() as f64 / 1e3,
        writing_time as f64 / 1e6
    );

    let distances = Array1::from_iter(0..sphere_volumes.shape()[0]);
    debug!("Writing sphere volumes to dataset");
    let svol_dataset = file
        .new_dataset_builder()
        .with_data(sphere_volumes.view())
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    svol_dataset
        .new_attr_builder()
        .with_data(&distances)
        .create("r")?;

    let distance_distribution = sphere_volumes.sum_axis(Axis(1));
    debug!("Writing distance distribution to dataset: {distance_distribution}");
    let ddist_dataset = file
        .new_dataset_builder()
        .with_data(distance_distribution.view())
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset
        .new_attr_builder()
        .with_data(&distances)
        .create("r")?;

    file.as_group()
}

/// Computes masks for distance matrix on disk with `default_chunk_size` for the masks datasets,
///
/// This function adds a new group for every distance in `distance-distribution` called `mask{r}`,
/// which contains a dataset `span` and `indices` containing the span and the indices for the
/// [BoolMatrix] of the mask.
///
/// !Note: this function assumes that `group` contains a distance matrix
/// in dataset `distance-matrix` and a distance distribution list in datset `distance-distribution`
/// and will return an **Error** if this is not the case.
fn compute_masks_disk(group: &Group, default_chunk_size: usize) -> Result<()> {
    let dmatrix_dataset = group.dataset("distance-matrix")?;
    let ddist_dataset = group.dataset("distance-distribution")?;
    let distance_distribution = ddist_dataset.read_1d::<usize>()?;
    debug!(
        "Loadin distance distribution up to max distance {}",
        distance_distribution.len() - 1
    );

    let n = dmatrix_dataset.shape()[0];
    debug!("Creating new datasets for masks");
    let masks: Vec<(Dataset, Dataset)> = distance_distribution
        .iter()
        .enumerate()
        // Skip the distance 0 elements as 0 is also the default value
        // for values above the maximum value.
        .skip(1)
        .map(|(r, &nr)| {
            let rgroup = group
                .create_group(&format!("{MASK_GROUP_BASE_NAME}{r}"))
                .unwrap();
            let rspan = rgroup
                .new_dataset::<usize>()
                .shape(n + 1)
                .create(MASK_SPAN_DATASET_NAME)
                .unwrap();
            let chunk_size = default_chunk_size / std::mem::size_of::<usize>();
            let rindices = if nr <= chunk_size {
                rgroup
                    .new_dataset::<usize>()
                    .shape(nr)
                    .create(MASK_INDICES_DATASET_NAME)
                    .unwrap()
            } else {
                rgroup
                    .new_dataset::<usize>()
                    .shape(nr)
                    .chunk(chunk_size)
                    .blosc_lz4(9, true)
                    .create(MASK_INDICES_DATASET_NAME)
                    .unwrap()
            };
            (rspan, rindices)
        })
        .collect();
    debug!("Creating buffers for the dataset");
    let (mut spans, mut indices): (Vec<BufWriter<usize>>, Vec<BufWriter<usize>>) = masks
        .iter()
        .map(|(span_dataset, indices_dataset)| {
            let mut span = BufWriter::with_capacity(span_dataset, default_chunk_size);
            span.push(0).unwrap();
            let indices = BufWriter::with_capacity(indices_dataset, default_chunk_size);
            (span, indices)
        })
        .unzip();
    debug!("Filling mask buffers");
    let chunk_number = dmatrix_dataset.num_chunks().unwrap();
    let start_timer = Instant::now();
    let mut prev_part_finished = 0.0;
    for (ichunk, chunk) in chunk_iter::<u8>(&dmatrix_dataset).enumerate() {
        for row in chunk.array.rows() {
            for (icol, &r) in row.indexed_iter() {
                let r = r as usize;
                if r == 0 {
                    continue;
                }
                indices[r - 1].push(icol)?; // Shift index down since 0 element is skipped
            }
            for (span, indices) in spans.iter_mut().zip(indices.iter()) {
                span.push(indices.index())?;
            }
        }
        if log_enabled!(log::Level::Info) {
            let part_finished = ichunk as f32 / chunk_number as f32;
            if part_finished - prev_part_finished > 1e-2 {
                prev_part_finished = part_finished;
                let time_passed = start_timer.elapsed().as_millis();
                let expected_total_time = time_passed as f32 / part_finished;
                let eta = Duration::from_secs_f32((expected_total_time - time_passed as f32) / 1e3);
                info!(
                    "Constructing distance masks at {:.0}%, expected to finish in {}",
                    part_finished * 100.0,
                    format_duration(Duration::from_secs(eta.as_secs()))
                );
            }
        }
    }
    debug!(
        "Finished filling mask buffers in {}s",
        start_timer.elapsed().as_millis() as f64 / 1e3
    );

    Ok(())
}

/// Returns a HDF5 group containing the distance matrix of `graph`,
/// stored in type `T` in dataset `"distance-matrix"` and maks
/// This function adds a new group for every distance in `distance-distribution` called `mask{r}`,
/// which contains a dataset `span` and `indices` containing the span and the indices for the
/// [BoolMatrix] of the mask.
///
/// Additionally, it stores the amount of pairs at each distance in dataset
/// `distance-distribution`, i.e. the amount of occurences of a distance in `distance-matrix`.
///
/// Type `T` should probably be a type like u8 or u64, best chosen as small as possible as this
/// reduces the size of the matrix on disk as well as the reading/writing speeds. If the matrix
/// is small enough to fit in memory, it is most likely faster to use [`compute_distance_matrix_with_masks()`].
/// The default chunk size is `DEFAULT_CHUNK_BYTE_SIZE`, use
/// [compute_distance_matrix_with_masks_with_chunk_size_disk()] to set a custom value.
pub fn compute_distance_matrix_with_masks_disk<T>(
    graph: &GenericGraph,
    max_distance: usize,
    file: &File,
) -> Result<Group>
where
    T: H5Type + Copy + num_traits::Num + num_traits::NumCast,
{
    compute_distance_matrix_with_masks_with_chunk_size_disk::<T>(
        graph,
        max_distance,
        file,
        DEFAULT_CHUNK_BYTE_SIZE,
        DEFAULT_CHUNK_BYTE_SIZE_MASK,
    )
}

/// See [compute_distance_matrix_with_masks_disk()] for documentation
pub fn compute_distance_matrix_with_masks_with_chunk_size_disk<T>(
    graph: &GenericGraph,
    max_distance: usize,
    file: &File,
    chunk_byte_size: usize,
    mask_chunk_byte_size: usize,
) -> Result<Group>
where
    T: H5Type + Copy + num_traits::Num + num_traits::NumCast,
{
    let group = compute_distance_matrix_disk_with_chunk_size::<T>(
        graph,
        max_distance,
        file,
        chunk_byte_size,
    )?;
    compute_masks_disk(&group, mask_chunk_byte_size)?;
    Ok(group)
}

/// Returns the distance matrix of `graph` with distances in type `T` as owned Array.
///
/// Type `T` should be somthing like u8 or u64, probably as small as possible to keep the
/// memory size of the distance matrix reasonable. If the owned array becomes to large to handle
/// in memory, consider [`compute_distance_matrix_disk()`] which stores the matrix to disk.
pub fn compute_distance_matrix<T>(graph: &GenericGraph, max_distance: usize) -> Array2<T>
where
    T: Copy + num_traits::Num + num_traits::NumCast,
{
    let n = graph.len();
    let mut dmatrix = Array2::zeros((n, n));

    debug!(
        "Computing the distance matrix to memory, expecting to use {}",
        bytes_to_string(n * n * std::mem::size_of::<T>())
    );
    for label in graph.get_nodes().labels() {
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(label) {
            if node.distance > max_distance {
                continue;
            }
            let row = usize::from(label);
            let column = usize::from(node.label);
            dmatrix[[row, column]] = num_traits::cast(node.distance).unwrap_or_else(|| {
                panic!(
                    "Could not fit {} in type {}",
                    node.distance,
                    type_name::<T>()
                )
            });
        }
    }

    dmatrix
}

/// Returns the distance matrix with masks, that is [BoolMatrix] that are the filters given
/// by the elements that match the corresponding `delta` given by `deltas`.
///
/// See [compute_distance_matrix()] for additional documentation
pub fn compute_distance_matrix_with_masks<T>(
    graph: &GenericGraph,
    deltas: Vec<usize>,
    max_distance: usize,
) -> DistanceMatrixMasks<T>
where
    T: Copy + num_traits::Num + num_traits::NumCast,
{
    let n = graph.len();
    let max_delta = *deltas.last().unwrap();
    let mut dmatrix = Array2::zeros((n, n));
    let mut masks: Vec<BoolMatrix> = std::iter::repeat_with(|| BoolMatrix::empty((n, n)))
        .take(deltas.len())
        .collect();

    debug!(
        "Computing the distance matrix to memory, expecting to use {}",
        bytes_to_string(n * n * std::mem::size_of::<T>())
    );
    for mask in masks.iter_mut() {
        mask.span.push(0);
    }
    let mut current_delta_index = 0;
    let mut current_delta = deltas[current_delta_index];
    for label in graph.get_nodes().labels() {
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(label) {
            if node.distance > max_distance {
                break;
            }
            let row = usize::from(label);
            let column = usize::from(node.label);
            dmatrix[[row, column]] = num_traits::cast(node.distance).unwrap_or_else(|| {
                panic!(
                    "Could not fit {} in type {}",
                    node.distance,
                    type_name::<T>()
                )
            });
            use std::cmp::Ordering::*;
            match node.distance.cmp(&current_delta) {
                Less => continue,
                Equal => {
                    masks[current_delta_index]
                        .indices
                        .push(usize::from(node.label));
                }
                Greater => {
                    if node.distance > max_delta {
                        continue;
                    }
                    current_delta_index += 1;
                    current_delta = deltas[current_delta_index];
                    if node.distance == current_delta {
                        masks[current_delta_index]
                            .indices
                            .push(usize::from(node.label));
                    }
                }
            }
        }
        for mask in masks.iter_mut() {
            mask.span.push(mask.indices.len())
        }
        current_delta_index = 0;
        current_delta = deltas[current_delta_index];
    }

    // Sorting the indices lists
    for mask in masks.iter_mut() {
        for i in 0..mask.span.len() - 1 {
            let istart = mask.span[i];
            let iend = mask.span[i + 1];
            heap_sort(&mut mask.indices[istart..iend]);
        }
    }

    let distances = deltas
        .into_iter()
        .map(|delta| num_traits::cast(delta).unwrap())
        .collect();
    DistanceMatrixMasks::new(dmatrix, masks, distances)
}

fn bytes_to_string(byte_size: usize) -> String {
    if byte_size > 1_000_000_000 {
        format!("{:.1}GB", byte_size as f64 / 1e9)
    } else if byte_size > 1_000_000 {
        format!("{:.1}MB", byte_size as f64 / 1e6)
    } else if byte_size > 1_000 {
        format!("{:.1}kB", byte_size as f64 / 1e3)
    } else {
        format!("{:.1}B", byte_size)
    }
}

fn heap_sort<T: PartialOrd>(input: &mut [T]) {
    if input.len() < 2 {
        return;
    }

    for i in (0..input.len() / 2).rev() {
        heap_max(input, i, input.len());
    }

    for i in (0..input.len()).rev() {
        input.swap(0, i);
        heap_max(input, 0, i);
    }
}

/// Max heapifies an embedded heap from given index.
fn heap_max<T: PartialOrd>(input: &mut [T], i: usize, heap_len: usize) {
    let left = 2 * i + 1;
    let right = left + 1;

    let mut largest = i;
    if left < heap_len && input[left] > input[largest] {
        largest = left;
    }
    if right < heap_len && input[right] > input[largest] {
        largest = right;
    }

    if largest != i {
        input.swap(i, largest);
        heap_max(input, largest, heap_len);
    }
}

#[cfg(test)]
mod tests {
    use super::heap_sort;

    #[test]
    fn test_heap_sort() {
        let mut list = vec![4, 2, 5, 3, 9, 5, 2];
        heap_sort(&mut list[0..3]);
        assert_eq!(&list, &[2, 4, 5, 3, 9, 5, 2]);
        heap_sort(&mut list[3..7]);
        assert_eq!(&list, &[2, 4, 5, 2, 3, 5, 9]);
    }
}
