//! Module defining [`LabelledList`], see that for more details.

use super::Label;
use rand::Rng;
use std::ops::{Index, IndexMut};

/// Enum representing the Elements of the LabelledList struct.
/// With None(usize) representing 'holes' in the LabelledList linking to previous hole
/// and Some(T) representing an item in the list of type T
enum Element<T> {
    None(usize),
    Some(T),
}

/// Collection type for storing labelled items with fast addition, removal, indexing, and length determination.
///
/// This is an implementation of a collection data structure which allows for storing items of general type `T`,
/// which can be (mutably) indexed by fixed `Label`s in O(1) in `LabelledList` size.
/// This implementation allows for `add`ing items (for which some available `Label` is used),
/// and removing items by a given `Label` in O(1) complexity.
/// Finally it allows for sampling a random item with O(1) complexity, but this can still
/// be slow in case the `LabelledList` contains much less items than the given `expected_length`.
///
/// This collection type is designed to hold an amount of items which is close to some
/// known amount, given by `max_length`; and it cannot hold more items than `max_length`.
/// It will give an `"out of bounds error"` if more items are attempted to be added.
///
/// #### When to use
/// This collection type is designed to be efficient for holding an amount of items close
/// to some known average amount, for which one needs constant Labels with indexing,
/// fast addition (at a non-predetermined `Label`), removal (by any `Label`) of items,
/// and checks of the length of the list.
/// As well as a reasonably fast random sampling of items (where the speed is heavily reliant
/// on having items close to or over the average amount).
pub struct LabelledList<T> {
    // The structure consists of a list of items and holes:
    // the items hold the active items of the list, and the holes
    // hold a reference to the next hole, which is used to find
    // the next active hole when the current hole gets filled up.
    //
    // Implemented by holding a statically sized `Box`ed `Array` of `Element`s,
    // and tracking the lastest or 'active' hole by index (`usize`).CA
    list: Box<[Element<T>]>,
    active_hole: usize,
    size: usize, // Track the amount of active elements
}

impl<T> LabelledList<T> {
    /// Constructor to create a new *empty* `LabelledList`.
    ///
    /// `max_length` should be chosen to be the maximum amount of items
    /// the `LabelledList` is expected to hold.
    pub fn new(max_length: usize) -> LabelledList<T> {
        LabelledList {
            list: (0..max_length).map(|i| Element::None(i + 1)).collect(),
            active_hole: 0,
            size: 0,
        }
    }
}

// Public methods for interacting with the LabelledList
impl<T> LabelledList<T> {
    /// Return the maximum possible length of the `LabelledList`.
    pub fn max_size(&self) -> usize {
        self.list.len()
    }

    /// Return the length of the `LabelledList`,
    /// meaning the amount of active elements in the list.
    pub fn len(&self) -> usize {
        self.size
    }

    /// Returns whether the list is empty,
    /// e.g. contains no elements
    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    /// Return whether the `LabelledList` contains a given `Label`
    pub fn contains(&self, label: Label<T>) -> bool {
        match self.list[usize::from(label)] {
            Element::Some(_) => true,
            Element::None(_) => false,
        }
    }

    /// Add an item in O(1) to the `LabelledList`, returning the `Label` the item is added at.
    pub fn add(&mut self, item: T) -> Label<T> {
        // This is implemented by filling `active_hole` with the item, and returning
        // the `Label` corresponding to that `active_hole`.
        // The `active_hole` is than updated to be the next hole, where the original
        // `active_hole` linked to.
        let label = self.active_hole;
        self.active_hole = match self.list[label] {
            Element::None(next_hole) => next_hole,
            Element::Some(_) => {
                panic!("Something was implemented wrong, active hole is not a hole")
            }
        };
        self.list[label] = Element::Some(item);
        self.size += 1;
        Label::from(label)
    }

    /// Remove an item indexed by `Label` from the `LabelledList`;
    /// this checks whether there is an item at `label` in the first place,
    /// if `label` points to an empty element the function `panic`s.
    ///
    /// For slighly faster removal without this check, use [`remove_checkless`](LabelledList::remove_checkless());
    /// this can however break the datastructure if called on non-existing item.
    pub fn remove(&mut self, label: Label<T>) {
        // This is implemented by setting the element at Label to be None(active_element),
        // and setting the new active_element to the index given by label.
        // But first checking whether the label points to Some() element, else panic!().
        let elem = &mut self.list[usize::from(label)];

        match elem {
            Element::Some(_) => {
                *elem = Element::None(self.active_hole);
                self.active_hole = usize::from(label);
                self.size -= 1;
            }
            Element::None(_) => panic!(
                "Out of bounds error: tried to remove at {:} where there was no element",
                label
            ),
        }
    }

    /// Remove an item indexed by `Label` from `LabelledList` without checking existence.
    ///
    /// This method does not check if there is an item at the given label, which can break
    /// the data structure [`remove_checkless()`](LabelledList::remove_checkless()) is called
    /// on a `Label` pointing to an empty element.
    /// **So [`remove_checkless()`](LabelledList::remove_checkless()) should _only_ be called
    /// on a `Label`, where one is certain there is an item!**
    ///
    /// NOTE: For a save removal use [`remove()`](LabelledList::remove()), which `panic`s
    /// if a hole is attempted to be removed.
    pub fn remove_checkless(&mut self, label: Label<T>) {
        // This is implemented by setting the element at Label to be None(active_element),
        // and setting the new active_element to the index given by label.
        // This is done regardless whether the element was previously None() or Some(),
        // so this can break the 'hole linking' if the element was previously None().
        self.list[usize::from(label)] = Element::None(self.active_hole);
        self.active_hole = usize::from(label);
        self.size -= 1;
    }

    /// Sample a random `Label` from `LabelledList`.
    ///
    /// This is an O(N^0) = O(1) implementation by repeated sampling until active element is found.
    /// This takes on average *occupation*/*length*, where occupation is the amount of occupied elements
    /// and length is `max_length` as specified in `LabelledList` creation.
    ///
    /// So note that this sampling is very inefficient for a `LabelledList` which holds much less items
    /// than specified by max_length.
    pub fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Label<T> {
        loop {
            let index = rng.gen_range(0..self.list.len());
            match &self.list[index] {
                Element::Some(_) => return Label::from(index),
                Element::None(_) => continue,
            }
        }
    }
}

/// `Index`ing implementation, allowing indexing by `Label` of the `LabelledList`;
/// this `panic!()` if no item is present at the given `Label`.
impl<T> Index<Label<T>> for LabelledList<T> {
    type Output = T;
    fn index(&self, label: Label<T>) -> &Self::Output {
        // Index item of `LabelledList`, `panic`s if there is no item.
        match &self.list[usize::from(label)] {
            Element::Some(item) => item,
            Element::None(_) => panic!("no item at label {}", label),
        }
    }
}

/// `Index`ing implementation, allowing mutable indexing by `Label` of the `LabelledList`;
/// this `panic!()` if no item is present at the given `Label`.
impl<T> IndexMut<Label<T>> for LabelledList<T> {
    fn index_mut(&mut self, label: Label<T>) -> &mut Self::Output {
        match &mut self.list[usize::from(label)] {
            // Mutably index item of LabelledList, raises an error if there is no item.
            Element::Some(item) => item,
            Element::None(_) => panic!("no item at label {}", label),
        }
    }
}

// Following are implementations that are less efficient but may still be useful
impl<T> LabelledList<T> {
    /// Return an iterator over the `Label`s in the `LabelledList`
    pub fn labels(&self) -> impl Iterator<Item = Label<T>> + '_ {
        Box::new(
            (0..self.list.len())
                .map(Label::from)
                .filter(|&label| self.contains(label)),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand_xoshiro::{rand_core::SeedableRng, Xoshiro256StarStar};
    use std::fmt::{self, Display};

    impl<T: Display> Display for LabelledList<T> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "[")?;
            let mut list_iter = self.list.iter();
            if let Element::Some(item) = list_iter.next().expect("List has no elements") {
                write!(f, "0: {:}", item)?
            }
            for (i, element) in list_iter.enumerate() {
                if let Element::Some(item) = element {
                    write!(f, ", {:}: {:}", i + 1, item)?
                }
            }
            write!(f, "]")
        }
    }

    #[test]
    fn list_creation() {
        let list = LabelledList::<f32>::new(10);
        println!("List: {}", list);
        assert_eq!(format!("{:}", list), "[]");
    }

    #[test]
    fn list_add() {
        let mut list = LabelledList::<f32>::new(10);
        let label1 = list.add(0.3);
        assert_eq!(label1, Label::from(0));
        let label2 = list.add(0.7);
        assert_eq!(label2, Label::from(1));
        let label3 = list.add(0.8);
        assert_eq!(label3, Label::from(2));

        assert_eq!(list[label1], 0.3);
        assert_eq!(list[label2], 0.7);
        assert_eq!(list[label3], 0.8);

        assert_eq!(list.len(), 3);

        println!("List: {}", list);
    }

    #[test]
    fn list_remove() {
        let mut list = LabelledList::<f32>::new(10);
        let label1 = list.add(0.3);
        let label2 = list.add(1.7);
        let label3 = list.add(0.8);
        let label4 = list.add(3.1);

        assert_eq!(list.len(), 4);

        assert!(list.contains(label2));
        list.remove(label2);
        assert_eq!(list.len(), 3);
        assert!(!list.contains(label2));
        assert_eq!(list.active_hole, 1);

        let label5 = list.add(2.4);
        assert_eq!(label5, Label::from(1));
        let label6 = list.add(7.4);
        assert_eq!(label6, Label::from(4));

        assert_eq!(list[label1], 0.3);
        assert_eq!(list[label3], 0.8);
        assert_eq!(list[label4], 3.1);
        assert_eq!(list[label5], 2.4);
        assert_eq!(list[label6], 7.4);

        assert_eq!(list.len(), 5);

        println!("List: {}", list);
    }

    #[test]
    fn list_modify() {
        #[derive(Debug, PartialEq)]
        struct Test(Label<Self>);

        let mut list: LabelledList<Test> = LabelledList::new(10);

        let initial_label = Label::from(0);
        let label1 = list.add(Test(initial_label));
        assert_eq!(label1, Label::from(0));
        let label2 = list.add(Test(label1));
        assert_eq!(label2, Label::from(1));

        assert_eq!(list[label1], Test(initial_label));
        assert_eq!(list[label2], Test(Label::from(0)));

        list[label1] = Test(label2); // Modification of hole object

        assert_eq!(list[label1], Test(Label::from(1)));
        assert_eq!(list[label2], Test(Label::from(0)));

        list[label2].0 = Label::from(8); // Modification of a field

        assert_eq!(list[label1], Test(Label::from(1)));
        assert_eq!(list[label2], Test(Label::from(8)));
    }

    #[test]
    fn list_sample() {
        let mut list = LabelledList::<f32>::new(20);
        let _label0 = list.add(0.3);
        let label1 = list.add(1.7);
        let label2 = list.add(0.8);
        let label3 = list.add(3.1);
        let _label4 = list.add(2.4);
        let label5 = list.add(7.4);

        let mut rng = Xoshiro256StarStar::seed_from_u64(7);
        assert_eq!(list.sample(&mut rng), label5);
        assert_eq!(list.sample(&mut rng), label1);
        assert_eq!(list.sample(&mut rng), label2);
        assert_eq!(list.sample(&mut rng), label3);
        assert_eq!(list.sample(&mut rng), label5);
        assert_eq!(list.sample(&mut rng), label3);

        println!("List: {}", list);
    }

    #[test]
    fn list_label_iteration() {
        let mut list = LabelledList::<f32>::new(10);
        list.add(0.3);
        list.add(1.7);
        list.add(0.8);
        list.add(3.1);
        list.add(2.4);
        list.add(7.4);
        list.remove(Label::from(2));
        list.remove(Label::from(4));

        let expected_labels: [usize; 4] = [0, 1, 3, 5];
        let expected_items: [f32; 4] = [0.3, 1.7, 3.1, 7.4];
        for (i, label) in list.labels().enumerate() {
            assert_eq!(label, Label::from(expected_labels[i]));
            assert_eq!(list[label], expected_items[i]);
        }
    }
}
