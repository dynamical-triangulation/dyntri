# dyntri
2D Causal Dynamical Triangulations (CDT) for Rust

[![GitLab License](https://img.shields.io/gitlab/license/dynamical-triangulation%2Fdyntri)](https://gitlab.com/dynamical-triangulation/dyntri/-/blob/main/LICENSE)
[![Documentation](https://img.shields.io/badge/docs%20-%20%20green?logo=readthedocs&logoColor=dark%20gray
)](https://dynamical-triangulation.gitlab.io/dyntri/)
[![Pipelin](https://gitlab.com/dynamical-triangulation/dyntri/badges/main/pipeline.svg)](https://gitlab.com/dynamical-triangulation/dyntri/-/jobs)

The library provides 2D CDT simulations, using Markov chain Monte Carlo methods.
And, it implements many observables to measure the generated triangulation.
As well as create embeddings of triangulation to be used for visualisation.

This library formed the the backbone for the 2D CDT simulations of my Master's thesis with professor Renate Loll, at Radboud University.
It is made open-source so future students or other researchers that would like a reference implementation of 2D CDT have one that is (almost) fully documented.


## Features
- Performing a Markov chain Monte Carlo simulation to generate random triangulations of 2D CDT.
- Data structures for handeling dynamically- and statically-sized triangulations.
- Support for graphs of different types, which will generally be generated as the vertex or dual graph of a triangulation. Implementing breadth first search, repeating periodic graphs, and more features that are useful to triangulations.
- Many measurably quantities on the triangulation or graphs to analyse them.
Here most of the common quantities are implemented to determine observables like the Hausdorf and spectral dimension as well as quantum Ricci curvature.
All graph measurements are generic over graph types so they can also be performed on graphs that do not come from 2D CDT by importing them from other models.
- Importing and exporting graphs to allow for sharing graphs with other programs.
One could for example import an EDT triangulation to measure the implemented observables.
- Create graph embeddings in 2D and 3D for visualisation of the underlying triangulations.


## Using the library
To use the Rust crate you can use `cargo`'s [feature](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#specifying-dependencies-from-git-repositories) to add dependencies directly from a git repository, just create a Rust project and to `Cargo.toml` add:
```toml
[dependencies]
dyntri = { git = "https://gitlab.com/dynamical-triangulation/dyntri.git" }
```
This will use the most recent commit of the `main` branch.
It is also possible to use other versions of the library, for options see the [Cargo docs](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#specifying-dependencies-from-git-repositories)



### Using HDF5
[HDF5](https://www.hdfgroup.org/solutions/hdf5/) is a HDF5 data storage format and library used to storge distance matrices effectively one disk.
For some features of the library [`hdf5-rust`](https://github.com/aldanor/hdf5-rust) is used. Currently HDF5 statically build alongside the crate (this does mean that the initial compilation time is very long).
_The goal is to make HDF5 an optional feature in the future._

## Example of use
A short and simple example of use of the library by generating a small CDT triangulation and measuring the volume profile distribution and average sphere distance on the vertex graph of the triangulation.

```rust
use dyntri::graph::{SampleGraph, periodic_spacetime_graph::PeriodicSpacetimeGraph};
use dyntri::monte_carlo::fixed_universe::Universe;
use dyntri::observables::{average_sphere_distance::double_sphere, volume_profile};
use dyntri::triangulation::FullTriangulation;
use rand::thread_rng;

// Create a new universe of 100 triangles with 10 timeslices
let mut universe = Universe::default(100, 10);
// Initialize a random number generator of choice
let mut rng = thread_rng();
// Perform 10_000 Monte Carlo moves on the triangulation
for _ in 0..10_000 {
    universe.step(&mut rng);
}
// Create a trianglulation with all connectivity information
// Not just the triangle connectivity stored in `universe.triangulation`
let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);

// Measure the volume profile of the triangulation
let volume_profile = volume_profile::measure(&triangulation);
println!("Volume profile: {:?}", volume_profile.volumes());

// Create the (cut-open) periodic vertex graph of the triangulation
let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
// Randomly select a label to use as an origin
let label = graph.sample_node(&mut rng);
// Measure the average sphere distance with delta = 3
let delta = 3;
let asd = double_sphere::measure(&graph, label, delta, &mut rng);
println!("Average sphere distance (delta = {delta}) is {asd}");
```
A CLI for using this library, which was used to perform most of the simulations and measurements in the related research papers, is also available at [https://gitlab.com/dynamical-triangulation/dyntri-cli](https://gitlab.com/dynamical-triangulation/dyntri-cli).
However, this project is very unpolished and just made to fit my personal use. I do no really recommend using it.
But it may provide a useful example use-case for those that want to use this Rust crate.


## Documentation
The generated `rustdoc` documentation is available online: [See the docs](https://dynamical-triangulation.gitlab.io/dyntri/).
Or can be viewed locally, by compiling the documentation using the command line:
```zsh
cd path/to/dyntri
cargo doc --no-deps --open
```
This builds the documentation of `dyntri` without building all dependencies, and opens the documentation in browser.

## Research notes
The research papers [J. van der Duin, R. Loll. _Curvature correlators in nonperturbative 2D Lorentzian quantum gravity_](https://doi.org/10.48550/arXiv.2404.17556) and [J. van der Duin, A. Silva. _Scalar curvature for metric spaces: Defining curvature for quantum gravity without coordinates_](https://doi.org/10.1103/PhysRevD.110.026013) as well as my [Master thesis](https://www.ru.nl/en/departments/institute-for-mathematics-astrophysics-and-particle-physics/high-energy-physics#internships) made use of this library.
These research projects were done along side the development of this library, so there is no single version of this library that was used. However, if reproduction of the results from these works is desired, release [0.2.1 | Research version 1](https://gitlab.com/dynamical-triangulation/dyntri/-/releases/v0.2.1) should be used.
It is possible that later versions change something in the RNG, such that different results are obtained with the same seed.

For any other use than exact reproduction of the results from these papers, also meaning verification of those results with different RNG seeds, it is recommended to use the most current version of the library.