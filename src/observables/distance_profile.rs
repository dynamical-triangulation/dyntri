//! Profile of the volume of spheres in the graph over radius.
//!
//! With the volume of a sphere, we mean the number of nodes at a given _radius_
//! away from some _origin_.
//! The scaling dimension of the average volume of a sphere in a graph is called the _Hausdorff
//! dimension_ (strictly speaking this scaling dimension is one lower than the Hausdorff dimension).
//! The Hausdorff dimension is one of the standard oservables to analyse the structure of the model
//! one is analysing.

use hdf5::H5Type;
use ndarray::{azip, Array1, Array2};
use serde::Serialize;
use std::fmt::Display;

use crate::graph::breadth_first_search::IterNeighbours;
use crate::graph::field::PeriodicField;
use crate::graph::periodic_graph::PeriodicLabel;
use crate::graph::IterableGraph;
use crate::graph::{
    breadth_first_search::traverse_nodes::TraverseNodeDistance, periodic_graph::PeriodicGraph,
};

use super::full_graph::chunking::chunk_iter;
use super::full_graph::distance_matrix;
use distance_matrix::{DISTANCE_DISTRIBUTION_DATASET_NAME, DISTANCE_MATRIX_DATASET_NAME};

/// Distance profile of a graph giving the number of vertices
/// at a given graph distance from some origin, also called the volume of the sphere of that radius.
#[derive(Debug, Clone, Serialize)]
pub struct DistanceProfile {
    /// Distance profile stored as a boxed slice
    pub profile: Box<[usize]>,
}

/// Distance profile for a periodic graph
pub struct DistanceProfilePeriodic {
    /// The distance profile, i.e. the sphere volume profile for increasing radius
    pub profile: Array1<usize>,
    /// The fraction of overlap of the sphere with itself
    pub overlap: Array1<f64>,
}

impl DistanceProfile {
    /// Returns the maximal distance that is measured in the [`DistanceProfile`]
    pub fn max_distance(&self) -> usize {
        self.profile.len()
    }

    /// Returns the amount of nodes at the given `distance`
    pub fn get_count(&self, distance: usize) -> usize {
        self.profile[distance]
    }
}

/// Measure a radial sphere area profile against distance from
/// a given `origin` on a `graph` up to and including `max_distance`
pub fn measure<'a, G: IterableGraph<'a, I>, I: IterNeighbours<G>>(
    graph: &'a G,
    origin: G::Label,
    max_distance: usize,
) -> DistanceProfile {
    let mut profile = vec![0; max_distance + 1];
    for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
        let r = node.distance;
        if r > max_distance {
            break;
        }
        profile[r] += 1;
    }

    DistanceProfile {
        profile: profile.into_boxed_slice(),
    }
}

/// Measure a radial sphere volume profile against distance from
/// a given `origin` on a `graph` up to and including `max_distance`
///
/// Including the amount of overlap of the spheres with itself, that is the amount of wrapping
/// that would have occured if this measurement was not performed on a periodic graph.
pub fn measure_periodic<'a, G, I>(
    graph: &'a G,
    origin: G::Label,
    max_distance: usize,
) -> DistanceProfilePeriodic
where
    G: IterableGraph<'a, I> + PeriodicGraph,
    G::FieldType<()>: PeriodicField<(), G>,
    G::Label: PeriodicLabel,
    I: IterNeighbours<G>,
{
    let mut profile = Array1::zeros(max_distance + 1);
    let mut overlap = Array1::zeros(max_distance + 1);
    let mut bfs = graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin);
    loop {
        let Some(node) = bfs.next() else { break };
        let r = node.distance;
        if r > max_distance {
            break;
        }
        profile[r] += 1;
        if bfs.explored_other_patch(node.label) && r < max_distance {
            // This will return true if it is still 1 radial distance away from
            // the visited part of the sphere as explored also contains all the
            // neighbours of the currently visited part of the sphere.
            // So we assign it to the next r over
            overlap[r + 1] += 1.0;
        }
    }
    azip!((o in &mut overlap, &s in &profile) *o /= s as f64);

    DistanceProfilePeriodic { profile, overlap }
}

/// Measure the average two-point function between all pairs at a given distance using `function`.
///
/// Note: `dmatrix_group` should be a HDF5 group containing the `distance-matrix` and
/// `distance-distribution` groups as given by [`distance_matrix::compute_distance_matrix_disk()`].
/// And `T` should a type be chosen to be large enough to hold the largest distances in the distance
/// matrix, but as small as possible fohdf5::r the best performance.
pub fn measure_full_graph_from_disk<T>(dmatrix_group: &hdf5::Group) -> hdf5::Result<Array2<usize>>
where
    T: Copy + H5Type + num_traits::NumCast,
{
    let dmatrix_dataset = dmatrix_group.dataset(DISTANCE_MATRIX_DATASET_NAME)?;
    let ddist_dataset = dmatrix_group.dataset(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let distances: Array1<usize> = ddist_dataset.attr("r")?.read_1d()?;
    let n = dmatrix_dataset.shape()[0];
    let mut sphere_volumes = Array2::zeros([distances.len(), n]);
    for chunk in chunk_iter::<T>(&dmatrix_dataset) {
        for row in chunk.array.rows() {
            for (icol, &r) in row.indexed_iter() {
                sphere_volumes[(num_traits::cast::<_, usize>(r).unwrap(), icol)] += 1;
            }
        }
    }
    Ok(sphere_volumes)
}

const DEFAULT_MAXIMUM_DISPLAY_LENGTH_DISTANCE_PROFILE: usize = 30;

impl Display for DistanceProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let max_length = match f.width() {
            Some(width) => width,
            None => DEFAULT_MAXIMUM_DISPLAY_LENGTH_DISTANCE_PROFILE,
        };
        let max_count = *self
            .profile
            .iter()
            .max()
            .expect("Distance profile is empty") as f32;
        for (i, &count) in self.profile.iter().enumerate() {
            writeln!(
                f,
                "{:>2} ({:>3$}): {:=<4$}",
                i,
                count,
                "",
                max_count.log10() as usize + 1,
                ((count as f32) / max_count * (max_length as f32)).round() as usize
            )?;
        }
        Ok(())
    }
}
