//! This module contains an alternative implementation of a `Triangulation`
//! where the amount of triangles is kept fixed by using a different set of
//! Markov Chain moves.

use crate::collections::{Label as TypedLabel, LabelSet, LabelledArray};
use crate::triangulation::triangle::{Orientation, Triangle};
use rand::Rng;

use super::Triangulation;

type Label = TypedLabel<Triangle>;

/// `Triangulation` struct with a static `Triangle` list,
/// and an order-four list.
/// This static implementation is meant to be used with the
/// 'flip' and 'relocate' Markov-chain moves.
#[derive(Debug, Clone)]
pub struct FixedTriangulation {
    triangles: LabelledArray<Triangle>, // List of all triangles, representing the triangulation
    order_four: LabelSet<Triangle>, // Set of all order four vertices, labelled by the upper right triangle
}

impl<'a> Triangulation<'a> for FixedTriangulation {
    fn labels(&'a self) -> Box<dyn Iterator<Item = Label> + 'a> {
        Box::new(self.triangles.labels())
    }

    fn get(&self, label: Label) -> &Triangle {
        &self.triangles[label]
    }

    fn size(&self) -> usize {
        self.size()
    }

    fn max_size(&self) -> usize {
        self.size()
    }
}

/// Implementations for the creation of new `Triangulation`s
impl FixedTriangulation {
    /// Create a new fixed size flat `Triangulation` with
    /// `vertex_length` vertices in each timeslice, and
    /// `timeslices` amount of timeslices.
    pub fn new(vertex_length: usize, timeslices: usize) -> FixedTriangulation {
        let start_length = 2 * vertex_length; // Determine construction L
        let size = start_length * timeslices; // Determine N

        let mut triangles: Vec<Triangle> = Vec::with_capacity(size);
        for t in 0..timeslices {
            for i in 0..start_length {
                let left = Label::from(t * start_length + (i + (start_length - 1)) % start_length);
                let right = Label::from(t * start_length + (i + 1) % start_length);
                #[allow(clippy::collapsible_else_if)]
                // TODO: This nested if statement is one unreadable mess
                let (center, orientation) = if (i + t) % 2 == 0 {
                    if t == timeslices - 1 {
                        if timeslices % 2 == 0 {
                            (Label::from(i), Orientation::Down)
                        } else {
                            (Label::from(i + 1), Orientation::Down)
                        }
                    } else {
                        (Label::from((t + 1) * start_length + i), Orientation::Down)
                    }

                    // if t == (timeslices - 1) {
                    //     let shift = (2 * (timeslices / 2)) % start_length;
                    //     (
                    //         Label::from((i + 1 + (start_length - shift)) % start_length),
                    //         Orientation::Down,
                    //     )
                    // } else {
                    //     (
                    //         Label::from((t + 1) * start_length + i + 1),
                    //         Orientation::Down,
                    //     )
                    // }
                } else {
                    if t == 0 {
                        if timeslices % 2 == 0 {
                            (
                                Label::from((timeslices - 1) * start_length + i),
                                Orientation::Up,
                            )
                        } else {
                            (
                                Label::from((timeslices - 1) * start_length + i - 1),
                                Orientation::Up,
                            )
                        }
                    } else {
                        (Label::from((t - 1) * start_length + i), Orientation::Up)
                    }
                    // if t == 0 {
                    //     let shift = 2 * (timeslices / 2);
                    //     (
                    //         Label::from(
                    //             (timeslices - 1) * start_length + (i - 1 + shift) % start_length,
                    //         ),
                    //         Orientation::Up,
                    //     )
                    // } else {
                    //     (Label::from((t - 1) * start_length + i - 1), Orientation::Up)
                    // }
                };
                triangles.push(Triangle {
                    left,
                    center,
                    right,
                    orientation,
                });
            }
        }

        let order_four = LabelSet::new(size);

        FixedTriangulation {
            triangles: triangles.into(),
            order_four,
        }
    }
}

/// Implementation of the Markov-chain Monte Carlo moves
impl FixedTriangulation {
    /// Flip a pair of `Triangle`s given by the right `Triangle` `Label`.
    ///
    /// NOTE that this method checks whether the the given `Label` actually
    /// corresponds to a flippable pair, if not it `panic`s.
    pub fn flip(&mut self, label: Label) {
        assert!(self.is_flip_pair(label), "Attempted to flip a Triangle pair, of which the right Triangle is not in the flip_pair list");
        self.flip_centers(label, Self::add_order_four);
    }

    /// Flip a pair of `Triangle`s given by the right `Triangle` `Label`,
    /// without checking whether this `Label` corresponds to a flippable pair.
    ///
    /// NOTE that this methods breaks the `Triangulation` if called on a `Label`
    /// which does not correspond to a flip pair; **so only use if certain that
    /// `Label` corresponds to the right `Triangle` of a flip pair!**
    /// Also note that this implementation assumes that the order_four list
    /// correctly represents the order four vertices in the `Triangulation`, if
    /// this is not it can break the data structures.
    pub fn flip_checkless(&mut self, label: Label) {
        self.flip_centers(label, Self::add_order_four_checkless);
    }

    /// Update the connectivity for flipping a `Triangle` pair, labelled by right `Triangle`.
    ///
    /// This flip is performed by by keeping left left, and right right, but flipping the orientations
    /// and updating the `center` neighbours; so effectively _flipping the `center`s_
    /// Alternatively one can change left to right and visa versa, and update the connectivity accordingly;
    /// NOTE that this also affects the possible update of what `Triangle` becomes the flip pair.
    fn flip_centers(
        &mut self,
        right: Label,
        add_order_four: fn(&mut FixedTriangulation, Label) -> bool,
    ) {
        // Find flip pair neighbour
        let left = self.triangles[right].left;

        // Flip orientations
        let orientation_left = self.triangles[left].orientation;
        self.triangles[left].orientation = self.triangles[right].orientation;
        self.triangles[right].orientation = orientation_left;

        // Flip `center` neighbour connectivity
        let neighbour_left = self.triangles[left].center; // Find old timelike neighbours
        let neighbour_right = self.triangles[right].center;
        self.triangles[left].center = neighbour_right; // Set new neighbours
        self.triangles[right].center = neighbour_left;
        self.triangles[neighbour_left].center = right;
        self.triangles[neighbour_right].center = left;

        // Re-evaluate the order_four list
        let slice_neighbour_right = self.triangles[right].right; // Right spacelike of the right triangle of the pair
        match orientation_left {
            Orientation::Up => {
                // Up-Down -> Down-Up
                let neighbour_top_right = self.triangles[neighbour_right].right; // Top right triangle
                self.order_four.remove(neighbour_top_right); // These two could have been order four,
                self.order_four.remove(left); // but certainly are not after the flip.
                add_order_four(self, neighbour_right); // These two could have become order four after
                add_order_four(self, slice_neighbour_right); // the flip, but can't have been before.
            }
            Orientation::Down => {
                // Down-Up -> Up-Down
                let neighbour_top_right = self.triangles[neighbour_left].right; // Top right triangle
                self.order_four.remove(neighbour_left); // These two could have been order four,
                self.order_four.remove(slice_neighbour_right); // but certainly are not after the flip.
                add_order_four(self, neighbour_top_right); // These two could have become order four after
                add_order_four(self, left); // the flip, but can't have been before.
            }
        }
    }

    /// Check if `label` is order four, based on connectivity.
    /// If this is so add it to `order_four` list, and return if it is.
    fn add_order_four(triangulation: &mut FixedTriangulation, label: Label) -> bool {
        if triangulation.is_order_four(label) {
            triangulation.order_four.add(label);
            true
        } else {
            false
        }
    }

    /// Add given `Label` to `order_four` list _if it is_ based on
    /// it's connectivity, returning whether it is of order four.
    ///
    /// NOTE: this does not check if the `label` was in the `order_four`
    /// before, and this will break the list if it was.
    /// So **only use this if certain that the `Triangle` wasn't in the
    /// `order_four` list before**, if uncertain use
    /// [`add_order_four()`](Triangulation::add_order_four())
    fn add_order_four_checkless(triangulation: &mut FixedTriangulation, label: Label) -> bool {
        if triangulation.is_order_four(label) {
            triangulation.order_four.add_checkless(label);
            true
        } else {
            false
        }
    }

    /// Relocate move which moves a shard `from` order four `to` the
    /// right of a new shard.
    ///
    /// So `from` should label a `Triangle` marking an order four vertex,
    /// and `to` should label any shard to the right of which one wants
    /// to move to.
    /// NOTE that this function check whether `from` indeed marks an
    /// order-four vertex, if not it `panic`s.
    ///
    /// Also note that if `from` points to the same shard as `to` no relocation happens,
    /// and no change is made to the triangulation.
    pub fn relocate(&mut self, from: Label, to: Label) {
        // Check that `from` labels an order four vertex
        assert!(self.order_four.contains(from));
        self.relocate_checkless(from, to);
    }

    /// Relocate move, which moves a shard `from` order four `to` the
    /// right of a new shard.
    ///
    /// So `from` should label a `Triangle` marking an order four vertex,
    /// and `to` should label any shard to the right of which one wants
    /// to move to.
    /// NOTE that this function DOES NOT check whether `from` indeed marks an
    /// order-four vertex, if not this function can break the `Triangulation`.
    /// When unsure use [`relocate()`](FixedTriangulation::relocate()) instead.
    ///
    /// Also note that if `from` points to the same shard as `to` no relocation happens,
    /// and no change is made to the triangulation.
    pub fn relocate_checkless(&mut self, from: Label, to: Label) {
        // Identify the `to` shard
        let (to_up, to_down) = match self.triangles[to].orientation {
            Orientation::Up => (to, self.triangles[to].center),
            Orientation::Down => (self.triangles[to].center, to),
        };

        // Simply return and do nothing is `from` shard
        if from == to_up {
            return;
        }

        // Identify shard triangles
        let shard_up = from;
        let shard_down = self.triangles[shard_up].center;

        // Identify the old neighbours
        let old_left_up = self.triangles[shard_up].left;
        let old_left_down = self.triangles[shard_down].left;
        let old_right_up = self.triangles[shard_up].right;
        let old_right_down = self.triangles[shard_down].right;

        // Move the shard out, patching the neighours
        self.triangles[old_left_up].right = old_right_up;
        self.triangles[old_left_down].right = old_right_down;
        self.triangles[old_right_up].left = old_left_up;
        self.triangles[old_right_down].left = old_left_down;

        // Identify the new neighbours
        let new_left_up = to_up;
        let new_left_down = to_down;
        let new_right_up = self.triangles[new_left_up].right;
        let new_right_down = self.triangles[new_left_down].right;

        // Move the shard into the right of `to`.
        self.triangles[new_left_up].right = shard_up;
        self.triangles[new_left_down].right = shard_down;
        self.triangles[new_right_up].left = shard_up;
        self.triangles[new_right_down].left = shard_down;

        self.triangles[shard_up].left = new_left_up;
        self.triangles[shard_up].right = new_right_up;
        self.triangles[shard_down].left = new_left_down;
        self.triangles[shard_down].right = new_right_down;

        // Note that no updating of the order four list is necessary,
        // because no neigbouring triangles get their order-fourness changed,
        // and the triangle itself moves along so stays order four.
    }
}

/// Implementations for sampling the triangulation
impl FixedTriangulation {
    /// Returns iterator over the triangles in the triangulation
    pub fn triangles(&self) -> crate::collections::labelled_array::Iter<Triangle> {
        self.triangles.iter()
    }

    /// Sample a random `Triangle` from `Triangulation` by `Label`
    ///
    /// This sampling makes use of the sampling of `LabelledList`,
    /// making it less efficient that the samplings of [`sample_flip()`](FixedTriangulation::sample_flip())
    /// and [`sample_order_four()`](FixedTriangulation::sample_order_four())
    pub fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        Label::from(rng.gen_range(0..self.size()))
    }

    /// Sample a random `Triangle` which labels a flip pair by `Label`.
    ///
    /// This is a simple rejection sampling, which uniformly samples all triangles
    /// and returns the `Label` of the first triangle labelling a flip_pair.
    /// The average rejection rate seems to be about 50%, but check this yourself if important.
    pub fn sample_flip<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        let mut label = self.sample(rng);
        while !self.is_flip_pair(label) {
            label = self.sample(rng);
        }
        label
    }

    /// Sample a random vertex of order four by the `Label` of the `Triangle`
    /// at the top right of the vertex.
    ///
    /// This sampling makes use of an extra list using `LabelSet`, making this sampling very efficient.
    pub fn sample_order_four<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        self.order_four.sample(rng)
    }
}

/// Implementation of check for special types of structures
impl FixedTriangulation {
    /// Check if connectivity of given `Up` `Label` matches that of
    /// one marking an order four vertex.
    ///
    /// NOTE that the given `Label` must point to an `Up`-`Triangle`
    /// otherwise one could get **false positives**; use
    /// [`is_order_four()`](FixedTriangulation::is_order_four()) for a safe
    /// check, which also checks the orientation.
    /// This is because the used convention is to mark the order four
    /// vertex with the upper right `Triangle`.
    /// This check does assume the `Triangulation` connectivity
    /// itself is correct.
    pub fn is_up_order_four(&self, label: Label) -> bool {
        // Check if 'left-down' is the same as 'down-left'
        let left = self.triangles[label].left;
        let down = self.triangles[label].center;
        self.triangles[left].center == self.triangles[down].left
    }

    /// Check if connectivity of given `Label` matches that of
    /// one marking an order four vertex.
    ///
    /// Note that the used convention is to mark the order four
    /// vertex with the upper right `Triangle`.
    /// This check does assume the `Triangulation` connectivity
    /// itself is correct.
    pub fn is_order_four(&self, label: Label) -> bool {
        if self.triangles[label].orientation == Orientation::Down {
            return false;
        }
        self.is_up_order_four(label)
    }

    /// Returns whether the `Triangle` pair given by the `Label` of the right `Triangle`
    /// forms a flip_pair, which is to say the orientations are opposite.
    pub fn is_flip_pair(&self, label: Label) -> bool {
        let left = self.triangles[label].left;
        self.triangles[label].orientation != self.triangles[left].orientation
    }

    /// Returns whether there are order four vertices in the `Triangulation`.
    pub fn has_order_four(&self) -> bool {
        !self.order_four.is_empty()
    }
}

/// Some sort of measurement implementations
impl FixedTriangulation {
    /// Returns the amount of `Triangle`s in the `Triangulation`.
    pub fn size(&self) -> usize {
        self.triangles.len()
    }

    /// Returns the order of the vertex associated with the `Triangle` given by `Label`.
    ///
    /// The vertex associated with a `Triangle` is the left-most vertex of a triangle,
    /// meaning the left vertex of the two vertices that are together in a timeslice.
    ///
    /// Note that this measurements requires the `Triangulation` to be setup correctly.
    pub fn vertex_order(&self, label: Label) -> usize {
        let (up, down) = match self.triangles[label].orientation {
            Orientation::Up => (label, self.triangles[label].center),
            Orientation::Down => (self.triangles[label].center, label),
        };
        let mut order: usize = 4; // Start at order 4, and see how many extra triangles there are
        let mut left = self.triangles[up].left;
        while self.triangles[left].orientation != Orientation::Up {
            left = self.triangles[left].left;
            order += 1;
        }
        left = self.triangles[down].left;
        while self.triangles[left].orientation != Orientation::Down {
            left = self.triangles[left].left;
            order += 1;
        }
        order
    }

    /// Return the amount of triangles that label an order four vertex
    pub fn order_four_count(&self) -> usize {
        self.order_four.len()
    }
}

#[cfg(test)]
mod tests {
    // Note some of these test use RNG, which is done with a reproducable RNG using a fixed seed
    use super::*;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    impl<T> LabelledArray<T> {
        fn contains(&self, label: Label) -> bool {
            usize::from(label) < self.len()
        }
    }
    /// Implementations for `Triangulation` verification
    impl FixedTriangulation {
        /// Perform possible check to assess the validity of the `Triangulation`.
        ///
        /// If the `Triangulation` is not valid something has gone wrong in construction,
        /// and unexpected behaviour can occur, so this function `panic`s.
        /// The validity is assessed by [`assert_biconnectivity()`](Triangulation::assert_biconnectivity()),
        /// [`assert_order_four()`](Triangulation::assert_order_four()),
        /// and [`assert_flip_pair()`](Triangulation::assert_flip_pair()).
        fn assert_triangulation(&self) {
            self.assert_biconnectivity();
            self.assert_order_four();
        }

        /// Check whether all set connections of Triangles, are correctly set in reverse, `panic`s if not.
        fn assert_biconnectivity(&self) {
            let triangles = &self.triangles;
            for label in triangles.labels() {
                // Get all neighbours
                let neighbour_left = triangles[label].left;
                let neighbour_center = triangles[label].center;
                let neighbour_right = triangles[label].right;
                // Check connections of neighbours to original triangle
                assert_eq!(
                    triangles[neighbour_left].right, label,
                    "Connection between {} and left neighbour {} is not correct",
                    label, neighbour_left
                );
                assert_ne!(
                    triangles[label].orientation, triangles[neighbour_center].orientation,
                    "Timelike coupling on non Up-Down pair {} and {}",
                    label, neighbour_center
                );
                assert_eq!(
                    triangles[neighbour_center].center, label,
                    "Connection between {} {:?} and center neighbour {} is not correct",
                    label, triangles[label], neighbour_center
                );
                assert_eq!(
                    triangles[neighbour_right].left, label,
                    "Connection between {} and right neighbour {} is not correct",
                    label, neighbour_right
                );
            }
        }

        /// Check whether order_four list is correct and complete, `panic`s if not.
        fn assert_order_four(&self) {
            // First check if `order_four` list doesn't contain non-existent triangles
            for label in self.order_four.iter() {
                assert!(
                    self.triangles.contains(label),
                    "A non-existent Triangle is present in the order four list."
                );
            }

            // Check all Triangles for order four-ness and compare to order four list
            for label in self.triangles.labels() {
                if self.is_order_four(label) {
                    assert!(
                        self.order_four.contains(label),
                        "Triangle {} is order four, but not in the order_four list",
                        label
                    );
                } else {
                    assert!(
                        !self.order_four.contains(label),
                        "Triangle {} is not order four, but is in the order_four list",
                        label
                    );
                }
            }
        }
    }

    #[test]
    fn triangulation_construction() {
        let triangulation = FixedTriangulation::new(8, 11);
        // let triangulation = FixedTriangulation::new(500, 120);
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four.len(),
            0,
            "Triangulation started out with some order four Triangles"
        );
    }

    #[test]
    fn triangulation_flip() {
        let mut triangulation = FixedTriangulation::new(8, 11);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four_count(),
            7,
            "Triangulation has an unexpected amount of order_four vertices"
        );

        for _ in 0..10000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four_count(),
            18,
            "Triangulation has an unexpected amount of order_four vertices"
        );
    }

    #[test]
    fn triangulation_relocation() {
        let mut triangulation = FixedTriangulation::new(8, 11);
        triangulation.assert_triangulation();

        let mut rng = Xoshiro256StarStar::seed_from_u64(13);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four_count(),
            7,
            "Triangulation has an unexpected amount of order_four vertices"
        );

        let order_four_before = triangulation.order_four_count();
        triangulation.relocate(
            triangulation.sample_order_four(&mut rng),
            triangulation.sample(&mut rng),
        );
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four_count(),
            order_four_before,
            "The amount of order four triangles has changed after performing a relocation move"
        );

        for _ in 0..10000 {
            let from = triangulation.sample_order_four(&mut rng);
            let to = triangulation.sample(&mut rng);
            triangulation.relocate(from, to);
        }
        triangulation.assert_triangulation();
        assert_eq!(
            triangulation.order_four_count(),
            order_four_before,
            "The amount of order four triangles has changed after performing a relocation move"
        );

        for _ in 0..10000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.relocate(
                triangulation.sample_order_four(&mut rng),
                triangulation.sample(&mut rng),
            );
        }
    }

    #[test]
    fn check_randomness() {
        let mut rng0 = Xoshiro256StarStar::seed_from_u64(137);
        let mut rng1 = rng0.clone();

        let mut triangulation0 = FixedTriangulation::new(20, 15);
        for _ in 0..10_000 {
            triangulation0.flip(triangulation0.sample_flip(&mut rng0));
        }
        for _ in 0..1_000 {
            triangulation0.flip(triangulation0.sample_flip(&mut rng0));
            triangulation0.relocate(
                triangulation0.sample_order_four(&mut rng0),
                triangulation0.sample(&mut rng0),
            )
        }

        let mut triangulation1 = FixedTriangulation::new(20, 15);
        for _ in 0..10_000 {
            triangulation1.flip(triangulation1.sample_flip(&mut rng1));
        }
        for _ in 0..1_000 {
            triangulation1.flip(triangulation1.sample_flip(&mut rng1));
            triangulation1.relocate(
                triangulation1.sample_order_four(&mut rng1),
                triangulation1.sample(&mut rng1),
            )
        }

        for label in triangulation0.labels() {
            assert_eq!(triangulation0.get(label), triangulation1.get(label))
        }
    }

    #[test]
    fn triangulaton_vertex_order() {
        let mut triangulation = FixedTriangulation::new(8, 11);
        let mut rng = Xoshiro256StarStar::seed_from_u64(12);

        // Check if flat indeed means vertex order is 6 everywhere
        for _ in 0..20 {
            let label = triangulation.sample(&mut rng);
            assert_eq!(
                triangulation.vertex_order(label),
                6,
                "Vertex order of Triangle in starting Triangulation is not 6"
            );
        }

        // Perform some moves to get a spread of vertex orders
        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..1000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.relocate(
                triangulation.sample_order_four(&mut rng),
                triangulation.sample(&mut rng),
            )
        }
        triangulation.assert_triangulation();

        // Check if vertex_order() indeed gives vertex order 4 for all those in order_four
        for label in triangulation.order_four.iter() {
            assert_eq!(
                triangulation.vertex_order(label),
                4,
                "Vertex order of Triangle in order_four list is not 4"
            );
        }
    }
}
