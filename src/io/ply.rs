//! PLY generation for 2D meshes

use std::fmt::Display;

const FORMAT: &str = "ascii 1.0";
const COMMENT: &str = "Mesh of CDT triangulation";

/// Struct for holding PLY information
/// Note that Triangle labels are stored in clockwise order
pub struct Ply {
    vertices: Vec<(f64, f64)>,
    edges: Vec<(usize, usize)>,
}

impl Ply {
    pub fn new(vertices: Vec<(f64, f64)>, edges: Vec<(usize, usize)>) -> Self {
        Self { vertices, edges }
    }
}

impl Display for Ply {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Write header
        writeln!(f, "ply")?;
        writeln!(f, "format {}", FORMAT)?;
        writeln!(f, "comment {}", COMMENT)?;

        writeln!(f, "element vertex {}", self.vertices.len())?;
        writeln!(f, "property float x")?;
        writeln!(f, "property float y")?;

        writeln!(f, "element edge {}", self.edges.len())?;
        writeln!(f, "property uint index0")?;
        writeln!(f, "property uint index1")?;

        writeln!(f, "end_header")?;

        for vertex in self.vertices.iter() {
            writeln!(f, "{} {}", vertex.0, vertex.1)?;
        }

        for edge in self.edges.iter() {
            writeln!(f, "{} {}", edge.0, edge.1)?;
        }

        Ok(())
    }
}
