//! Implementation of graph traits for the [`Periodic2DGenericGraph`], see that for more details.

use std::cmp::Ordering;
use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

use super::field::{Field as FieldTrait, FieldCollection, FieldValue, PeriodicField};
use super::periodic1d_generic_graph::i8usize_central_mapping;
use super::periodic_graph::{IterPeriodicGraph, PeriodicGraph, PeriodicLabel, PeriodicLabelMatch};
use super::IterableGraph;
use super::{generic_graph::Node, FieldGraph, Graph, SampleGraph};
use crate::collections::{Label, LabelledArray};

pub mod bounded_graph;

/// [Label](Graph::Label) used to label nodes of [`Periodic2DGenericGraph`]
///
/// Using [`Patch2D`] as a patch label and [`Label`] a a node label in each patch
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Periodic2DLabel {
    label: Label<Node>,
    patch: Patch2D,
}

/// A patch label for [`Periodic2DGenericGraph`]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Patch2D {
    x: i8,
    y: i8,
}

impl super::Label for Periodic2DLabel {}

impl PeriodicLabel for Periodic2DLabel {
    type BaseLabel = Label<Node>;

    #[inline]
    fn cmp(self, other: Self) -> PeriodicLabelMatch {
        if self.label != other.label {
            return PeriodicLabelMatch::None;
        }
        if self.patch != other.patch {
            return PeriodicLabelMatch::Patch;
        }
        PeriodicLabelMatch::Equal
    }

    #[inline]
    fn base_label(self) -> Self::BaseLabel {
        self.label
    }
}

/// A list over the links of a node to keep track of which links are in a boundary
///
/// This is simply implemented as a wrapper over a [`Vec`]
#[derive(Debug, Clone)]
pub struct NodeLinks<T>(pub Vec<T>);

/// A graph structure that has no additional local information but has a global topology of an
/// infinite plane, with a periodically repeating structure.
///
/// This is achieved by identifying left, up, right, and down boundaries of a single graph patch
/// and making it infinite by allowing infinite copies of this base patch, stiched together along
/// these boundaries.
///
/// At the moment (14 Sep 2023) there is no model implemented in this library to create such a
/// graph. Likely, the only way you will use this graph type is by importing a boundary with
/// [crate::io::graph::import_boundary2d_adjacency_list].
#[derive(Debug, Clone)]
pub struct Periodic2DGenericGraph {
    nodes: LabelledArray<Node>,
    bounds: LabelledArray<Option<NodeLinks<Boundary2D>>, Label<Node>>,
}

/// Enum keeping tracking of the type of boundary of a link in a graph.
///
/// The enums use cardinal directions. A boundary link being one of these enum variants means that
/// the directed link ends up in a patch that is connected to the starting patch in that direction.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(missing_docs)]
pub enum Boundary2D {
    Center,
    West,
    NorthWest,
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
}

/// Implementation is achieved by mapping `i8` to `usize` in such a way that:
/// (0, -1, 1, -2, 2, -3, ...) -> (0, 1, 2, 3, 4, 5, 6, ...)
/// This results in the internal vector being able to be small since patches will usually be
/// only -1, 0, or 1; and it will grow in both directions in 2D.
///
/// Note: the amount of items in the first dimension is fixed with respect to the position in
/// the second dimension, while the amount of items in the second dimension can very at
/// different positions; choose which is more convient to use in the given usecase.
pub type Field<T> = FieldCollection<T, Vec<Vec<LabelledArray<FieldValue<T>, Label<Node>>>>>;

/// Iterator over the neighbours of a node in [`Periodic2DGenericGraph`]
#[derive(Debug, Clone)]
pub struct IterNeighbours<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<Boundary2D>>,
    patch: Patch2D,
    cursor: usize,
}

impl Periodic2DGenericGraph {
    /// Create a new [`Periodic2DGenericGraph`] from a set of given `nodes` and `bounds`
    pub fn new(
        nodes: LabelledArray<Node>,
        bounds: LabelledArray<Option<NodeLinks<Boundary2D>>, Label<Node>>,
    ) -> Self {
        Self { nodes, bounds }
    }

    /// Creates a [`Periodic2DGenericGraph`] from `nodes` and boundaries given by a list of
    /// [`Label`]and [`NodeLinks<Boundary2D>`] pairs as tuples. Checking if the [`NodeLinks`]
    /// match the links of the corresponding [`Node`].
    pub fn from_nodes_and_boundary_vec(
        nodes: LabelledArray<Node>,
        bound_list: Vec<(Label<Node>, NodeLinks<Boundary2D>)>,
    ) -> Result<Self, NodeBoundaryError> {
        let mut bounds = LabelledArray::fill_with(|| None, nodes.len());
        for (label, bound) in bound_list {
            if nodes[label].vertex_degree() != bound.0.len() {
                return Err(NodeBoundaryError);
            }
            bounds[label] = Some(bound);
        }

        Ok(Self { nodes, bounds })
    }
}

/// Connectivity error a boundary
///
/// This signifies that a graph has been attempted to be created with some boundaries and node
/// connectivity that does not match one another.
#[derive(Debug, Clone, Copy)]
pub struct NodeBoundaryError;

impl Display for NodeBoundaryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Discrepancy between connectivity of the nodes and the boundaries."
        )
    }
}

impl Error for NodeBoundaryError {}

impl Graph for Periodic2DGenericGraph {
    type Label = Periodic2DLabel;

    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.nodes[label.label].vertex_degree()
    }
}

impl SampleGraph for Periodic2DGenericGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        Periodic2DLabel {
            label: self.nodes.sample_label(rng),
            patch: Patch2D { x: 0, y: 0 },
        }
    }
}

impl FieldGraph for Periodic2DGenericGraph {
    type FieldType<T> = Field<T>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        FieldCollection::new(vec![vec![LabelledArray::fill_with(
            || FieldValue::None,
            self.nodes.len(),
        )]])
    }
}

impl<T> FieldTrait<T, Periodic2DGenericGraph> for Field<T> {
    fn get(&self, label: Periodic2DLabel) -> &FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.x);
        let inner_index = i8usize_central_mapping(label.patch.y);
        match self
            .0
            .get(outer_index)
            .and_then(|list| list.get(inner_index))
        {
            None => self.none(),
            Some(values) => &values[label.label],
        }
    }

    fn get_mut(&mut self, label: Periodic2DLabel) -> &mut FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.x);
        let inner_index = i8usize_central_mapping(label.patch.y);
        let label = label.label;
        let node_count = self
            .0
            .first()
            .and_then(|list| list.first())
            .expect("First element should be initialized by creation")
            .len();

        if self.0.len() <= outer_index {
            self.0.resize_with(outer_index + 1, Vec::new);
        }
        let inner_list = &mut self.0[outer_index];
        if inner_list.len() <= inner_index {
            inner_list.resize_with(inner_index + 1, || {
                LabelledArray::fill_with(|| FieldValue::None, node_count)
            });
        };

        &mut inner_list[inner_index][label]
    }
}

impl<T> PeriodicField<T, Periodic2DGenericGraph> for Field<T> {
    /// This field collection has been implemented as a collection of LabelledArrays, so checking
    /// for collisions in other patches is relatively expensive
    fn has(&self, label: Periodic2DLabel) -> PeriodicLabelMatch {
        if FieldTrait::<T, Periodic2DGenericGraph>::get(self, label).is_some() {
            return PeriodicLabelMatch::Equal;
        }
        let outer = &self.0;
        if outer
            .iter()
            .flat_map(|inner| inner.iter())
            .any(|array| array[label.label].is_some())
        {
            return PeriodicLabelMatch::Patch;
        }
        PeriodicLabelMatch::None
    }

    /// This field collection has been implemented as a collection of LabelledArrays, so checking
    /// for collisions in other patches is relatively expensive
    fn has_in_patches(&self, label: Label<Node>) -> usize {
        let outer = &self.0;
        return outer
            .iter()
            .flat_map(|inner| inner.iter())
            .filter(|array| array[label].is_some())
            .count();
    }

    fn has_patch(&self, patch: Patch2D) -> bool {
        let outer_index = i8usize_central_mapping(patch.x);
        let inner_index = i8usize_central_mapping(patch.y);
        self.0
            .get(outer_index)
            .and_then(|inner| inner.get(inner_index))
            .is_some()
    }
}

impl<'a> IterableGraph<'a, IterNeighbours<'a>> for Periodic2DGenericGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighbours<'a> {
        IterNeighbours::from_graph(self, label)
    }
}

impl<'a> IterNeighbours<'a> {
    /// Start iterator at index 0 going around clockwise
    fn new(node: &'a Node, bound: Option<&'a NodeLinks<Boundary2D>>, patch: Patch2D) -> Self {
        IterNeighbours {
            node,
            bound,
            patch,
            cursor: 0,
        }
    }

    fn from_graph(graph: &'a Periodic2DGenericGraph, label: Periodic2DLabel) -> Self {
        Self::new(
            &graph.nodes[label.label],
            graph.bounds[label.label].as_ref(),
            label.patch,
        )
    }

    #[inline]
    fn get_bound(&self, link: usize) -> Boundary2D {
        if let Some(bounds) = self.bound {
            bounds.0[link]
        } else {
            Boundary2D::Center
        }
    }
}

impl<'a> Iterator for IterNeighbours<'a> {
    type Item = Periodic2DLabel;

    fn next(&mut self) -> Option<Self::Item> {
        let label = self.node.get_neighbour(self.cursor)?;
        let bound = self.get_bound(self.cursor);
        self.cursor += 1;
        let patch = match bound {
            Boundary2D::Center => self.patch,
            Boundary2D::West => self.patch.add_x(-1),
            Boundary2D::NorthWest => self.patch.add(-1, 1),
            Boundary2D::North => self.patch.add_y(1),
            Boundary2D::NorthEast => self.patch.add(1, 1),
            Boundary2D::East => self.patch.add_x(1),
            Boundary2D::SouthEast => self.patch.add(1, -1),
            Boundary2D::South => self.patch.add_y(-1),
            Boundary2D::SouthWest => self.patch.add(-1, -1),
        };
        Some(Periodic2DLabel { label, patch })
    }
}

impl PeriodicGraph for Periodic2DGenericGraph {
    type Patch = Patch2D;

    #[inline]
    fn base_len(&self) -> usize {
        self.nodes.len()
    }
}

/// Iterator over all node labels in a given patch of [`Periodic2DGenericGraph`]
pub struct PatchIterator {
    current: Label<Node>,
    max: Label<Node>,
    patch: Patch2D,
}

impl Iterator for PatchIterator {
    type Item = Periodic2DLabel;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current.cmp(&self.max) {
            Ordering::Less => (),
            Ordering::Equal => return None,
            Ordering::Greater => panic!("Somehow the iterator went past max"),
        }

        let next_label = self.current.succ();
        let old_label = std::mem::replace(&mut self.current, next_label);
        Some(Periodic2DLabel::new(old_label, self.patch))
    }
}

impl IterPeriodicGraph<PatchIterator> for Periodic2DGenericGraph {
    fn iter_base(&self) -> PatchIterator {
        Self::iter_patch(self, Patch2D::origin())
    }

    fn iter_patch(&self, patch: Patch2D) -> PatchIterator {
        PatchIterator {
            current: Label::initial(),
            max: Label::from(self.base_len()),
            patch,
        }
    }
}

impl Periodic2DLabel {
    /// Create a new [`Periodic2DLabel`] from a `patch` label and a node `label` within the patch
    pub fn new(label: Label<Node>, patch: Patch2D) -> Self {
        Self { label, patch }
    }
}

impl Patch2D {
    /// Create a new patch label corresponding to patch coordinates `x` and `y`
    ///
    /// Note that `(x, y) = (0, 0)` corresponds to the original patch
    pub fn new(x: i8, y: i8) -> Self {
        Self { x, y }
    }

    /// Create a origin patch, that is (0, 0)
    pub fn origin() -> Self {
        Self { x: 0, y: 0 }
    }

    #[inline]
    fn add(&self, x: i8, y: i8) -> Self {
        Self {
            x: self.x + x,
            y: self.y + y,
        }
    }

    #[inline]
    fn add_x(&self, x: i8) -> Self {
        Self {
            x: self.x + x,
            y: self.y,
        }
    }

    #[inline]
    fn add_y(&self, y: i8) -> Self {
        Self {
            x: self.x,
            y: self.y + y,
        }
    }

    fn modulo(&self, xmax: i8, ymax: i8) -> Self {
        let xmod = 2 * xmax + 1;
        let ymod = 2 * ymax + 1;
        Self {
            x: ((self.x + xmax) % xmod + xmod) % xmod - xmax,
            y: ((self.y + ymax) % ymod + ymod) % ymod - ymax,
        }
    }
}

impl Periodic2DLabel {
    /// Create a new [`Periodic2DLabel`] at the origin patch
    pub fn new_origin(label: Label<Node>) -> Self {
        Self {
            label,
            patch: Patch2D { x: 0, y: 0 },
        }
    }
}

impl Display for Periodic2DLabel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}({},{})", self.label, self.patch.x, self.patch.y)
    }
}

/// Error signifying an error in parsing a character to a [`Boundary2D`]
#[derive(Debug, Clone)]
pub struct ParseBoundary2DError;

impl Display for ParseBoundary2DError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Only the symbols 'C'/'W'/'NW'/'N'/'NE'/'E'/'SE'/'S'/'SW'"
        )
    }
}

impl Error for ParseBoundary2DError {}

impl FromStr for Boundary2D {
    type Err = ParseBoundary2DError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let bound = match s.to_ascii_uppercase().as_str() {
            "C" => Self::Center,
            "W" => Self::West,
            "NW" => Self::NorthWest,
            "N" => Self::North,
            "NE" => Self::NorthEast,
            "E" => Self::East,
            "SE" => Self::SouthEast,
            "S" => Self::South,
            "SW" => Self::SouthWest,
            _ => return Err(ParseBoundary2DError),
        };
        Ok(bound)
    }
}

impl FromStr for NodeLinks<Boundary2D> {
    type Err = ParseBoundary2DError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut bounds: Vec<Boundary2D> = Vec::new();
        for bound in s.split(',') {
            bounds.push(bound.trim().parse()?)
        }
        Ok(Self(bounds))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::breadth_first_search::traverse_nodes::{
        TraverseNodeDistance, TraverseNodeLabel,
    };
    use crate::io::graph;
    use indoc::indoc;
    use std::io::Cursor;

    const EXAMPLE_GRAPH: &str = indoc! {"
        8, 5, 9, 1, 2, 10
        0, 9, 2, 4, 3, 6, 5, 2
        9, 10, 0, 1, 5, 4, 1
        1, 4, 7, 6
        2, 5, 8, 10, 7, 3
        2, 1, 6, 9, 0, 8, 4
        1, 3, 7, 9, 5
        3, 4, 10, 9, 6
        4, 5, 0, 10
        6, 7, 10, 2, 1, 0, 5
        7, 4, 8, 0, 2, 9
    "};
    const EXAMPLE_BOUND: &str = "C, NW, N, N, NE, E";
    const EXAMPLE_BOUNDS: [(usize, &str); 7] = [
        (1, "NW, N, C, C, C, C, W, W"),
        (2, "N, N, N, E, C, C, C"),
        (5, "C, E, E, E, C, C, C"),
        (6, "C, C, C, C, W"),
        (9, "C, C, C, S, S, W, W"),
        (10, "C, C, C, C, S, C"),
        (0, "C, C, E, SE, S, C"),
    ];

    #[test]
    fn parse_example_bound() {
        let bound: NodeLinks<Boundary2D> = EXAMPLE_BOUND.parse().unwrap();
        println!("{:?}", bound);
    }

    fn parse_graph() -> Periodic2DGenericGraph {
        let nodes = graph::import_adjacency_list(Cursor::new(EXAMPLE_GRAPH)).unwrap();
        let mut bounds: LabelledArray<Option<NodeLinks<Boundary2D>>, Label<Node>> =
            LabelledArray::fill_with(|| None, nodes.len());
        for (label_index, bound_str) in EXAMPLE_BOUNDS {
            bounds[Label::from(label_index)] = Some(bound_str.parse().unwrap());
        }
        Periodic2DGenericGraph::new(nodes, bounds)
    }

    #[test]
    fn parsing_test() {
        parse_graph();
    }

    #[test]
    fn bfs_plain() {
        let graph = parse_graph();
        let origin = Periodic2DLabel::new_origin(Label::from(7));

        // Display search results
        println!("Plain breadth first search");
        for node in graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .take(20)
        {
            print!("{} ", node.label)
        }
        println!();
        // Assert search
        let expected_search: [(usize, (i8, i8)); 17] = [
            (7, (0, 0)),
            (3, (0, 0)),
            (4, (0, 0)),
            (10, (0, 0)),
            (9, (0, 0)),
            (6, (0, 0)),
            (1, (0, 0)),
            (2, (0, 0)),
            (5, (0, 0)),
            (8, (0, 0)),
            (0, (0, 0)),
            (2, (0, -1)),
            (1, (0, -1)),
            (0, (-1, 0)),
            (5, (-1, 0)),
            (0, (-1, 1)),
            (9, (0, 1)),
        ];
        for (left, right) in graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .map(|node| {
                let label = node.label;
                (usize::from(label.label), (label.patch.x, label.patch.y))
            })
            .zip(expected_search.into_iter())
        {
            assert_eq!(left, right)
        }
    }

    #[test]
    fn bfs_distance() {
        let graph = parse_graph();
        let origin = Periodic2DLabel::new_origin(Label::from(7));

        // Assert search
        let expected_search: [(usize, (i8, i8), usize); 17] = [
            (7, (0, 0), 0),
            (3, (0, 0), 1),
            (4, (0, 0), 1),
            (10, (0, 0), 1),
            (9, (0, 0), 1),
            (6, (0, 0), 1),
            (1, (0, 0), 2),
            (2, (0, 0), 2),
            (5, (0, 0), 2),
            (8, (0, 0), 2),
            (0, (0, 0), 2),
            (2, (0, -1), 2),
            (1, (0, -1), 2),
            (0, (-1, 0), 2),
            (5, (-1, 0), 2),
            (0, (-1, 1), 3),
            (9, (0, 1), 3),
        ];
        for (left, right) in graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .map(|node| {
                let label = node.label;
                (
                    usize::from(label.label),
                    (label.patch.x, label.patch.y),
                    node.distance,
                )
            })
            .zip(expected_search.into_iter())
        {
            assert_eq!(left, right)
        }
    }
}
