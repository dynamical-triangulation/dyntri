//! Many graph traits and various types of graphs implementing those traits.
//!
//! Many graph traits are defined in this module, which are used in the
//! [`observables`](crate::observables) module to be generic over the graphs on which the
//! measurement is performed.
//! One can implement these traits on their own graph type or use one of the provided graph types.
//!
//! Note that the `spacetime` graphs have a local notion of a preferred direction, in the sense that
//! each edge is either in the time-forward, time-backward, left or right direction. This is used to
//! preserve that information from the causal triangulations we have in CDT.
//! The `generic` graphs do not have the feature and are just a standard graph.
//! Notably, there are some `periodic` graph types that can be used to represent infinite graphs
//! with a repeating structure.

#![allow(missing_docs)]

pub mod breadth_first_search;
pub mod field;

pub mod generic_graph;
pub mod periodic1d_generic_graph;
pub mod periodic2d_generic_graph;
pub mod periodic_graph;
pub mod periodic_spacetime_graph;
pub mod spacetime_graph;
pub mod timeperiodic_spacetime_graph;

use crate::collections::Label as LabelStruct;

use self::breadth_first_search::{IterBreadthFirst, TraverseNode};
use self::field::Field;
use rand::Rng;

// Re-exporting
pub use generic_graph::GenericGraph;

/// Main [`Graph`] trait, defining the `Label` type used for the [`Graph`].
///
/// This trait is required for all other `Graph` traits.
pub trait Graph {
    /// The [`Label`] type by which each node in the graph is labelled
    type Label: Label;

    /// Return the number of neighbours of the node associated with `label`,
    /// i.e. the _vertex degree_ of the node.
    fn get_vertex_degree(&self, label: Self::Label) -> usize;
}

/// Trait representing any type that can act as a label for a node in a graph
pub trait Label: Copy + Eq + std::fmt::Debug {}

/// Trait representing graphs that have finite size, i.e. a finite number of nodes
pub trait SizedGraph: Graph {
    /// Returns the number of nodes in the [`Graph`]
    fn len(&self) -> usize;

    /// Returns `true` if there are no nodes in the [`Graph`], otherwise `false`
    fn is_empty(&self) -> bool;
}

/// Trait representing graphs that can iterator over _all_ nodes
///
/// Since the iterator needs to run over all nodes, the graph needs to be of finite size.
pub trait IterSizedGraph<I>: SizedGraph
where
    I: Iterator<Item = Self::Label>,
{
    /// Iterate over all labels in a sized graph in order,
    /// i.e. it should be implemented such that the iteration starts at the label
    /// representing 0 and each consecutive element should it's succesor.
    fn iter(&self) -> I;
}

/// Graph that can be sampled over, i.e. a random node can be selected.
pub trait SampleGraph: Graph {
    /// Returns the [`Label`](Graph::Label) of a randomly selected node, using random number
    /// generator `rng`.
    fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Self::Label;
}

/// Graph that allows sampling of the neighbours of a given node
pub trait SampleNeighbours: Graph {
    /// Returns the [`Label`](Graph::Label) of a randomly selected neighbour of the node represented
    /// by `label` using random number generator `rng`.
    fn sample_neighbours<R: Rng + ?Sized>(&self, label: Self::Label, rng: &mut R) -> Self::Label;
}

/// Graph that can iterate over the neighbours of every node, this allows a breadth first search
/// to be performed
pub trait IterableGraph<'a, I>: FieldGraph + 'a {
    /// Returns an iterator `I` over all the neighbours of the node represented by `label`
    fn iter_neighbours(&'a self, label: Self::Label) -> I;

    /// Returns breadth first search iterator over the graph starting from `origin`
    fn iter_breadth_first<N>(&'a self, origin: Self::Label) -> IterBreadthFirst<'a, Self, I, N>
    where
        N: TraverseNode<Self::Label>,
    {
        IterBreadthFirst::new(self, origin)
    }
}

/// Graph that can have a field on top of its nodes, i.e. a value associated with each node
/// in the graph.
pub trait FieldGraph: Graph + Sized {
    /// The type of the field collection on top the graph, with field value type `F`
    type FieldType<F>: Field<F, Self>;

    /// Create a new field of field value F` on the graph
    fn new_field<F>(&self) -> Self::FieldType<F>;
}

impl<T> Label for LabelStruct<T> {}
