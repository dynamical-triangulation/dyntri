//! Module for dealing with large HDF5 datasets by loading them in chunks.
//!
//! The main important function here is [`chunk_iter()`], which returns an iterator over
//! [Chunk]s of the given [Dataset]. This iterator is used by other functions to efficiently
//! do operations over the entire [Dataset] whilst not having to load it in its entirety,
//! yet also need to load in each [Chunk] only once.

use hdf5::{Dataset, H5Type};
use ndarray::prelude::*;
use std::{iter::once_with, ops::Range};

/// Struct holding a chunk of data from a [Dataset], including the range in the dataset
/// the chunks takes in. This assumes the dataset is chunked in its rows, but not its columns.
pub struct Chunk<T> {
    /// The array data that chould be held by the chunk
    pub array: Array2<T>,
    /// [`Range`] representing the rows of the chunk in the full dataset
    pub range: Range<usize>,
}

impl<T: H5Type> Chunk<T> {
    fn from_dataset(dataset: &Dataset, range: Range<usize>) -> Self {
        let array = dataset
            .read_slice_2d(s![range.clone(), ..])
            .expect("Chunk slice is not correct");
        Self { array, range }
    }
}

impl<T> Chunk<T> {
    /// Returns an [`ArrayView`] to the data in the [`Chunk`].
    pub fn view(&self) -> ArrayView2<T> {
        self.array.view()
    }
}

/// Returns an iterator over the [`Chunk`]s of `dataset`.
///
/// This function can be used to do operations on the `dataset` by operating on it
/// chunk by chunk, keeping memory usage low, and reducing the amount of times the
/// same chunk has to be read from disk.
///
/// Type `T` is the type as which the data will be stored in the [Chunk]; one needs to make
/// sure the data in `dataset` can be converted to type `T`. Also the reading will be the fastest
/// if the stored type is small and `T` is of the same type.
pub fn chunk_iter<T: H5Type>(dataset: &Dataset) -> impl Iterator<Item = Chunk<T>> + '_ {
    assert!(
        dataset.is_chunked(),
        "Can only iterate chunks over a chunked dataset"
    );

    let [chunk_row_size, ..] = dataset.chunk().unwrap()[..] else {
        panic!("Empty chunk size is not valid")
    };
    let chunk_num = dataset.num_chunks().unwrap();
    let max_row_index = dataset.shape()[0];

    (0..chunk_num - 1)
        .map(move |i| Chunk::from_dataset(dataset, i * chunk_row_size..(i + 1) * chunk_row_size))
        .chain(once_with(move || {
            Chunk::from_dataset(dataset, (chunk_num - 1) * chunk_row_size..max_row_index)
        }))
}
