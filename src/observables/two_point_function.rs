//! Two point functions of an field quantity of choice.
//!
//! With two point function we mean an average of (often the product of) a pair of field values of
//! nodes, which are separated by a given distance. What this exactly entails for dynamical
//! triangulations is discussed in the
//! [Master thesis of J. Duin](https://www.ru.nl/publish/pages/913395/jduin-master-thesis.pdf).

use log::trace;
use ndarray::prelude::*;
use num_traits::Zero;
use rand::Rng;
use rand_distr::Uniform;
use serde::Serialize;

use super::{
    average_sphere_distance::geodesic_spheres::construct_geodesic_spheres,
    full_graph::{distance_matrix::MASK_GROUP_BASE_NAME, sparse::BoolMatrix},
};
use crate::graph::{breadth_first_search::IterNeighbours, IterableGraph};
use crate::observables::full_graph::distance_matrix;
use distance_matrix::DISTANCE_MATRIX_DATASET_NAME;

/// General two point function container
///
/// Stores the value at the original points and the profile of the second point at several
/// distances, together with the sphere volumes at the same distances.
#[derive(Debug, Serialize, Clone)]
pub struct TwoPointFunction<T> {
    /// The value of the field at the origin point
    pub origin: T,
    /// A list of the the field values at second points at increasing distance from
    /// the origin
    pub profile: Vec<T>,
    /// The volume of spheres at the same distances from the origins as the `profile`
    pub sphere_volume_profile: Vec<usize>,
}

/// Return the [`TwoPointFunction`] of a `graph` from a given `origin` up to distance
/// `max_distance`.
///
/// The two point function is computed for the field given by `observable`, which should be
/// a function that gives the field value on the graph for a given label; see signature of `F`.
pub fn measure<'a, R, T, G, F, I>(
    graph: &'a G,
    origin: G::Label,
    mut observable: F,
    max_distance: usize,
    rng: &mut R,
) -> TwoPointFunction<T>
where
    T: Zero,
    R: Rng + ?Sized,
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I>,
    F: FnMut(&'a G, G::Label) -> T,
{
    let origin_value = observable(graph, origin);

    let spheres = construct_geodesic_spheres(graph, origin, max_distance);
    let mut profile = Vec::new();
    for r in 0..max_distance + 1 {
        if spheres.get_sphere(r).is_empty() {
            profile.push(T::zero());
            continue;
        }
        let label = spheres.sample_node(r, rng);
        profile.push(observable(graph, label));
    }
    let sphere_volume_profile = spheres.0.iter().map(|sphere| sphere.len()).collect();
    TwoPointFunction {
        origin: origin_value,
        profile,
        sphere_volume_profile,
    }
}

/// Measure the average two-point function between all pairs at a given distance using `function`.
///
/// Note: `dmatrix_group` should be a HDF5 group containing all the `mask`s groups as given by
/// [`distance_matrix::compute_distance_matrix_with_masks_disk()`].
pub fn measure_full_graph_from_disk<T, F>(
    dmatrix_group: &hdf5::Group,
    field: ArrayView1<T>,
    function: &F,
    distances: impl Iterator<Item = usize>,
) -> hdf5::Result<Array1<f64>>
where
    T: Copy,
    F: Fn(T, T) -> f64,
{
    let two_point_profile = distances
        .map(|r| {
            let mask_group = dmatrix_group.group(&format!("{MASK_GROUP_BASE_NAME}{r}"))?;
            trace!("Loading in distance mask{r}");
            let mask = BoolMatrix::from_disk(&mask_group)?;
            let mean_two_point = mask
                .view()
                .masked_vector_operation_mean(field, field, function);
            Ok(mean_two_point)
        })
        .collect::<hdf5::Result<_>>()?;
    Ok(two_point_profile)
}

/// Returns `samples` amount of two-point samples for each of the `distances` given, based on the
/// distance matrix given by `dmatrix_group`.
///
/// The pairs are sampled using a uniform sampling, i.e. all two-point pairs of a given distance
/// have an equal probability of being sampled.
/// A pairs of arrays is returned, each corresponding to one of the points in the two-point pair;
/// each array has the distance on the first axis and the sample on the second.
pub fn sample_uniform_full_graph_from_disk<R>(
    dmatrix_group: &hdf5::Group,
    distances: impl Iterator<Item = usize>,
    samples: usize,
    rng: &mut R,
) -> hdf5::Result<(Array2<usize>, Array2<usize>)>
where
    R: Rng + ?Sized,
{
    let mut two_point_sample0 = Array2::default([0, samples]);
    let mut two_point_sample1 = Array2::default([0, samples]);
    for r in distances {
        let (point_pairs0, point_pairs1): (Vec<usize>, Vec<usize>) = if r == 0 {
            let dmatrix_dataset = dmatrix_group
                .dataset(DISTANCE_MATRIX_DATASET_NAME)
                .unwrap_or_else(|err| {
                    panic!(
                        "Dataset `{}` should exist: {err}",
                        DISTANCE_MATRIX_DATASET_NAME
                    )
                });
            let shape = dmatrix_dataset.shape();
            assert_eq!(shape[0], shape[1], "Distance matrix should be symmetric");
            let vertex_count = shape[0];
            let uniform_range = Uniform::new(0, vertex_count);
            let point_pairs0: Vec<usize> = std::iter::repeat_with(|| rng.sample(uniform_range))
                .take(samples)
                .collect();
            let point_pairs1 = point_pairs0.clone();
            (point_pairs0, point_pairs1)
        } else {
            let mask_group = dmatrix_group.group(&format!("{MASK_GROUP_BASE_NAME}{r}"))?;
            trace!("Loading in distance mask{r}");
            let mask = BoolMatrix::from_disk(&mask_group)?;
            std::iter::repeat_with(|| mask.sample_uniform(rng))
                .take(samples)
                .map(|points| (points[0], points[1]))
                .unzip()
        };
        two_point_sample0
            .push_row(ArrayView1::from(&point_pairs0))
            .expect("Shapes should match by construction");
        two_point_sample1
            .push_row(ArrayView1::from(&point_pairs1))
            .expect("Shapes should match by construction");
    }
    Ok((two_point_sample0, two_point_sample1))
}

/// Returns `samples` amount of two-point samples for each of the `distances` given, based on the
/// distance matrix given by `dmatrix_group`.
///
/// The pairs are sampled using a origin-based sampling, i.e. the first point is sampled uniformly
/// from all points and the second point uniformly from all points at the given distance from the
/// first point.
/// A pairs of arrays is returned, the first one corrsponding to the first points of each two-point
/// sample and the second one to the second point;
/// each array has the distance on the first axis and the sample on the second.
pub fn sample_from_origin_full_graph_from_disk<R>(
    dmatrix_group: &hdf5::Group,
    distances: impl Iterator<Item = usize>,
    samples: usize,
    rng: &mut R,
) -> hdf5::Result<(Array2<usize>, Array2<usize>)>
where
    R: Rng + ?Sized,
{
    let mut two_point_sample0 = Array2::default([0, samples]);
    let mut two_point_sample1 = Array2::default([0, samples]);
    for r in distances {
        let (point_pairs0, point_pairs1): (Vec<usize>, Vec<usize>) = if r == 0 {
            let dmatrix_dataset = dmatrix_group
                .dataset(DISTANCE_MATRIX_DATASET_NAME)
                .unwrap_or_else(|err| {
                    panic!(
                        "Dataset `{}` should exist: {err}",
                        DISTANCE_MATRIX_DATASET_NAME
                    )
                });
            let shape = dmatrix_dataset.shape();
            assert_eq!(shape[0], shape[1], "Distance matrix should be symmetric");
            let vertex_count = shape[0];
            let uniform_range = Uniform::new(0, vertex_count);
            let point_pairs0: Vec<usize> = std::iter::repeat_with(|| rng.sample(uniform_range))
                .take(samples)
                .collect();
            let point_pairs1 = point_pairs0.clone();
            (point_pairs0, point_pairs1)
        } else {
            let mask_group = dmatrix_group.group(&format!("{MASK_GROUP_BASE_NAME}{r}"))?;
            trace!("Loading in distance mask{r}");
            let mask = BoolMatrix::from_disk(&mask_group)?;
            std::iter::repeat_with(|| mask.sample_from_origin(rng))
                .take(samples)
                .map(|points| (points[0], points[1]))
                .unzip()
        };
        two_point_sample0
            .push_row(ArrayView1::from(&point_pairs0))
            .expect("Shapes should match by construction");
        two_point_sample1
            .push_row(ArrayView1::from(&point_pairs1))
            .expect("Shapes should match by construction");
    }
    Ok((two_point_sample0, two_point_sample1))
}

/// Retuturns two-point samples based on a distance matrix with masks
pub fn sample_uniform_full_graph<T, R>(
    dmatrix: ArrayView2<T>,
    distances: impl Iterator<Item = usize>,
    samples: usize,
    rng: &mut R,
) -> hdf5::Result<(Array2<usize>, Array2<usize>)>
where
    T: Copy + Eq + Into<usize>,
    R: Rng + ?Sized,
{
    let mut two_point_sample0 = Array2::default([0, samples]);
    let mut two_point_sample1 = Array2::default([0, samples]);
    for r in distances {
        let mask = BoolMatrix::from_dense_filter(dmatrix, |&entry| entry.into() == r);
        let (point_pairs0, point_pairs1): (Vec<usize>, Vec<usize>) =
            std::iter::repeat_with(|| mask.sample_uniform(rng))
                .take(samples)
                .map(|points| (points[0], points[1]))
                .unzip();
        two_point_sample0
            .push_row(ArrayView1::from(&point_pairs0))
            .expect("Shapes should match by construction");
        two_point_sample1
            .push_row(ArrayView1::from(&point_pairs1))
            .expect("Shapes should match by construction");
    }
    Ok((two_point_sample0, two_point_sample1))
}

/// Retuturns two-point samples based on a distance matrix with masks
pub fn sample_from_origin_full_graph<T, R>(
    dmatrix: ArrayView2<T>,
    distances: impl Iterator<Item = usize>,
    samples: usize,
    rng: &mut R,
) -> hdf5::Result<(Array2<usize>, Array2<usize>)>
where
    T: Copy + Eq + Into<usize>,
    R: Rng + ?Sized,
{
    let mut two_point_sample0 = Array2::default([0, samples]);
    let mut two_point_sample1 = Array2::default([0, samples]);
    for r in distances {
        let mask = BoolMatrix::from_dense_filter(dmatrix, |&entry| entry.into() == r);
        let (point_pairs0, point_pairs1): (Vec<usize>, Vec<usize>) =
            std::iter::repeat_with(|| mask.sample_from_origin(rng))
                .take(samples)
                .map(|points| (points[0], points[1]))
                .unzip();
        two_point_sample0
            .push_row(ArrayView1::from(&point_pairs0))
            .expect("Shapes should match by construction");
        two_point_sample1
            .push_row(ArrayView1::from(&point_pairs1))
            .expect("Shapes should match by construction");
    }
    Ok((two_point_sample0, two_point_sample1))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::spacetime_graph::SpacetimeGraph;
    use crate::graph::GenericGraph;
    use crate::graph::{periodic_spacetime_graph::PeriodicSpacetimeGraph, SampleGraph};
    use crate::observables::average_sphere_distance;
    use crate::observables::full_graph::distance_matrix::compute_distance_matrix;
    use crate::triangulation::{FixedTriangulation, FullTriangulation};
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    fn random_triangulation() -> FullTriangulation {
        let mut triangulation = FixedTriangulation::new(10, 5);
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..28 {
            triangulation.relocate(
                triangulation.sample_order_four(&mut rng),
                triangulation.sample(&mut rng),
            );
        }
        for _ in 0..300 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn asd_correlation_test() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let triangulation = random_triangulation();
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        let origin = graph.sample_node(&mut rng);
        let max_delta = 5;
        let max_distance = 8;
        let asd_observable = |graph, label| {
            *average_sphere_distance::single_sphere::profile(graph, label, 0..max_delta)
                .profile
                .last()
                .unwrap()
        };
        let correlation = measure(&graph, origin, asd_observable, max_distance, &mut rng);
        println!("{correlation:#?}");
    }

    #[test]
    fn asd_double_correlation_test() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let triangulation = random_triangulation();
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        let origin = graph.sample_node(&mut rng);
        let max_delta = 5;
        let max_distance = 8;
        let mut rng2 = rng.clone();
        let asd_observable = |graph, label| {
            *average_sphere_distance::double_sphere::profile(graph, label, 0..max_delta, &mut rng)
                .profile
                .last()
                .unwrap()
        };
        let correlation = measure(&graph, origin, asd_observable, max_distance, &mut rng2);
        println!("{correlation:#?}");
    }

    #[test]
    fn two_point_sample_test() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let triangulation = random_triangulation();
        let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&triangulation);
        let graph = GenericGraph::from_spacetime_graph(&spacetime_graph);

        let max_distance = 10;
        let dmatrix = compute_distance_matrix::<u8>(&graph, max_distance);

        let sample_count = 10;
        let distances = 0..5;
        let (samples0, samples1) =
            sample_uniform_full_graph(dmatrix.view(), distances.clone(), sample_count, &mut rng)
                .unwrap();
        for r in distances {
            println!("Distance {r}");
            println!("{}", samples0.slice(s![r, ..]));
            println!("{}", samples1.slice(s![r, ..]));
            azip!((&a in samples0.slice(s![r, ..]), &b in samples1.slice(s![r, ..])) assert_eq!(dmatrix[(a, b)] as usize, r));
        }
    }
}
