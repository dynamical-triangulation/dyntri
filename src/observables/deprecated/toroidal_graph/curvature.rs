//! Module for computing the Quantum Ricci Curvature of a graph
//!
//! More accurately the computations will be determining an average sphere distance
//! from which the QRC could be extracted by looking at the higher-order deviation
//! from linearity.

use std::ops::Index;

use serde::Serialize;

use crate::triangulation::observables::Observable;

use super::*;

pub fn overlapping_sphere_distance<G>(
    graph: &G,
    origin: G::Label,
    delta: usize,
) -> AverageSphereDistance
where
    G: Graph,
{
    let sphere = get_geodesic_sphere(graph, origin, delta);
    overlapping_sphere_distance_from_sphere(graph, &sphere, delta)
}

pub(super) fn overlapping_sphere_distance_from_sphere<G>(
    graph: &G,
    sphere: &GeodesicSphere<G::Label>,
    delta: usize,
) -> AverageSphereDistance
where
    G: Graph,
{
    // Normalize by the amount of pairs that are measured (N_sphere^2)
    let norm = (sphere.len() * (sphere.len() - 1)) as f64;
    // Use a continuously updating average such that the total does not need to become large
    let mut average = 0.0;
    let mut violation_count = 0;
    // Reuse marked list because it SHOULD BE emptied every node
    let mut marked = MarkedList::new(graph);
    // Filter out the origin distance calculation as all of those are `delta` and already added
    for &start_node in sphere.iter() {
        // This could break if sphere contained duplicate labels, but the BFS for getting the
        // geodesic sphere should guarantee this never occurs.
        marked.add_multiple_unchecked(sphere.iter());
        for node in graph.iter_breadth_first(start_node) {
            assert!(
                // This assert should be able to be removed (but performance impact is
                // minimal)
                node.distance <= 2 * delta,
                "Something went wrong, not all sphere points were found within 3*delta"
            );

            // Mark the point as visisted
            match marked.try_remove(&node.label) {
                NodeLabelMatch::Different => continue,
                NodeLabelMatch::Equal => {
                    average += (node.distance as f64) / norm;
                    // Stop BFS if all nodes are visited
                    if marked.is_empty() {
                        break;
                    }
                }
                NodeLabelMatch::DifferentPatch => {
                    violation_count += 1;
                }
            }
        }
    }

    AverageSphereDistance {
        asd: average,
        violation_ratio: violation_count as f64 / norm,
    }
}

/// Computes the Average Sphere Distance from an origin
pub fn average_sphere_distance<G, R>(
    graph: &G,
    origin: G::Label,
    delta: usize,
    rng: &mut R,
) -> AverageSphereDistance
where
    G: Graph,
    R: Rng + ?Sized,
{
    let spheres = SpheresASD::from_origin(graph, origin, delta, rng);
    spheres.average_sphere_distance()
}

/// Computes the Average sphere distance from a center point
pub fn average_sphere_distance_center<G, R>(
    graph: &G,
    point: G::Label,
    delta: usize,
    rng: &mut R,
) -> AverageSphereDistance
where
    G: Graph,
    R: Rng + ?Sized,
{
    let spheres = SpheresASD::from_center(graph, point, delta, rng);
    spheres.average_sphere_distance()
}

/// Struct for bundling the precomputed spheres and origin for ASD
#[derive(Debug, Clone)]
pub struct SpheresASD<'a, G: Graph> {
    graph: &'a G,
    delta: usize,
    // `origin` is supposed to be the origin of `sphere0`
    origin: G::Label,
    sphere0: GeodesicSphere<G::Label>,
    sphere1: GeodesicSphere<G::Label>,
}

impl<'a, G: Graph> SpheresASD<'a, G> {
    /// Compute spheres from the origin of one of the spheres, checking whether a given point
    /// is within the region averaged over.
    ///
    /// See [from_origin()] for more details
    pub(super) fn from_origin_contains<R>(
        graph: &'a G,
        origin: G::Label,
        delta: usize,
        node: G::Label,
        rng: &mut R,
    ) -> (Self, NodeLabelMatch)
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        // Get sphere around the origin with radius `delta`
        let (sphere, contains) = get_geodesic_sphere_contains(graph, origin, delta, node);
        let (spheres, other_contains) =
            Self::from_origin_sphere_contains(graph, origin, sphere, delta, node, rng);
        (spheres, contains.combine(other_contains))
    }

    /// Compute spheres from the origin of one of the spheres
    pub fn from_origin<R>(graph: &'a G, origin: G::Label, delta: usize, rng: &mut R) -> Self
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        // Get sphere around the origin with radius `delta`
        let sphere = get_geodesic_sphere(graph, origin, delta);
        Self::from_origin_sphere(graph, origin, sphere, delta, rng)
    }

    /// Compute spheres with a single sphere already computed, checking if the given
    /// [Node] is within the second sphere.
    pub(super) fn from_origin_sphere_contains<R>(
        graph: &'a G,
        origin: G::Label,
        sphere: GeodesicSphere<G::Label>,
        delta: usize,
        node: G::Label,
        rng: &mut R,
    ) -> (Self, NodeLabelMatch)
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        // Choose random second origin from sphere nodes
        let origin1 = sphere[rng.gen_range(0..sphere.len())];
        let (sphere1, contains) = get_geodesic_sphere_contains(graph, origin1, delta, node);

        (
            Self::from_spheres(graph, delta, origin, sphere, sphere1),
            contains,
        )
    }

    /// Compute spheres with a single sphere already computed
    ///
    /// NOTE: This function assumes the given `sphere` has `origin`
    /// as its origin, if this is NOT the case the function gives
    /// incorrect results.
    pub(super) fn from_origin_sphere<R>(
        graph: &'a G,
        origin: G::Label,
        sphere: GeodesicSphere<G::Label>,
        delta: usize,
        rng: &mut R,
    ) -> Self
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        // Choose random second origin from sphere nodes
        let origin1 = sphere[rng.gen_range(0..sphere.len())];
        let sphere1 = get_geodesic_sphere(graph, origin1, delta);

        Self::from_spheres(graph, delta, origin, sphere, sphere1)
    }

    /// Get SphereASD from precomputed spheres
    pub(super) fn from_spheres(
        graph: &'a G,
        delta: usize,
        origin0: G::Label,
        sphere0: GeodesicSphere<G::Label>,
        sphere1: GeodesicSphere<G::Label>,
    ) -> Self {
        SpheresASD {
            graph,
            delta,
            origin: origin0,
            sphere0,
            sphere1,
        }
    }

    /// Generate SphereASD from a center point, such that the center point
    /// will be in the middle of the geodesic between the origins.
    ///
    /// NOTE: this center point is not unique, and an arbitrary point is chosen.
    /// NOTE!!: Computing these sphere may not always be possible if the chosen
    /// center point happens to be a 'singularity'.
    pub(super) fn from_center<R>(graph: &'a G, center: G::Label, delta: usize, rng: &mut R) -> Self
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        let half_delta = delta / 2;
        let (origin, half_sphere) = if delta % 2 == 0 {
            // Even delta
            let sphere = get_geodesic_sphere(graph, center, half_delta);
            let origin = sphere[rng.gen_range(0..sphere.len())];
            (origin, sphere)
        } else {
            // Odd delta
            let (inner_sphere, outer_sphere) =
                get_two_geodesic_spheres(graph, center, (half_delta, half_delta + 1));
            let origin = outer_sphere[rng.gen_range(0..inner_sphere.len())];
            (origin, inner_sphere)
        };

        let sphere0 = get_geodesic_sphere(graph, origin, delta);
        let mut origin1: Option<G::Label> = None;
        for &node in half_sphere.iter() {
            if sphere0.contains(&node) {
                origin1 = Some(node);
            }
        }
        let origin1 = origin1.expect("There was no overlap between the two spheres");
        let sphere1 = get_geodesic_sphere(graph, origin1, delta);

        SpheresASD {
            graph,
            delta,
            origin,
            sphere0,
            sphere1,
        }
    }

    /// Generate SphereASD from a center point, also returning whether given [Node] was in the
    /// region averaged over or not.
    pub(super) fn from_center_contains<R>(
        graph: &'a G,
        center: G::Label,
        delta: usize,
        node: G::Label,
        rng: &mut R,
    ) -> (Self, NodeLabelMatch)
    where
        G: Graph,
        R: Rng + ?Sized,
    {
        let half_delta = delta / 2;
        let (origin, half_sphere) = if delta % 2 == 0 {
            // Even delta
            let sphere = get_geodesic_sphere(graph, center, half_delta);
            let origin = sphere[rng.gen_range(0..sphere.len())];
            (origin, sphere)
        } else {
            // Odd delta
            let (inner_sphere, outer_sphere) =
                get_two_geodesic_spheres(graph, center, (half_delta, half_delta + 1));
            let origin = outer_sphere[rng.gen_range(0..inner_sphere.len())];
            (origin, inner_sphere)
        };

        let (sphere0, contains0) = get_geodesic_sphere_contains(graph, origin, delta, node);
        let mut origin1: Option<G::Label> = None;
        for &node in half_sphere.iter() {
            if sphere0.contains(&node) {
                origin1 = Some(node);
            }
        }
        let origin1 = origin1.expect("There was no overlap between the two spheres");
        let (sphere1, contains1) = get_geodesic_sphere_contains(graph, origin1, delta, node);

        (
            SpheresASD {
                graph,
                delta,
                origin,
                sphere0,
                sphere1,
            },
            contains0.combine(contains1),
        )
    }
}

impl<G: Graph> SpheresASD<'_, G> {
    /// Compute the average sphere distance given the two necessary spheres and the origin of one of them
    ///
    /// NOTE: Important to note is that this function assumes that `origin` lies on `sphere1` for a slight
    /// optimisation, and DOES NOT give the correct answer when this is not the case! So make sure that
    /// `origin` is indeed part of `sphere1`.
    pub(super) fn average_sphere_distance(&self) -> AverageSphereDistance
    where
        G: Graph,
    {
        let SpheresASD {
            graph,
            origin,
            delta,
            sphere0,
            sphere1,
        } = self;

        // Track amount of times a violation would have occured
        let mut violation_count = 0;

        // Set sphere0 and sphere1 such that the main loop has the fewest iterations (least BFSs)
        let (sphere0, sphere1) = if sphere0.len() < sphere1.len() {
            (sphere1, sphere0)
        } else {
            (sphere0, sphere1)
        };

        // Compute average distance
        let count = (sphere0.len() * sphere1.len()) as f64;
        // Use a continously updating average such that the total does not need to become large
        let mut average = ((delta * sphere0.len()) as f64) / count;
        // Reuse marked list because it SHOULD BE emptied every node
        let mut marked = MarkedList::new(*graph);
        // Filter out the origin distance calculation as all of those are `delta` and already added
        for &start_node in sphere1.iter().filter(|&label| label != origin) {
            // This could break if sphere0 contained duplicate labels, but the BFS for getting the
            // geodesic sphere should guarantee this never occurs.
            marked.add_multiple_unchecked(sphere0.iter());
            // OPTIMIZABLE: BFS iterator is the other hot function
            for node in graph.iter_breadth_first(start_node) {
                // Mark the point as visisted
                assert!(
                    // This assert should be able to be removed (but performance impact is
                    // minimal)
                    node.distance <= 3 * delta,
                    "Something went wrong, not all sphere points were found within 3*delta"
                );

                match marked.try_remove(&node.label) {
                    NodeLabelMatch::Different => continue,
                    NodeLabelMatch::Equal => {
                        average += (node.distance as f64) / count;
                        // Stop BFS if all nodes are visited
                        if marked.is_empty() {
                            break;
                        }
                    }
                    NodeLabelMatch::DifferentPatch => {
                        violation_count += 1;
                    }
                }
            }
        }

        AverageSphereDistance {
            asd: average,
            violation_ratio: violation_count as f64 / count,
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize)]
/// Struct holding the [AverageSphereDistance] including the `violation_ratio`,
/// meaning the ratio of geodesic distance measurements that would have wrapped around in
/// a torus topology, compared to the used graph topology.
///
/// Note: the `violation_ratio` only gives useful results when used with [StackedCylinderGraph]
/// or [TiledPlanarGraph]
pub struct AverageSphereDistance {
    pub asd: f64,
    /// The part of the sphere distance measurements that would have wrapping
    /// problems when compared to a normal [ToroidalGraph].
    /// Hence should always be `0.0` when determined for [ToroidalGraph]
    pub violation_ratio: f64,
}

impl AverageSphereDistance {
    /// Pairs two [AverageSphereDistance]s together, returning the pair
    /// of average sphere distance floats, with the average violation ratio
    pub fn pair(self, other: Self) -> ((f64, f64), f64) {
        let pair = (self.asd, other.asd);
        let violation_ratio = (self.violation_ratio + other.violation_ratio) / 2.0;

        (pair, violation_ratio)
    }
}

impl Observable for AverageSphereDistance {}

impl Display for AverageSphereDistance {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}; {}", self.asd, self.violation_ratio)
    }
}

#[derive(Debug, Clone)]
pub(super) struct GeodesicSphere<L>(Box<[L]>);

impl<L> Index<usize> for GeodesicSphere<L> {
    type Output = L;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<L> GeodesicSphere<L> {
    pub(super) fn from_vec(vec: Vec<L>) -> Self {
        Self(vec.into_boxed_slice())
    }
}

impl<L> GeodesicSphere<L> {
    pub(super) fn len(&self) -> usize {
        self.0.len()
    }

    fn iter(&self) -> std::slice::Iter<'_, L> {
        self.0.iter()
    }
}

impl<L: PartialEq> GeodesicSphere<L> {
    fn contains(&self, x: &L) -> bool {
        self.0.contains(x)
    }
}

/// Returns a list of [NodeLabel]s that label the [Node]s a distance `radius` from the given `origin`
pub(super) fn get_geodesic_sphere<G: Graph>(
    graph: &G,
    origin: G::Label,
    radius: usize,
) -> GeodesicSphere<G::Label> {
    // TODO: Make better prediction for vector size based on Haussdorf dimension measurement
    let expected_length = 10 * radius;
    let mut sphere = Vec::with_capacity(expected_length); // List of nodes in sphere around origin

    // Find origin sphere
    for node in graph.iter_breadth_first(origin) {
        match node.distance.cmp(&radius) {
            Ordering::Less => continue,
            Ordering::Equal => sphere.push(node.label),
            Ordering::Greater => break,
        }
    }

    GeodesicSphere::from_vec(sphere)
}

/// Returns a list of [NodeLabel]s that label the [Node]s a distance `radius` from the given
/// `origin`, whilst checking if the given [Node] is contained in the sphere.
///
/// For further details see [`get_geodesic_sphere()`]
pub(super) fn get_geodesic_sphere_contains<G: Graph>(
    graph: &G,
    origin: G::Label,
    radius: usize,
    node: G::Label,
) -> (GeodesicSphere<G::Label>, NodeLabelMatch) {
    // Containment marker
    let mut nodematch = NodeLabelMatch::Different;

    // TODO: Make better prediction for vector size based on Haussdorf dimension measurement
    let expected_length = 10 * radius;
    let mut sphere = Vec::with_capacity(expected_length); // List of nodes in sphere around origin

    // Find origin sphere
    for node_iter in graph.iter_breadth_first(origin) {
        nodematch.update(node.cmp_label(node_iter.label));
        match node_iter.distance.cmp(&radius) {
            Ordering::Less => continue,
            Ordering::Equal => sphere.push(node_iter.label),
            Ordering::Greater => break,
        }
    }

    (GeodesicSphere::from_vec(sphere), nodematch)
}

type GeodesicSpherePair<L> = (GeodesicSphere<L>, GeodesicSphere<L>);

pub(super) fn get_two_geodesic_spheres<G: Graph>(
    graph: &G,
    origin: G::Label,
    radii: (usize, usize),
) -> GeodesicSpherePair<G::Label> {
    // Sort radii such that first is always smaller
    let (radii, flipped) = match radii.0.cmp(&radii.1) {
        Ordering::Less => (radii, false),
        Ordering::Greater => ((radii.1, radii.0), true),
        Ordering::Equal => {
            // If equal simply use the single geodesic sphere finder
            let sphere = get_geodesic_sphere(graph, origin, radii.0);
            return (sphere.clone(), sphere);
        }
    };

    // OPTIMIZABLE: Make prediction for vector size based on Haussdorf dimension measurement, and pre-allocate
    let mut inner_sphere = Vec::new();
    let mut outer_sphere = Vec::new();

    // Find origin inner sphere
    let mut bfs = graph.iter_breadth_first(origin);
    for node in &mut bfs {
        match node.distance.cmp(&radii.0) {
            Ordering::Less => continue,
            Ordering::Equal => inner_sphere.push(node.label),
            Ordering::Greater => {
                // Also include the node if radii.1 == radii.0 + 1
                if node.distance == radii.1 {
                    outer_sphere.push(node.label);
                }
                break;
            }
        }
    }
    // Find outer sphere
    for node in bfs {
        match node.distance.cmp(&radii.1) {
            Ordering::Less => continue,
            Ordering::Equal => outer_sphere.push(node.label),
            Ordering::Greater => break,
        }
    }

    let inner_sphere = GeodesicSphere::from_vec(inner_sphere);
    let outer_sphere = GeodesicSphere::from_vec(outer_sphere);
    if flipped {
        (outer_sphere, inner_sphere)
    } else {
        (inner_sphere, outer_sphere)
    }
}

pub(super) fn get_two_geodesic_spheres_contains_inner<G: Graph>(
    graph: &G,
    origin: G::Label,
    radii: (usize, usize),
    other: G::Label,
) -> (GeodesicSpherePair<G::Label>, NodeLabelMatch) {
    // Containment marker
    let mut nodematch = NodeLabelMatch::Different;

    // Sort radii such that first is always smaller
    let (radii, flipped) = match radii.0.cmp(&radii.1) {
        Ordering::Less => (radii, false),
        Ordering::Greater => ((radii.1, radii.0), true),
        Ordering::Equal => {
            // If equal simply use the single geodesic sphere finder
            let (sphere, nodematch) = get_geodesic_sphere_contains(graph, origin, radii.0, other);
            return ((sphere.clone(), sphere), nodematch);
        }
    };

    // OPTIMIZABLE: Make prediction for vector size based on Haussdorf dimension measurement, and pre-allocate
    let mut inner_sphere = Vec::new();
    let mut outer_sphere = Vec::new();

    // Find origin inner sphere
    let mut bfs = graph.iter_breadth_first(origin);
    for node in &mut bfs {
        nodematch.update(other.cmp_label(node.label));
        match node.distance.cmp(&radii.0) {
            Ordering::Less => continue,
            Ordering::Equal => inner_sphere.push(node.label),
            Ordering::Greater => {
                // Also include the node if radii.1 == radii.0 + 1
                if node.distance == radii.1 {
                    outer_sphere.push(node.label);
                }
                break;
            }
        }
    }
    // Find outer sphere
    for node in bfs {
        match node.distance.cmp(&radii.1) {
            Ordering::Less => continue,
            Ordering::Equal => outer_sphere.push(node.label),
            Ordering::Greater => break,
        }
    }

    let inner_sphere = GeodesicSphere::from_vec(inner_sphere);
    let outer_sphere = GeodesicSphere::from_vec(outer_sphere);
    if flipped {
        ((outer_sphere, inner_sphere), nodematch)
    } else {
        ((inner_sphere, outer_sphere), nodematch)
    }
}

/// Struct holding the average sphere distance profile for different length scales
#[derive(Debug, Clone, Serialize)]
pub struct AverageSphereDistanceProfile {
    profile: Box<[f64]>,
    violation_ratios: Box<[f64]>,
}

impl Observable for AverageSphereDistanceProfile {}

impl Display for AverageSphereDistanceProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "                         ")?;
        for asd in 1..self.profile.len() + 1 {
            write!(f, "{:^8} ", asd)?;
        }
        writeln!(f)?;
        write!(f, "Average sphere distance: ")?;
        for asd in self.profile.iter() {
            write!(f, "{:>8.5} ", asd)?;
        }
        writeln!(f)?;
        write!(f, "Violation ratios:        ")?;
        for ratio in self.violation_ratios.iter() {
            write!(f, "{:>8.5} ", ratio)?;
        }
        writeln!(f)
    }
}

/// Compute the Average sphere distance with epsilon = 0, for different delta
pub fn overlapping_sphere_distance_profile<G, R>(
    graph: &G,
    deltas: impl Iterator<Item = usize>,
    rng: &mut R,
) -> AverageSphereDistanceProfile
where
    G: Graph,
    R: Rng + ?Sized,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let origin = graph.sample_node(rng);
        let AverageSphereDistance {
            asd,
            violation_ratio,
        } = overlapping_sphere_distance(graph, origin, delta);
        profile.push(asd);
        violation_ratios.push(violation_ratio);
    }

    AverageSphereDistanceProfile {
        profile: profile.into_boxed_slice(),
        violation_ratios: violation_ratios.into_boxed_slice(),
    }
}

/// Compute the Average sphere distance for different deltas
pub fn average_sphere_distance_profile<G, R>(
    graph: &G,
    deltas: impl Iterator<Item = usize>,
    rng: &mut R,
) -> AverageSphereDistanceProfile
where
    G: Graph,
    R: Rng + ?Sized,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let origin = graph.sample_node(rng);
        let AverageSphereDistance {
            asd,
            violation_ratio,
        } = average_sphere_distance(graph, origin, delta, rng);
        profile.push(asd);
        violation_ratios.push(violation_ratio);
    }

    AverageSphereDistanceProfile {
        profile: profile.into_boxed_slice(),
        violation_ratios: violation_ratios.into_boxed_slice(),
    }
}

/// Collection for keeping track of a list,
/// marking the items
#[derive(Debug, Clone)]
pub struct MarkedList<L: NodeLabel> {
    list: LabelledArray<Vec<<L::PatchCollection as PatchCollection>::Patch>, Label<Node>>,
    len: usize,
}

impl<'a, L: NodeLabel + 'a> MarkedList<L> {
    fn new(graph: &impl Graph) -> Self {
        let list = LabelledArray::fill(|| Vec::with_capacity(2), graph.len());
        MarkedList { list, len: 0 }
    }
}

impl<'a, L: NodeLabel + 'a> MarkedList<L> {
    /// Adds an iterator of labels to the [MarkedList]
    ///
    /// Note that the label is added without checking if it is already present in the in the
    /// [MarkedList] so duplicates may arise which, depending on use case, could break the use
    /// of the list.
    fn add_multiple_unchecked(&mut self, marks: impl Iterator<Item = &'a L>) {
        for mark_label in marks {
            self.add_unchecked(mark_label);
        }
    }
}

impl<L: NodeLabel> MarkedList<L> {
    #[inline]
    fn len(&self) -> usize {
        self.len
    }

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[inline]
    /// Adds a label to the [MarkedList]
    ///
    /// Note that the label is added without checking if it is already present in the in the
    /// [MarkedList] so duplicates may arise which, depending on use case, could break the use
    /// of the list.
    fn add_unchecked(&mut self, label: &L) {
        self.list[label.get_label()].push(label.get_patch());
        self.len += 1;
    }

    /// Try to remove the label from the [MarkedList], returns the [NodeLabelMatch::DifferentPatch]
    /// if the label was not found, [NodeLabelMatch::DifferentPatch] if it was found but in a
    /// different patch, and [NodeLabelMatch::Equal] if it was found in the same patch and is
    /// removed from the marked list.
    fn try_remove(&mut self, label: &L) -> NodeLabelMatch {
        let mut labelmatch = NodeLabelMatch::Different;
        if let Some(index) = self.list[label.get_label()].iter().position(|&patch| {
            if patch == label.get_patch() {
                labelmatch = NodeLabelMatch::Equal;
                true
            } else {
                labelmatch = NodeLabelMatch::DifferentPatch;
                false
            }
        }) {
            self.list[label.get_label()].swap_remove(index);
            self.len -= 1;
        }
        labelmatch
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    use crate::triangulation::FixedTriangulation;

    use super::*;

    fn random_triangulation<R: Rng + ?Sized>(rng: &mut R) -> FullTriangulation {
        random_triangulation_sized(50, 40, rng)
    }

    fn random_triangulation_sized<R: Rng + ?Sized>(
        vertex_length: usize,
        timeslices: usize,
        rng: &mut R,
    ) -> FullTriangulation {
        let mut triangulation = FixedTriangulation::new(vertex_length, timeslices);
        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(rng));
        }
        for _ in 0..1_000 {
            triangulation.flip(triangulation.sample_flip(rng));
            triangulation.relocate(
                triangulation.sample_order_four(rng),
                triangulation.sample(rng),
            )
        }
        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn geodesic_sphere() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(137);
        let graph = random_triangulation(rng).construct_vertex_graph();
        let node = graph.sample_node(rng);
        let neighbour_count = graph.get_links(node).count();
        let sphere = get_geodesic_sphere(&graph, node, 1);
        assert_eq!(neighbour_count, sphere.len());

        let delta = 2;
        println!(
            "{:?}",
            get_geodesic_sphere(&graph, graph.sample_node(rng), delta)
        )
    }

    #[test]
    fn average_sphere_distance_check() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(137);
        let delta = 2;
        let graph = random_triangulation(rng).construct_vertex_graph();
        let label = graph.sample_node(rng);

        let asd = average_sphere_distance(&graph, label, delta, rng).asd;
        println!("asd: {}", asd);
        println!("Finished");
    }

    #[test]
    fn average_sphere_distance_check_large() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(42);
        let delta = 2;
        let stacked_graph = random_triangulation(rng).construct_stacked_vertex_graph();
        let graph = TiledPlanarGraph::from_stacked_graph(stacked_graph);

        let mut asd = vec![];
        for _ in 0..20 {
            let label = graph.sample_node(rng);
            asd.push(average_sphere_distance(&graph, label, delta, rng).asd);
        }
        let mean_asd: f64 = (asd.iter().sum::<f64>()) / (asd.len() as f64);
        let std_asd: f64 = (asd.iter().fold(0.0, |acc, x| acc + (x - mean_asd).powi(2))
            / (asd.len() as f64 - 1.0))
            .sqrt();
        println!(
            "Average sphere distance: {:.2} +/- {:.2}",
            mean_asd,
            std_asd / (asd.len() as f64).sqrt()
        );
    }

    #[test]
    fn asd_violation_check() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(137);
        let max_delta = 6;
        let stacked_graph =
            random_triangulation_sized(30, 50, rng).construct_stacked_vertex_graph();
        let graph = TiledPlanarGraph::from_stacked_graph(stacked_graph);

        for _ in 0..3 {
            let profile = average_sphere_distance_profile(&graph, 0..max_delta, rng);
            println!("{}", profile);
        }
    }

    #[test]
    #[should_panic]
    fn asd_from_center() {
        let rng = &mut Xoshiro256StarStar::seed_from_u64(42);
        let graph = TiledPlanarGraph::from_stacked_graph(
            random_triangulation(rng).construct_stacked_vertex_graph(),
        );
        let delta = 6;

        println!("Calculating ASD");
        for _ in 0..50 {
            let label = graph.sample_node(rng);
            let asd_spheres = SpheresASD::from_center(&graph, label, delta, rng);
            let asd = asd_spheres.average_sphere_distance();
            println!("asd: {}", asd);
        }
    }
}
