//! Graph implementations of [`BoundedPeriodic2DGenericGraph`], a variant of
//! [`Periodic2DGenericGraph`] that is not infinite but has a bounded number of copies of
//! a base patch.

use crate::{
    collections::{Label, LabelledArray},
    graph::{
        field::{FieldValue, PeriodicField},
        generic_graph::Node,
        periodic1d_generic_graph::i8usize_central_mapping,
        periodic_graph::{BoundedPeriodicGraph, IterPeriodicGraph, PeriodicGraph},
        Field as FieldTrait, FieldGraph, Graph, IterableGraph, SampleGraph,
    },
};

use super::{
    Boundary2D, Field, NodeLinks, Patch2D, PatchIterator, Periodic2DGenericGraph, Periodic2DLabel,
};

/// Struct to represet a periodic spacetime graph with a maximum amount of patches
/// from (-xmax, -tmax) to (+xmax, +tmax)
pub struct BoundedPeriodic2DGenericGraph {
    graph: Periodic2DGenericGraph,
    xmax: i8,
    ymax: i8,
}

impl BoundedPeriodic2DGenericGraph {
    /// Create a [`BoundedPeriodic2DGenericGraph`] by adding a bound on the number of patches that
    /// can be used.
    ///
    /// `xmax` and `ymax` give the max extention in the `x`- and `y`-direction respectively,
    /// such that there are `2*xmax + 1` patches in the `x`-direction and an equivalent number in
    /// the `y`-direction.
    pub fn from_periodic_graph(graph: Periodic2DGenericGraph, xmax: u8, ymax: u8) -> Self {
        Self {
            graph,
            xmax: xmax as i8,
            ymax: ymax as i8,
        }
    }
}

impl Graph for BoundedPeriodic2DGenericGraph {
    type Label = Periodic2DLabel;

    #[inline]
    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.graph.get_vertex_degree(label)
    }
}

impl FieldGraph for BoundedPeriodic2DGenericGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        self.graph.new_field()
    }
}

impl<T> FieldTrait<T, BoundedPeriodic2DGenericGraph> for Field<T> {
    fn get(&self, label: Periodic2DLabel) -> &FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.x);
        let inner_index = i8usize_central_mapping(label.patch.y);
        match self
            .0
            .get(outer_index)
            .and_then(|list| list.get(inner_index))
        {
            None => self.none(),
            Some(values) => &values[label.label],
        }
    }

    fn get_mut(&mut self, label: Periodic2DLabel) -> &mut FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.x);
        let inner_index = i8usize_central_mapping(label.patch.y);
        let label = label.label;
        let node_count = self
            .0
            .first()
            .and_then(|list| list.first())
            .expect("First element should be initialized by creation")
            .len();

        if self.0.len() <= outer_index {
            self.0.resize_with(outer_index + 1, Vec::new);
        }
        let inner_list = &mut self.0[outer_index];
        if inner_list.len() <= inner_index {
            inner_list.resize_with(inner_index + 1, || {
                LabelledArray::fill_with(|| FieldValue::None, node_count)
            });
        };

        &mut inner_list[inner_index][label]
    }
}

impl<T> PeriodicField<T, BoundedPeriodic2DGenericGraph> for Field<T> {
    fn has(
        &self,
        label: <BoundedPeriodic2DGenericGraph as Graph>::Label,
    ) -> crate::graph::periodic_graph::PeriodicLabelMatch {
        <Field<T> as PeriodicField<T, Periodic2DGenericGraph>>::has(self, label)
    }

    fn has_in_patches(&self, label: Label<Node>) -> usize {
        <Field<T> as PeriodicField<T, Periodic2DGenericGraph>>::has_in_patches(self, label)
    }

    fn has_patch(&self, patch: <BoundedPeriodic2DGenericGraph as PeriodicGraph>::Patch) -> bool {
        <Field<T> as PeriodicField<T, Periodic2DGenericGraph>>::has_patch(self, patch)
    }
}

impl PeriodicGraph for BoundedPeriodic2DGenericGraph {
    type Patch = <Periodic2DGenericGraph as PeriodicGraph>::Patch;
    #[inline]
    fn base_len(&self) -> usize {
        self.graph.base_len()
    }
}

impl SampleGraph for BoundedPeriodic2DGenericGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        self.graph.sample_node(rng)
    }
}

impl<'a> IterableGraph<'a, IterNeighboursBounded<'a>> for BoundedPeriodic2DGenericGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighboursBounded<'a> {
        IterNeighboursBounded::from_graph(self, label)
    }
}

/// Iterator over neighbours of nodes in [`BoundedPeriodic2DGenericGraph`]
#[derive(Debug, Clone)]
pub struct IterNeighboursBounded<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<Boundary2D>>,
    patch: Patch2D,
    cursor: usize,
    xmax: i8,
    ymax: i8,
}

impl<'a> IterNeighboursBounded<'a> {
    /// Start iterator at index 0 going around clockwise
    fn new(
        node: &'a Node,
        bound: Option<&'a NodeLinks<Boundary2D>>,
        patch: Patch2D,
        xmax: i8,
        ymax: i8,
    ) -> Self {
        IterNeighboursBounded {
            node,
            bound,
            patch,
            cursor: 0,
            xmax,
            ymax,
        }
    }

    fn from_graph(graph: &'a BoundedPeriodic2DGenericGraph, label: Periodic2DLabel) -> Self {
        Self::new(
            &graph.graph.nodes[label.label],
            graph.graph.bounds[label.label].as_ref(),
            label.patch,
            graph.xmax,
            graph.ymax,
        )
    }

    #[inline]
    fn get_bound(&self, link: usize) -> Boundary2D {
        if let Some(bounds) = self.bound {
            bounds.0[link]
        } else {
            Boundary2D::Center
        }
    }
}

impl<'a> Iterator for IterNeighboursBounded<'a> {
    type Item = Periodic2DLabel;

    fn next(&mut self) -> Option<Self::Item> {
        let label = self.node.get_neighbour(self.cursor)?;
        let bound = self.get_bound(self.cursor);
        self.cursor += 1;
        let patch = match bound {
            Boundary2D::Center => self.patch,
            Boundary2D::West => self.patch.add_x(-1),
            Boundary2D::NorthWest => self.patch.add(-1, 1),
            Boundary2D::North => self.patch.add_y(1),
            Boundary2D::NorthEast => self.patch.add(1, 1),
            Boundary2D::East => self.patch.add_x(1),
            Boundary2D::SouthEast => self.patch.add(1, -1),
            Boundary2D::South => self.patch.add_y(-1),
            Boundary2D::SouthWest => self.patch.add(-1, -1),
        };
        let patch = patch.modulo(self.xmax, self.ymax);
        Some(Periodic2DLabel { label, patch })
    }
}

impl IterPeriodicGraph<PatchIterator> for BoundedPeriodic2DGenericGraph {
    #[inline]
    fn iter_base(&self) -> PatchIterator {
        self.graph.iter_base()
    }

    #[inline]
    fn iter_patch(&self, patch: Self::Patch) -> PatchIterator {
        self.graph.iter_patch(patch)
    }
}

/// Iterator over all the patches of the bounded graph [`BoundedPeriodic2DGenericGraph`]
pub struct IterPatches {
    xmax: i8,
    ymax: i8,
    x: i8,
    y: i8,
}

impl IterPatches {
    fn new(xmax: i8, ymax: i8) -> Self {
        Self {
            xmax,
            ymax,
            x: -xmax,
            y: -ymax,
        }
    }
}

impl Iterator for IterPatches {
    type Item = Patch2D;

    fn next(&mut self) -> Option<Self::Item> {
        let current_patch = Patch2D::new(self.x, self.y);
        self.x += 1;
        if self.x > self.xmax {
            self.y += 1;
            if self.y > self.ymax {
                return None;
            }
            self.x = -self.xmax;
        }

        Some(current_patch)
    }
}

impl BoundedPeriodicGraph<IterPatches> for BoundedPeriodic2DGenericGraph {
    fn iter_patches(&self) -> IterPatches {
        IterPatches::new(self.xmax, self.ymax)
    }
}
