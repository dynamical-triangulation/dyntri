//! Module implementing cylindirical embeddings of 2D CDT graphs
//!
//! By cylindrical embedding an embedding with the topology of a cylinder is meant

use argmin::core::{CostFunction, Executor, Gradient};
use argmin::solver::gradientdescent::SteepestDescent;
use argmin::solver::linesearch::MoreThuenteLineSearch;
use log::debug;
use ndarray::{arr2, Array1, Array2, ArrayView1};
use num_traits::Zero;

use std::f64::consts::{FRAC_PI_3, PI, TAU};
use std::fmt::Display;

use crate::collections::{Label, LabelledArray};
use crate::io::embedding3d::{Edge, EdgeKind, Embedding3D, Embedding3DField, Triangle, Vertex};
use crate::triangulation::full_triangulation::vertex::Vertex as TriangulationVertex;
use crate::triangulation::{FullTriangulation, Orientation};

use super::Point3D;

const TIMESLICE_ROTATION_MAX_ITERATIONS: u64 = 100;
const EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION: u64 = 5000;
const EDGEDISTANCE_DIFFERENCE_TOLERANCE: f64 = 1e-3;
const TIMESLICE_WANTED_AVERAGE_LENGTH: f64 = 1.0;

type VertexLabel = Label<TriangulationVertex>;

/// Struct for holding the embedding of a [`FullTriangulation`]
pub struct CylindricalEmbedding<'a> {
    triangulation: &'a FullTriangulation,
    coordinates: LabelledArray<Point3D, VertexLabel>,
}

impl CylindricalEmbedding<'_> {
    fn get_time(&self, label: VertexLabel) -> usize {
        self.triangulation.get_vertex(label).time()
    }
}

fn compute_coordinates(t: usize, i: usize, length: usize) -> Point3D {
    let t = t as f64;
    let i = i as f64;
    let length = length as f64;

    let th = ((i + 0.5 * t) / length) * TAU;
    // Choose `r` such that distance between vertices is 1.0
    let r = 0.5 * length / (length * (PI / length).sin());

    Point3D {
        x: r * th.cos(),
        y: r * th.sin(),
        // Choose `z` such that timelike distance is 1 for flat triangulation
        z: t * FRAC_PI_3.sin(),
    }
}

impl<'a> CylindricalEmbedding<'a> {
    /// Generates an simple cylindrical embedding with independent timeslices
    /// kept in equidistantly spaced planes.
    pub fn initialize(triangulation: &'a FullTriangulation) -> Self {
        // Create empty list of coordinates
        let mut coordinates: LabelledArray<Option<Point3D>, VertexLabel> =
            LabelledArray::fill(None, triangulation.vertex_count());

        // Compute the coordinates of all vertices by walking through the timeslices
        let tmax = triangulation.time_len();
        for t in 0..tmax {
            let length = triangulation.timeslice_length(t);
            for (i, vertex) in triangulation.iter_time(t).enumerate() {
                coordinates[vertex] = Some(compute_coordinates(t, i, length));
            }
        }

        let coordinates =
            coordinates.map(|element| element.expect("A vertex coordinate was not set"));
        CylindricalEmbedding {
            triangulation,
            coordinates,
        }
    }
}

impl CylindricalEmbedding<'_> {
    pub fn export(&self) -> Embedding3D {
        let mut vertices: Vec<Vertex> = self
            .coordinates
            .enumerate()
            .map(|(label, point)| {
                let coordinate = (point.x, point.y, point.z);
                let time = self.triangulation.get_vertex(label).time() as i32;
                Vertex { coordinate, time }
            })
            .collect();

        let timelen = self.triangulation.time_len();
        let mut final_mapping: LabelledArray<Option<usize>, VertexLabel> =
            LabelledArray::fill(None, self.triangulation.vertex_count());
        for label in self.triangulation.iter_time(0) {
            final_mapping[label] = Some(vertices.len());
            vertices.push({
                let point0 = &self.coordinates[label];
                let coordinate = (
                    point0.x,
                    point0.y,
                    point0.z + timelen as f64 * FRAC_PI_3.sin(),
                );
                let time = timelen as i32;
                Vertex { coordinate, time }
            })
        }

        let mut triangles = Vec::with_capacity(self.triangulation.size());
        let mut edges = Vec::with_capacity(3 * self.triangulation.size() / 2);
        for triangle_label in self.triangulation.triangles.labels() {
            let triangle = self.triangulation.get_triangle(triangle_label);

            let vleft: usize = triangle.left_vertex.into();
            let vcenter: usize = triangle.center_vertex.into();
            let vright: usize = triangle.right_vertex.into();

            if triangle.time() == timelen - 1 {
                match triangle.orientation {
                    Orientation::Up => {
                        let vcenter_mapped = final_mapping[triangle.center_vertex]
                            .expect("Must be set because it is part of timeslice 0");
                        edges.push(Edge {
                            start: vleft,
                            end: vright,
                            kind: EdgeKind::Spacelike,
                        });
                        edges.push(Edge {
                            start: vleft,
                            end: vcenter_mapped,
                            kind: EdgeKind::Timelike,
                        });
                        triangles.push(Triangle {
                            vertices: (vleft, vcenter_mapped, vright),
                            label: triangle_label.into(),
                        });
                    }
                    Orientation::Down => {
                        let vleft_mapped = final_mapping[triangle.left_vertex]
                            .expect("Must be set because it is part of timeslice 0");
                        let vright_mapped = final_mapping[triangle.right_vertex]
                            .expect("Must be set because it is part of timeslice 0");
                        edges.push(Edge {
                            start: vleft_mapped,
                            end: vright_mapped,
                            kind: EdgeKind::Spacelike,
                        });
                        edges.push(Edge {
                            start: vcenter,
                            end: vleft_mapped,
                            kind: EdgeKind::Timelike,
                        });
                        triangles.push(Triangle {
                            vertices: (vleft_mapped, vright_mapped, vcenter),
                            label: triangle_label.into(),
                        });
                    }
                };
                continue;
            }

            edges.push(Edge {
                start: vleft,
                end: vcenter,
                kind: EdgeKind::Timelike,
            });
            if matches!(triangle.orientation, Orientation::Up) {
                edges.push(Edge {
                    start: vleft,
                    end: vright,
                    kind: EdgeKind::Spacelike,
                });
            }

            let tvertices: (usize, usize, usize) = match triangle.orientation {
                Orientation::Up => (vleft, vcenter, vright),
                Orientation::Down => (vleft, vright, vcenter),
            };
            triangles.push(Triangle {
                vertices: tvertices,
                label: triangle_label.into(),
            });
        }

        Embedding3D {
            vertices,
            edges,
            triangles,
        }
    }

    pub fn export_with_field<T: Display + Copy>(
        &self,
        field: ArrayView1<T>,
    ) -> Embedding3DField<T> {
        let embedding = self.export();
        let fullfield = self
            .coordinates
            .labels()
            .map(|label| field[usize::from(label)])
            .chain(
                self.triangulation
                    .iter_time(0)
                    .map(|label| field[usize::from(label)]),
            )
            .collect();
        Embedding3DField {
            embedding,
            field: fullfield,
        }
    }
}

struct SlicedEmbedding<'a> {
    embedding: &'a CylindricalEmbedding<'a>,
}

impl<'a> CostFunction for SlicedEmbedding<'a> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, angles: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let tlen = self.embedding.triangulation.time_len();
        let mut counter = 0.0;
        let mut total_cost = 0.0;
        for label in self.embedding.triangulation.vertices.labels() {
            let x0 = &self.embedding.coordinates[label];
            let t0 = self.embedding.get_time(label);
            for &nbr in self.embedding.triangulation.get_vertex(label).up.iter() {
                let t1 = self.embedding.get_time(nbr);
                let mut x1 = self.embedding.coordinates[nbr];
                if t0 == tlen - 1 && t1 == 0 {
                    x1.z += tlen as f64 * FRAC_PI_3.sin();
                }
                let x0rot = x0.rotated_z(angles[t0]);
                let x1rot = x1.rotated_z(angles[t1]);
                let cost = x0rot.distance2(&x1rot);

                counter += 1.0;
                total_cost += cost;
            }
        }

        Ok(total_cost / counter)
    }
}

impl<'a> Gradient for SlicedEmbedding<'a> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, angles: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let tlen = self.embedding.triangulation.time_len();
        let tmatrices: Vec<Array2<f64>> = angles
            .iter()
            .map(|a| {
                arr2(&[
                    [-a.sin(), -a.cos(), 0.0],
                    [a.cos(), -a.sin(), 0.0],
                    [0.0, 0.0, 0.0],
                ])
            })
            .collect();

        let mut counter = 0.0;
        let mut gradient = Array1::zeros([tlen]);
        for label in self.embedding.triangulation.vertices.labels() {
            let x0 = &self.embedding.coordinates[label];
            let t0 = self.embedding.get_time(label);
            for &nbr in self.embedding.triangulation.get_vertex(label).up.iter() {
                let t1 = self.embedding.get_time(nbr);
                let mut x1 = self.embedding.coordinates[nbr];
                if t0 == tlen - 1 && t1 == 0 {
                    x1.z += tlen as f64 * FRAC_PI_3.sin();
                }
                let x0rot = x0.rotated_z(angles[t0]);
                let x1rot = x1.rotated_z(angles[t1]);
                let dxrot = x1rot - x0rot;
                let x0trans = x0.transformed(tmatrices[t0].view());
                let x1trans = x1.transformed(tmatrices[t1].view());
                gradient[t0] -= 2.0 * dxrot.dot(&x0trans);
                gradient[t1] += 2.0 * dxrot.dot(&x1trans);

                counter += 1.0;
            }
        }

        gradient /= counter;
        gradient -= gradient[0];

        Ok(gradient)
    }
}

impl CylindricalEmbedding<'_> {
    pub fn rotate_slices(&mut self) {
        let sliced_embedding = SlicedEmbedding { embedding: self };
        let initial_rotations = Array1::zeros(self.triangulation.time_len());
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        debug!(
            "Initial cost: {}",
            sliced_embedding
                .cost(&initial_rotations)
                .expect("Initial cost should exist")
        );
        let result = Executor::new(sliced_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_rotations.clone())
                    .max_iters(TIMESLICE_ROTATION_MAX_ITERATIONS)
                    .target_cost(TIMESLICE_WANTED_AVERAGE_LENGTH)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        debug!(
            "Terminated best rotation search with {}",
            result.state().termination_status
        );
        debug!("Best cost: {}", result.state.get_best_cost());
        let best_rotations = result
            .state
            .best_param
            .expect("Optimal rotations could not be determined");
        for label in self.coordinates.labels() {
            let t = self.get_time(label);
            self.coordinates[label].rotate_z(best_rotations[t]);
        }
    }
}

struct MinimalDistanceEmbedding<'a> {
    embedding: &'a CylindricalEmbedding<'a>,
    power: i32,
}

impl<'a> MinimalDistanceEmbedding<'a> {
    /// Returns iterator over every edge twice
    fn iter_edges(&'a self) -> impl Iterator<Item = (VertexLabel, VertexLabel)> + 'a {
        self.embedding
            .triangulation
            .iter_triangles()
            .flat_map(|triangle| {
                [
                    (triangle.left_vertex, triangle.right_vertex),
                    (triangle.left_vertex, triangle.center_vertex),
                    (triangle.right_vertex, triangle.center_vertex),
                ]
                .into_iter()
            })
    }
}

impl CostFunction for MinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, param: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let vertex_count = self.embedding.triangulation.vertex_count();
        let tlen = self.embedding.triangulation.time_len();
        let coordinates: LabelledArray<Point3D, VertexLabel> = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        let mut cost = 0.0;
        let mut count = 0;
        for (label0, label1) in self.iter_edges() {
            let mut v0: Point3D = coordinates[label0];
            let mut v1: Point3D = coordinates[label1];
            let t0 = self.embedding.get_time(label0);
            let t1 = self.embedding.get_time(label1);
            if t0 == 0 && t1 == tlen - 1 {
                v0.z += tlen as f64 * FRAC_PI_3.sin();
            } else if t0 == tlen - 1 && t1 == 0 {
                v1.z += tlen as f64 * FRAC_PI_3.sin();
            }
            cost += (v0.distance2(&v1) - 1.0).powi(self.power);
            count += 1;
        }
        Ok(0.5 * cost / count as f64)
    }
}
impl Gradient for MinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let nu = self.power;
        let vertex_count = self.embedding.triangulation.vertex_count();
        let tlen = self.embedding.triangulation.time_len();
        let coordinates: LabelledArray<Point3D, VertexLabel> = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        let mut gradient = LabelledArray::fill_with(
            || Point3D {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            vertex_count,
        );

        let mut count = 0;
        for (label0, label1) in self.iter_edges() {
            let mut v0: Point3D = coordinates[label0];
            let mut v1: Point3D = coordinates[label1];
            let t0 = self.embedding.get_time(label0);
            let t1 = self.embedding.get_time(label1);
            if t0 == 0 && t1 == tlen - 1 {
                v0.z += tlen as f64 * FRAC_PI_3.sin();
            } else if t0 == tlen - 1 && t1 == 0 {
                v1.z += tlen as f64 * FRAC_PI_3.sin();
            }
            let dv = v1 - v0;
            let prefactor = (nu as f64) * (dv.norm2() - 1.0).powi(nu - 1);
            gradient[label1] += dv * prefactor;
            gradient[label0] -= dv * prefactor;
            count += 1;
        }
        let norm = 0.5 / count as f64;
        let gradient = Array1::from_vec(gradient.into_vec());
        let grad_mean = gradient.sum() / vertex_count as f64;
        let gradient = Array1::from_iter(gradient.iter().flat_map(|v| {
            [
                norm * (v.x - grad_mean.x),
                norm * (v.y - grad_mean.y),
                norm * (v.z - grad_mean.z),
            ]
            .into_iter()
        }));
        Ok(gradient)
    }
}

struct InflatedMinimalDistanceEmbedding<'a> {
    embedding: &'a MinimalDistanceEmbedding<'a>,
    power: f64,
    strength: f64,
}

impl InflatedMinimalDistanceEmbedding<'_> {
    fn triangulation(&self) -> &FullTriangulation {
        self.embedding.embedding.triangulation
    }
}

impl CostFunction for InflatedMinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, param: &Self::Param) -> Result<Self::Output, argmin::core::Error> {
        let minimal_cost = self.embedding.cost(param)?;
        let vertex_count = self.triangulation().vertex_count();

        let sum_cost: f64 = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .enumerate()
            .map(|(ilabel, row)| {
                let t = self.embedding.embedding.get_time(Label::from(ilabel));
                let tvol = self.triangulation().timeslice_length(t) as f64;
                let base_r = 0.5 * tvol / (tvol * (PI / tvol).sin());
                let x = row[0];
                let y = row[1];
                let base_cost = base_r.powf(2.0 * self.power);
                base_cost * (x.powi(2) + y.powi(2)).powf(-self.power)
            })
            .sum();
        let inflation_cost = self.strength * sum_cost / vertex_count as f64;

        Ok(minimal_cost + inflation_cost)
    }
}

impl Gradient for InflatedMinimalDistanceEmbedding<'_> {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let minimal_gradient = self.embedding.gradient(param)?;
        let vertex_count = self.triangulation().vertex_count();

        let sum_cost: Array1<f64> = param
            .to_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .enumerate()
            .flat_map(|(ilabel, row)| {
                let t = self.embedding.embedding.get_time(Label::from(ilabel));
                let tvol = self.triangulation().timeslice_length(t) as f64;
                let base_r = 0.5 * tvol / (tvol * (PI / tvol).sin());
                let base_cost = base_r.powf(2.0 * self.power);

                let x = row[0];
                let y = row[1];
                let force_factor = (x.powi(2) + y.powi(2)).powf(-(self.power + 1.0));
                let prefactor = -2.0 * self.power * force_factor * base_cost;
                [prefactor * x, prefactor * y, 0.0].into_iter()
            })
            .collect();
        let total_gradient = self.strength * sum_cost + minimal_gradient;
        Ok(total_gradient)
    }
}

impl CylindricalEmbedding<'_> {
    pub fn minimize_edge_lengths(&mut self) {
        let min_embedding = MinimalDistanceEmbedding {
            embedding: self,
            power: 2,
        };
        let vertex_count = self.triangulation.vertex_count();
        println!(
            "Initial Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        let initial_coordinates = Array1::from_iter(
            self.coordinates
                .iter()
                .flat_map(|v| [v.x, v.y, v.z].into_iter()),
        );
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        debug!(
            "Initial cost: {}",
            min_embedding
                .cost(&initial_coordinates)
                .expect("Initial cost should exist")
        );
        let result = Executor::new(min_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_coordinates)
                    .max_iters(EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION)
                    .target_cost(EDGEDISTANCE_DIFFERENCE_TOLERANCE)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        debug!(
            "Terminated contant edge length search with {}",
            result.state().termination_status
        );
        debug!("Best cost: {}", result.state.get_best_cost());
        let best_coordinates: Vec<Point3D> = result
            .state
            .best_param
            .expect("Optimal coordinates could not be determined")
            .into_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        println!(
            "Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        self.coordinates = LabelledArray::from(best_coordinates);
    }

    pub fn minimize_edge_lengths_inflate(&mut self, power: f64, strength: f64) {
        let min_embedding = MinimalDistanceEmbedding {
            embedding: self,
            power: 2,
        };
        let inflated_embedding = InflatedMinimalDistanceEmbedding {
            embedding: &min_embedding,
            power,
            strength,
        };
        let vertex_count = self.triangulation.vertex_count();
        println!(
            "Initial Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        let initial_coordinates = Array1::from_iter(
            self.coordinates
                .iter()
                .flat_map(|v| [v.x, v.y, v.z].into_iter()),
        );
        let linesearch = MoreThuenteLineSearch::new();
        let solver = SteepestDescent::new(linesearch);
        debug!(
            "Initial cost: {}",
            min_embedding
                .cost(&initial_coordinates)
                .expect("Initial cost should exist")
        );
        let result = Executor::new(inflated_embedding, solver)
            .configure(|state| {
                state
                    .param(initial_coordinates)
                    .max_iters(EDGEDISTANCE_MINIMALIZATION_MAX_ITERATION)
                    .target_cost(EDGEDISTANCE_DIFFERENCE_TOLERANCE)
            })
            .run()
            .expect("The gradient decent could not run correctly");
        debug!(
            "Terminated contant edge length search with {}",
            result.state().termination_status
        );
        debug!("Best cost: {}", result.state.get_best_cost());
        debug!(
            "Best cost edge length: {}",
            min_embedding
                .cost(result.state.best_param.as_ref().unwrap())
                .unwrap()
        );
        let best_coordinates: Vec<Point3D> = result
            .state
            .best_param
            .expect("Optimal coordinates could not be determined")
            .into_shape([vertex_count, 3])
            .expect("The parameter should be 3*vertex_count by construction")
            .rows()
            .into_iter()
            .map(|row| Point3D {
                x: row[0],
                y: row[1],
                z: row[2],
            })
            .collect();
        println!(
            "Com: {}",
            self.coordinates.iter().fold(Point3D::zero(), |c, &v| c + v) / vertex_count as f64
        );
        self.coordinates = LabelledArray::from(best_coordinates);
    }
}
