//! An implementation of the breadth first search algorithm being generic of what information is
//! recorded at each step in the search.

use std::{collections::VecDeque, marker::PhantomData};

use self::traverse_nodes::TraverseNodeLabel;

use super::{
    field::{Field, PeriodicField},
    periodic_graph::{PeriodicGraph, PeriodicLabel, PeriodicLabelMatch},
    FieldGraph, Graph, IterableGraph,
};

/// Trait representing an iterator that iterators over the neighbours of a node in a graph
pub trait IterNeighbours<G: Graph>: Iterator<Item = G::Label> {}

impl<G: Graph, I: Iterator<Item = G::Label>> IterNeighbours<G> for I {}

/// Struct used to keep trach of the process of a breadth first search
///
/// `N` should be a [`TraverseNode`] dictating what information is kept track of during
/// the BFS. `I` should be a [`IterNeighbours`] dictating how the neighbours are determined.
pub struct IterBreadthFirst<'a, G, I, N = TraverseNodeLabel<<G as Graph>::Label>>
where
    G: FieldGraph,
{
    graph: &'a G,
    to_visit: VecDeque<N>,
    explored: G::FieldType<()>,
    neighbour_exploration_type: PhantomData<I>,
}

impl<'a, I, G: FieldGraph + 'a, N: TraverseNode<G::Label>> IterBreadthFirst<'a, G, I, N> {
    /// Create a new breadth first search iterator for a given `origin`
    /// Define `N` to be the [`TraverseNode`] implementation that is desired to indicate what
    /// information to keep track of; for pre-defined implementation see [`traverse_nodes`].
    pub fn new(graph: &'a G, origin: G::Label) -> Self {
        // OPTIMIZABLE: Pre-allocate space with some good estimation
        let mut to_visit = VecDeque::new();
        let mut explored = graph.new_field();
        to_visit.push_back(N::origin(origin));
        explored.get_or_insert(origin, ());
        Self {
            graph,
            to_visit,
            explored,
            neighbour_exploration_type: PhantomData,
        }
    }
}

impl<'a, I, G: FieldGraph, N: TraverseNode<G::Label>> IterBreadthFirst<'a, G, I, N> {
    /// Returns whether the given `label` is in the explored list.
    /// This is the list containing the labels that have been iterator over and
    /// their neighbours that have been identified as next possible candidates.
    pub fn explored(&'a self, label: G::Label) -> bool {
        self.explored.has(label)
    }
}

impl<'a, I, G, N> IterBreadthFirst<'a, G, I, N>
where
    G: FieldGraph + PeriodicGraph,
    N: TraverseNode<G::Label>,
    G::FieldType<()>: PeriodicField<(), G>,
    G::Label: PeriodicLabel,
{
    /// Returns whether the given `label` has already been explored
    /// This is the list containing the labels that have been iterator over and
    /// their neighbours that have been identified as next possible candidates.
    pub fn explored_periodic(&'a self, label: G::Label) -> PeriodicLabelMatch {
        PeriodicField::has(&self.explored, label)
    }

    /// Returns whether another patch copy of the given `label` has already been
    /// explored. The explored list, is the list containing the labels that have
    /// been iterator over and their neighbours that have been identified as
    /// next possible candidates.
    pub fn explored_other_patch(&'a self, label: G::Label) -> bool {
        PeriodicField::has_in_other_patch(&self.explored, label)
    }
}

/// Trait for defined what information should be kept track of in a breadth first search.
/// For pre-defined implementations see [`traverse_nodes`].
///
/// Every implementation of this trait should at least keep track of the label of the node, but can
/// store additional information if desired.
pub trait TraverseNode<L> {
    /// Returns a [`TraverseNode`] of a given `label`
    fn origin(label: L) -> Self;

    /// Returns a [`TraverseNode`] of the neighbour `label` of `self`
    fn succ(&self, nbr_label: L) -> Self;

    /// Returns the label of the [`TraverseNode`]
    fn label(&self) -> L;
}

// pub struct LabelledTraverseNode<L, N> {
//     pub label: L,
//     pub info: N,
// }

impl<'a, G, I, N> Iterator for IterBreadthFirst<'a, G, I, N>
where
    G: IterableGraph<'a, I>,
    I: IterNeighbours<G>,
    N: TraverseNode<G::Label>,
{
    type Item = N;

    fn next(&mut self) -> Option<Self::Item> {
        // Get next node with info to visit
        let traversed_node = self.to_visit.pop_front()?;
        let label = traversed_node.label();
        // Find all new neighbours and add them to list `to_vist`
        self.to_visit.extend(
            self.graph
                .iter_neighbours(label)
                // Filter out the neighbours that are already explored
                .filter(|&nbr| {
                    // Add neighbour to `explored` list and check whether
                    // it was previously unexplored
                    self.explored.set(nbr, ())
                })
                // Update the additional info of the neighbours
                .map(|nbr| traversed_node.succ(nbr)),
        );

        // Return the currently explored node
        Some(traversed_node)
    }
}

pub mod traverse_nodes {
    //! Module containing examples of implementations of [`crate::graph::TraverseNode`],
    //! which can be used to do a breadth first search with [`crate::graph::IterableGraph::iter_breadth_first()`]
    use super::*;

    /// Simplest [`crate::graph::TraverseNode`], which tracks no info other than the label
    #[derive(Debug, Clone, Copy)]
    pub struct TraverseNodeLabel<L> {
        /// The label of the node represented by the [`TraverseNodeLabel`]
        pub label: L,
    }

    impl<L: Copy> TraverseNode<L> for TraverseNodeLabel<L> {
        fn origin(label: L) -> Self {
            TraverseNodeLabel { label }
        }

        fn succ(&self, label: L) -> Self {
            TraverseNodeLabel { label }
        }

        fn label(&self) -> L {
            self.label
        }
    }

    /// A [`crate::graph::TraverseNode`] which tracks the distance of the traversed
    /// node to the original node of the breadth first search.
    #[derive(Debug, Clone, Copy)]
    pub struct TraverseNodeDistance<L> {
        /// The label of the node represented by the [`TraverseNodeDistance`]
        pub label: L,
        /// The distance of the node away from the origin in a BFS process
        pub distance: usize,
    }

    impl<L: Copy> TraverseNode<L> for TraverseNodeDistance<L> {
        fn origin(label: L) -> Self {
            TraverseNodeDistance { label, distance: 0 }
        }

        fn succ(&self, label: L) -> Self {
            TraverseNodeDistance {
                label,
                distance: self.distance + 1,
            }
        }

        fn label(&self) -> L {
            self.label
        }
    }
}
