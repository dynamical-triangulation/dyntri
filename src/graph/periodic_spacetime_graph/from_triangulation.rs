use crate::graph::spacetime_graph::IterLinksFromTo;
use crate::graph::timeperiodic_spacetime_graph::{
    path::{shortest_forward_path, ClosedPath},
    TimeBoundary, TimePeriodicLabel, TimePeriodicSpacetimeGraph,
};
use crate::triangulation::full_triangulation::{triangle::Triangle, vertex::Vertex};
use crate::triangulation::FullTriangulation;

use super::*;

impl PeriodicSpacetimeGraph {
    /// Note that the `usize` numbers the labels correspond to are the same between the vertices
    /// of the [`FullTriangulation`] and the nodes of the resulting graph
    pub fn from_triangulation_vertex(triangulation: &FullTriangulation) -> Self {
        let timeperiodic_vertex_graph =
            TimePeriodicSpacetimeGraph::from_triangulation_vertex(triangulation);
        construct_spacetime_periodic_graph(&timeperiodic_vertex_graph)
    }

    pub fn from_triangulation_dual(triangulation: &FullTriangulation) -> Self {
        let timeperiodic_dual_graph =
            TimePeriodicSpacetimeGraph::from_triangulation_dual(triangulation);
        construct_spacetime_periodic_graph(&timeperiodic_dual_graph)
    }

    pub fn from_timeperiodic_graph(graph: &TimePeriodicSpacetimeGraph) -> Self {
        construct_spacetime_periodic_graph(graph)
    }

    /// Note that the `usize` numbers the labels correspond to are the same between the vertices
    /// of the [`FullTriangulation`] and the nodes of the resulting graph
    pub fn from_triangulation_vertex_cut_at(
        triangulation: &FullTriangulation,
        cutting_label: Label<Vertex>,
    ) -> Self {
        let timeperiodic_vertex_graph =
            TimePeriodicSpacetimeGraph::from_triangulation_vertex_cut_at(
                triangulation,
                cutting_label,
            );
        construct_spacetime_periodic_graph_cut_at(
            &timeperiodic_vertex_graph,
            cutting_label.convert(),
        )
    }

    pub fn from_triangulation_dual_cut_at(
        triangulation: &FullTriangulation,
        cutting_label: Label<Triangle>,
    ) -> Self {
        let timeperiodic_dual_graph = TimePeriodicSpacetimeGraph::from_triangulation_dual_cut_at(
            triangulation,
            cutting_label,
        );
        construct_spacetime_periodic_graph_cut_at(&timeperiodic_dual_graph, cutting_label.convert())
    }

    pub fn from_timeperiodic_graph_cut_at(
        graph: &TimePeriodicSpacetimeGraph,
        cutlabel: Label<Node>,
    ) -> Self {
        construct_spacetime_periodic_graph_cut_at(graph, cutlabel)
    }
}

fn construct_spacetime_periodic_graph(
    timeperiodic_graph: &TimePeriodicSpacetimeGraph,
) -> PeriodicSpacetimeGraph {
    construct_spacetime_periodic_graph_cut_at(timeperiodic_graph, Label::initial())
}

/// Generate a [`TiledPlanarGraph`] from a [`StackedCylinderGraph`] by making an arbitrary vertical cut.
///
/// This procedure may fail if the graph has diameter 2 or less in one of the dimensions
pub fn construct_spacetime_periodic_graph_cut_at(
    timeperiodic_graph: &TimePeriodicSpacetimeGraph,
    initial_label: Label<Node>,
) -> PeriodicSpacetimeGraph {
    // Construct non-contractible path next to which we will cut the graph open
    let start_label = TimePeriodicLabel::new(initial_label, 0);
    let end_label = TimePeriodicLabel::new(initial_label, 1);

    let path = shortest_forward_path(timeperiodic_graph, start_label, end_label);
    let closed_path =
        ClosedPath::from_path(&path).expect("The obtained path should always be closed");

    // Add relevant bounds to make the vertical (spatial) cut in the graph open
    let nodes = &timeperiodic_graph.nodes;
    let mut bounds: BoundaryList =
        LabelledArray::fill_with(|| None, timeperiodic_graph.nodes.len());
    for &pathnode in closed_path.iter() {
        let label = pathnode.label;
        let node = &nodes[label];
        bounds[label] = Some(NodeLinks::fill_like_node(
            || SpacetimeBoundary::Center,
            node,
        ));
        for link in IterLinksFromTo::new(&nodes[label], pathnode.out_link, pathnode.in_link).skip(1)
        {
            // Set boundaries for node in path
            bounds[label]
                .as_mut()
                .expect("Already initialized by construction")
                .set_neighbour(link, SpacetimeBoundary::Right);
            // Set boundaries for all nodes to the right of the path (the reverse connections)
            let nbr = node.get_neighbour_unchecked(link);
            let nbr_node = &nodes[nbr];
            // TODO: This works because I choose to start searching Left, but it could fail
            // if this arbitrary choice is not made for size 1 slices, so should look for better
            // alternative that can deal with multiple links to the same vertex
            let back_link = nbr_node
                .get_link_of_neighbour(label)
                .expect("The original node should always be a neighbour of its neighbour");
            if nbr == label {
                println!("label: {label} goes to itself with backlink {back_link:?}");
            }
            bounds[nbr]
                .get_or_insert_with(|| {
                    NodeLinks::fill_like_node(|| SpacetimeBoundary::Center, nbr_node)
                })
                .set_neighbour(back_link, SpacetimeBoundary::Left);
        }
    }

    // Add the temporal cuts from the TimePeriodicGraph back in
    fn combine_boundaries(spacetime: &mut SpacetimeBoundary, time: TimeBoundary) {
        match time {
            TimeBoundary::Center => (),
            TimeBoundary::Forward => match spacetime {
                SpacetimeBoundary::Center => *spacetime = SpacetimeBoundary::Forward,
                SpacetimeBoundary::Left => *spacetime = SpacetimeBoundary::ForwardLeft,
                SpacetimeBoundary::Right => *spacetime = SpacetimeBoundary::ForwardRight,
                _ => panic!("Cannot happen by construction"),
            },
            TimeBoundary::Backward => match spacetime {
                SpacetimeBoundary::Center => *spacetime = SpacetimeBoundary::Backward,
                SpacetimeBoundary::Left => *spacetime = SpacetimeBoundary::BackwardLeft,
                SpacetimeBoundary::Right => *spacetime = SpacetimeBoundary::BackwardRight,
                _ => panic!("Cannot happen by construction"),
            },
        }
    }

    for (label, old_bound) in timeperiodic_graph
        .bounds
        .enumerate()
        .filter_map(|(label, optional_bound)| optional_bound.as_ref().map(|bound| (label, bound)))
    {
        bounds[label]
            .get_or_insert_with(|| {
                NodeLinks::fill_like_node(|| SpacetimeBoundary::Center, &nodes[label])
            })
            .combine(old_bound, combine_boundaries)
    }

    PeriodicSpacetimeGraph {
        nodes: nodes.clone(),
        bounds,
    }
}
