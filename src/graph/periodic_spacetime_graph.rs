//! Implementation of the the graph traits for [`PeriodicSpacetimeGraph`], see that for details.

use std::{cmp::Ordering, error::Error, fmt::Display, str::FromStr};

use super::{
    field::{Field as FieldTrait, FieldCollection, FieldValue, PeriodicField},
    periodic1d_generic_graph::i8usize_central_mapping,
    periodic_graph::{IterPeriodicGraph, PeriodicGraph, PeriodicLabel, PeriodicLabelMatch},
    spacetime_graph::{LinkLabel, Node},
    timeperiodic_spacetime_graph::NodeLinks,
    FieldGraph, Graph, IterableGraph, SampleGraph,
};
use crate::collections::{Label, LabelledArray};

pub mod bounded_graph;
mod from_triangulation;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PeriodicSpacetimeLabel {
    pub label: Label<Node>,
    pub patch: SpacetimePatch,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SpacetimePatch {
    pub t: i8,
    pub x: i8,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SpacetimeBoundary {
    Center,
    Forward,
    ForwardLeft,
    ForwardRight,
    Right,
    Left,
    Backward,
    BackwardLeft,
    BackwardRight,
}

type BoundaryList = LabelledArray<Option<NodeLinks<SpacetimeBoundary>>, Label<Node>>;

#[derive(Debug, Clone)]
pub struct PeriodicSpacetimeGraph {
    nodes: LabelledArray<Node>,
    bounds: BoundaryList,
}

// The label `t` is supposed to index the outer Vec, `x` the inner
pub type Field<T> = FieldCollection<T, Vec<Vec<LabelledArray<FieldValue<T>, Label<Node>>>>>;

#[derive(Debug, Clone)]
pub struct IterNeighbours<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<SpacetimeBoundary>>,
    patch: SpacetimePatch,
    cursor: LinkLabel,
}

impl PeriodicSpacetimeGraph {
    pub fn new(
        nodes: LabelledArray<Node>,
        bounds: LabelledArray<Option<NodeLinks<SpacetimeBoundary>>, Label<Node>>,
    ) -> Self {
        PeriodicSpacetimeGraph { nodes, bounds }
    }

    /// Retunrs the [`SpacetimeBoundary`] belonging to `label` in the direction of `link`
    pub fn get_bound(&self, label: Label<Node>, link: LinkLabel) -> SpacetimeBoundary {
        let Some(bounds) = &self.bounds[label] else {
            return SpacetimeBoundary::Center;
        };
        *bounds
            .get_neighbour(link)
            .expect("Requested link label does not exist")
    }

    #[inline]
    pub fn get_base_nodes(&self) -> &LabelledArray<Node> {
        &self.nodes
    }

    #[inline]
    pub fn get_node(&self, label: PeriodicSpacetimeLabel) -> Option<&Node> {
        self.nodes.get(label.label)
    }

    #[inline]
    pub fn get_node_unchecked(&self, label: PeriodicSpacetimeLabel) -> &Node {
        &self.nodes[label.label]
    }

    pub fn get_neighbour(
        &self,
        label: PeriodicSpacetimeLabel,
        link: LinkLabel,
    ) -> Option<PeriodicSpacetimeLabel> {
        let nbr_label = self.nodes[label.label].get_neighbour(link)?;
        let boundary = if let Some(bound) = &self.bounds[label.label] {
            *bound.get_neighbour(link)?
        } else {
            SpacetimeBoundary::Center
        };
        let nbr_patch = label.patch.add_boundary(boundary);
        Some(PeriodicSpacetimeLabel {
            label: nbr_label,
            patch: nbr_patch,
        })
    }

    pub fn get_neighbour_unchecked(
        &self,
        label: PeriodicSpacetimeLabel,
        link: LinkLabel,
    ) -> PeriodicSpacetimeLabel {
        let nbr_label = self.nodes[label.label].get_neighbour_unchecked(link);
        let boundary = if let Some(bound) = &self.bounds[label.label] {
            *bound.get_neighbour_unchecked(link)
        } else {
            SpacetimeBoundary::Center
        };
        let nbr_patch = label.patch.add_boundary(boundary);
        PeriodicSpacetimeLabel {
            label: nbr_label,
            patch: nbr_patch,
        }
    }
}

impl Graph for PeriodicSpacetimeGraph {
    type Label = PeriodicSpacetimeLabel;

    #[inline]
    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.nodes[label.label].vertex_degree()
    }
}

impl FieldGraph for PeriodicSpacetimeGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        FieldCollection::new(vec![vec![LabelledArray::fill_with(
            || FieldValue::None,
            self.nodes.len(),
        )]])
    }
}

impl super::Label for PeriodicSpacetimeLabel {}

impl PeriodicLabel for PeriodicSpacetimeLabel {
    type BaseLabel = Label<Node>;

    #[inline]
    fn cmp(self, other: Self) -> PeriodicLabelMatch {
        if self.label != other.label {
            return PeriodicLabelMatch::None;
        }
        if self.patch != other.patch {
            return PeriodicLabelMatch::Patch;
        }
        PeriodicLabelMatch::Equal
    }

    #[inline]
    fn base_label(self) -> Self::BaseLabel {
        self.label
    }
}

impl PeriodicGraph for PeriodicSpacetimeGraph {
    type Patch = SpacetimePatch;

    #[inline]
    fn base_len(&self) -> usize {
        self.nodes.len()
    }
}

impl SampleGraph for PeriodicSpacetimeGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        PeriodicSpacetimeLabel::at_origin(self.nodes.sample_label(rng))
    }
}

impl<T> FieldTrait<T, PeriodicSpacetimeGraph> for Field<T> {
    fn get(&self, label: PeriodicSpacetimeLabel) -> &FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.t);
        let inner_index = i8usize_central_mapping(label.patch.x);
        match self
            .0
            .get(outer_index)
            .and_then(|list| list.get(inner_index))
        {
            None => self.none(),
            Some(values) => &values[label.label],
        }
    }

    fn get_mut(&mut self, label: PeriodicSpacetimeLabel) -> &mut FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.t);
        let inner_index = i8usize_central_mapping(label.patch.x);
        let label = label.label;
        let node_count = self
            .0
            .first()
            .and_then(|list| list.first())
            .expect("First element should be initialized by creation")
            .len();

        if self.0.len() <= outer_index {
            self.0.resize_with(outer_index + 1, Vec::new);
        }
        let inner_list = &mut self.0[outer_index];
        if inner_list.len() <= inner_index {
            inner_list.resize_with(inner_index + 1, || {
                LabelledArray::fill_with(|| FieldValue::None, node_count)
            });
        };

        &mut inner_list[inner_index][label]
    }
}

impl<T> PeriodicField<T, PeriodicSpacetimeGraph> for Field<T> {
    /// This field collection has been implemented as a collection of LabelledArrays, so checking
    /// for collisions in other patches is relatively expensive
    fn has(&self, label: PeriodicSpacetimeLabel) -> PeriodicLabelMatch {
        if FieldTrait::<T, PeriodicSpacetimeGraph>::get(self, label).is_some() {
            return PeriodicLabelMatch::Equal;
        }
        let outer = &self.0;
        if outer
            .iter()
            .flat_map(|inner| inner.iter())
            .any(|array| array[label.label].is_some())
        {
            return PeriodicLabelMatch::Patch;
        }
        PeriodicLabelMatch::None
    }

    /// This field collection has been implemented as a collection of LabelledArrays, so checking
    /// for collisions in other patches is relatively expensive
    fn has_in_patches(&self, label: Label<Node>) -> usize {
        let outer = &self.0;
        return outer
            .iter()
            .flat_map(|inner| inner.iter())
            .filter(|array| array[label].is_some())
            .count();
    }

    fn has_patch(&self, patch: SpacetimePatch) -> bool {
        let outer_index = i8usize_central_mapping(patch.t);
        let inner_index = i8usize_central_mapping(patch.x);
        self.0
            .get(outer_index)
            .and_then(|inner| inner.get(inner_index))
            .is_some()
    }
}

impl<'a> IterableGraph<'a, IterNeighbours<'a>> for PeriodicSpacetimeGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighbours<'a> {
        IterNeighbours::from_graph(self, label)
    }
}

impl<'a> IterNeighbours<'a> {
    fn new(
        node: &'a Node,
        bound: Option<&'a NodeLinks<SpacetimeBoundary>>,
        patch: SpacetimePatch,
    ) -> Self {
        IterNeighbours {
            node,
            bound,
            patch,
            cursor: LinkLabel::Left,
        }
    }

    fn from_graph(graph: &'a PeriodicSpacetimeGraph, label: PeriodicSpacetimeLabel) -> Self {
        IterNeighbours::new(
            &graph.nodes[label.label],
            graph.bounds[label.label].as_ref(),
            label.patch,
        )
    }

    fn get_bound(&self, link: LinkLabel) -> SpacetimeBoundary {
        let Some(bounds) = self.bound else {
            return SpacetimeBoundary::Center;
        };
        *bounds
            .get_neighbour(link)
            .expect("Requested link should exist")
    }
}

impl<'a> Iterator for IterNeighbours<'a> {
    type Item = PeriodicSpacetimeLabel;
    fn next(&mut self) -> Option<Self::Item> {
        let mut result = self.node.get_neighbour(self.cursor);
        let (new_cursor, bound) = match self.cursor {
            LinkLabel::Left => (LinkLabel::Up(0), self.get_bound(self.cursor)),
            LinkLabel::Up(i) => {
                if result.is_some() {
                    (LinkLabel::Up(i + 1), self.get_bound(self.cursor))
                } else {
                    result = self.node.get_neighbour(LinkLabel::Right);
                    (LinkLabel::Down(0), self.get_bound(LinkLabel::Right))
                }
            }
            LinkLabel::Right => panic!("Right should be skipped by construction."),
            // If Down just increase, it will stop here in any case after the last
            LinkLabel::Down(i) => {
                result?;
                (LinkLabel::Down(i + 1), self.get_bound(self.cursor))
            }
        };
        self.cursor = new_cursor;
        let patch = self.patch.add_boundary(bound);
        Some(PeriodicSpacetimeLabel {
            label: result.expect("If this were None the function should have returned early"),
            patch,
        })
    }
}

impl SpacetimePatch {
    fn origin() -> Self {
        SpacetimePatch { t: 0, x: 0 }
    }

    fn add(&self, t: i8, x: i8) -> Self {
        Self {
            t: self.t + t,
            x: self.x + x,
        }
    }

    fn add_t(&self, t: i8) -> Self {
        Self {
            t: self.t + t,
            x: self.x,
        }
    }

    fn add_x(&self, x: i8) -> Self {
        Self {
            t: self.t,
            x: self.x + x,
        }
    }

    /// Return [`SpacetimePatch`] of the patch on the other side of the `bound`;
    /// effectively applying the boundary.
    fn add_boundary(&self, bound: SpacetimeBoundary) -> Self {
        match bound {
            SpacetimeBoundary::Center => *self,
            SpacetimeBoundary::Left => self.add_x(-1),
            SpacetimeBoundary::ForwardLeft => self.add(1, -1),
            SpacetimeBoundary::Forward => self.add_t(1),
            SpacetimeBoundary::ForwardRight => self.add(1, 1),
            SpacetimeBoundary::Right => self.add_x(1),
            SpacetimeBoundary::BackwardRight => self.add(-1, 1),
            SpacetimeBoundary::Backward => self.add_t(-1),
            SpacetimeBoundary::BackwardLeft => self.add(-1, -1),
        }
    }

    /// Get patch that is the remainder of the coordinates
    fn modulo(self, tmax: i8, xmax: i8) -> Self {
        let tmod = 2 * tmax + 1;
        let xmod = 2 * xmax + 1;
        Self {
            t: ((self.t + tmax) % tmod + tmod) % tmod - tmax,
            x: ((self.x + xmax) % xmod + xmod) % xmod - xmax,
        }
    }
}

impl PeriodicSpacetimeLabel {
    pub fn at_origin(label: Label<Node>) -> Self {
        PeriodicSpacetimeLabel {
            label,
            patch: SpacetimePatch::origin(),
        }
    }
}

impl Display for PeriodicSpacetimeLabel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}({},{})", self.label, self.patch.t, self.patch.x)
    }
}

#[derive(Debug, Clone)]
pub struct ParseSpacetimeBoundaryError;

impl Display for ParseSpacetimeBoundaryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Only the characters 'C'/'L'/'FL'/'F'/'FR'/'R'/'BR'/'B'/'BL' can be used."
        )
    }
}

impl Error for ParseSpacetimeBoundaryError {}

impl FromStr for SpacetimeBoundary {
    type Err = ParseSpacetimeBoundaryError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let bound = match s.to_ascii_uppercase().as_str() {
            "C" => SpacetimeBoundary::Center,
            "L" => SpacetimeBoundary::Left,
            "F" => SpacetimeBoundary::Forward,
            "R" => SpacetimeBoundary::Right,
            "B" => SpacetimeBoundary::Backward,
            "FL" => SpacetimeBoundary::ForwardLeft,
            "FR" => SpacetimeBoundary::ForwardRight,
            "BL" => SpacetimeBoundary::BackwardLeft,
            "BR" => SpacetimeBoundary::BackwardRight,
            _ => return Err(ParseSpacetimeBoundaryError),
        };

        Ok(bound)
    }
}

impl PeriodicSpacetimeLabel {
    pub fn new(label: Label<Node>, patch: SpacetimePatch) -> Self {
        Self { label, patch }
    }
}

impl SpacetimePatch {
    pub fn new(t: i8, x: i8) -> Self {
        Self { t, x }
    }
}

pub struct PatchIterator {
    current: Label<Node>,
    max: Label<Node>,
    patch: SpacetimePatch,
}

impl Iterator for PatchIterator {
    type Item = PeriodicSpacetimeLabel;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current.cmp(&self.max) {
            Ordering::Less => (),
            Ordering::Equal => return None,
            Ordering::Greater => panic!("Somehow the iterator went past max"),
        }

        let next_label = self.current.succ();
        let old_label = std::mem::replace(&mut self.current, next_label);
        Some(PeriodicSpacetimeLabel::new(old_label, self.patch))
    }
}

impl IterPeriodicGraph<PatchIterator> for PeriodicSpacetimeGraph {
    fn iter_base(&self) -> PatchIterator {
        Self::iter_patch(self, SpacetimePatch::origin())
    }

    fn iter_patch(&self, patch: SpacetimePatch) -> PatchIterator {
        PatchIterator {
            current: Label::initial(),
            max: Label::from(self.base_len()),
            patch,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::io::graph;
    use crate::observables::{
        average_sphere_distance, distance_profile, vertex_degree_distribution, volume_profile,
    };
    use crate::triangulation::FullTriangulation;
    use crate::{graph::breadth_first_search::traverse_nodes, triangulation::FixedTriangulation};
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;
    use traverse_nodes::{TraverseNodeDistance, TraverseNodeLabel};

    use super::*;
    use indoc::indoc;
    use std::io::Cursor;

    const EXAMPLE_NODE: &str = "3; 1, 4, 6; 5; 4";
    const EXAMPLE_BOUND: &str = "C; F, F, FR; R; B";
    const EXAMPLE_GRAPH: &str = indoc! {"
        2; 6, 3, 4; 1; 11, 10, 14, 13
        0; 4, 5, 6; 2; 11
        1; 6; 0; 13, 12, 11
        6; 7; 4; 0
        3; 7; 5; 1, 0
        4; 7, 8, 9; 6; 1
        5; 9, 7; 3; 0, 2, 1
        9; 14, 10, 11; 8; 5, 4, 3, 6
        7; 11; 9; 5
        8; 11, 12, 13, 14; 7; 6, 5
        14; 0; 11; 7
        10; 0, 1, 2; 12; 9, 8, 7
        11; 2; 13; 9
        12; 2, 0; 14; 9
        13; 0; 10; 7, 9
    "};
    const EXAMPLE_BOUNDS: [(usize, &str); 12] = [
        (0, "L; L, C, C; C; B, B, BL, BL"),
        (1, "C; C, C, C; C; B"),
        (2, "C; C; R; B, B, B"),
        (3, "L; C; C; C"),
        (6, "C; C, R; R; R, C, C"),
        (7, "L; L, C, C; C; C, C, C, L"),
        (9, "C; C, C, C, C; R; C, C"),
        (10, "L; F; C; C"),
        (11, "C; F, F, F; C; C, C, C"),
        (12, "C; F; C; C"),
        (13, "C; F, FR; C; C"),
        (14, "C; FR; R; R, C"),
    ];

    impl SpacetimeBoundary {
        fn opposite(self) -> Self {
            match self {
                SpacetimeBoundary::Center => SpacetimeBoundary::Center,
                SpacetimeBoundary::Forward => SpacetimeBoundary::Backward,
                SpacetimeBoundary::ForwardLeft => SpacetimeBoundary::BackwardRight,
                SpacetimeBoundary::ForwardRight => SpacetimeBoundary::BackwardLeft,
                SpacetimeBoundary::Right => SpacetimeBoundary::Left,
                SpacetimeBoundary::Left => SpacetimeBoundary::Right,
                SpacetimeBoundary::Backward => SpacetimeBoundary::Forward,
                SpacetimeBoundary::BackwardLeft => SpacetimeBoundary::ForwardRight,
                SpacetimeBoundary::BackwardRight => SpacetimeBoundary::ForwardLeft,
            }
        }
    }

    impl BoundaryList {
        /// Returns boundary corresponding to label and link, will panic if the link
        /// doesn't point to a valid boundary.
        fn get_bound(&self, label: Label<Node>, link: LinkLabel) -> SpacetimeBoundary {
            self[label]
                .as_ref()
                .map_or(SpacetimeBoundary::Center, |bounds| {
                    *bounds.get_neighbour(link).unwrap()
                })
        }
    }

    #[test]
    fn parse_example_node() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();
        println!("{:?}", node)
    }

    #[test]
    fn parse_example_bound() {
        let bound: NodeLinks<SpacetimeBoundary> = EXAMPLE_BOUND.parse().unwrap();
        println!("{:?}", bound)
    }

    #[test]
    fn node_neighbour_iterator() {
        let node: Node = EXAMPLE_NODE.parse().unwrap();
        let bound: NodeLinks<SpacetimeBoundary> = EXAMPLE_BOUND.parse().unwrap();

        println!("Neigbours in center");
        for node in IterNeighbours::new(&node, None, SpacetimePatch::origin()) {
            print!("{:} ", node);
        }
        println!();
        let expected_result: [(usize, i8, i8); 6] = [
            (3, 0, 0),
            (1, 0, 0),
            (4, 0, 0),
            (6, 0, 0),
            (5, 0, 0),
            (4, 0, 0),
        ];
        assert!(
            IterNeighbours::new(&node, None, SpacetimePatch::origin()).eq(expected_result
                .into_iter()
                .map(|l| {
                    PeriodicSpacetimeLabel {
                        label: Label::from(l.0),
                        patch: SpacetimePatch { t: l.1, x: l.2 },
                    }
                }))
        );

        println!("Neigbours on edge");
        for node in IterNeighbours::new(&node, Some(&bound), SpacetimePatch::origin()) {
            print!("{:} ", node)
        }
        println!();
        let expected_result: [(usize, i8, i8); 6] = [
            (3, 0, 0),
            (1, 1, 0),
            (4, 1, 0),
            (6, 1, 1),
            (5, 0, 1),
            (4, -1, 0),
        ];
        assert!(
            IterNeighbours::new(&node, Some(&bound), SpacetimePatch::origin()).eq(expected_result
                .into_iter()
                .map(|l| {
                    PeriodicSpacetimeLabel {
                        label: Label::from(l.0),
                        patch: SpacetimePatch { t: l.1, x: l.2 },
                    }
                }))
        );
    }

    fn example_graph() -> PeriodicSpacetimeGraph {
        let nodes = graph::import_spacetime_adjacency_list(Cursor::new(EXAMPLE_GRAPH)).unwrap();
        let mut bounds = LabelledArray::fill_with(|| None, nodes.len());
        for (node_label, bound_str) in EXAMPLE_BOUNDS {
            let label = Label::from(node_label);
            let node_bounds: NodeLinks<SpacetimeBoundary> = bound_str.parse().unwrap();
            bounds[label] = Some(node_bounds);
        }
        PeriodicSpacetimeGraph::new(nodes, bounds)
    }

    #[test]
    fn import_example_graph() {
        example_graph();
    }

    #[test]
    fn check_example_graph() {
        let graph = example_graph();
        check_graph_undirectedness(&graph);
    }

    fn check_graph_undirectedness(graph: &PeriodicSpacetimeGraph) {
        for label in graph.nodes.labels() {
            // let origin_label = PeriodicSpacetimeLabel::at_origin(label);
            // for nbr in graph.iter_neighbours(origin_label) {
            //     graph
            //         .iter_neighbours(nbr)
            //         .find(|&node| node == origin_label)
            //         .expect(
            //             "The neighbour of a node should have a connection to the original node",
            //         );
            // }
            let node = &graph.nodes[label];
            let label_left = node.left;
            let labels_up = &node.up;
            let label_right = node.right;
            let labels_down = &node.down;
            // Check left side
            assert_eq!(
                graph.bounds.get_bound(label, LinkLabel::Left).opposite(),
                graph.bounds.get_bound(label_left, LinkLabel::Right)
            );
            assert_eq!(graph.nodes[label_left].right, label);
            // Check top side
            for (i, &label_up) in labels_up.iter().enumerate() {
                // Check if there is a link back and with what index
                let iback = graph.nodes[label_up]
                    .down
                    .iter()
                    .position(|&nbr_label| nbr_label == label)
                    .unwrap();
                assert_eq!(
                    graph.bounds.get_bound(label, LinkLabel::Up(i)).opposite(),
                    graph.bounds.get_bound(label_up, LinkLabel::Down(iback)),
                    "left {}: {:?}({:?}), right {}: {:?}({:?})",
                    label,
                    graph.nodes[label],
                    graph.bounds[label],
                    label_up,
                    graph.nodes[label_up],
                    graph.bounds[label_up]
                );
            }
            // Check right side
            assert_eq!(
                graph.bounds.get_bound(label, LinkLabel::Right).opposite(),
                graph.bounds.get_bound(label_right, LinkLabel::Left)
            );
            // Check bottom side
            assert_eq!(graph.nodes[label_right].left, label);
            for (i, &label_down) in labels_down.iter().enumerate() {
                let iback = graph.nodes[label_down]
                    .up
                    .iter()
                    .position(|&nbr_label| nbr_label == label)
                    .unwrap();
                assert_eq!(
                    graph.bounds.get_bound(label, LinkLabel::Down(i)).opposite(),
                    graph.bounds.get_bound(label_down, LinkLabel::Up(iback)),
                    "left: {:?}, right: {:?}",
                    graph.bounds[label],
                    graph.bounds[label_down]
                );
            }
        }
    }

    #[test]
    fn bfs_plain() {
        let graph = example_graph();
        let origin = PeriodicSpacetimeLabel::at_origin(Label::from(8));

        // Display search results
        println!("Plain breadth first search");
        for node in graph
            .iter_breadth_first::<TraverseNodeLabel<_>>(origin)
            .take(40)
        {
            print!("{} ", node.label)
        }
        println!();

        // // Assert search
        // graph
        //     .iter_breadth_first::<TraverseNodeLabel>(origin)
        //     .map(|node| (usize::from(node.label.label), node.label.patch))
        //     .zip(EXPECTED_EXAMPLE_SEARCH.into_iter().map(|x| (x.0, x.2)))
        //     .for_each(|(left, right)| assert_eq!(left, right));
    }

    #[test]
    fn bfs_distance() {
        let graph = example_graph();
        let origin = PeriodicSpacetimeLabel::at_origin(Label::from(8));

        // Display search results
        println!("Distanced breadth first search");
        let mut current_distance = 0;
        print!("{}: ", current_distance);
        for node in graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .take(40)
        {
            if node.distance > current_distance {
                println!();
                current_distance = node.distance;
                print!("{}: ", current_distance);
            }
            print!("{} ", node.label)
        }
        println!();

        //     // Assert search
        //     graph
        //         .iter_breadth_first::<TraverseNodeDistance>(origin)
        //         .map(|node| {
        //             (
        //                 usize::from(node.label.label),
        //                 node.info.distance,
        //                 node.label.patch,
        //             )
        //         })
        //         .zip(EXPECTED_EXAMPLE_SEARCH.into_iter())
        //         .for_each(|(left, right)| assert_eq!(left, right));
    }

    fn random_triangulation() -> FullTriangulation {
        let mut triangulation = FixedTriangulation::new(10, 5);
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..28 {
            triangulation.relocate(
                triangulation.sample_order_four(&mut rng),
                triangulation.sample(&mut rng),
            );
        }
        for _ in 0..300 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn from_triangulation_check() {
        let triangulation = random_triangulation();
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
        check_graph_undirectedness(&graph);
    }

    #[test]
    fn from_triangulation_test() {
        let triangulation = random_triangulation();
        let cutting_label = Label::from(0);
        let graph =
            PeriodicSpacetimeGraph::from_triangulation_vertex_cut_at(&triangulation, cutting_label);

        let profile = distance_profile::measure(
            &graph,
            PeriodicSpacetimeLabel::at_origin(Label::from(0)),
            10,
        );
        println!("Sphere area profile: {}", profile);

        let graph = PeriodicSpacetimeGraph::from_triangulation_dual(&triangulation);
        let profile = distance_profile::measure(
            &graph,
            PeriodicSpacetimeLabel::at_origin(Label::from(0)),
            10,
        );
        println!("Dual sphere area profile: {}", profile);
    }

    #[test]
    fn asd_test() {
        let triangulation = random_triangulation();
        let vprofile = volume_profile::measure(&triangulation);
        println!("{:?}", vprofile.volumes());

        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
        let origin = PeriodicSpacetimeLabel::at_origin(Label::from(5));
        println!("vertex degree: {}", graph.get_vertex_degree(origin));
        let asd = average_sphere_distance::single_sphere::measure(&graph, origin, 5);
        println!("ASD: {asd:?}")
    }

    #[test]
    fn sphere_volume_test() {
        let triangulation = FixedTriangulation::new(20, 20);
        let triangulation = FullTriangulation::from_triangulation(&triangulation);
        let vdegree = vertex_degree_distribution::measure_triangulation(&triangulation).0;
        assert!(vdegree[6] != 0);
        assert!(vdegree
            .iter()
            .copied()
            .enumerate()
            .filter(|&(degree, _)| degree != 6)
            .all(|(_, count)| count == 0));
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
        let vdegree = vertex_degree_distribution::measure_periodic_graph(&graph).0;
        assert!(vdegree[6] != 0);
        assert!(vdegree
            .iter()
            .copied()
            .enumerate()
            .filter(|&(degree, _)| degree != 6)
            .all(|(_, count)| count == 0));

        let sphere_volumes = distance_profile::measure(
            &graph,
            PeriodicSpacetimeLabel::at_origin(Label::from(0)),
            20,
        )
        .profile;
        println!("{sphere_volumes:?}");
        assert!(sphere_volumes
            .iter()
            .copied()
            .enumerate()
            .all(|(r, vol)| if r == 0 { vol == 1 } else { vol == r * 6 }))
    }

    #[test]
    fn bfs_flat_check() {
        let triangulation = FixedTriangulation::new(20, 20);
        let triangulation = FullTriangulation::from_triangulation(&triangulation);
        let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

        check_graph_undirectedness(&graph);

        // Display search results
        let origin = PeriodicSpacetimeLabel::at_origin(Label::from(0));
        let mut current_distance = 0;
        print!("{}: ", current_distance);
        for node in graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(origin)
            .take(40)
        {
            if node.distance > current_distance {
                println!();
                current_distance = node.distance;
                print!("{}: ", current_distance);
            }
            print!("{} ", node.label);
        }
        println!();

        println!("{:?}", graph.nodes[Label::from(0)]);
        println!("{:?}", graph.bounds[Label::from(0)]);
        println!();
        println!("{:?}", graph.nodes[Label::from(19)]);
        println!("{:?}", graph.bounds[Label::from(19)]);
        println!("{:?}", graph.nodes[Label::from(39)]);
        println!("{:?}", graph.bounds[Label::from(39)]);
        println!("{:?}", graph.nodes[Label::from(20)]);
        println!("{:?}", graph.bounds[Label::from(20)]);
        println!("{:?}", graph.nodes[Label::from(1)]);
        println!("{:?}", graph.bounds[Label::from(1)]);
        println!("{:?}", graph.nodes[Label::from(381)]);
        println!("{:?}", graph.bounds[Label::from(381)]);
        println!("{:?}", graph.nodes[Label::from(380)]);
        println!("{:?}", graph.bounds[Label::from(380)]);
    }

    #[test]
    fn spacetime_patch_modulo() {
        assert_eq!(
            SpacetimePatch::new(0, 0).modulo(1, 2),
            SpacetimePatch::new(0, 0)
        );
        assert_eq!(
            SpacetimePatch::new(1, 1).modulo(1, 2),
            SpacetimePatch::new(1, 1)
        );
        assert_eq!(
            SpacetimePatch::new(2, 2).modulo(1, 2),
            SpacetimePatch::new(-1, 2)
        );
        assert_eq!(
            SpacetimePatch::new(3, 3).modulo(1, 2),
            SpacetimePatch::new(0, -2)
        );
        assert_eq!(
            SpacetimePatch::new(4, 4).modulo(1, 2),
            SpacetimePatch::new(1, -1)
        );
        assert_eq!(
            SpacetimePatch::new(5, 5).modulo(1, 2),
            SpacetimePatch::new(-1, 0)
        );

        assert_eq!(
            SpacetimePatch::new(-1, -1).modulo(1, 2),
            SpacetimePatch::new(-1, -1)
        );
        assert_eq!(
            SpacetimePatch::new(-2, -2).modulo(1, 2),
            SpacetimePatch::new(1, -2)
        );
        assert_eq!(
            SpacetimePatch::new(-3, -3).modulo(1, 2),
            SpacetimePatch::new(0, 2)
        );
        assert_eq!(
            SpacetimePatch::new(-4, -4).modulo(1, 2),
            SpacetimePatch::new(-1, 1)
        );
        assert_eq!(
            SpacetimePatch::new(-5, -5).modulo(1, 2),
            SpacetimePatch::new(1, 0)
        );
        assert_eq!(
            SpacetimePatch::new(-6, -6).modulo(1, 2),
            SpacetimePatch::new(0, -1)
        );
    }
}
