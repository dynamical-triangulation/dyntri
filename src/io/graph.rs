//! Importing and Exporting differnt graph structs to different formats,
//! like the MTX (Matrix Market Exchange) format

use crate::collections::{Label, LabelledArray};
use crate::graph::generic_graph::{self, GenericGraph};
use crate::graph::periodic2d_generic_graph::{self, Boundary2D, ParseBoundary2DError};
use crate::graph::spacetime_graph::ParseNodeError;
use crate::graph::{spacetime_graph, IterableGraph, SizedGraph};
use csv::ReaderBuilder;
use serde::{Deserialize, Serialize};
use std::io::{BufRead, BufReader, LineWriter, Read, Write};
use thiserror::Error as ThisError;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct SparseMatrixEntry {
    row: usize,
    column: usize,
    value: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct SparseMatrixHeader {
    row_count: usize,
    column_count: usize,
    specified_values: usize,
}

impl GenericGraph {
    /// Import an MTX file representing the adjacency matrix of a Graph
    pub fn import_mtx<R: Read>(buffer: R) -> Result<GenericGraph, csv::Error> {
        let mut reader = ReaderBuilder::new()
            .delimiter(b' ')
            .comment(Some(b'%'))
            .has_headers(false)
            .from_reader(buffer);
        let header: SparseMatrixHeader = reader.byte_headers()?.deserialize(None)?;
        let mut nodes: LabelledArray<
            Option<Vec<Label<generic_graph::Node>>>,
            Label<generic_graph::Node>,
        > = LabelledArray::fill_with(|| None, header.row_count);
        for record in reader.deserialize() {
            let entry: SparseMatrixEntry = record?;
            let label0: Label<generic_graph::Node> = Label::from(entry.row - 1);
            let label1: Label<generic_graph::Node> = Label::from(entry.column - 1);
            nodes[label0].get_or_insert_with(Vec::new).push(label1);
            nodes[label1].get_or_insert_with(Vec::new).push(label0);
        }

        Ok(GenericGraph::new(nodes.map_into(|node| {
            generic_graph::Node::new(
                node.expect("A Node is disconnected from the rest of the graph"),
            )
        })))
    }
}

#[derive(ThisError, Debug)]
pub enum AdjacencyListParseError {
    #[error("Could not read line from file")]
    ReadLine(#[from] std::io::Error),
    #[error("Could not parse label from item")]
    ParseItem(#[from] std::num::ParseIntError),
}

/// Import a neighbour list file, each line of which corresponds to a node with
/// the linenumber (0 indexed) as label, and the given comma-separated numbers correspond
/// to the labels of the neighbouring nodes.
pub fn import_adjacency_list<R: Read>(
    buffer: R,
) -> Result<LabelledArray<generic_graph::Node>, AdjacencyListParseError> {
    let reader = BufReader::new(buffer);
    let mut nodes: Vec<generic_graph::Node> = Vec::new();
    for line in reader.lines() {
        let line = line?;
        let mut nbrs = Vec::with_capacity(10);
        for item in line.split(',') {
            let index: usize = item.trim().parse()?;
            nbrs.push(Label::from(index))
        }
        nodes.push(generic_graph::Node::new(nbrs));
    }

    Ok(LabelledArray::from(nodes))
}

/// See `import_adjacency_list()` with a patch to read in the format Thijs send me:
/// Here the first item as the origin label (which is superfluous as this is the same as the line
/// number (minus one because of 0/1-indexing differences)); and self loops are only counted once
/// instead of the desired two times for each half edge (this is just a different convention that
/// works better in the cicumstance I'm considereing, but the other is by no means wrong).
pub fn import_adjacency_list_thijs_patch<R: Read>(
    buffer: R,
) -> Result<LabelledArray<generic_graph::Node>, AdjacencyListParseError> {
    let reader = BufReader::new(buffer);
    let mut nodes: Vec<generic_graph::Node> = Vec::new();
    for (i, line) in reader.lines().enumerate() {
        let line = line?;
        let mut nbrs = Vec::with_capacity(10);
        for item in line.split(',').skip(1) {
            let index: usize = item.trim().parse()?;
            // This is the case of the self loop so we push once extra (total twice)
            if index == i {
                nbrs.push(Label::from(index))
            }
            nbrs.push(Label::from(index))
        }
        nodes.push(generic_graph::Node::new(nbrs));
    }

    Ok(LabelledArray::from(nodes))
}

#[derive(ThisError, Debug)]
pub enum SpacetimeAdjacencyListParseError {
    #[error("Could not read line from file")]
    ReadLine(#[from] std::io::Error),
    #[error("Could not parse node from line")]
    ParseItem(#[from] ParseNodeError),
}

/// Import a neighbour list file, each line of which corresponds to a node with
/// the linenumber (0 indexed) as label, and the given numbers correspond to the labels of
/// the neighbouring nodes.
/// Each line should have the format of: "1; 2, 3, 4; 5; 6, 7". Hence a seperation with ';'
/// between the different directions: `left`, `up`, `right`, `left` respectively. And a
/// seperation with ',' between the different links in the `up` and `down` direction.
pub fn import_spacetime_adjacency_list<R: Read>(
    buffer: R,
) -> Result<LabelledArray<spacetime_graph::Node>, SpacetimeAdjacencyListParseError> {
    let reader = BufReader::new(buffer);
    let mut nodes: Vec<spacetime_graph::Node> = Vec::new();
    for line in reader.lines() {
        nodes.push(line?.parse()?);
    }

    Ok(LabelledArray::from(nodes))
}

type BoundaryList = Vec<(
    Label<generic_graph::Node>,
    periodic2d_generic_graph::NodeLinks<Boundary2D>,
)>;

#[derive(ThisError, Debug)]
pub enum BoundaryListParseError {
    #[error("Could not read line from file")]
    ReadLine(#[from] std::io::Error),
    #[error("Could not parse label from item")]
    ParseLabel(#[from] std::num::ParseIntError),
    #[error("Could not parse boudary from item")]
    ParseBound(#[from] ParseBoundary2DError),
    #[error("Missing boundaries for label")]
    MissingsBounds,
}

pub fn import_boundary2d_adjacency_list<R: Read>(
    buffer: R,
) -> Result<BoundaryList, BoundaryListParseError> {
    let reader = BufReader::new(buffer);
    let mut bounds = Vec::new();
    for line in reader.lines() {
        let line = line?;
        let mut items = line.split(',').map(|x| x.trim());
        let label: Label<generic_graph::Node> = Label::from(
            items
                .next()
                .ok_or(BoundaryListParseError::MissingsBounds)?
                .parse::<usize>()?,
        );
        let bound: Result<Vec<Boundary2D>, ParseBoundary2DError> =
            items.map(|bound| bound.parse::<Boundary2D>()).collect();
        bounds.push((label, periodic2d_generic_graph::NodeLinks(bound?)));
    }
    Ok(bounds)
}

// pub fn export_mtx(graph: &ToroidalGraph, file: &Path) -> Result<(), Box<dyn Error>> {
//     let mut file = File::create(file)?;
//     writeln!(file, "%%MatrixMarket matrix coordinate integer symmetric")?;
//     let mut wrt = WriterBuilder::new()
//         .delimiter(b' ')
//         .has_headers(false)
//         .from_writer(file);
//     let nodes = graph.nodes.len();
//     // Write header (predict 3*V = E as is the case for toroidal topology without boundaries)
//     wrt.serialize([nodes, nodes, 3 * nodes])?;
//     for (label, node) in graph.nodes.enumerate() {
//         let index0: usize = label.into();
//         for label1 in node.get_neighbours() {
//             let index1: usize = label1.into();
//             // Only write the symmetric half
//             if index0 >= index1 {
//                 // Indices are 1 based in MTX
//                 wrt.serialize([index0 + 1, index1 + 1, 1])?;
//             }
//         }
//     }

//     Ok(())
// }

pub fn export_compact_adjacency_list<W>(buffer: W, graph: &GenericGraph) -> std::io::Result<()>
where
    W: Write,
{
    let mut writer = LineWriter::new(buffer);
    let mut index_pointer: Vec<usize> = Vec::with_capacity(graph.len() + 1);

    let mut index = 0;
    index_pointer.push(0);
    for label in graph.get_nodes().labels() {
        for nbr in graph.iter_neighbours(label) {
            index += 1;
            writeln!(writer, "{nbr}")?;
        }
        index_pointer.push(index);
    }
    writeln!(writer, "-------")?;
    for pointer in index_pointer {
        writeln!(writer, "{pointer}")?;
    }

    Ok(())
}

pub fn export_adjacency_list<W>(buffer: W, graph: &GenericGraph) -> std::io::Result<()>
where
    W: Write,
{
    let mut writer = LineWriter::new(buffer);

    for label in graph.get_nodes().labels() {
        let mut nbr_iter = graph.iter_neighbours(label);
        if let Some(first_nbr) = nbr_iter.next() {
            write!(writer, "{first_nbr}")?;
        }
        for nbr in nbr_iter {
            write!(writer, ",{nbr}")?;
        }
        writeln!(writer)?;
    }

    Ok(())
}
