use super::{
    BoundaryList, PatchIterator, PeriodicSpacetimeGraph, PeriodicSpacetimeLabel, SpacetimeBoundary,
    SpacetimePatch,
};
use super::{Field, FieldTrait};
use crate::collections::Label;
use crate::graph::field::{FieldValue, PeriodicField};
use crate::graph::periodic1d_generic_graph::i8usize_central_mapping;
use crate::graph::periodic_graph::{BoundedPeriodicGraph, IterPeriodicGraph, PeriodicGraph};
use crate::graph::spacetime_graph::LinkLabel;
use crate::graph::timeperiodic_spacetime_graph::NodeLinks;
use crate::graph::{IterableGraph, SampleGraph};
use crate::{
    collections::LabelledArray,
    graph::{spacetime_graph::Node, FieldGraph, Graph},
};

/// Struct to represet a periodic spacetime graph with a maximum amount of patches
/// from (-xmax, -tmax) to (+xmax, +tmax)
pub struct BoundedPeriodicSpacetimeGraph {
    graph: PeriodicSpacetimeGraph,
    tmax: i8,
    xmax: i8,
}

impl BoundedPeriodicSpacetimeGraph {
    pub fn new(nodes: LabelledArray<Node>, bounds: BoundaryList, tmax: u8, xmax: u8) -> Self {
        Self::from_periodic_graph(PeriodicSpacetimeGraph { nodes, bounds }, tmax, xmax)
    }

    pub fn from_periodic_graph(graph: PeriodicSpacetimeGraph, tmax: u8, xmax: u8) -> Self {
        Self {
            graph,
            tmax: tmax as i8,
            xmax: xmax as i8,
        }
    }
}

impl Graph for BoundedPeriodicSpacetimeGraph {
    type Label = PeriodicSpacetimeLabel;

    #[inline]
    fn get_vertex_degree(&self, label: Self::Label) -> usize {
        self.graph.get_vertex_degree(label)
    }
}

impl FieldGraph for BoundedPeriodicSpacetimeGraph {
    type FieldType<F> = Field<F>;

    fn new_field<F>(&self) -> Self::FieldType<F> {
        self.graph.new_field()
    }
}

impl<T> FieldTrait<T, BoundedPeriodicSpacetimeGraph> for Field<T> {
    fn get(&self, label: PeriodicSpacetimeLabel) -> &FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.t);
        let inner_index = i8usize_central_mapping(label.patch.x);
        match self
            .0
            .get(outer_index)
            .and_then(|list| list.get(inner_index))
        {
            None => self.none(),
            Some(values) => &values[label.label],
        }
    }

    fn get_mut(&mut self, label: PeriodicSpacetimeLabel) -> &mut FieldValue<T> {
        let outer_index = i8usize_central_mapping(label.patch.t);
        let inner_index = i8usize_central_mapping(label.patch.x);
        let label = label.label;
        let node_count = self
            .0
            .first()
            .and_then(|list| list.first())
            .expect("First element should be initialized by creation")
            .len();

        if self.0.len() <= outer_index {
            self.0.resize_with(outer_index + 1, Vec::new);
        }
        let inner_list = &mut self.0[outer_index];
        if inner_list.len() <= inner_index {
            inner_list.resize_with(inner_index + 1, || {
                LabelledArray::fill_with(|| FieldValue::None, node_count)
            });
        };

        &mut inner_list[inner_index][label]
    }
}

impl<T> PeriodicField<T, BoundedPeriodicSpacetimeGraph> for Field<T> {
    fn has(
        &self,
        label: <BoundedPeriodicSpacetimeGraph as Graph>::Label,
    ) -> crate::graph::periodic_graph::PeriodicLabelMatch {
        <Field<T> as PeriodicField<T, PeriodicSpacetimeGraph>>::has(self, label)
    }

    fn has_in_patches(&self, label: Label<Node>) -> usize {
        <Field<T> as PeriodicField<T, PeriodicSpacetimeGraph>>::has_in_patches(self, label)
    }

    fn has_patch(&self, patch: <BoundedPeriodicSpacetimeGraph as PeriodicGraph>::Patch) -> bool {
        <Field<T> as PeriodicField<T, PeriodicSpacetimeGraph>>::has_patch(self, patch)
    }
}

impl PeriodicGraph for BoundedPeriodicSpacetimeGraph {
    type Patch = <PeriodicSpacetimeGraph as PeriodicGraph>::Patch;
    #[inline]
    fn base_len(&self) -> usize {
        self.graph.base_len()
    }
}

impl SampleGraph for BoundedPeriodicSpacetimeGraph {
    fn sample_node<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Self::Label {
        self.graph.sample_node(rng)
    }
}

impl<'a> IterableGraph<'a, IterNeighboursBounded<'a>> for BoundedPeriodicSpacetimeGraph {
    fn iter_neighbours(&'a self, label: Self::Label) -> IterNeighboursBounded<'a> {
        IterNeighboursBounded::from_graph(self, label)
    }
}

#[derive(Debug, Clone)]
pub struct IterNeighboursBounded<'a> {
    node: &'a Node,
    bound: Option<&'a NodeLinks<SpacetimeBoundary>>,
    patch: SpacetimePatch,
    cursor: LinkLabel,
    tmax: i8,
    xmax: i8,
}

impl<'a> IterNeighboursBounded<'a> {
    fn new(
        node: &'a Node,
        bound: Option<&'a NodeLinks<SpacetimeBoundary>>,
        patch: SpacetimePatch,
        tmax: i8,
        xmax: i8,
    ) -> Self {
        Self {
            node,
            bound,
            patch,
            cursor: LinkLabel::Left,
            tmax,
            xmax,
        }
    }

    fn from_graph(graph: &'a BoundedPeriodicSpacetimeGraph, label: PeriodicSpacetimeLabel) -> Self {
        Self::new(
            &graph.graph.nodes[label.label],
            graph.graph.bounds[label.label].as_ref(),
            label.patch,
            graph.tmax,
            graph.xmax,
        )
    }

    fn get_bound(&self, link: LinkLabel) -> SpacetimeBoundary {
        let Some(bounds) = self.bound else {
            return SpacetimeBoundary::Center;
        };
        *bounds
            .get_neighbour(link)
            .expect("Requested link should exist")
    }
}

impl<'a> Iterator for IterNeighboursBounded<'a> {
    type Item = PeriodicSpacetimeLabel;
    fn next(&mut self) -> Option<Self::Item> {
        let mut result = self.node.get_neighbour(self.cursor);
        let (new_cursor, bound) = match self.cursor {
            LinkLabel::Left => (LinkLabel::Up(0), self.get_bound(self.cursor)),
            LinkLabel::Up(i) => {
                if result.is_some() {
                    (LinkLabel::Up(i + 1), self.get_bound(self.cursor))
                } else {
                    result = self.node.get_neighbour(LinkLabel::Right);
                    (LinkLabel::Down(0), self.get_bound(LinkLabel::Right))
                }
            }
            LinkLabel::Right => panic!("Right should be skipped by construction."),
            // If Down just increase, it will stop here in any case after the last
            LinkLabel::Down(i) => {
                result?;
                (LinkLabel::Down(i + 1), self.get_bound(self.cursor))
            }
        };
        self.cursor = new_cursor;
        let patch = self.patch.add_boundary(bound).modulo(self.tmax, self.xmax);
        Some(PeriodicSpacetimeLabel {
            label: result.expect("If this were None the function should have returned early"),
            patch,
        })
    }
}

impl IterPeriodicGraph<PatchIterator> for BoundedPeriodicSpacetimeGraph {
    #[inline]
    fn iter_base(&self) -> PatchIterator {
        self.graph.iter_base()
    }

    #[inline]
    fn iter_patch(&self, patch: Self::Patch) -> PatchIterator {
        self.graph.iter_patch(patch)
    }
}

pub struct IterPatches {
    tmax: i8,
    xmax: i8,
    t: i8,
    x: i8,
}

impl IterPatches {
    fn new(tmax: i8, xmax: i8) -> Self {
        Self {
            tmax,
            xmax,
            t: -tmax,
            x: -xmax,
        }
    }
}

impl Iterator for IterPatches {
    type Item = SpacetimePatch;

    fn next(&mut self) -> Option<Self::Item> {
        let current_patch = SpacetimePatch::new(self.t, self.x);
        self.t += 1;
        if self.t > self.tmax {
            self.x += 1;
            if self.x > self.xmax {
                return None;
            }
            self.t = -self.tmax;
        }

        Some(current_patch)
    }
}

impl BoundedPeriodicGraph<IterPatches> for BoundedPeriodicSpacetimeGraph {
    fn iter_patches(&self) -> IterPatches {
        IterPatches::new(self.tmax, self.xmax)
    }
}
