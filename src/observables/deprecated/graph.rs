mod distance_profile;
pub use distance_profile::*;

use std::{
    collections::{HashMap, HashSet},
    iter,
    ops::Index,
};

use rand::Rng;

use crate::{
    collections::{TypedLabel, LabelSet},
    triangulation::{triangle::Orientation, Triangulation},
};

use super::{length_profile, LengthProfile};

type Label = TypedLabel<Node>;

/// The average maximum vertex order that is used to allocate memory for a graph distance measurements
const MAX_VERTEX_ORDER: usize = 10;

/// A graph `Link` representing a link to another `Node` labelled by `Label`
///
/// Can be extended to hold `Directed` `Link`s as well
#[derive(PartialEq, Eq, Hash)]
enum Link {
    Undirected(Label),
}

impl Link {
    fn to(&self) -> Label {
        match self {
            Link::Undirected(label) => *label,
        }
    }
}

/// A graph `Node` containing all its `Link`s to other `Node`s labelled by `Label`
pub struct Node {
    links: Vec<Link>,
}

/// `Graph` containing a list of `Node`s using the index as it's `Label`
pub struct Graph {
    nodes: Box<[Node]>,
}

impl Index<Label> for [Node] {
    type Output = Node;
    fn index(&self, index: Label) -> &Self::Output {
        &self[usize::from(index)]
    }
}

impl Graph {
    #[allow(dead_code)]
    fn empty() -> Graph {
        Graph {
            nodes: Box::new([]),
        }
    }
}

struct VertexTriangle {
    left_vertex: (usize, usize),
    middle_vertex: (usize, usize),
    right_vertex: (usize, usize),
}

fn create_vertex_triangles<'a>(
    triangulation: &'a impl Triangulation<'a>,
    length_profile: &LengthProfile,
) -> Vec<Option<VertexTriangle>> {
    let origin = if let Some(label) = triangulation.labels().next() {
        label
    } else {
        panic!("Cannot create vertex triangles from empty triangulation");
    };

    let lengths = length_profile.lengths();
    let t_max = lengths.len();

    let mut vertex_triangles: Vec<Option<VertexTriangle>> = iter::repeat_with(|| None)
        .take(triangulation.max_size())
        .collect();

    // Let us first label all vertices with a tuple (t, x) where the first labels the timeslice
    // and the second the positions in the timeslice.
    let mut slice_origin = origin;
    let mut upper_start = 0;
    let mut lower_start = 0;
    for t in 0..t_max {
        let mut upper: usize = upper_start;
        let mut lower: usize = lower_start;
        let upper_max = lengths[(t + 1) % t_max];
        let lower_max = lengths[t];

        let mut marker = slice_origin;
        let mut next_slice = None;
        for _ in 0..upper_max + lower_max {
            let current_triangle = &triangulation.get(marker);
            match current_triangle.orientation {
                Orientation::Up => {
                    vertex_triangles[usize::from(marker)] = Some(VertexTriangle {
                        left_vertex: (t, lower % lower_max),
                        middle_vertex: ((t + 1) % t_max, upper % upper_max),
                        right_vertex: (t, (lower + 1) % lower_max),
                    });
                    lower += 1;
                }
                Orientation::Down => {
                    vertex_triangles[usize::from(marker)] = Some(VertexTriangle {
                        left_vertex: ((t + 1) % t_max, upper % upper_max),
                        middle_vertex: (t, lower % lower_max),
                        right_vertex: ((t + 1) % t_max, (upper + 1) % upper_max),
                    });
                    if next_slice.is_none() {
                        next_slice = Some(current_triangle.center);
                        lower_start = upper;
                    }
                    upper += 1;
                }
            }
            marker = current_triangle.right;
        }
        slice_origin = next_slice.expect("No down triangle found in timeslice.");
        if t == t_max - 2 {
            let mut label = slice_origin;
            while matches!(triangulation.get(label).orientation, Orientation::Up) {
                label = triangulation.get(label).right
            }
            upper_start = vertex_triangles[usize::from(triangulation.get(label).center)]
                .as_ref()
                .expect("The original triangle is non existent.")
                .left_vertex
                .1;
        }
    }

    vertex_triangles
}

/// Create a vertex_graph from a `Triangulation`
pub fn vertex_graph<'a>(triangulation: &'a impl Triangulation<'a>) -> Graph {
    let length_profile = length_profile(triangulation);
    let lengths = length_profile.lengths();

    let vertex_triangles = create_vertex_triangles(triangulation, &length_profile);

    // Now we have a list of VertexTriangles labelled with the same indexes as the labels of the LabelledList
    let mut vertices: Vec<HashSet<Link>> =
        iter::repeat_with(|| HashSet::with_capacity(MAX_VERTEX_ORDER))
            .take(triangulation.size() / 2)
            .collect();
    let lensum: Vec<usize> = iter::once(0)
        .chain(lengths.iter().scan(0, |cumsum, &len| {
            *cumsum += len;
            Some(*cumsum)
        }))
        .collect();
    let map = |(t, x): (usize, usize)| x + lensum[t];
    for label in triangulation.labels() {
        let vertex_triangle = vertex_triangles[usize::from(label)]
            .as_ref()
            .expect("Somehow a vertex triangle does not exist while the triangle does.");

        // Add all possible links (using a Hashset to ensure no link is added twice)
        vertices[map(vertex_triangle.left_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.right_vertex))));
        vertices[map(vertex_triangle.left_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.middle_vertex))));

        vertices[map(vertex_triangle.middle_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.right_vertex))));
        vertices[map(vertex_triangle.middle_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.left_vertex))));

        vertices[map(vertex_triangle.right_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.left_vertex))));
        vertices[map(vertex_triangle.right_vertex)]
            .insert(Link::Undirected(Label::from(map(vertex_triangle.middle_vertex))));
    }

    // Map to Graph
    Graph {
        nodes: vertices
            .into_iter()
            .map(|linkset| Node {
                links: Vec::from_iter(linkset),
            })
            .collect(),
    }
}

/// Create a dual graph (graph based on triangle connectivity)
/// from a `Triangulation`
///
/// NOTE: currently this implemented using a generic `Graph` struct,
/// but the trivalent structure of this graph allows for optimalisation
/// if required.
pub fn dual_graph<'a>(triangulation: &'a impl Triangulation<'a>) -> Graph {
    // The dual graph is basically the `triangles` list of the `triangulation`
    // it needs only be converted to the correct graph types.
    // Curretnly this conversion is not at all optimized, but this probably is not the slow part anyway.
    let mut nodes: Vec<Option<Node>> = iter::repeat_with(|| None)
        .take(triangulation.size())
        .collect();

    // Compute map for condensing the `Label`s (could be made faster)
    let mut label_map = vec![
        None;
        usize::from(
            triangulation
                .labels()
                .max()
                .expect("Triangulation is empty")
        ) + 1
    ];
    for (new, old_label) in triangulation.labels().enumerate() {
        label_map[usize::from(old_label)] = Some(new)
    }

    // Build graph from triangulation data (could be changed to included directional data)
    for label in triangulation.labels() {
        let triangle = &triangulation.get(label);
        nodes[label_map[usize::from(label)].expect("label_map not complete")] = Some(Node {
            links: vec![
                Link::Undirected(Label::from(
                    label_map[usize::from(triangle.left)].expect("label_map not complete"),
                )),
                Link::Undirected(Label::from(
                    label_map[usize::from(triangle.center)].expect("label_map not complete"),
                )),
                Link::Undirected(Label::from(
                    label_map[usize::from(triangle.right)].expect("label_map not complete"),
                )),
            ],
        })
    }

    // Create graph struct
    Graph {
        nodes: nodes
            .into_iter()
            .map(|node| node.expect("Not all triangles are includedin graph creation"))
            .collect(),
    }
}

impl<'a> Graph {
    /// Returns the amount of nodes in the `Graph`
    fn len(&self) -> usize {
        self.nodes.len()
    }

    /// Returns iterator over the neighbours of the given `Node`
    fn get_neighbours(&'a self, node: Label) -> impl Iterator<Item = Label> + 'a {
        self.nodes[node].links.iter().map(|link| link.to())
    }

    /// Return a list of the amount of nodes at given distances
    ///
    /// Requires the `Label`s to be 'dense', such that `highest_label == graph.len()`
    pub fn distance_profile(&self, origin: Label, max_distance: usize) -> DistanceProfile {
        let mut distance_profile: Vec<usize> = Vec::with_capacity(max_distance);

        // TODO: LabelSet may be overkill for this, we only need the LabelList
        let mut visited: LabelSet<Node> = LabelSet::new(self.len());
        visited.add(origin);

        let mut inner = vec![origin];
        for _ in 0..max_distance {
            distance_profile.push(inner.len());
            let mut outer: Vec<Label> = Vec::with_capacity(MAX_VERTEX_ORDER * inner.len());
            for node in inner {
                // Add only nodes that have not yet been visited (and add them to be visited)
                outer.extend(self.get_neighbours(node).filter(|&node| !visited.add(node)));
            }
            inner = outer;
        }

        DistanceProfile::from_vec(distance_profile, max_distance)
    }

    /// Return the `Label` of a random `Node` in the `Graph`
    pub fn sample_node<R: Rng + ?Sized>(&self, rng: &mut R) -> Label {
        Label::from(rng.gen_range(0..self.nodes.len()))
    }

    /// Return the vertex order distribution based on the `Graph`
    pub fn vertex_order_distribution(&self) -> HashMap<usize, usize> {
        let mut vertex_order_distribution = HashMap::new();
        for node in self.nodes.iter() {
            *vertex_order_distribution
                .entry(node.links.len())
                .or_insert_with(|| 0) += 1;
        }
        vertex_order_distribution
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::triangulation::{
        dynamic_triangulation::DynTriangulation,
        observables::vertex_order::vertex_order_distribution,
    };
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    /// Assert items in list with first in distance_profile
    fn assert_distance_profile<I: IntoIterator<Item = usize>>(
        distance_profile: DistanceProfile,
        list: I,
    ) {
        for (i, count) in list.into_iter().enumerate() {
            assert!(
                i < distance_profile.max_distance(),
                "Given list longer than measured DistanceProfile."
            );
            assert_eq!(distance_profile.get_count(i), count);
        }
    }

    #[test]
    fn graph_creation() {
        let mut triangulation = DynTriangulation::new(20, 15);
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);

        for _ in 0..1000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.add(triangulation.sample(&mut rng));
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }

        let _ = dual_graph(&triangulation);
    }

    #[test]
    fn distance_profile() {
        let mut triangulation = DynTriangulation::new(20, 15);
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);

        for _ in 0..1000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.add(triangulation.sample(&mut rng));
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }

        let dual_graph = dual_graph(&triangulation);
        let dprofile = dual_graph.distance_profile(dual_graph.sample_node(&mut rng), 15);

        println!("{:50}", dprofile);
        assert_distance_profile(dprofile, [1, 3, 6, 8, 9, 13, 15, 17, 22, 26, 31, 43, 49])
    }

    // Temporary testing for my own interest
    #[test]
    fn distance_profile_large() {
        let mut triangulation = DynTriangulation::new(200, 150);
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);

        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..1_000_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.add(triangulation.sample(&mut rng));
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }

        let dual_graph = dual_graph(&triangulation);
        let dprofile = dual_graph.distance_profile(dual_graph.sample_node(&mut rng), 30);

        println!("{:50}", dprofile);

        let vertex_graph = vertex_graph(&triangulation);
        let dprofile = vertex_graph.distance_profile(vertex_graph.sample_node(&mut rng), 30);

        println!("{:50}", dprofile);
    }

    #[test]
    fn vertex_order_test() {
        let mut triangulation = DynTriangulation::new(20, 20);
        let mut rng = Xoshiro256StarStar::seed_from_u64(137);

        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..1_000_000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.add(triangulation.sample(&mut rng));
            triangulation.flip(triangulation.sample_flip(&mut rng));
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }

        let dual_graph = dual_graph(&triangulation);
        let vertex_graph = vertex_graph(&triangulation);

        println!("{:?}", dual_graph.vertex_order_distribution());
        println!("{:?}", vertex_graph.vertex_order_distribution());
        println!(
            "expected count order four: {}, total count: {}, {}",
            triangulation.order_four_count(),
            vertex_graph
                .vertex_order_distribution()
                .values()
                .sum::<usize>(),
            vertex_graph
                .vertex_order_distribution()
                .iter()
                .map(|(order, count)| order * count)
                .sum::<usize>()
        );
        println!("{:}", vertex_order_distribution(&triangulation));

        // let vertex_triangles = create_vertex_triangles(&triangulation, &length_profile(&triangulation));
        // for ovt in vertex_triangles {
        //     if let Some(vt) = ovt {
        //         println!("{:?}, {:?}, {:?}", vt.left_vertex, vt.middle_vertex, vt.right_vertex)
        //     };
        // }

        // println!("{}", vertex_graph.nodes.len());
        // for node in vertex_graph.nodes.iter().take(10) {
        //     println!(
        //         "{:?}",
        //         node.links
        //             .iter()
        //             .map(|link| match link {
        //                 Link::Undirected(l) => l.0,
        //             })
        //             .collect::<Vec<usize>>()
        //     )
        // }
    }
}
