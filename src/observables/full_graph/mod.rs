//! Module containing observables measurements for the full graph/triangulation
//! simultaniously, which allows for optimization in determining distances on the graph.
//!
//! These methods mostly make use of the fact that the distance matrix can be constructed
//! effecitvely beforehand, stored, and reused for all sorts of observable measurements.
//! However, distance matrices are the size of the graph^2, so can get very big. So this
//! module has methods of storing the matrix on disk, an loading it in smaller batches
//! to allow the RAM usage to remain reasonable.

pub mod bufwriter;
pub mod chunking;
pub mod distance_matrix;
pub mod sparse;
