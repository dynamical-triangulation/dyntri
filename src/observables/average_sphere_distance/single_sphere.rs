//! The average sphere distance of two spheres that are fully on top of one another, i.e. are fully
//! overlapping.
//!
//! This means that the distance between the origins of the two spheres is 0. One could also view
//! this as the average sphere distance of a sphere to itself; this is why we also call this
//! variant of the average sphere distance `single_sphere` ASD. Note that this is not 0 as the
//! average sphere distance is defined as the average distance between all sphere pairs. In this
//! sense it is no a distance in the mathematical sense that it satisfies the requirements of a
//! distance function.

use std::time;

use hdf5::{Dataset, Group, H5Type};
use humantime::format_duration;
use log::{debug, info, trace};
use ndarray::prelude::*;

use super::geodesic_spheres::{get_geodesic_sphere, GeodesicSphere};
use super::marked_list::MarkedList;
use super::{AverageSphereDistance, AverageSphereDistanceProfile};
use crate::collections::LabelledArray;
use crate::graph::breadth_first_search::traverse_nodes::TraverseNodeDistance;
use crate::graph::breadth_first_search::IterNeighbours;
use crate::graph::field::PeriodicField;
use crate::graph::periodic_graph::{PeriodicGraph, PeriodicLabel, PeriodicLabelMatch};
use crate::graph::{FieldGraph, GenericGraph, IterableGraph, SampleGraph, SizedGraph};
use crate::observables::full_graph::distance_matrix;
use crate::observables::full_graph::sparse::{mask_product_disk, mask_product_elem, BoolMatrix};
use distance_matrix::{
    compute_distance_matrix, compute_distance_matrix_with_masks, DistanceMatrixMasks,
    DISTANCE_MATRIX_DATASET_NAME, MASK_GROUP_BASE_NAME,
};

/// Returns the single sphere average sphere distance from a given `origin` at length scale `delta`.
///
/// This function requires `graph` to be a [periodic graph](crate::graph::periodic_graph::PeriodicGraph)
/// and also computes the [violation ratio](AverageSphereDistance::violation_ratio) of the ASD.
/// For the average sphere distance of a [sized graph](crate::graph::SizedGraph) see functon
/// [measure_sized_graph] instead.
pub fn measure<'a, I, F, G>(graph: &'a G, origin: G::Label, delta: usize) -> AverageSphereDistance
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G: IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    G::Label: PeriodicLabel,
{
    overlapping_sphere_distance(graph, origin, delta)
}

/// Returns the single sphere average sphere distance from a given `origin` at length scale `delta`.
///
/// This function requires `graph` to be a [sized graph](crate::graph::SizedGraph)
/// For the average sphere distance of a [periodic graph](crate::graph::periodic_graph::PeriodicGraph)
/// see [measure] instead.
pub fn measure_sized_graph<'a, I, G>(graph: &'a G, origin: G::Label, delta: usize) -> f64
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
{
    overlapping_sphere_distance_non_periodic(graph, origin, delta)
}

fn overlapping_sphere_distance<'a, I, F, G>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
) -> AverageSphereDistance
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G: IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    G::Label: PeriodicLabel,
{
    let sphere = get_geodesic_sphere(graph, origin, delta);
    overlapping_sphere_distance_from_sphere(graph, &sphere, delta)
}

fn overlapping_sphere_distance_from_sphere<'a, I, G>(
    graph: &'a G,
    sphere: &GeodesicSphere<G::Label>,
    delta: usize,
) -> AverageSphereDistance
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + PeriodicGraph,
    G::Label: PeriodicLabel,
    G::FieldType<()>: PeriodicField<(), G>,
{
    // Normalize by the amount of pairs that are measured (N_sphere^2)
    let mut norm = 0.0;
    // Use a continuously updating average such that the total does not need to become large
    let mut average = 0.0;
    let mut violation_count = 0;
    // Reuse marked list because it SHOULD BE emptied every node
    let mut marked: MarkedList<(), _, _> = MarkedList::new(graph);
    for &start_node in sphere.iter() {
        // This could break if sphere contained duplicate labels, but the BFS for getting the
        // geodesic sphere should guarantee this never occurs.
        marked.add_multiple(sphere.iter().copied());
        let mut violation_marks: LabelledArray<bool, <G::Label as PeriodicLabel>::BaseLabel> =
            LabelledArray::fill_with(|| false, graph.base_len());
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(start_node) {
            assert!(
                // This assert should be able to be removed (but performance impact is
                // minimal)
                node.distance <= 2 * delta + 1,
                "Something went wrong, {}/{} sphere points were not found within 2*delta",
                marked.len(),
                sphere.len()
            );

            // Mark the point as visisted
            match marked.try_remove_periodic(node.label) {
                PeriodicLabelMatch::None => continue,
                PeriodicLabelMatch::Equal => {
                    norm += 1.0;
                    average += (node.distance as f64 - average) / norm;
                    // Stop BFS if all nodes are visited
                    if marked.is_empty() {
                        break;
                    }
                }
                PeriodicLabelMatch::Patch => {
                    let marked = &mut violation_marks[node.label.base_label()];
                    if !*marked {
                        *marked = true;
                        violation_count += 1;
                    }
                }
            }
        }
    }

    AverageSphereDistance {
        asd: average,
        violation_ratio: violation_count as f64 / sphere.len().pow(2) as f64,
    }
}

/// Compute the Average sphere distance with epsilon = 0, for different delta
pub fn profile<'a, I, F, G>(
    graph: &'a G,
    point: G::Label,
    deltas: impl Iterator<Item = usize>,
) -> AverageSphereDistanceProfile
where
    I: IterNeighbours<G>,
    F: PeriodicField<(), G>,
    G: SampleGraph + IterableGraph<'a, I> + FieldGraph<FieldType<()> = F> + PeriodicGraph,
    G::Label: PeriodicLabel,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let AverageSphereDistance {
            asd,
            violation_ratio,
        } = overlapping_sphere_distance(graph, point, delta);
        profile.push(asd);
        violation_ratios.push(violation_ratio);
    }

    AverageSphereDistanceProfile {
        profile: profile.into_boxed_slice(),
        violation_ratios: violation_ratios.into_boxed_slice(),
    }
}

/// Compute the Average sphere distance with epsilon = 0, for different delta
pub fn profile_sized_graph<'a, I, G>(
    graph: &'a G,
    point: G::Label,
    deltas: impl Iterator<Item = usize>,
) -> Vec<f64>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
{
    let mut profile: Vec<f64> = Vec::with_capacity(deltas.size_hint().0);

    for delta in deltas {
        let asd = overlapping_sphere_distance_non_periodic(graph, point, delta);
        profile.push(asd);
    }

    profile
}

fn overlapping_sphere_distance_non_periodic<'a, I, G>(
    graph: &'a G,
    origin: G::Label,
    delta: usize,
) -> f64
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
{
    let sphere = get_geodesic_sphere(graph, origin, delta);
    overlapping_sphere_distance_from_sphere_non_periodic(graph, &sphere, delta)
}

fn overlapping_sphere_distance_from_sphere_non_periodic<'a, I, G>(
    graph: &'a G,
    sphere: &GeodesicSphere<G::Label>,
    delta: usize,
) -> f64
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I> + FieldGraph + SizedGraph,
{
    // Use a continuously updating average such that the total does not need to become large
    let mut norm = 0.0;
    let mut average = 0.0;
    // Reuse marked list because it SHOULD BE emptied every node
    let mut marked: MarkedList<(), _, _> = MarkedList::new(graph);
    for &start_node in sphere.iter() {
        // This could break if sphere contained duplicate labels, but the BFS for getting the
        // geodesic sphere should guarantee this never occurs.
        marked.add_multiple(sphere.iter().copied());
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(start_node) {
            assert!(
                // This assert should be able to be removed (but performance impact is
                // minimal)
                node.distance <= 2 * delta + 1,
                "Something went wrong, {}/{} sphere points were not found within 2*delta",
                marked.len(),
                sphere.len()
            );

            // Mark the point as visisted
            if marked.try_remove(node.label) {
                norm += 1.0;
                average += (node.distance as f64 - average) / norm;
                // Stop BFS if all nodes are visited
                if marked.is_empty() {
                    break;
                }
            }
        }
    }
    average
}

/// Returns the average sphere distance of every node in the graph from a distance matrix.
///
/// Requires the distance matrix `dmatrix` and a `mask` at the wanted distance scale `delta`
pub fn measure_full_graph_from_dmatrix<T, U>(
    dmatrix: ArrayView2<T>,
    mask: &BoolMatrix,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
{
    let norm = mask.view().row_sum();
    let n = dmatrix.nrows();
    (0..n)
        .zip(norm)
        .map(|(k, norm)| {
            num_traits::cast::<_, f64>(mask_product_elem::<T, U>(
                mask.view(),
                dmatrix.view(),
                mask.transpose(),
                Dim([k, k]),
            ))
            .expect("U should be castable to f64")
                / (norm as f64).powi(2)
        })
        .collect()
}

/// Returns the average sphere distance for every point in `points` in the graph
/// from a distance matrix.
///
/// Requires the distance matrix `dmatrix` and a `mask` at the wanted distance scale `delta`
pub fn measure_points_from_dmatrix<T, U>(
    dmatrix: ArrayView2<T>,
    mask: &BoolMatrix,
    points: ArrayView1<usize>,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
{
    let norms = mask.view().row_sum();
    points
        .iter()
        .map(|&k| {
            let norm = norms[k];
            num_traits::cast::<_, f64>(mask_product_elem::<T, U>(
                mask.view(),
                dmatrix.view(),
                mask.transpose(),
                Dim([k, k]),
            ))
            .expect("U should be castable to f64")
                / (norm as f64).powi(2)
        })
        .collect()
}

/// Returns the average sphere distance of every node in the graph on memory.
///
/// Computes the distance matrix of the graph on memory. This can be very taxing on the memory if
/// the graph is reasonably large. In this case it is advisable to store the distance matrix on
/// disk, see [measure_full_graph_from_disk].
pub fn measure_full_graph<T, U>(graph: &GenericGraph, delta: usize) -> Array1<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
{
    let dmatrix = compute_distance_matrix::<T>(graph, 2 * delta + 1);

    trace!("Constructing mask from distance matrix at delta {delta}");
    let delta: T = num_traits::cast(delta).expect("Delta could not be expressed in requested type");
    let mask = BoolMatrix::from_dense_filter(dmatrix.view(), |&r| r == delta);

    measure_full_graph_from_dmatrix::<T, U>(dmatrix.view(), &mask)
}

/// Returns the average sphere distance of every node in the graph from a distance matrix with
/// masks.
///
/// Requires the distance matrix `dmatrix` with masks, and asks for an iterator over deltas on which
/// the computation should be performed.
///
/// Here the first axis of the array is gives the different `deltas`, and
/// the second axis gives the different points (where technically the index corresponds to
/// the label value). So each column of the array is the asd profile of a single point.
pub fn profile_full_graph_from_dmatrix<T, U>(
    dmatrix: DistanceMatrixMasks<T>,
    deltas: impl Iterator<Item = usize>,
) -> Array2<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
{
    let distance_matrix = dmatrix.distance_matrix;
    let mut profile = Array2::zeros([0, distance_matrix.nrows()]);
    for (i, delta) in deltas.enumerate() {
        let mask = &dmatrix.masks[i];
        trace!(
            "Computing average sphere distances for all points at delta={}",
            delta
        );
        let asd_field = measure_full_graph_from_dmatrix::<T, U>(distance_matrix.view(), mask);
        profile
            .push_row(asd_field.view())
            .expect("Shape should be correct by construction");
    }
    profile
}

/// Measure the single sphere average sphere distance of several points in the graph with a
/// distance matrix with masks from memory.
///
/// This performs the same measurement as [`profile_full_graph_from_dmatrix`], but only
/// for a limited number of points.
/// Similarlu the first axis of the array is gives the different `deltas`, and
/// the second axis gives the different point, where the index corresponds with the index of the
/// requested points.
pub fn profile_points_from_dmatrix<T, U>(
    dmatrix: DistanceMatrixMasks<T>,
    points: ArrayView1<usize>,
    deltas: impl Iterator<Item = usize>,
) -> Array2<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
{
    let distance_matrix = dmatrix.distance_matrix;
    let mut profile = Array2::zeros([0, points.len()]);
    for (i, delta) in deltas.enumerate() {
        let mask = &dmatrix.masks[i];
        trace!(
            "Computing average sphere distances for all points at delta={}",
            delta
        );
        let asd_field = measure_points_from_dmatrix::<T, U>(distance_matrix.view(), mask, points);
        profile
            .push_row(asd_field.view())
            .expect("Shape should be correct by construction");
    }
    profile
}

/// Returns the single sphere average sphere distance profile for all points in the triangulation
/// at once.
///
/// Here the first axis of the array is gives the different `deltas`, and
/// the second axis gives the different points (where technically the index corresponds to
/// the label value). So each column of the array is the asd profile of a single point.
pub fn profile_full_graph<T, U, I>(graph: &GenericGraph, deltas: I) -> Array2<f64>
where
    T: Copy + Eq + num_traits::Num + num_traits::NumCast,
    U: From<T> + num_traits::Zero + num_traits::NumCast,
    I: Iterator<Item = usize> + Clone,
{
    let max_delta = deltas
        .clone()
        .max()
        .expect("`deltas` should have a largest delta");
    trace!("Starting distance matrix measurement");
    let dmatrix =
        compute_distance_matrix_with_masks::<T>(graph, deltas.clone().collect(), 2 * max_delta + 1);
    trace!("Finished determining distance matrix");
    profile_full_graph_from_dmatrix::<T, U>(dmatrix, deltas)
}

/// Measure the single sphere average sphere distance of the full graph with a distance matrix
/// from disk.
///
/// This performs the same measurement as [`measure_full_graph`] but with a distance matrix
/// from disk. `dmatrix` should be a [`Dataset`] representing the distance matrix of a graph.
pub fn measure_full_graph_from_disk<T, U>(dmatrix: &Dataset, delta: usize) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    info!("Constructing mask from distance matrix at delta {delta}");
    let delta: T = num_traits::cast(delta).expect("Delta could not be expressed in requested type");
    let mask = BoolMatrix::from_dense_filter_disk::<T, _>(dmatrix, |&r| r == delta);
    measure_full_graph_from_disk_with_mask::<T, U>(dmatrix, &mask)
}

/// Measure the single sphere average sphere distance of the full graph with a distance matrix
/// from disk and a precomputed mask.
///
/// This performs the same measurement as [`measure_full_graph`] but with a distance matrix
/// from disk. `dmatrix` should be a [`Dataset`] representing the distance matrix of a graph,
/// and `mask` the mask of the distance matrix of the desired distance scale `delta`.
pub fn measure_full_graph_from_disk_with_mask<T, U>(
    dmatrix: &Dataset,
    mask: &BoolMatrix,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    let norm = mask.view().row_sum();
    let n = dmatrix.shape()[0];
    let indices = Array1::from_iter((0..n).map(|k| Dim([k, k])));
    let unnorm_asd: Array1<U> =
        mask_product_disk::<T, U>(mask.view(), dmatrix, mask.transpose(), indices.view());
    unnorm_asd.mapv(|v| num_traits::cast::<_, f64>(v).expect("U should be castable to f64"))
        / norm.mapv(|v| (v as f64).powi(2))
}

/// Measure the single sphere average sphere distance of several points in the graph with a
/// distance matrix from disk and a precomputed mask.
///
/// This performs the same measurement as [`measure_full_graph_from_disk_with_mask`], but only
/// for a limited number of points.
pub fn measure_points_full_graph_from_disk_with_mask<T, U>(
    dmatrix: &Dataset,
    points: ArrayView1<usize>,
    mask: &BoolMatrix,
) -> Array1<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
{
    let norm_full = mask.view().row_sum();
    let norm: Array1<f64> = points
        .iter()
        .map(|&k| (norm_full[k] as f64).powi(2))
        .collect();
    let indices = points.map(|&k| Dim([k, k]));
    let unnorm_asd: Array1<U> =
        mask_product_disk::<T, U>(mask.view(), dmatrix, mask.transpose(), indices.view());
    unnorm_asd.mapv(|v| num_traits::cast::<_, f64>(v).expect("U should be castable to f64")) / norm
}

/// Measured the average sphere distance profile at every vertex in the graph given by
/// the distance matrix stored in the group file.
///
/// The first axis of the results gives the delta, each row corresponding to the delta in
/// `deltas` with the same index. And the second axis corresponds to each point in the graph,
/// where each index corresponds to the label of the corresponding node.
///
/// Note: this function needs masks of the distance profile to be present in the `group` to
/// work, other wise it will return an error. E.g. using
/// [distance_matrix::compute_distance_matrix_with_masks_disk()] gives the correct group.
pub fn profile_full_graph_from_disk<T, U, I>(group: &Group, deltas: I) -> Array2<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
    I: Iterator<Item = usize> + Clone,
{
    debug!("Loading distance matrix data from HDF5 file");
    let dmatrix_dataset = group
        .dataset(DISTANCE_MATRIX_DATASET_NAME)
        .unwrap_or_else(|err| {
            panic!(
                "Dataset `{}` should exist: {err}",
                DISTANCE_MATRIX_DATASET_NAME
            )
        });
    let n = dmatrix_dataset.shape()[0];

    let mut profile = Array2::zeros([0, n]);
    for delta in deltas {
        trace!("Retrieving mask for delta {delta} from HDF5 file");
        let mask_group = group
            .group(&format!("{MASK_GROUP_BASE_NAME}{delta}"))
            .expect("Mask for delta does not exist");
        let mask =
            BoolMatrix::from_disk(&mask_group).expect("BoolMatrix could not be formed from disk");
        trace!(
            "Computing average sphere distances for all points at delta={}",
            delta
        );
        let start = time::Instant::now();
        let asd_field = measure_full_graph_from_disk_with_mask::<T, U>(&dmatrix_dataset, &mask);
        trace!(
            "Finished ASD computation at delta={delta} in {}",
            format_duration(start.elapsed())
        );
        profile
            .push_row(asd_field.view())
            .expect("Shape should be correct by construction");
    }
    profile
}

/// Returns average sphere distance profile with delta on axis 0 and the points axis 1
pub fn profile_points_full_graph_from_disk<T, U, I>(
    group: &Group,
    points: ArrayView1<usize>,
    deltas: I,
) -> Array2<f64>
where
    T: Copy + Eq + num_traits::NumCast + H5Type,
    U: Copy + From<T> + num_traits::NumAssign + num_traits::NumCast,
    I: Iterator<Item = usize> + Clone,
{
    debug!("Loading distance matrix data from HDF5 file");
    let dmatrix_dataset = group
        .dataset(DISTANCE_MATRIX_DATASET_NAME)
        .unwrap_or_else(|err| {
            panic!(
                "Dataset `{}` should exist: {err}",
                DISTANCE_MATRIX_DATASET_NAME
            )
        });
    let n = points.len();
    let mut profile = Array2::zeros([0, n]);
    for delta in deltas {
        trace!("Retrieving mask for delta {delta} from HDF5 file");
        let mask_group = group
            .group(&format!("{MASK_GROUP_BASE_NAME}{delta}"))
            .expect("Mask for delta does not exist");
        let mask =
            BoolMatrix::from_disk(&mask_group).expect("BoolMatrix could not be formed from disk");
        trace!(
            "Computing average sphere distances for {n} points at delta={}",
            delta
        );
        let start = time::Instant::now();
        let asd_field =
            measure_points_full_graph_from_disk_with_mask::<T, U>(&dmatrix_dataset, points, &mask);
        trace!(
            "Finished ASD computation at delta={delta} in {}",
            format_duration(start.elapsed())
        );
        profile
            .push_row(asd_field.view())
            .expect("Shape should be correct by construction");
    }
    profile
}
