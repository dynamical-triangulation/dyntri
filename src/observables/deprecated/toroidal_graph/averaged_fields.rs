use std::ops::{AddAssign, Div, Sub};

use super::Graph;

pub trait Field<G: Graph> {
    type Type: Default
        + Copy
        + AddAssign
        + Sub<Output = Self::Type>
        + Div<Output = Self::Type>
        + From<u32>;

    /// Get the field value at a given point
    fn get(&self, point: G::Label) -> Self::Type;

    /// Returns the graph on which the field is defined
    fn graph(&self) -> &G;

    /// Computes the average of a given field on the graph within a geodesic sphere of size delta,
    /// *including* the boundary points.
    fn get_sphere_average(&self, point: G::Label, delta: usize) -> Self::Type {
        let mut mean_field = Self::Type::default();
        let mut count = 0;
        for node in self.graph().iter_breadth_first(point) {
            if node.distance > delta {
                break;
            }
            let field_value = self.get(node.label);
            let diff = field_value - mean_field;
            count += 1;
            mean_field += diff / Self::Type::from(count);
        }
        mean_field
    }
}

#[derive(Debug, Clone, Copy)]
pub struct VertexDegree<'a, G: Graph> {
    graph: &'a G,
}

impl<'a, G: Graph> VertexDegree<'a, G> {
    pub fn new(graph: &'a G) -> Self {
        Self {
            graph
        }
    }
}

impl<'a, G: Graph> Field<G> for VertexDegree<'a, G> {
    type Type = f64;

    fn get(&self, point: G::Label) -> Self::Type {
        self.graph.get_vertex_order(point) as f64
    }

    #[inline]
    fn graph(&self) -> &G {
        self.graph
    }
}
