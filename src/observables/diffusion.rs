//! Diffusion processes on a graph to measure the probability of a random walk returning to the
//! same node.
//!
//! Diffusion processes like these that are linked to a random walk are often used in the analysis
//! of graph-like structures to determine the _spectral dimension_, the scaling dimension of the
//! heat kernel of the diffusion process.
//! See for example [arXiv:1412.3434](https://arxiv.org/abs/1412.3434) for an use case of
//! the spectral dimension in 2D CDT (in this case coupled to matter fields).

use std::marker::PhantomData;

use log::trace;
use serde::Serialize;

use crate::{
    collections::LabelledArray,
    graph::{
        breadth_first_search::{traverse_nodes::TraverseNodeDistance, IterNeighbours},
        field::{Field, FieldValue, PeriodicField},
        periodic_graph::{BoundedPeriodicGraph, IterPeriodicGraph, PeriodicLabel},
        FieldGraph, IterSizedGraph, IterableGraph,
    },
};

/// The return probability over diffusion time (diffusion steps)
///
/// This is simply a wrapper over a [`Vec`]
#[derive(Debug, Clone, Serialize)]
pub struct ReturnProbabilityProfile(pub Vec<f64>);

/// Compute the heat kernel of the graph, specifically the return probabilities
/// of diffusion for different diffusion times (starting at sigma=0)
pub fn measure<'a, G: IterableGraph<'a, I>, I: IterNeighbours<G>>(
    graph: &'a G,
    origin: G::Label,
    max_diffusion_time: usize,
    diffusion_coefficient: f64,
) -> ReturnProbabilityProfile {
    // List storing the return probabilities
    let mut diffusion_profile: Vec<f64> = Vec::with_capacity(max_diffusion_time);
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field: G::FieldType<f64> = graph.new_field();
    let _ = *probability_field.get_or_insert_with(origin, || 1.0);
    // Set return probability at diffusion time zero (sigma = 0)
    diffusion_profile.push(
        probability_field
            .get(origin)
            .expect("Origin probability was not set"),
    );

    // Empty probability field for storing updated diffusion probabilities
    let mut new_probs = graph.new_field();
    for sigma in 1..max_diffusion_time {
        // Diffuse probabilities (from origin outward)
        for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
            // If distance is larger or equal to the diffusions steps that
            // already took place changes cannot have reached that area yet.
            // Also if the distance is larger than the diffusion steps that
            // will take place, changes cannot reach the origin anymore.
            // In both case the resulting return probabilities will not be
            // affected so stop diffusion for this step.
            // TODO: Check that this restriction really doesn't miss any relevant points
            if node.distance >= sigma || node.distance > max_diffusion_time - sigma {
                break;
            }
            // Get probability
            let label = node.label;
            let FieldValue::Some(prob) = probability_field.get(label) else {
                // TODO: This should never occur, as the distance checks beforehand
                // guarantee no irrelevant points are looped over. Check this explicitly
                continue;
            };
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            *new_probs.get_or_insert(label, 0.0) += (1.0 - diffusion_coefficient) * prob;
            let nbr_prob = diffusion_coefficient * prob / graph.get_vertex_degree(label) as f64;
            for neighbour in graph.iter_neighbours(label) {
                *new_probs.get_or_insert(neighbour, 0.0) += nbr_prob;
            }
        }
        // Set new found probability field
        probability_field = new_probs;
        // Reset temporary probabilities
        new_probs = graph.new_field();
        // Set return probability for current `sigma`
        diffusion_profile.push(
            probability_field
                .get(origin)
                .expect("Origin probability was not set"),
        );
    }

    ReturnProbabilityProfile(diffusion_profile)
}

/// Measure the return probability for a [sized graph](crate::graph::SizedGraph).
///
/// Note that function performs the same measurements as [measure], but it is faster due to a better
/// iterator for sized graphs.
pub fn measure_sized<'a, G, I1, I2>(
    graph: &'a G,
    origin: G::Label,
    max_diffusion_time: usize,
    diffusion_coefficient: f64,
) -> ReturnProbabilityProfile
where
    G: IterSizedGraph<I1> + IterableGraph<'a, I2>,
    G::Label: Into<usize>,
    I1: Iterator<Item = G::Label>,
    I2: IterNeighbours<G>,
{
    // List storing the return probabilities
    let mut diffusion_profile: Vec<f64> = Vec::with_capacity(max_diffusion_time);
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field = LabelledArray::zeros(graph.len());
    probability_field[origin] = 1.0;
    // Set return probability at diffusion time zero (sigma = 0)
    diffusion_profile.push(probability_field[origin]);

    // Empty probability field for storing updated diffusion probabilities
    let mut new_probs = LabelledArray::zeros(graph.len());
    for _ in 1..max_diffusion_time {
        // Diffuse probabilities for every point
        for label in graph.iter() {
            let prob = probability_field[label];
            // Skip step if probability is still 0
            if prob == 0.0 {
                continue;
            }
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            new_probs[label] += (1.0 - diffusion_coefficient) * prob;
            let nbr_prob = diffusion_coefficient * prob / graph.get_vertex_degree(label) as f64;
            for neighbour in graph.iter_neighbours(label) {
                new_probs[neighbour] += nbr_prob;
            }
        }
        // Set new found probability field
        probability_field = new_probs;
        // Reset temporary probabilities
        new_probs = LabelledArray::zeros(graph.len());
        // Set return probability for current `sigma`
        diffusion_profile.push(probability_field[origin]);
    }

    ReturnProbabilityProfile(diffusion_profile)
}

/// Measure the return probability for a
/// [bounded graph](crate::graph::periodic_graph::BoundedPeriodicGraph).
///
/// Note that function performs the same measurements as [measure], but it is faster due to a better
/// iterator for bounded periodic graphs.
pub fn measure_bounded<'a, G, I1, I2, I3>(
    graph: &'a G,
    origin: G::Label,
    max_diffusion_time: usize,
    diffusion_coefficient: f64,
) -> ReturnProbabilityProfile
where
    G: IterableGraph<'a, I1> + BoundedPeriodicGraph<I2> + IterPeriodicGraph<I3> + FieldGraph,
    G::FieldType<f64>: PeriodicField<f64, G>,
    G::Label: PeriodicLabel,
    I1: IterNeighbours<G>,
    I2: Iterator<Item = G::Patch>,
    I3: Iterator<Item = G::Label>,
{
    // List storing the return probabilities
    let mut diffusion_profile: Vec<f64> = Vec::with_capacity(max_diffusion_time);
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field: G::FieldType<f64> = graph.new_field();
    let _ = *probability_field.get_or_insert_with(origin, || 1.0);
    // Set return probability at diffusion time zero (sigma = 0)
    diffusion_profile.push(
        probability_field
            .get(origin)
            .expect("Origin probability was not set for sigma 0"),
    );

    // Empty probability field for storing updated diffusion probabilities
    let mut new_probs = graph.new_field();
    for sigma in 1..max_diffusion_time {
        trace!("Diffusion step {sigma}");
        for label in graph
            .iter_patches()
            .filter(|&patch| probability_field.has_patch(patch))
            .flat_map(|patch| graph.iter_patch(patch))
        {
            // Get probability
            let FieldValue::Some(prob) = probability_field.get(label) else {
                    continue;
                };
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            *new_probs.get_or_insert(label, 0.0) += (1.0 - diffusion_coefficient) * prob;
            let nbr_prob = diffusion_coefficient * prob / graph.get_vertex_degree(label) as f64;
            for neighbour in graph.iter_neighbours(label) {
                *new_probs.get_or_insert(neighbour, 0.0) += nbr_prob;
            }
        }
        // Set new found probability field
        probability_field = new_probs;
        // Reset temporary probabilities
        new_probs = graph.new_field();
        // Set return probability for current `sigma`
        diffusion_profile.push(
            probability_field
                .get(origin)
                .expect(&format!("Origin probability was not set at sigma {sigma}")),
        );
    }

    ReturnProbabilityProfile(diffusion_profile)
}

/// Iterator of a diffusion process to allow for external control of the diffusion steps
pub struct Iter<'a, G: FieldGraph, I> {
    graph: &'a G,
    origin: G::Label,
    diffusion_coefficient: f64,
    sigma: usize,
    probability_field: G::FieldType<f64>,
    neighbour_iterator_type: PhantomData<I>,
}

impl<'a, G: FieldGraph, I> Iter<'a, G, I> {
    /// Returns the current diffusion step the process is on.
    ///
    /// Note that function returns `None` if the diffusion process has no started yet.
    pub fn current_sigma(&self) -> Option<usize> {
        if self.sigma > 0 {
            Some(self.sigma - 1)
        } else {
            None
        }
    }

    /// Returns the number of the next diffusion step that will be performed.
    pub fn next_sigma(&self) -> usize {
        self.sigma
    }

    /// Returns the current state of the probability field of the diffusion process
    pub fn current_state(&self) -> &G::FieldType<f64> {
        &self.probability_field
    }

    /// Returns the current return probability.
    ///
    /// Note that this return probability is the same probability as that of the probability field
    /// of [current_state](Iter::current_state) at the origin.
    pub fn current_return_probability(&self) -> f64 {
        self.probability_field
            .get(self.origin)
            .expect("Origin probability was not set")
    }
}

/// Compute the heat kernel of the graph, specifically the return probabilities
/// of diffusion for different diffusion times (starting at sigma=0)
pub fn iterable<G: FieldGraph, I>(
    graph: &G,
    origin: G::Label,
    diffusion_coefficient: f64,
) -> Iter<'_, G, I> {
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field: G::FieldType<f64> = graph.new_field();
    probability_field.get_or_insert_with(origin, || 1.0);
    Iter {
        graph,
        origin,
        diffusion_coefficient,
        sigma: 0,
        probability_field,
        neighbour_iterator_type: PhantomData,
    }
}

impl<'a, G: IterableGraph<'a, I>, I: IterNeighbours<G>> Iterator for Iter<'a, G, I> {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.sigma == 0 {
            self.sigma += 1;
            return Some(self.current_return_probability());
        }
        // Empty probability field for storing updated diffusion probabilities
        let mut new_probs = self.graph.new_field();
        for node in self
            .graph
            .iter_breadth_first::<TraverseNodeDistance<_>>(self.origin)
        {
            // If distance is larger or equal to the diffusions steps that already took
            // place changes cannot have reached that area yet. The resulting return
            // probabilities will not be affected so stop diffusion for this step.
            // TODO: Check that this restriction really doesn't miss any relevant points
            if node.distance >= self.sigma {
                break;
            }
            // Get probability
            let label = node.label;
            let FieldValue::Some(prob) = self.probability_field.get(label) else {
                // TODO: This should never occur, as the distance checks beforehand
                // guarantee no irrelevant points are looped over. Check this explicitly
                continue;
            };
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            *new_probs.get_or_insert(label, 0.0) += (1.0 - self.diffusion_coefficient) * prob;
            let nbr_prob =
                self.diffusion_coefficient * prob / self.graph.get_vertex_degree(label) as f64;
            for neighbour in self.graph.iter_neighbours(label) {
                *new_probs.get_or_insert(neighbour, 0.0) += nbr_prob;
            }
        }
        // Set new found probability field
        self.probability_field = new_probs;
        // Increment sigma for next iteration
        self.sigma += 1;
        // Set return probability for current `sigma`
        Some(self.current_return_probability())
    }
}

/// Perform a diffusion process iteration over each diffusion step
///
/// Iterator I1 is designed to iterator over all nodes in the graph,
/// Iterator I2 is designed to iterator over all neighbours of a given node.
pub fn iterable_sized<G: FieldGraph, I1, I2>(
    graph: &G,
    origin: G::Label,
    diffusion_coefficient: f64,
) -> IterSized<'_, G, I1, I2> {
    // Create starting probability of 1.0 at origin and 0.0 elsewhere
    let mut probability_field: G::FieldType<f64> = graph.new_field();
    probability_field.get_or_insert_with(origin, || 1.0);
    IterSized {
        graph,
        origin,
        diffusion_coefficient,
        sigma: 0,
        probability_field,
        neighbour_iterator_type: PhantomData,
        sized_iterator_type: PhantomData,
    }
}

/// Iterator of the diffusion process to control the diffusion steps externally.
pub struct IterSized<'a, G: FieldGraph, I1, I2> {
    graph: &'a G,
    origin: G::Label,
    diffusion_coefficient: f64,
    sigma: usize,
    probability_field: G::FieldType<f64>,
    sized_iterator_type: PhantomData<I1>,
    neighbour_iterator_type: PhantomData<I2>,
}

impl<'a, G: FieldGraph, I1, I2> IterSized<'a, G, I1, I2> {
    /// Returns the current diffusion step the process is on.
    ///
    /// Note that function returns `None` if the diffusion process has no started yet.
    pub fn current_sigma(&self) -> Option<usize> {
        if self.sigma > 0 {
            Some(self.sigma - 1)
        } else {
            None
        }
    }

    /// Returns the number of the next diffusion step that will be performed.
    pub fn next_sigma(&self) -> usize {
        self.sigma
    }

    /// Returns the current state of the probability field of the diffusion process
    pub fn current_state(&self) -> &G::FieldType<f64> {
        &self.probability_field
    }

    /// Returns the current return probability.
    ///
    /// Note that this return probability is the same probability as that of the probability field
    /// of [current_state](Iter::current_state) at the origin.
    pub fn current_return_probability(&self) -> f64 {
        self.probability_field
            .get(self.origin)
            .expect("Origin probability was not set")
    }
}

impl<'a, G, I1, I2> Iterator for IterSized<'a, G, I1, I2>
where
    G: IterableGraph<'a, I2> + IterSizedGraph<I1>,
    I1: Iterator<Item = G::Label>,
    I2: IterNeighbours<G>,
{
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.sigma == 0 {
            self.sigma += 1;
            return Some(self.current_return_probability());
        }
        // Empty probability field for storing updated diffusion probabilities
        let mut new_probs = self.graph.new_field();
        for label in self.graph.iter() {
            // Get probability skipping if not yet set
            let FieldValue::Some(prob) = self.probability_field.get(label) else {
                continue;
            };
            // Diffuse probability to neighbours and itself ($D*prob$ divided over
            // neighbours and $(1-D)*prob$ to itself)
            *new_probs.get_or_insert(label, 0.0) += (1.0 - self.diffusion_coefficient) * prob;
            let nbr_prob =
                self.diffusion_coefficient * prob / self.graph.get_vertex_degree(label) as f64;
            for neighbour in self.graph.iter_neighbours(label) {
                *new_probs.get_or_insert(neighbour, 0.0) += nbr_prob;
            }
        }
        // Set new found probability field
        self.probability_field = new_probs;
        // Increment sigma for next iteration
        self.sigma += 1;
        // Set return probability for current `sigma`
        Some(self.current_return_probability())
    }
}
