//! Module containting all observables used to make measurements on
//! [Triangulation](crate::triangulation::FullTriangulation)s and [Graph](crate::graph::Graph)s
//!
//! Currently available observables or measurments are:
//!  - [Volume profile](volume_profile): Volume of spatial slices over time
//!  - [Vertex degree distribution](vertex_degree_distribution): Distribution of vertex degrees
//! over a triangulation
//!  - [Distance profile](distance_profile): Volume of spheres of increasing radius. This is
//! used to determine the well-known local Hausdorff dimension
//!  - [Diffusion](diffusion): Diffusion process on the graph, giving a return probability of a
//! random walk on the graph. This is used to determine the well-known spectral dimension
//! - [Average sphere distance](average_sphere_distance): Measure of distance between spheres used
//! to determine _quantum Ricci curvature_, a measure of curvature of a graph(/triangulation).
//! - [Random walker](random_walk): Single random walk to determine the average spread of a
//! random walk, which is unavailable from a full diffusion process.
//! - [Two point functions](two_point_function): Two point functions or correlators of any field
//! quantity on the graph; this could be using some of the other quantities.

pub mod average_sphere_distance;
pub mod diffusion;
pub mod distance_profile;
pub mod random_walk;
pub mod two_point_function;
pub mod vertex_degree_distribution;
pub mod volume_profile;

pub mod full_graph;
