//! Module defining the [`Label`] struct, which is a wrapper over `usize` used to label items
//! in the various collections of the [`collections`](crate::collections) module.

use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::marker::PhantomData;

/// Generic `Label` that marks it will label data of type T
#[derive(Serialize, Deserialize)]
pub struct Label<T>(usize, #[serde(skip)] PhantomData<*const T>);

impl<T> Display for Label<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl<T> Debug for Label<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Label({})", self.0)
    }
}
impl<T> Hash for Label<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}
impl<T> Copy for Label<T> {}

#[allow(clippy::non_canonical_clone_impl)]
impl<T> Clone for Label<T> {
    fn clone(&self) -> Self {
        Label(self.0, PhantomData)
    }
}
impl<T> PartialEq for Label<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl<T> Eq for Label<T> {}
#[allow(clippy::non_canonical_partial_ord_impl)]
impl<T> PartialOrd for Label<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
impl<T> Ord for Label<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T> From<Label<T>> for usize {
    fn from(label: Label<T>) -> Self {
        label.0
    }
}
impl<T> From<usize> for Label<T> {
    fn from(index: usize) -> Self {
        Label(index, PhantomData)
    }
}
impl<T> From<Label<Option<T>>> for Label<T> {
    fn from(label: Label<Option<T>>) -> Self {
        Label(label.0, PhantomData)
    }
}
impl<T> From<Label<T>> for Label<Option<T>> {
    fn from(label: Label<T>) -> Self {
        Label(label.0, PhantomData)
    }
}

impl<F> Label<F> {
    #[inline]
    /// Convert a [`Label<F>`] to a label of another type [`Label<T>`]
    pub fn convert<T>(self) -> Label<T> {
        Label(self.0, PhantomData)
    }
}

impl<T> Label<T> {
    /// Construct a new `Label` starting at some first element,
    /// i.e. the label representing 0; internally implemented by starting at 0
    pub fn initial() -> Self {
        Label(0, PhantomData)
    }
}

impl<T> Label<T> {
    /// Returns the succesor `Label` of itself
    pub fn succ(&self) -> Self {
        Label(self.0 + 1, PhantomData)
    }
    /// Advances the `Label` to it's succesor
    pub fn advance(&mut self) {
        self.0 += 1
    }
}
