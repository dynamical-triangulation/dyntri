use std::collections::btree_map;
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::iter;

use crate::{collections, triangulation};
use collections::Label;
use collections::LabelledArray;
use triangulation::triangle::Orientation;

use super::Triangulation;

pub mod timeslice;
pub mod triangle;
pub mod vertex;
use self::timeslice::Timeslice;
use self::triangle::Triangle;
use self::vertex::Vertex;

/// Struct containing full connectivity information
///
/// That is connectivity of the triangles, vertices to
/// the triangles, triangles to the vertices and
/// vertices to oneother.
pub struct FullTriangulation {
    pub triangles: LabelledArray<Triangle>,
    pub vertices: LabelledArray<Vertex>,
    pub timeslices: Box<[Timeslice]>,
}

impl FullTriangulation {
    pub fn time_shift(&mut self, shift: usize) {
        self.timeslices.rotate_right(shift);
        for label in self.triangles.labels() {
            self.triangles[label].time = (self.triangles[label].time + shift) % self.time_len();
        }
        for label in self.vertices.labels() {
            self.vertices[label].time = (self.vertices[label].time + shift) % self.time_len();
        }
    }
}

impl FullTriangulation {
    /// Create an empty `FullTriangulation`
    fn empty() -> Self {
        FullTriangulation {
            triangles: LabelledArray::empty(),
            vertices: LabelledArray::empty(),
            timeslices: Box::new([]),
        }
    }

    // This should be refactored in some more seperated functions for readability
    pub fn from_triangulation<'a>(triangulation: &'a impl Triangulation<'a>) -> FullTriangulation {
        FullTriangulation::from_triangulation_with_mapping(triangulation).0
    }

    /// Return [`FullTriangulation`] with back mapping of [FullTriangulation] labels
    /// to the original `triangulation` labels.
    pub fn from_triangulation_with_mapping<'a>(
        triangulation: &'a impl Triangulation<'a>,
    ) -> (
        FullTriangulation,
        LabelledArray<Label<triangulation::Triangle>, Label<Triangle>>,
    ) {
        // Get original `Label` iterator
        let mut labels = triangulation.labels().peekable();

        // Choose arbitrary origin (by peeking at the first element in the Iterator)
        let origin = if let Some(&label) = labels.peek() {
            label
        } else {
            return (FullTriangulation::empty(), LabelledArray::empty());
        };

        // 1. Walk through triangulation, labelling the triangulation as they are explored;
        //    also identify all vertices with tuple labels (t, x), and mark the timeslices
        let VerticesTimeslicesMapping {
            triangle_map,
            mut vertex_map,
            triangles_vertices,
            shift,
            timeslices,
        } = find_vertices_timeslices_mapping(triangulation, origin);

        // 1b. Remap the vertices to make them truly condensed based on some mapping
        let t_max = timeslices.len();
        let map_vertex_coordinates = |(t, x): (usize, usize)| {
            assert!(
                t <= t_max,
                "Vertex has t {}, higher than amount of timeslices {}",
                t,
                t_max
            );
            let new_t = t % t_max;
            assert_eq!(
                timeslices[new_t].0 .0, new_t,
                "Timeslice time indices not set correctly"
            );
            let x_max = timeslices[new_t].1;
            let new_x = if t >= t_max { x + shift } else { x } % x_max;
            (new_t, new_x)
        };
        let mut vertex_remap: LabelMap<(usize, usize), Vertex> = LabelMap::new();
        // NOTE: this iteration over the map is arbitrary (not reproducible) if for example HashMap is used
        for (&old_label, new_label) in vertex_map.iter_mut() {
            *new_label = vertex_remap.add(map_vertex_coordinates(old_label));
        }

        // 2. Walk through all labels using the obtained connectivity associated relevant
        //    vertices with new `Label`s and constructing FullTriangulation
        let triangle_count = triangulation.size();
        assert_eq!(
            triangle_count,
            triangle_map.len(),
            "Not all or too many triangles have a mapping"
        );
        assert_eq!(
            triangle_count % 2,
            0,
            "triangle_count is not even, this should be impossible"
        );
        let mut triangles: LabelledArray<Option<Triangle>> =
            LabelledArray::fill_with(|| None, triangle_count);
        let mut vertices: LabelledArray<Option<Vertex>> =
            LabelledArray::fill_with(|| None, triangle_count / 2);
        for label in labels {
            let current_triangle = triangulation.get(label);
            let orientation = current_triangle.orientation;

            // Obtain newly mapped labels
            let &new_label = triangle_map
                .get(&label)
                .expect("Triangle Label has not been added to triangle_map");
            let &left = triangle_map
                .get(&current_triangle.left)
                .expect("Triangle Label has not been added to triangle_map");
            let &center = triangle_map
                .get(&current_triangle.center)
                .expect("Triangle Label has not been added to triangle_map");
            let &right = triangle_map
                .get(&current_triangle.right)
                .expect("Triangle Label has not been added to triangle_map");
            let triangle_vertices = triangles_vertices[usize::from(label)]
                .as_ref()
                .expect("Triangle Label has not been added to triangle_vertices");
            let &left_vertex = vertex_map
                .get(&triangle_vertices.left)
                .expect("Vertex Label has not been added to vertex_map");
            let &center_vertex = vertex_map
                .get(&triangle_vertices.center)
                .expect("Vertex Label has not been added to vertex_map");
            let &right_vertex = vertex_map
                .get(&triangle_vertices.right)
                .expect("Vertex Label has not been added to vertex_map");
            let time = triangle_vertices.time;
            triangles[new_label] = Some(Triangle {
                time,
                orientation,
                left,
                center,
                right,
                left_vertex,
                center_vertex,
                right_vertex,
            });

            // Find all vertex neigbours by walking around vertex, only for up to avoid double counting
            if matches!(orientation, Orientation::Down) {
                // If Down triangle is found skip vertex construction and go to next label
                continue;
            }
            let vertex_label = left_vertex; // Associate left vertex with triangle
            let right = right_vertex;

            let mut down = Vec::new();
            // Walk to left until you find down triangle, adding all edges as we go
            let down_triangle = triangulation.get(current_triangle.center);
            let mut mark = down_triangle.left;
            let mut marked_triangle = triangulation.get(mark);
            while matches!(marked_triangle.orientation, Orientation::Up) {
                let triangle_vertices = triangles_vertices[usize::from(mark)]
                    .as_ref()
                    .expect("Triangle Label has not been added to triangle_vertices");
                down.push(
                    *vertex_map
                        .get(&triangle_vertices.right)
                        .expect("Vertex Label is not in Map"),
                );
                mark = marked_triangle.left;
                marked_triangle = triangulation.get(mark);
            }
            let triangle_vertices = triangles_vertices[usize::from(mark)]
                .as_ref()
                .expect("Triangle Label has not been added to triangle_vertices");
            down.push(
                *vertex_map
                    .get(&triangle_vertices.center)
                    .expect("Vertex Label is not in Map"),
            );

            let &left = vertex_map
                .get(&triangle_vertices.left)
                .expect("Vertex Label is not in Map");

            let mut up = Vec::new();
            // Walk to right until you find up triangle, adding all edges as we go
            let up_triangle = triangulation.get(marked_triangle.center);
            let mut mark = up_triangle.right;
            let mut marked_triangle = triangulation.get(mark);
            while matches!(marked_triangle.orientation, Orientation::Down) {
                let triangle_vertices = &triangles_vertices[usize::from(mark)]
                    .as_ref()
                    .expect("Triangle Label has not been added to triangle_vertices");
                up.push(
                    *vertex_map
                        .get(&triangle_vertices.left)
                        .expect("Vertex Label is not in Map"),
                );
                mark = marked_triangle.right;
                marked_triangle = triangulation.get(mark);
            }
            let triangle_vertices = &triangles_vertices[usize::from(mark)]
                .as_ref()
                .expect("Triangle Label has not been added to triangle_vertices");
            up.push(
                *vertex_map
                    .get(&triangle_vertices.center)
                    .expect("Vertex Label is not in Map"),
            );

            vertices[vertex_label] = Some(Vertex {
                time,
                triangle: new_label,
                left,
                up: up.into_boxed_slice(),
                right,
                down: down.into_boxed_slice(),
            })
        }

        let mut reverse_map: LabelledArray<
            Option<Label<triangulation::Triangle>>,
            Label<Triangle>,
        > = LabelledArray::fill_with(|| None, triangulation.size());
        for (from, to) in triangle_map.map.into_iter() {
            reverse_map[to] = Some(from);
        }
        let reverse_map = reverse_map.map_into(|l| l.expect("The reverse map should be complete"));

        (
            FullTriangulation {
                triangles: triangles.map_into(|item| {
                    item.expect("Some of the Triangles have not yet been identified")
                }),
                vertices: vertices.map_into(|item| {
                    item.expect("Some of the Vertices have not yet been identified")
                }),
                timeslices: timeslices
                    .into_iter()
                    .map(|(old_label, len)| Timeslice {
                        start: *vertex_map
                            .get(&old_label)
                            .expect("Vertex label could not be found in map"),
                        length: len,
                    })
                    .collect::<Vec<Timeslice>>()
                    .into_boxed_slice(),
            },
            reverse_map,
        )
    }
}

impl FullTriangulation {
    /// Return the amount of triangles in the triangulation
    ///
    /// _This should be twice the amount of vertices._
    pub fn size(&self) -> usize {
        self.triangles.len()
    }

    /// Returns the amount of vertices in the triangulation
    ///
    /// _This should be half the amount of triangles._
    pub fn vertex_count(&self) -> usize {
        self.vertices.len()
    }

    /// Return the amount of timeslices in the triangulation
    pub fn time_len(&self) -> usize {
        self.timeslices.len()
    }

    /// Returns the volume or length of the timeslice given by `t`
    pub fn timeslice_length(&self, t: usize) -> usize {
        self.timeslices[t].len()
    }

    /// Return triangle by label
    pub fn get_triangle(&self, label: Label<Triangle>) -> &Triangle {
        &self.triangles[label]
    }

    /// Return vertex by label
    pub fn get_vertex(&self, label: Label<Vertex>) -> &Vertex {
        &self.vertices[label]
    }

    /// Return vertex degree of vertex given by `label`
    pub fn vertex_degree(&self, label: Label<Vertex>) -> usize {
        self.vertices[label].degree()
    }

    /// Returns and iterator over [`Triangle`]s
    pub fn iter_triangles(&self) -> impl Iterator<Item = &Triangle> {
        self.triangles.iter()
    }

    /// Return iterator over timeslice at time `t` towards the right
    ///
    /// Towards the right here means in the positive rotation
    /// directions with respect to forward time.
    /// For the opposite direction see
    /// [`iter_time_left()`][FullTriangulation::iter_time_left()]
    pub fn iter_time(&self, t: usize) -> TimesliceIterator {
        TimesliceIterator {
            vertices: &self.vertices,
            current_vertex: None,
            original_vertex: self.timeslices[t].start,
        }
    }

    /// Return iterator over timeslice at time `t` towards the left
    ///
    /// Towards the left here means in the negative rotation
    /// directions with respect to forward time.
    pub fn iter_time_left(&self, t: usize) -> TimesliceIteratorLeft {
        TimesliceIteratorLeft {
            vertices: &self.vertices,
            current_vertex: None,
            original_vertex: self.timeslices[t].start,
        }
    }

    /// Return iterator over timeslice defined by vertex label `vertex`
    ///
    /// For notes on the direction see [`iter_time()`][FullTriangulation::iter_time()]
    pub fn iter_time_vertex(&self, vertex: Label<Vertex>) -> TimesliceIterator {
        TimesliceIterator {
            vertices: &self.vertices,
            current_vertex: None,
            original_vertex: vertex,
        }
    }

    /// Return iterator over timeslice defined by vertex label `vertex`
    ///
    /// For notes on the direction see [`iter_time()`][FullTriangulation::iter_time()]
    pub fn iter_time_vertex_left(&self, vertex: Label<Vertex>) -> TimesliceIteratorLeft {
        TimesliceIteratorLeft {
            vertices: &self.vertices,
            current_vertex: None,
            original_vertex: vertex,
        }
    }
}

// Iterator struct for use in return types
pub struct TimesliceIterator<'a> {
    vertices: &'a LabelledArray<Vertex>,
    current_vertex: Option<Label<Vertex>>,
    original_vertex: Label<Vertex>,
}

impl Iterator for TimesliceIterator<'_> {
    type Item = Label<Vertex>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(label) = self.current_vertex {
            let new_label = self.vertices[label].right;
            if new_label == self.original_vertex {
                return None;
            }
            self.current_vertex = Some(new_label)
        } else {
            self.current_vertex = Some(self.original_vertex)
        }
        self.current_vertex
    }
}

pub struct TimesliceIteratorLeft<'a> {
    vertices: &'a LabelledArray<Vertex>,
    current_vertex: Option<Label<Vertex>>,
    original_vertex: Label<Vertex>,
}

impl Iterator for TimesliceIteratorLeft<'_> {
    type Item = Label<Vertex>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(label) = self.current_vertex {
            let new_label = self.vertices[label].left;
            if new_label == self.original_vertex {
                return None;
            }
            self.current_vertex = Some(new_label)
        } else {
            self.current_vertex = Some(self.original_vertex)
        }
        self.current_vertex
    }
}

// ==========================================================================
// Helper structures and functions for construction of the full triangulation

/// Struct for mapping arbitrary labelling to `Label<T>`s
///
/// Supposed to be used by adding labels from the original
/// arbitrary labelling using [`add()`](LabelMap::add()),
/// such that `LabelMap` can assign new condensed unique
/// `Label<T>`ing.
#[derive(Debug, Clone)]
struct LabelMap<L, T> {
    // OPTIMIZABLE: BTreeMap may not be the best option for LabelMap
    map: BTreeMap<L, Label<T>>,
    current_label: Label<T>,
}

impl<L, T> LabelMap<L, T> {
    fn new() -> Self {
        LabelMap {
            map: BTreeMap::new(),
            current_label: Label::initial(),
        }
    }
}

impl<L: Ord, T> LabelMap<L, T> {
    /// Add element to map and set and return a new unique, dense label for it
    /// Note when adding an element which is already in the map, nothing is done
    fn add(&mut self, old_label: L) -> Label<T> {
        match self.map.entry(old_label) {
            Entry::Vacant(item) => {
                let new_label = *item.insert(self.current_label);
                self.current_label.advance();
                new_label
            }
            Entry::Occupied(item) => *item.get(),
        }
    }

    /// Return whether the `LabelMap` already contains a mapping
    /// for `old_label` or not.
    fn contains(&self, old_label: L) -> bool {
        self.map.contains_key(&old_label)
    }

    /// Return the length, i.e. amount of elements in the map
    fn len(&self) -> usize {
        self.map.len()
    }

    fn get(&self, key: &L) -> Option<&Label<T>> {
        self.map.get(key)
    }

    // fn get_mut(&mut self, key: &L) -> Option<&mut Label<T>> {
    //     self.map.get_mut(key)
    // }

    // fn iter(&self) -> hash_map::Iter<'_, L, Label<T>> {
    //     self.map.iter()
    // }

    fn iter_mut(&mut self) -> btree_map::IterMut<'_, L, Label<T>> {
        self.map.iter_mut()
    }
}

/// Struct holding the temporary double labels (t, x)
/// of the vertices of a triangle.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct TriangleVertices {
    time: usize,
    left: (usize, usize),
    center: (usize, usize),
    right: (usize, usize),
}

/// Struct purely for returning information of `find_vertices_timeslices_mapping()`
struct VerticesTimeslicesMapping {
    triangle_map: LabelMap<triangulation::Label, Triangle>,
    vertex_map: LabelMap<(usize, usize), Vertex>,
    triangles_vertices: Vec<Option<TriangleVertices>>,
    shift: usize, // Represents
    timeslices: Vec<((usize, usize), usize)>,
}

/// Walk through the triangulation, identifing all vertices, labelling the vertices
/// and triangles, constructing the timeslices.
fn find_vertices_timeslices_mapping<'a>(
    triangulation: &'a impl Triangulation<'a>,
    origin: super::Label,
) -> VerticesTimeslicesMapping {
    // Construct Maps to keep track of the labels
    let mut triangle_map: LabelMap<triangulation::Label, Triangle> = LabelMap::new();
    let mut vertex_map: LabelMap<(usize, usize), Vertex> = LabelMap::new();
    // Keep all vertices linked to triangles in a list
    let mut triangles_vertices: Vec<Option<TriangleVertices>> = iter::repeat_with(|| None)
        .take(triangulation.max_size())
        .collect();
    // Represents the different between the coordinates of the stitching timeslice
    let mut shift: Option<usize> = None;
    let mut timeslices: Vec<((usize, usize), usize)> = Vec::new();

    // Walk through the triangulation from the origin
    let mut slice_origin = origin;
    let mut lower_start = 0;
    // Use t as t-coordinate and set max to avoid infinite looping
    'time: for t in 0..(triangulation.max_size() / 2) {
        let mut marker = slice_origin;
        let mut upper: usize = 0;
        let mut lower: usize = lower_start;

        let mut vertex_count = 0; // Counts amount of vertices in lower timeslice
        let mut start_label: Option<(usize, usize)> = None; // Marks first vertex in the lower timeslice
        let mut next_slice: Option<triangulation::Label> = None;
        let mut next_start: Option<usize> = None;
        // Loop over all triangles in a thick timeslice (with upper bound to avoid infinite looping)
        'space: for _ in 0..(triangulation.max_size() / 2) {
            let current_triangle = triangulation.get(marker);
            // Add triangle label to map
            triangle_map.add(marker);
            // Find vertices around marked triangle
            triangles_vertices[usize::from(marker)] = match current_triangle.orientation {
                Orientation::Up => {
                    // Determine surrounding vertex coordinates
                    let left = (t, lower);
                    let center = (t + 1, upper);
                    let right = (t, lower + 1);

                    // Add vertices ensuring existing vertices do not get added doubly
                    vertex_map.add(left);
                    vertex_map.add(center);
                    vertex_map.add(right);

                    // Find the first up-triangle to determine the starting vertex
                    if start_label.is_none() {
                        start_label = Some(left);
                    }
                    // Add to vertex count of lower timeslice
                    vertex_count += 1;
                    // Update labelling
                    lower += 1;

                    Some(TriangleVertices {
                        time: t,
                        left,
                        center,
                        right,
                    })
                }
                Orientation::Down => {
                    // Determine surrounding vertex coordinates
                    let left = (t + 1, upper);
                    let center = (t, lower);
                    let right = (t + 1, upper + 1);

                    // Add vertices ensuring existing vertices do not get added doubly
                    vertex_map.add(left);
                    vertex_map.add(center);
                    vertex_map.add(right);

                    // Find triangle to go up to the next slice
                    if next_slice.is_none() {
                        next_slice = Some(current_triangle.center);
                        next_start = Some(upper);
                    }
                    // Update labelling
                    upper += 1;

                    Some(TriangleVertices {
                        time: t,
                        left,
                        center,
                        right,
                    })
                }
            };

            marker = current_triangle.right;
            // Check if gone around fully, if so break the loop
            if marker == slice_origin {
                break 'space;
            }
        }

        // Add timeslice information
        timeslices.push((
            start_label
                .unwrap_or_else(|| panic!("No up-triangle was in the timeslice marked {}", t)),
            vertex_count,
        ));
        // Setup new starting position
        slice_origin = next_slice.unwrap_or_else(|| {
            panic!(
                "Could not find a down-triangle in the timeslice marked {}",
                t
            )
        });
        lower_start = next_start.unwrap_or_else(|| {
            panic!(
                "Could not find a down-triangle in the timeslice marked {}",
                t
            )
        });
        // Check if next slice is the original slice, i.e. current slice is last slice
        let upper_label = slice_origin;
        if triangle_map.contains(upper_label) {
            // The vertex coordinates of the upper vertices of the last timeslice
            // have an incorrect shift, which does not match with the original lower
            // vertices of the first timeslice.
            let lower_label = triangulation.get(upper_label).center;
            let lower_x = triangles_vertices[usize::from(lower_label)]
                .as_ref()
                .expect("Triangle from last timeslice is not in `triangle_vertices`")
                .left
                .1;
            let upper_x = triangles_vertices[usize::from(upper_label)]
                .as_ref()
                .expect("Triangle from first timeslice is not in `triangle_vertices`")
                .left
                .1;
            shift = Some(upper_x - lower_x);
            // Stop walk through triangulation
            break 'time;
        }
    }

    VerticesTimeslicesMapping {
        triangle_map,
        vertex_map,
        triangles_vertices,
        shift: shift.expect("Somehow the shift could not be calculated!"),
        timeslices,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::observables::volume_profile;
    use crate::triangulation::FixedTriangulation;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;
    use triangulation::DynTriangulation;

    impl FullTriangulation {
        fn assert_triangulation(&self) {
            self.assert_timeslices();
            self.assert_triangle_connectivity();
            self.assert_vertex_connectivity();
        }

        fn assert_triangle_connectivity(&self) {
            let triangles = &self.triangles;
            for label in triangles.labels() {
                let triangle = &triangles[label];
                // Check for no mapping to oneself
                assert_ne!(label, triangle.center);
                assert_ne!(label, triangle.right);
                assert_ne!(label, triangle.left);

                // Check triangles themselves
                let nbr_center = &triangles[triangle.center];
                let nbr_left = &triangles[triangle.left];
                let nbr_right = &triangles[triangle.right];
                assert_eq!(
                    nbr_left.right, label,
                    "Triangle to left of {} is not connected back",
                    label
                );
                assert_eq!(
                    nbr_right.left, label,
                    "Triangle to right of {} is not connected back",
                    label
                );
                assert_eq!(
                    nbr_center.center, label,
                    "Triangle to center of {} is not connected back",
                    label
                );
                assert_ne!(
                    triangle.orientation, nbr_center.orientation,
                    "Triangle {} is not opposite direction from timelike connected triangle",
                    label
                );
                // Check vertices associated with the triangles
                assert_eq!(
                    nbr_center.right_vertex, triangle.right_vertex,
                    "Timelike vertices of triangle {} do not match those of timelike neighbour {} ({0}: {:?}, {1}: {:?})",
                    label, triangle.center, triangle, nbr_center
                );
                assert_eq!(
                    nbr_center.left_vertex, triangle.left_vertex,
                    "Timelike vertices of triangle {} do not match those of timelike neighbour {} ({0}: {:?}, {1}: {:?})",
                    label, triangle.center, nbr_center, triangle
                );
                if nbr_left.orientation == triangle.orientation {
                    assert_eq!(nbr_left.right_vertex, triangle.left_vertex);
                    assert_eq!(nbr_left.center_vertex, triangle.center_vertex);
                } else {
                    assert_eq!(nbr_left.right_vertex, triangle.center_vertex);
                    assert_eq!(nbr_left.center_vertex, triangle.left_vertex);
                }
                if nbr_right.orientation == triangle.orientation {
                    assert_eq!(nbr_right.left_vertex, triangle.right_vertex);
                    assert_eq!(nbr_right.center_vertex, triangle.center_vertex);
                } else {
                    assert_eq!(nbr_right.left_vertex, triangle.center_vertex);
                    assert_eq!(nbr_right.center_vertex, triangle.right_vertex);
                }
                // Check if time coordinates match up
                assert_eq!(triangle.time, nbr_left.time);
                assert_eq!(triangle.time, nbr_right.time);
                match triangle.orientation {
                    Orientation::Down => {
                        assert_eq!((triangle.time + 1) % self.time_len(), nbr_center.time);
                        assert_eq!(triangle.time, self.vertices[triangle.center_vertex].time);
                        assert_eq!(
                            (triangle.time + 1) % self.time_len(),
                            self.vertices[triangle.left_vertex].time
                        );
                        assert_eq!(
                            (triangle.time + 1) % self.time_len(),
                            self.vertices[triangle.right_vertex].time
                        );
                    }
                    Orientation::Up => {
                        assert_eq!((nbr_center.time + 1) % self.time_len(), triangle.time);
                        assert_eq!(triangle.time, self.vertices[triangle.left_vertex].time);
                        assert_eq!(triangle.time, self.vertices[triangle.right_vertex].time);
                        assert_eq!(
                            (triangle.time + 1) % self.time_len(),
                            self.vertices[triangle.center_vertex].time
                        );
                    }
                }
            }
        }

        fn assert_vertex_connectivity(&self) {
            let vertices = &self.vertices;
            for label in vertices.labels() {
                let vertex = &vertices[label];
                // Check connection to associated triangle
                let associated_triangle = &self.triangles[vertex.triangle];
                assert!(matches!(associated_triangle.orientation, Orientation::Up));
                assert_eq!(associated_triangle.left_vertex, label);
                // Check biconnectivity of the triangles to oneother and the times
                let nbr_left = &vertices[vertex.left];
                let nbr_right = &vertices[vertex.right];
                assert_eq!(nbr_left.right, label);
                assert_eq!(nbr_right.left, label);
                assert_eq!(nbr_left.time, vertex.time);
                assert_eq!(nbr_right.time, vertex.time);
                for &nbr_label in vertex.up.iter() {
                    let nbr_up = &vertices[nbr_label];
                    assert!(nbr_up.down.contains(&label));
                    assert_eq!(nbr_up.time, (vertex.time + 1) % self.time_len());
                }
                for &nbr_label in vertex.down.iter() {
                    let nbr_down = &vertices[nbr_label];
                    assert!(
                        nbr_down.up.contains(&label),
                        "Label {} is not contained in {:?}",
                        label,
                        nbr_down.up
                    );
                    assert_eq!(vertex.time, (nbr_down.time + 1) % self.time_len());
                }
            }
        }

        fn assert_timeslices(&self) {
            for (t, timeslice) in self.timeslices.iter().enumerate() {
                assert_eq!(
                    self.vertices[timeslice.start].time, t,
                    "Timeslice start is not the same as time marked at the vertex"
                )
            }
        }
    }

    #[test]
    fn create_full_triangulation() {
        let mut triangulation = DynTriangulation::new(8, 11);
        let mut rng = Xoshiro256StarStar::seed_from_u64(12);

        for _ in 0..100 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..28 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..18 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        for _ in 0..300 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        println!("=============================================================");
        println!("Original triangulation size: {}", triangulation.size());

        let full_triangulation = FullTriangulation::from_triangulation(&triangulation);
        println!(
            "New triangulation triangle count: {}, vertex count: {}, timeslice count: {}",
            full_triangulation.size(),
            full_triangulation.vertex_count(),
            full_triangulation.time_len()
        );
        println!(
            "New length profile: {:?}",
            volume_profile::measure(&full_triangulation)
        );
        let vertex = full_triangulation.get_triangle(Label::from(16)).left_vertex;
        full_triangulation
            .iter_time_vertex(vertex)
            .for_each(|label| print!("{} ", label));
        println!();
        println!("=============================================================");
        full_triangulation.assert_triangulation()
    }

    // // Check if two list are equal where their relative shift may vary
    // fn equate_periodic_lists<T: Eq>(one: &[T], two: &[T]) -> bool {
    //     let length = one.len();
    //     if length != two.len() {
    //         return false;
    //     }
    //     'shift: for shift in 0..length {
    //         for i in 0..length {
    //             if one[i] != two[(i + shift) % length] {
    //                 continue 'shift;
    //             }
    //         }
    //         return true;
    //     }
    //     false
    // }

    #[test]
    // Check if the sample FullTriangulation is generated with the same seed
    fn check_randomness() {
        let mut rng0 = Xoshiro256StarStar::seed_from_u64(137);
        let mut rng1 = rng0.clone();

        let mut triangulation0 = FixedTriangulation::new(20, 15);
        for _ in 0..10_000 {
            triangulation0.flip(triangulation0.sample_flip(&mut rng0));
        }
        for _ in 0..1_000 {
            triangulation0.flip(triangulation0.sample_flip(&mut rng0));
            triangulation0.relocate(
                triangulation0.sample_order_four(&mut rng0),
                triangulation0.sample(&mut rng0),
            )
        }

        let mut triangulation1 = FixedTriangulation::new(20, 15);
        for _ in 0..10_000 {
            triangulation1.flip(triangulation1.sample_flip(&mut rng1));
        }
        for _ in 0..1_000 {
            triangulation1.flip(triangulation1.sample_flip(&mut rng1));
            triangulation1.relocate(
                triangulation1.sample_order_four(&mut rng1),
                triangulation1.sample(&mut rng1),
            )
        }

        for label in triangulation0.labels() {
            assert_eq!(triangulation0.get(label), triangulation1.get(label))
        }

        let full0 = FullTriangulation::from_triangulation(&triangulation0);
        let full1 = FullTriangulation::from_triangulation(&triangulation1);
        for triangle in full0.triangles.labels() {
            assert_eq!(full0.get_triangle(triangle), full1.get_triangle(triangle))
        }
    }

    #[test]
    fn timeshift() {
        let mut triangulation = DynTriangulation::new(25, 30);
        let mut rng = Xoshiro256StarStar::seed_from_u64(12);

        for _ in 0..1000 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }
        for _ in 0..280 {
            triangulation.add(triangulation.sample(&mut rng));
        }
        for _ in 0..180 {
            triangulation.remove(triangulation.sample_order_four(&mut rng))
        }
        for _ in 0..300 {
            triangulation.flip(triangulation.sample_flip(&mut rng));
        }

        println!("=============================================================");
        println!("Original triangulation size: {}", triangulation.size());

        let mut full_triangulation = FullTriangulation::from_triangulation(&triangulation);
        full_triangulation.assert_triangulation();

        full_triangulation.time_shift(10);
        full_triangulation.assert_triangulation();
    }
}
