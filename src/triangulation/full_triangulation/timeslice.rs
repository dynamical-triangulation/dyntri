use super::vertex::Vertex;
use crate::collections::Label;

/// Struct holding a `Label` to a 'first' vertex in the timeslice
/// and the length of the timeslice.
pub struct Timeslice {
    pub start: Label<Vertex>,
    pub(super) length: usize,
}

impl Timeslice {
    /// Return the length of the timeslice
    pub fn len(&self) -> usize {
        self.length
    }

    pub fn is_empty(&self) -> bool {
        self.length == 0
    }
}
