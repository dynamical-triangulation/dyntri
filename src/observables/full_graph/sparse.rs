//! Module with methods and types for working with Sparse matrices,
//! specifically sparse boolean matrices (these are actually equivalent to graphs)

use hdf5::{Dataset, Group, H5Type};
use ndarray::prelude::*;
use rand::Rng;

use super::{
    chunking::{chunk_iter, Chunk},
    distance_matrix::{MASK_INDICES_DATASET_NAME, MASK_SPAN_DATASET_NAME},
};

/// A simple boolean matrix stored as a sparse matrix in compressed sparse (row) format
pub type BoolMatrix = BoolMatrixCSBase;

/// Boolean matrix base given in [CSR format](https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_(CSR,_CRS_or_Yale_format))
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BoolMatrixCSBase {
    /// The span of the CSR format corresponding to row indices
    pub span: Vec<usize>,
    /// The indices of the CSR format corresponding to column indices
    pub indices: Vec<usize>,
}

impl<'a> From<&'a BoolMatrix> for BoolMatrixCSR<'a> {
    fn from(matrix: &'a BoolMatrix) -> Self {
        matrix.view()
    }
}

/// A reference view of a sparse matrix in the CSR format, see:
/// [https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_(CSR,_CRS_or_Yale_format)]()
#[derive(Debug, Clone, Copy)]
pub struct BoolMatrixCSR<'a> {
    /// The span of the CSR format
    pub row_index: &'a [usize],
    /// The indices of the CSR format
    pub col_index: &'a [usize],
}

/// A reference view of a sparse matrix in the CSC format, see:
/// [https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_column_(CSC_or_CCS)]()
#[derive(Debug, Clone, Copy)]
pub struct BoolMatrixCSC<'a> {
    /// The indices of the CSC format
    pub row_index: &'a [usize],
    /// The span of the CSC format
    pub col_index: &'a [usize],
}

impl BoolMatrix {
    /// Returns a [`BoolMatrixCSR`] view of `self`, which is a sort of reference
    /// to the owned self matrix [`BoolMatrix`]
    pub fn view(&self) -> BoolMatrixCSR {
        BoolMatrixCSR {
            row_index: &self.span,
            col_index: &self.indices,
        }
    }

    /// Returns a tranposed [`BoolMatrixCSC`] view of `self`,
    /// which is a sort of reference to the owned self matrix [`BoolMatrix`]
    pub fn transpose(&self) -> BoolMatrixCSC {
        BoolMatrixCSC {
            row_index: &self.indices,
            col_index: &self.span,
        }
    }

    /// Return a dense [bool] matrix [`Array2`] from the sparse [`BoolMatrix`]
    pub fn to_dense(&self) -> Array2<bool> {
        self.view().to_dense()
    }
}

impl BoolMatrixCSR<'_> {
    /// Returns the element at (`row`, `column`) by binary search.
    pub fn get(&self, row: usize, column: usize) -> bool {
        binary_search(self.column_indices(row), &column).is_some()
    }

    fn column_indices(&self, row: usize) -> &[usize] {
        let start = self.row_index[row];
        let end = self.row_index[row + 1];
        &self.col_index[start..end]
    }

    /// Returns the sum of each row in the matrix,
    /// meaning the amount of `true` elements in each row.
    pub fn row_sum(&self) -> Array1<usize> {
        Array1::from_iter(
            self.row_index
                .iter()
                .skip(1)
                .zip(self.row_index.iter())
                .map(|(next, prev)| next - prev),
        )
    }

    /// Returns the sum of the full matrix, meaning the amout of `true` elements in total.
    pub fn sum(&self) -> usize {
        self.col_index.len()
    }

    /// Returns a view to the tranpose of `self` as [BoolMatrixCSC]
    pub fn transpose(&self) -> BoolMatrixCSC<'_> {
        BoolMatrixCSC {
            row_index: self.col_index,
            col_index: self.row_index,
        }
    }

    /// See [BoolMatrixCSR::transpose()]
    pub fn t(&self) -> BoolMatrixCSC<'_> {
        self.transpose()
    }

    /// Returns the number of rows of the [`BoolMatrixCSR`]
    pub fn nrows(&self) -> usize {
        self.row_index.len() - 1
    }

    /// Return a dense [bool] matrix [`Array2`] from the sparse [`BoolMatrixCSR`]
    pub fn to_dense(&self) -> Array2<bool> {
        let n = self.nrows();
        let mut dense = Array2::from_elem([n, n], false);

        for row in 0..n {
            for &column in self.column_indices(row) {
                dense[(column, row)] = true;
            }
        }

        dense
    }
}

impl BoolMatrixCSC<'_> {
    /// Returns the element at (`row`, `column`) by binary search.
    pub fn get(&self, row: usize, column: usize) -> bool {
        binary_search(self.row_indices(column), &row).is_some()
    }

    fn row_indices(&self, column: usize) -> &[usize] {
        let start = self.col_index[column];
        let end = self.col_index[column + 1];
        &self.row_index[start..end]
    }

    /// Returns a view to the tranpose of `self` as [BoolMatrixCSR]
    pub fn transpose(&self) -> BoolMatrixCSR<'_> {
        BoolMatrixCSR {
            row_index: self.col_index,
            col_index: self.row_index,
        }
    }

    /// See [BoolMatrixCSC::transpose()]
    pub fn t(&self) -> BoolMatrixCSR<'_> {
        self.transpose()
    }
}

/// Returns the index of the given `element` in the list or None if not present
///
/// Note: This search uses binary search and only works on a SORTED list! And if repeats are
/// present this search will return the index of an arbitrary element
fn binary_search<T: Ord>(list: &[T], element: &T) -> Option<usize> {
    let mut start = 0;
    let mut end = list.len();
    while start < end {
        let index = start + (end - start) / 2;
        let value = &list[index];
        match element.cmp(value) {
            std::cmp::Ordering::Less => {
                end = index;
            }
            std::cmp::Ordering::Equal => {
                return Some(index);
            }
            std::cmp::Ordering::Greater => {
                start = index + 1;
            }
        }
    }

    None
}

impl BoolMatrixCSBase {
    /// Construct a [BoolMatrix] as a mask or filter of `matrix`, where each element in the
    /// [BoolMatrix] is the result of `predicate` on each element in `matrix`.
    pub fn from_dense_filter<T, F>(matrix: ArrayView2<T>, predicate: F) -> Self
    where
        T: Eq,
        F: Fn(&T) -> bool,
    {
        let (nrow, ncol) = matrix.dim();
        let mut row_index = Vec::with_capacity(nrow + 1);
        // TODO: Capacity should be the amount of non-zero elements, which is a priori unkown,
        // take the amount of columns as this is the order of magnitude for which a matrix
        // is usually considered sparse.
        let mut col_index = Vec::with_capacity(ncol);
        row_index.push(0);

        for row in matrix.rows() {
            col_index.extend(
                row.iter()
                    .enumerate()
                    .filter(|&(_, value)| predicate(value))
                    .map(|(i, _)| i),
            );
            row_index.push(col_index.len());
        }

        Self {
            span: row_index,
            indices: col_index,
        }
    }

    /// Construct a [BoolMatrix] as a mask or filter of `matrix`, where each element in the
    /// [BoolMatrix] is the result of `predicate` on each element in `matrix`.
    /// `matrix` is here a [Dataset] and is read in batches, such that the memory usage stays low.
    pub fn from_dense_filter_disk<T, F>(matrix: &Dataset, predicate: F) -> Self
    where
        T: H5Type,
        F: Fn(&T) -> bool,
    {
        let shape = matrix.shape();
        let nrow = shape[0];
        let ncol = shape[1];

        let mut row_index = Vec::with_capacity(nrow + 1);
        // Capacity should be the amount of non-zero elements, which is a priori unkown,
        // take the amount of columns as this is the order of magnitude for which a matrix
        // is usually considered sparse.
        let mut col_index = Vec::with_capacity(ncol);
        row_index.push(0);

        for chunk in chunk_iter::<T>(matrix) {
            for row in chunk.array.rows() {
                col_index.extend(
                    row.iter()
                        .enumerate()
                        .filter(|&(_, value)| predicate(value))
                        .map(|(i, _)| i),
                );
                row_index.push(col_index.len());
            }
        }

        Self {
            span: row_index,
            indices: col_index,
        }
    }

    /// Creates a new [`BoolMatrix`] from a given `span` and `indices`
    pub fn new(span: Vec<usize>, indices: Vec<usize>) -> Self {
        Self { span, indices }
    }

    /// Creates an empty [`BoolMatrix`] initialized with a capacity that should approximatly
    /// be enough to store the all information of the [`BoolMatrix`] when filled.
    pub fn empty(shape: (usize, usize)) -> Self {
        Self {
            span: Vec::with_capacity(shape.0 + 1), // This capacity should be exact
            indices: Vec::with_capacity(shape.1),  // This cap is just an estimated lower bound
        }
    }

    /// Construct a [BoolMatrix] from a HDF5 Group representing a mask
    ///
    /// Note: this assumes the group has a _span_ and _indices_ dataset.
    pub fn from_disk(mask_group: &Group) -> hdf5::Result<Self> {
        let span = mask_group.dataset(MASK_SPAN_DATASET_NAME)?.read_1d()?;
        let indices = mask_group.dataset(MASK_INDICES_DATASET_NAME)?.read_1d()?;
        Ok(Self {
            span: span.into_raw_vec(),
            indices: indices.into_raw_vec(),
        })
    }
}

/// Compute the index element of the matrix product `dense`.`mask`
///
/// e.g `dot(matrix.view(), mask, ndarray::Dim([4, 5]))`.
/// It has an input type `T` and output type `U`, such that it is possible to use a small type
/// for `T` like `u8` and a larger type for the matrix product result `U` like `u32`, such that
/// there is no overflow in the additions and `dense` can remain small.
pub fn dot<T, U>(dense: ArrayView2<T>, mask: BoolMatrixCSC, index: Ix2) -> U
where
    T: Copy,
    U: num_traits::Zero + From<T>,
{
    mask.row_indices(index[1])
        .iter()
        .map(|&k| dense[(index[0], k)])
        .fold(U::zero(), |acc, value| acc + U::from(value))
}

/// Returns the index element of the `left_mask`.`dense`.`right_mask` double matrix product
///
/// `T` is the input type and `U` the intermediate (`dense`.`right_mask`) sum and output type.
/// For explanation on use see: [dot()].
pub fn mask_product_elem<T, U>(
    left_mask: BoolMatrixCSR,
    dense: ArrayView2<T>,
    right_mask: BoolMatrixCSC,
    index: Ix2,
) -> U
where
    T: Copy,
    U: num_traits::Zero + From<T>,
{
    left_mask
        .column_indices(index[0])
        .iter()
        .map(|&k| dot::<T, U>(dense, right_mask, Dim([k, index[1]])))
        .fold(U::zero(), |acc, value| acc + value)
}

/// Returns the index element of the `left_mask`.`chunk`.`right_mask` double matrix product.
///
/// Note that this is not a full matrix product! It only represent the part of the matrix product
/// that comes from the `chunk`, so to get the full matrix product one has to take the sum of the
/// results from all `chunk` products. See [mask_product_disk_elem()] or [mask_product_disk()]
/// for the full sum.
///
/// `T` is the input type and `U` the intermediate (`dense`.`right_mask`) sum and output type.
/// For explanation on use see: [dot()].
pub fn mask_product_chunk<T, U>(
    left_mask: BoolMatrixCSR,
    chunk: &Chunk<T>,
    right_mask: BoolMatrixCSC,
    index: Ix2,
) -> U
where
    T: Copy,
    U: num_traits::Zero + From<T>,
{
    left_mask
        .column_indices(index[0])
        .iter()
        .filter(|&k| chunk.range.contains(k))
        .map(|&k| {
            let k = k - chunk.range.start;
            dot::<T, U>(chunk.view(), right_mask, Dim([k, index[1]]))
        })
        .fold(U::zero(), |acc, value| acc + value)
}

/// Returns the index element of the `left_mask`.`dense`.`right_mask` double matrix product.
///
/// If you need to compute the product for multiple indices, consider using [mask_product_disk()]
/// instead which reuses the loaded chunks from disk, which makes it a lot faster.
/// `T` is the input type and `U` the intermediate (`dense`.`right_mask`) sum and output type.
/// For explanation on use see: [dot()].
pub fn mask_product_disk_elem<T, U>(
    left_mask: BoolMatrixCSR,
    dense: &Dataset,
    right_mask: BoolMatrixCSC,
    index: Ix2,
) -> U
where
    T: Copy + hdf5::H5Type,
    U: num_traits::Zero + From<T> + std::iter::Sum,
{
    chunk_iter(dense)
        .map(|chunk| mask_product_chunk::<T, U>(left_mask, &chunk, right_mask, index))
        .sum()
}

/// Returns the elements of the `left_mask`.`dense`.`right_mask` double matrix
/// product given by `indices`.
/// This function is implmented such that each `chunk` in disk has to be loaded in only once
/// for the full list of `indices`, making it much more efficient that [mask_product_disk_elem()]
/// for multiple indices.
///
/// `T` is the input type and `U` the intermediate (`dense`.`right_mask`) sum and output type.
/// For explanation on use see: [dot()].
pub fn mask_product_disk<T, U>(
    left_mask: BoolMatrixCSR,
    dense: &Dataset,
    right_mask: BoolMatrixCSC,
    indices: ArrayView1<Ix2>,
) -> Array1<U>
where
    T: Copy + hdf5::H5Type,
    U: Copy + std::ops::AddAssign + num_traits::Zero + From<T>,
{
    let mut output = Array1::zeros(indices.dim());
    for chunk in chunk_iter::<T>(dense) {
        for (i, &index) in indices.iter().enumerate() {
            output[i] += mask_product_chunk::<T, U>(left_mask, &chunk, right_mask, index);
        }
    }
    output
}

impl BoolMatrixCSR<'_> {
    /// Returns the result of `self`.`vec` sparse matrix-vector product.
    pub fn vector_product<T, U>(&self, vec: ArrayView1<T>) -> Array1<U>
    where
        T: Copy,
        U: num_traits::Zero + From<T>,
    {
        (0..self.nrows())
            .map(|irow| {
                self.column_indices(irow)
                    .iter()
                    .map(|&icol| vec[icol])
                    .fold(U::zero(), |acc, val| acc + val.into())
            })
            .collect()
    }

    /// Returns the summed result of the binary operation between all masked pairs `left_vec`
    /// and `right_vec`, masked by [BoolMatrix] `self`.
    ///
    /// This is equivalent to a vector-sparse matrix-vector product for operation `left`*`right`.
    pub fn masked_vector_operation<T, U, V, F>(
        &self,
        left_vec: ArrayView1<T>,
        right_vec: ArrayView1<U>,
        operation: F,
    ) -> V
    where
        F: Fn(T, U) -> V + Copy,
        T: Copy,
        U: Copy,
        V: num_traits::Num,
    {
        (0..self.nrows())
            .flat_map(|irow| {
                self.column_indices(irow)
                    .iter()
                    .map(move |&icol| operation(left_vec[irow], right_vec[icol]))
            })
            .fold(V::zero(), |acc, val| acc + val)
    }

    /// Returns the mean result of the binary operation between all masked pairs `left_vec`
    /// and `right_vec`, masked by [BoolMatrix] `self`.
    ///
    /// This is equivalent to a vector-sparse matrix-vector product divided by the element sum of the
    /// sparse matrix for operation `left`*`right`.
    pub fn masked_vector_operation_mean<T, U, F>(
        &self,
        left_vec: ArrayView1<T>,
        right_vec: ArrayView1<U>,
        operation: &F,
    ) -> f64
    where
        F: Fn(T, U) -> f64,
        T: Copy,
        U: Copy,
    {
        (0..self.nrows())
            .flat_map(|irow| {
                self.column_indices(irow)
                    .iter()
                    .map(move |&icol| operation(left_vec[irow], right_vec[icol]))
            })
            .zip(1..)
            .fold(0.0, |acc, (val, k)| acc + (val - acc) / k as f64)
    }
}

impl BoolMatrix {
    /// Sample a `true` entry of the boolean matrix uniformly over all entries
    pub fn sample_uniform<R: Rng + ?Sized>(&self, rng: &mut R) -> Ix2 {
        let list_index = rng.gen_range(0..self.indices.len());
        let span_index = binary_search_lower(&self.span, &list_index);

        Dim([span_index, self.indices[list_index]])
    }

    /// Sample a `true` entry of the boolean matrix by first sampling from the `span`
    /// and then sampling from the `indices`. If this is a CSR format this means sampling
    /// unformly from the rows first, and then within the row sampling uniformly from
    /// the `true` entries.
    /// Note: if a row is sampled that has no `true` entries another sample is taken.
    pub fn sample_from_origin<R: Rng + ?Sized>(&self, rng: &mut R) -> Ix2 {
        let span_index = rng.gen_range(0..self.span.len() - 1);
        let istart = self.span[span_index];
        let iend = self.span[span_index + 1];
        let range = istart..iend;
        if range.is_empty() {
            return self.sample_from_origin(rng);
        }
        let list_index = rng.gen_range(range);

        Dim([span_index, self.indices[list_index]])
    }
}

/// Returns the index of the last list entry that is smaller than element
///
/// Note that the list MUST be ordered for this to work, and that the given element
/// MUST be larger or equal to the smallest and thus first element of the list, and
/// strictly smaller than the largest and thus last element of the list.
fn binary_search_lower<T: Ord>(list: &[T], element: &T) -> usize {
    let mut start = 0;
    let mut end = list.len() - 1;
    while end > start {
        let index = start + (end - start) / 2;
        let value = &list[index];
        if element < value {
            end = index;
        } else {
            start = index + 1;
        }
    }

    start - 1
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256PlusPlus;

    use super::*;
    use std::fmt::Display;

    const SORTED_LIST: [usize; 10] = [1, 4, 6, 7, 8, 10, 13, 14, 16, 26];
    const DISTANCE_MATRIX: [[usize; 5]; 5] = [
        [0, 2, 3, 1, 3],
        [2, 0, 5, 2, 4],
        [3, 5, 0, 3, 4],
        [1, 2, 3, 0, 2],
        [3, 4, 4, 2, 0],
    ];

    impl Display for BoolMatrixCSR<'_> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            fn mask_value(list: &[usize], value: usize) -> i8 {
                if binary_search(list, &value).is_some() {
                    1
                } else {
                    0
                }
            }

            write!(f, "[")?;
            let nrows = self.row_index.len() - 1;
            let ncols = *self.col_index.iter().max().unwrap();
            for row in 0..nrows {
                let start = self.row_index[row];
                let end = self.row_index[row + 1];
                let col_indices = &self.col_index[start..end];
                write!(f, "[{}", mask_value(col_indices, 0))?;
                for col in 1..ncols {
                    write!(f, ", {}", mask_value(col_indices, col))?;
                }
                if row == nrows - 1 {
                    write!(f, "]")?;
                    continue;
                } else {
                    writeln!(f, "],")?;
                    write!(f, " ")?;
                }
            }
            write!(f, "]")
        }
    }

    #[test]
    fn test_binary_search_low() {
        assert_eq!(binary_search_lower(&SORTED_LIST, &9), 4);
        assert_eq!(binary_search_lower(&SORTED_LIST, &1), 0);
        assert_eq!(binary_search_lower(&SORTED_LIST, &25), 8);
        assert_eq!(binary_search_lower(&SORTED_LIST, &16), 8);
    }

    #[test]
    fn test_binary_search() {
        assert_eq!(binary_search(&SORTED_LIST, &5), None);
        assert_eq!(binary_search(&SORTED_LIST, &8), Some(4));
        assert_eq!(binary_search(&SORTED_LIST, &1), Some(0));
        assert_eq!(binary_search(&SORTED_LIST, &26), Some(9));
    }

    #[test]
    fn check_sampling_mask() {
        let dmatrix = arr2(&DISTANCE_MATRIX);
        for value in 0..10 {
            let mask = BoolMatrix::from_dense_filter(dmatrix.view(), |&v| v == value);
            if mask.indices.is_empty() {
                continue;
            }
            let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
            for _ in 0..100 {
                let sample = mask.sample_uniform(&mut rng);
                assert_eq!(dmatrix[sample], value, "Sample {sample:?} did not work");
            }
        }
    }

    #[test]
    fn mask_matrix_sparse() {
        let dmatrix = arr2(&DISTANCE_MATRIX);

        for k in 0..10 {
            let dense_mask = dmatrix.mapv(|v| v == k);
            let mask = BoolMatrix::from_dense_filter(dmatrix.view(), |&value| value == k);
            for ((i, j), &value) in dense_mask.indexed_iter() {
                assert_eq!(
                    mask.view().get(i, j),
                    value,
                    "Failed for checking element {k}"
                );
            }
        }
    }

    #[test]
    fn mask_matrix_sparse_tranpose() {
        let dmatrix = arr2(&DISTANCE_MATRIX);

        for k in 0..10 {
            let dense_mask = dmatrix.mapv(|v| v == k);
            let mask_data = BoolMatrix::from_dense_filter(dmatrix.view(), |&value| value == k);
            let mask = mask_data.transpose();
            for ((i, j), &value) in dense_mask.indexed_iter() {
                assert_eq!(mask.get(i, j), value, "Failed for checking element {k}");
            }
        }
    }

    #[test]
    fn row_sum_test() {
        let dmatrix = arr2(&DISTANCE_MATRIX);
        let mask_data = BoolMatrix::from_dense_filter(dmatrix.view(), |&value| value == 3);
        assert_eq!(mask_data.view().row_sum(), arr1(&[2, 0, 2, 1, 1]));
    }
}
