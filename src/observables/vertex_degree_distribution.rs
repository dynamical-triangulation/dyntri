//! Distribution of the vertex degree over a graph or directly of the vertices of a triangulation.

use hdf5::Group;
use ndarray::{Array1, ArrayView1};
use serde::Serialize;

use crate::{
    graph::{periodic_graph::IterPeriodicGraph, IterSizedGraph},
    triangulation::FullTriangulation,
};

use super::full_graph::{distance_matrix::MASK_GROUP_BASE_NAME, sparse::BoolMatrix};

/// Maximimally expected vertex order.
/// This is used to pre-allocate memory for the vertex orders,
/// if there turn out to be more vertex orders than this new
/// memory is allocated which may make it slightly slower.
const MAX_EXPECTED_VERTEX_DEGREE: usize = 30;

/// Struct representing the vertex order distribution
///
/// Implemented simply as a vector containing the counts of each vertex order,
/// where the first element in the array corresponds to coordination number 0,
/// the second coordination number 1, etc.
/// Thus for a CDT the vertex order distribution should have 0 counts for the first
/// four entries.
#[derive(Debug, Clone, Serialize)]
pub struct VertexDegreeDistribution(pub Vec<usize>);

/// Returns the [`VertexDegreeDistribution`] of a `graph`.
pub fn measure_graph<I, G>(graph: &G) -> VertexDegreeDistribution
where
    I: Iterator<Item = G::Label>,
    G: IterSizedGraph<I>,
{
    let mut distribution: Vec<usize> = Vec::with_capacity(MAX_EXPECTED_VERTEX_DEGREE + 1);
    for label in graph.iter() {
        let degree = graph.get_vertex_degree(label);
        if distribution.len() <= degree {
            distribution.resize(degree + 1, 0);
        }
        distribution[degree] += 1;
    }
    VertexDegreeDistribution(distribution)
}

/// Returns the [`VertexDegreeDistribution`] of a periodic `graph` by just considering
/// the distribution of the base patch of the periodic graph.
pub fn measure_periodic_graph<I, G>(graph: &G) -> VertexDegreeDistribution
where
    I: Iterator<Item = G::Label>,
    G: IterPeriodicGraph<I>,
{
    let mut distribution: Vec<usize> = Vec::with_capacity(MAX_EXPECTED_VERTEX_DEGREE + 1);
    for label in graph.iter_base() {
        let degree = graph.get_vertex_degree(label);
        if distribution.len() <= degree {
            distribution.resize(degree + 1, 0);
        }
        distribution[degree] += 1;
    }
    VertexDegreeDistribution(distribution)
}

/// Returns the [`VertexDegreeDistribution`] of the vertex graph a [`FullTriangulation`].
pub fn measure_triangulation(triangulation: &FullTriangulation) -> VertexDegreeDistribution {
    let mut distribution: Vec<usize> = Vec::with_capacity(MAX_EXPECTED_VERTEX_DEGREE + 1);
    for vertex in triangulation.vertices.iter() {
        let degree = vertex.degree();
        if distribution.len() <= degree {
            distribution.resize(degree + 1, 0);
        }
        distribution[degree] += 1;
    }
    VertexDegreeDistribution(distribution)
}

/// Determine the vertex degree field of a given [`SizedGraph`](crate::graph::SizedGraph) `graph`.
///
/// The returned [`Array1`] is simply an array with the vertex degree of every node in the graph.
/// The index of the the array corresponds to the `usize` represented by the
/// [`Label`](crate::collections::Label) of each node in the graph.
pub fn field_from_graph<I, G>(graph: &G) -> Array1<usize>
where
    I: Iterator<Item = G::Label>,
    G: IterSizedGraph<I>,
{
    Array1::from_iter(graph.iter().map(|label| graph.get_vertex_degree(label)))
}

/// Determine the vertex degree field of a given
/// [`PeriodicGraph`](crate::graph::periodic_graph::PeriodicGraph) `graph`.
///
/// The returned [`Array1`] is simply an array with the vertex degree of every node in the base
/// patch of the periodic graph.
/// The index of the the array corresponds to the `usize` represented by the
/// [`Label`](crate::collections::Label) of each ndoe in the graph.
pub fn field_from_periodic_graph<I, G>(graph: &G) -> Array1<usize>
where
    I: Iterator<Item = G::Label>,
    G: IterPeriodicGraph<I>,
{
    let degree_iter = graph
        .iter_base()
        .map(|label| graph.get_vertex_degree(label));
    Array1::from_iter(degree_iter)
}

/// Returns the vertex degree field based on [`BoolMatrix`] `mask`.
///
/// The `mask` should be the [`BoolMatrix`] mask representing the mask of distance matrix at
/// distance `1`. As the row sum of this matrix is exactly the vertex degree field.
/// The returned [`Array1`] is simply an array with the vertex degree of every node in the graph.
/// The index of the the array corresponds to the `usize` represented by the
/// [`Label`](crate::collections::Label) of each node in the graph of which the mask is generated.
pub fn field_from_mask(mask: &BoolMatrix) -> Array1<usize> {
    let vertex_degree_field = mask.view().row_sum();
    vertex_degree_field
}

/// Returns the vertex degree or coordination number at every vertex in the graph represented by
/// `group`.
///
/// Note that `group` should be a [Group] containing at least a group for the _mask_
/// at distance _1_. Representing the sparse graph of all nodes that are distance 1 from
/// one other, i.e. this represents the connectivity of the graph. The indices of the result
/// correspond to the node labels of the nodes that have the given vertex degree.
pub fn field_from_disk(group: &Group) -> hdf5::Result<Array1<usize>> {
    let distance = 1;
    let mask_group = group.group(&format!("{MASK_GROUP_BASE_NAME}{distance}"))?;
    let mask = BoolMatrix::from_disk(&mask_group)?;
    Ok(field_from_mask(&mask))
}

/// Returns the average vertex degree of the neighbours of each node of a graph from a `mask` and
/// a `vertex_degree_field`.
///
/// The `mask` must be mask of distance 1, see also [`field_from_mask`].
/// Note that if the `vertex_degree_field` is not yet computed, one can use
/// [`average_neighbour_field_from_mask`] instead.
pub fn average_neighbour_field_from_mask_with_degree_field(
    mask: &BoolMatrix,
    vertex_degree_field: ArrayView1<usize>,
) -> Array1<f64> {
    let nbr_sum = mask
        .view()
        .vector_product::<usize, usize>(vertex_degree_field.view());
    nbr_sum
        .into_iter()
        .zip(vertex_degree_field.iter())
        .map(|(sum, &degree)| sum as f64 / degree as f64)
        .collect()
}

/// Returns the average vertex degree of the neighbours of each node of a graph from a `mask`.
///
/// The `mask` must be mask of distance 1, see also [`field_from_mask`].
/// Note that if the `vertex_degree_field` is already computed, one can use
/// [`average_neighbour_field_from_mask_with_degree_field`] to save some computation.
pub fn average_neighbour_field_from_mask(mask: &BoolMatrix) -> Array1<f64> {
    let vertex_degree_field = field_from_mask(mask);
    average_neighbour_field_from_mask_with_degree_field(mask, vertex_degree_field.view())
}

/// Returns the average vertex degree or coordination number of all the neighbours of each vertex
/// in the graph represented by `group`.
///
/// Note that `group` should be a [Group] containing at least a group for the _mask_
/// at distance _1_. Representing the sparse graph of all nodes that are distance 1 from
/// one other, i.e. this represents the connectivity of the graph. The indices of the result
/// correspond to the node labels of the nodes that have the given vertex degree.
pub fn average_neighbour_field_from_disk(group: &Group) -> hdf5::Result<Array1<f64>> {
    let distance = 1;
    let mask_group = group.group(&format!("{MASK_GROUP_BASE_NAME}{distance}"))?;
    let mask = BoolMatrix::from_disk(&mask_group)?;
    Ok(average_neighbour_field_from_mask(&mask))
}
