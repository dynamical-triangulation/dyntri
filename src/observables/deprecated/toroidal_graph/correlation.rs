use std::iter;
use std::slice::Iter;

use rand_distr::{Distribution, Normal};
use serde::Serialize;

use crate::triangulation::observables::toroidal_graph::curvature::get_geodesic_sphere;
use crate::triangulation::observables::Observable;

use super::averaged_fields::Field;
use super::curvature::{
    get_two_geodesic_spheres, get_two_geodesic_spheres_contains_inner,
    overlapping_sphere_distance_from_sphere, AverageSphereDistance, GeodesicSphere, SpheresASD,
};
use super::*;

/// Struct holding the information for a two-point correlation
/// of the average spehre distance (useable for quantum Ricci curvature)
#[derive(Debug, Clone, Copy, Serialize)]
pub struct ASDCorrelation {
    /// The average sphere distance at the first point,
    /// which is symmetric so values could be interchanged
    origin_asd: f64,
    other_asd: f64,
    /// Average ratio of violations in computing ASD
    violation_ratio: f64,
    /// Whether correlation distance was too large and
    /// resulted in violation (this is defined in the
    /// relevant functions)
    correlation_violation: NodeLabelMatch,
}

/// Compute the average sphere distance correlation pair at given `distance`
pub fn asd_correlation<G, R>(
    graph: &G,
    origin: G::Label,
    distance: usize,
    delta: usize,
    rng: &mut R,
) -> ASDCorrelation
where
    G: Graph,
    R: Rng + ?Sized,
{
    // Call correlation points 0 and 1 (not to be confused with 0/1 from the two spheres for single ASD)
    let origin0 = origin;
    let (sphere0, sphere1) = get_two_geodesic_spheres(graph, origin, (delta, distance));
    // OPTIMIZABLE: This does recompute all spheres from 0 to delta/2, so this may be improved;
    // Future Jesse thinks this recomputation does not happen, but leave it here just in case
    let spheres0 = SpheresASD::from_origin_sphere(graph, origin0, sphere0, delta, rng);
    let asd0 = spheres0.average_sphere_distance();

    let origin1 = sphere1[rng.gen_range(0..sphere1.len())];
    let (spheres1, correlation_violation) =
        SpheresASD::from_origin_contains(graph, origin1, delta, origin0, rng);
    let asd1 = spheres1.average_sphere_distance();

    let ((origin_asd, other_asd), violation_ratio) = asd0.pair(asd1);
    ASDCorrelation {
        origin_asd,
        other_asd,
        violation_ratio,
        correlation_violation,
    }
}

/// Compute the average sphere distance correlation pair at given `distance`
/// where the point associated with the ASD is the center point of the two
/// sphere origins
///
/// NOTE: Computing the ASD from the center may not always be possible,
/// if the chosen point is some sort of singularity.
pub fn asd_center_correlation<G, R>(
    graph: &G,
    point: G::Label,
    distance: usize,
    delta: usize,
    rng: &mut R,
) -> ASDCorrelation
where
    G: Graph,
    R: Rng + ?Sized,
{
    let spheres0 = SpheresASD::from_center(graph, point, delta, rng);
    let asd0 = spheres0.average_sphere_distance();
    let distance_sphere = get_geodesic_sphere(graph, point, distance);
    let point1 = distance_sphere[rng.gen_range(0..distance_sphere.len())];
    let (spheres1, correlation_violation) =
        SpheresASD::from_center_contains(graph, point1, delta, point1, rng);
    let asd1 = spheres1.average_sphere_distance();

    let ((origin_asd, other_asd), violation_ratio) = asd0.pair(asd1);
    ASDCorrelation {
        origin_asd,
        other_asd,
        violation_ratio,
        correlation_violation,
    }
}

/// Computes the correlation profile of random scalar field from a given point
pub fn area_correlation_profile<G, R>(
    graph: &G,
    point: G::Label,
    max_distance: usize,
    max_delta: usize,
    rng: &mut R,
) -> CorrelationProfile<Vec<usize>>
where
    G: Graph,
    R: Rng + ?Sized,
{
    let point0 = point;
    let spheres = GeodesicSpheres::from_origin(graph, point0, max_distance);
    let origin: Vec<usize> = spheres
        .iter()
        .take(max_delta)
        .map(|sphere| sphere.len())
        .collect();

    let mut profile = Vec::with_capacity(max_distance);
    for distance in 0..max_distance {
        let point1 = spheres.sample_node(distance, rng);
        let mut areas = vec![0; max_delta];
        for node in graph.iter_breadth_first(point1) {
            let r = node.distance;
            if r >= max_delta {
                break;
            }
            areas[r] += 1;
        }
        profile.push(areas);
    }

    CorrelationProfile {
        origin,
        profile: profile.into_boxed_slice(),
    }
}

/// Computes the correlation profile of random scalar field from a given point
pub fn field_correlation_profile<G, F, R>(
    graph: &G,
    point: G::Label,
    max_distance: usize,
    delta: usize,
    field: F,
    rng: &mut R,
) -> CorrelationProfile<f64>
where
    G: Graph,
    F: Field<G, Type = f64>,
    R: Rng + ?Sized,
{
    let point0 = point;

    let spheres = GeodesicSpheres::from_origin(graph, point, max_distance);
    let origin: f64 = field.get_sphere_average(point0, delta);

    let mut profile = Vec::with_capacity(max_distance);
    for distance in 0..max_distance {
        let point1 = spheres.sample_node(distance, rng);
        profile.push(field.get_sphere_average(point1, delta));
    }

    CorrelationProfile {
        origin,
        profile: profile.into_boxed_slice(),
    }
}

/// Struct holding the correlation profile of the average sphere distance
#[derive(Debug, Clone, Serialize)]
pub struct ASDCorrelationProfile {
    origin_asd: f64,
    profile: Box<[f64]>,
    violation_ratio: f64,
    correlation_violations: Box<[NodeLabelMatch]>,
    area_profile_origin: Box<[usize]>,
    area_profile_pair: Box<[usize]>,
}

impl Observable for ASDCorrelationProfile {}

impl Display for ASDCorrelationProfile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Average sphere distance correlation profile:")?;

        for second in self.profile.iter() {
            write!(f, "({:>5.3}, {:<5.3}) ", self.origin_asd, second)?;
        }
        writeln!(f)?;
        for violation in self.correlation_violations.iter() {
            write!(f, "{:^14} ", violation.violation_string())?;
        }
        writeln!(f)?;
        writeln!(f, "violation ratio: {}", self.violation_ratio)
    }
}

/// Computes the correlation profile from a given origin
pub fn osd_correlation_profile<G, R>(
    graph: &G,
    origin: G::Label,
    max_distance: usize,
    delta: usize,
    rng: &mut R,
) -> ASDCorrelationProfile
where
    G: Graph,
    R: Rng + ?Sized,
{
    let spheres = GeodesicSpheres::from_origin(graph, origin, max_distance);
    let osd0 = overlapping_sphere_distance_from_sphere(graph, spheres.get_sphere(delta), delta);

    let mut osd1 = Vec::with_capacity(max_distance);
    let mut sphere_area_origin = Vec::with_capacity(max_distance);
    let mut sphere_area_pair = Vec::with_capacity(max_distance);
    let mut correlation_violations = Vec::with_capacity(max_distance);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(max_distance + 1);
    violation_ratios.push(osd0.violation_ratio);

    for distance in 0..max_distance {
        let origin1 = spheres.sample_node(distance, rng);
        // Determine sphere areas for bias compensation
        sphere_area_origin.push(spheres.get_sphere(distance).len());
        let ((sphere1, sphere_outer), correlation_violation) =
            get_two_geodesic_spheres_contains_inner(graph, origin1, (delta, distance), origin);
        sphere_area_pair.push(sphere_outer.len());
        let AverageSphereDistance {
            asd: osd,
            violation_ratio,
        } = overlapping_sphere_distance_from_sphere(graph, &sphere1, delta);
        osd1.push(osd);
        correlation_violations.push(correlation_violation);
        violation_ratios.push(violation_ratio);
    }

    let size = violation_ratios.len() as f64;
    ASDCorrelationProfile {
        origin_asd: osd0.asd,
        profile: osd1.into_boxed_slice(),
        violation_ratio: violation_ratios.iter().sum::<f64>() / size,
        correlation_violations: correlation_violations.into_boxed_slice(),
        area_profile_origin: sphere_area_origin.into_boxed_slice(),
        area_profile_pair: sphere_area_pair.into_boxed_slice(),
    }
}

struct RandomScalarField<'a, G> {
    graph: &'a G,
    field: LabelledArray<Option<f64>, Label<Node>>,
    dist: Normal<f64>,
}

impl<'a, G: Graph> RandomScalarField<'a, G> {
    /// Create a new [RandomScalarField] on a given `graph`
    fn new(graph: &'a G, mean: f64, std: f64) -> Self {
        let field = LabelledArray::fill(|| None, graph.len());
        let dist = Normal::new(mean, std).unwrap();
        RandomScalarField { graph, field, dist }
    }

    fn get<R: Rng + ?Sized>(&mut self, node: G::Label, rng: &mut R) -> f64 {
        let elem = &mut self.field[node.get_label()];
        *elem.get_or_insert_with(|| self.dist.sample(rng))
    }

    /// Compute the average field value, averaged over a geodesic sphere of radius `delta` from
    /// origin `point`.
    fn sphere_average<R: Rng + ?Sized>(
        &mut self,
        point: G::Label,
        delta: usize,
        rng: &mut R,
    ) -> f64 {
        let mut sum = 0.0;
        let mut count: u32 = 0;
        for node in self.graph.iter_breadth_first(point) {
            if node.distance > delta {
                break;
            }
            sum += self.get(node.label, rng);
            count += 1;
        }
        sum / count as f64
    }
}

/// Struct representing a general correlation profile of any observable of type T
#[derive(Debug, Clone, Serialize)]
pub struct CorrelationProfile<T> {
    origin: T,
    profile: Box<[T]>,
}

impl<T: Serialize + Display> Observable for CorrelationProfile<T> {}

impl<T: Display> Display for CorrelationProfile<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Correlation profile:")?;

        for second in self.profile.iter() {
            write!(f, "({:>5.3}, {:<5.3}) ", self.origin, second)?;
        }
        writeln!(f)
    }
}

/// Computes the correlation profile of random scalar field from a given point
pub fn random_correlation_profile<G, R>(
    graph: &G,
    point: G::Label,
    max_distance: usize,
    delta: usize,
    rng: &mut R,
    mean: f64,
    std: f64,
) -> CorrelationProfile<f64>
where
    G: Graph,
    R: Rng + ?Sized,
{
    let point0 = point;
    let mut field = RandomScalarField::new(graph, mean, std);

    let spheres = GeodesicSpheres::from_origin(graph, point, max_distance);
    let origin: f64 = field.sphere_average(point0, delta, rng);

    let mut profile = Vec::with_capacity(max_distance);
    for distance in 0..max_distance {
        let point1 = spheres.sample_node(distance, rng);
        profile.push(field.sphere_average(point1, delta, rng));
    }

    CorrelationProfile {
        origin,
        profile: profile.into_boxed_slice(),
    }
}

/// Computes the correlation profile from a given origin
///
/// E.g. it computes the correlation pairs for all distances from 0
/// up to `max_distance` at given scale `delta`, from a given `origin`
pub fn asd_correlation_profile<G, R>(
    graph: &G,
    origin: G::Label,
    max_distance: usize,
    delta: usize,
    rng: &mut R,
) -> ASDCorrelationProfile
where
    G: Graph,
    R: Rng + ?Sized,
{
    let spheres = GeodesicSpheres::from_origin(graph, origin, max_distance);
    let asd_spheres = SpheresASD::from_origin_sphere(
        graph,
        origin,
        spheres.get_sphere(delta).clone(),
        delta,
        rng,
    );
    let asd0 = asd_spheres.average_sphere_distance();

    let mut asd1 = Vec::with_capacity(max_distance);
    let mut sphere_area_origin = Vec::with_capacity(max_distance);
    let mut sphere_area_pair = Vec::with_capacity(max_distance);
    let mut correlation_violations = Vec::with_capacity(max_distance);
    let mut violation_ratios: Vec<f64> = Vec::with_capacity(max_distance + 1);
    violation_ratios.push(asd0.violation_ratio);

    for distance in 0..max_distance {
        let origin1 = spheres.sample_node(distance, rng);
        sphere_area_origin.push(spheres.get_sphere(distance).len());
        let ((sphere1, sphere_outer), mut correlation_violation) =
            get_two_geodesic_spheres_contains_inner(graph, origin1, (delta, distance), origin);
        sphere_area_pair.push(sphere_outer.len());
        let (spheres1, correlation_violation_other) =
            SpheresASD::from_origin_sphere_contains(graph, origin1, sphere1, delta, origin, rng);
        correlation_violation.update(correlation_violation_other);
        let AverageSphereDistance {
            asd,
            violation_ratio,
        } = spheres1.average_sphere_distance();
        asd1.push(asd);
        correlation_violations.push(correlation_violation);
        violation_ratios.push(violation_ratio);
    }

    let size = violation_ratios.len() as f64;
    ASDCorrelationProfile {
        origin_asd: asd0.asd,
        profile: asd1.into_boxed_slice(),
        violation_ratio: violation_ratios.iter().sum::<f64>() / size,
        correlation_violations: correlation_violations.into_boxed_slice(),
        area_profile_origin: sphere_area_origin.into_boxed_slice(),
        area_profile_pair: sphere_area_pair.into_boxed_slice(),
    }
}

// /// Computes the correlation profile from a given origin,
// /// where the ASD is associated with the point in the middle of the two sphere origins.
// ///
// /// For correlation profile of ASD using the origin of one of the circles see:
// /// [[asd_correlation_profile]]
// pub fn asd_center_correlation_profile<G, R>(
//     graph: &G,
//     point: G::Label,
//     max_distance: usize,
//     delta: usize,
//     rng: &mut R,
// ) -> ASDCorrelationProfile
// where
//     G: Graph,
//     R: Rng + ?Sized,
// {
//     let spheres = GeodesicSpheres::from_origin(graph, point, max_distance);
//     // OPTIMIZABLE: This used some of the geodesic sphere from `spheres`, so could be optimized
//     let asd_spheres0 = SpheresASD::from_center(graph, point, delta, rng);
//     let asd0 = asd_spheres0.average_sphere_distance();
//
//     let mut asd1 = Vec::with_capacity(max_distance);
//     let mut correlation_violations = Vec::with_capacity(max_distance);
//     let mut violation_ratios: Vec<f64> = Vec::with_capacity(max_distance + 1);
//     violation_ratios.push(asd0.violation_ratio);
//
//     for distance in 0..max_distance {
//         let point1 = spheres.sample_node(distance, rng);
//         let (spheres1, correlation_violation) =
//             SpheresASD::from_center_contains(graph, point1, delta, point, rng);
//         let AverageSphereDistance {
//             asd,
//             violation_ratio,
//         } = spheres1.average_sphere_distance();
//         asd1.push(asd);
//         correlation_violations.push(correlation_violation);
//         violation_ratios.push(violation_ratio);
//     }
//
//     let size = violation_ratios.len() as f64;
//     ASDCorrelationProfile {
//         origin_asd: asd0.asd,
//         profile: asd1.into_boxed_slice(),
//         violation_ratio: violation_ratios.iter().sum::<f64>() / size,
//         correlation_violations: correlation_violations.into_boxed_slice(),
//     }
// }

struct GeodesicSpheres<G: Graph> {
    spheres: Box<[GeodesicSphere<G::Label>]>,
}

impl<G: Graph> GeodesicSpheres<G> {
    fn from_origin(graph: &G, origin: G::Label, max_radius: usize) -> Self {
        let mut spheres: Vec<Vec<G::Label>> =
            iter::repeat_with(Vec::new).take(max_radius + 1).collect();
        let bfs = graph.iter_breadth_first(origin);
        for node in bfs {
            if node.distance > max_radius {
                break;
            }
            spheres[node.distance].push(node.label);
        }
        Self {
            spheres: spheres
                .into_iter()
                .map(GeodesicSphere::from_vec)
                .collect::<Vec<GeodesicSphere<G::Label>>>()
                .into_boxed_slice(),
        }
    }
}

impl<G: Graph> GeodesicSpheres<G> {
    fn get_sphere(&self, radius: usize) -> &GeodesicSphere<G::Label> {
        &self.spheres[radius]
    }

    fn iter(&self) -> Iter<GeodesicSphere<G::Label>> {
        self.spheres.iter()
    }

    /// Sample a node at the sphere at given `radius`
    fn sample_node<R: Rng + ?Sized>(&self, radius: usize, rng: &mut R) -> G::Label {
        let sphere = self.get_sphere(radius);
        sphere[rng.gen_range(0..sphere.len())]
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;

    use super::*;
    use crate::triangulation::FixedTriangulation;

    impl<G: Graph> GeodesicSpheres<G> {
        fn origin(&self) -> G::Label {
            self.spheres[0][0]
        }

        fn volumes(&self) -> Box<[usize]> {
            self.spheres
                .iter()
                .map(|sphere| sphere.len())
                .collect::<Vec<usize>>()
                .into_boxed_slice()
        }
    }

    fn random_triangulation<R: Rng + ?Sized>(rng: &mut R) -> FullTriangulation {
        random_triangulation_sized(50, 40, rng)
    }

    fn random_triangulation_sized<R: Rng + ?Sized>(
        vertex_length: usize,
        timeslices: usize,
        rng: &mut R,
    ) -> FullTriangulation {
        let mut triangulation = FixedTriangulation::new(vertex_length, timeslices);
        for _ in 0..10_000 {
            triangulation.flip(triangulation.sample_flip(rng));
        }
        for _ in 0..50_000 {
            triangulation.flip(triangulation.sample_flip(rng));
            triangulation.relocate(
                triangulation.sample_order_four(rng),
                triangulation.sample(rng),
            )
        }
        FullTriangulation::from_triangulation(&triangulation)
    }

    #[test]
    fn two_geodesic_spheres() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let graph = random_triangulation(&mut rng).construct_vertex_graph();
        let origin = graph.sample_node(&mut rng);

        let (sphere, sphere_cor) = get_two_geodesic_spheres(&graph, origin, (1, 2));
        println!(
            "delta size: {}, correlation size: {}",
            sphere.len(),
            sphere_cor.len()
        );
        assert_eq!(sphere.len(), 6);
        assert_eq!(sphere_cor.len(), 17);

        let (sphere, sphere_cor) = get_two_geodesic_spheres(&graph, origin, (3, 3));
        println!(
            "delta size: {}, correlation size: {}",
            sphere.len(),
            sphere_cor.len()
        );
        assert_eq!(sphere.len(), 32);
        assert_eq!(sphere_cor.len(), 32);

        let (sphere, sphere_cor) = get_two_geodesic_spheres(&graph, origin, (3, 2));
        println!(
            "delta size: {}, correlation size: {}",
            sphere.len(),
            sphere_cor.len()
        );
        assert_eq!(sphere.len(), 32);
        assert_eq!(sphere_cor.len(), 17);
    }

    #[test]
    fn correlation() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let graph = random_triangulation(&mut rng).construct_vertex_graph();
        let origin = graph.sample_node(&mut rng);

        let delta = 3;
        let distance = 5;
        let cor = asd_correlation(&graph, origin, distance, delta, &mut rng);
        println!("{:?}", cor);
    }

    #[test]
    fn geodesic_spheres() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let graph = random_triangulation(&mut rng).construct_vertex_graph();
        let origin = graph.sample_node(&mut rng);

        let spheres = GeodesicSpheres::from_origin(&graph, origin, 5);
        assert_eq!(spheres.origin(), origin);
        for (i, &distance) in spheres.volumes().iter().enumerate() {
            assert_eq!(graph.distance_profile(origin, 5).get_count(i), distance);
        }
        println!("sphere sizes: {:?}", spheres.volumes())
    }

    #[test]
    fn correlation_profile() {
        let mut rng = Xoshiro256StarStar::seed_from_u64(42);
        let graph = TiledPlanarGraph::from_stacked_graph(
            random_triangulation_sized(20, 30, &mut rng).construct_stacked_vertex_graph(),
        );
        let origin = graph.sample_node(&mut rng);

        let delta = 3;
        let distance = 12;
        let cor = asd_correlation_profile(&graph, origin, distance, delta, &mut rng);
        println!("{:}", cor);
    }
}
