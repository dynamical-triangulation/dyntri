//! Markov chain Monte Carlo implementation of 2D Causal Dynamcial Triangulations (CDT)
//!
//! An implementation with volume changing Monte Carlo moves is implemented in [`dynamic_universe`]
//! and a implementation with volume preserving moves is implemented in [`fixed_universe`].
//! The [`fixed_universe`] implementation should be faster and give more triangulation of the
//! desired size compared to the dynamic one, so should probably be preferred.
//! The correlation time of the fixed size universe does seem to be higher however, but in my tests
//! it seems the benifits still outweight this drawback.

pub mod dynamic_universe;
pub mod fixed_universe;
