use crate::graph::{
    field::{Field, PeriodicField},
    periodic_graph::{PeriodicGraph, PeriodicLabel, PeriodicLabelMatch},
    FieldGraph,
};

/// Collection for keeping track of a list, marking the items
#[derive(Debug, Clone)]
pub struct MarkedList<T, F, G: FieldGraph<FieldType<T> = F>> {
    list: G::FieldType<T>,
    len: usize,
}

impl<T, G: FieldGraph> MarkedList<T, G::FieldType<T>, G> {
    pub fn new(graph: &G) -> Self {
        let list = graph.new_field();
        MarkedList { list, len: 0 }
    }
}

impl<G: FieldGraph> MarkedList<(), G::FieldType<()>, G> {
    /// Adds an iterator of labels to the [MarkedList]
    pub fn add_multiple(&mut self, marks: impl Iterator<Item = G::Label>) {
        for mark_label in marks {
            self.add(mark_label);
        }
    }
}

impl<T, G: FieldGraph> MarkedList<T, G::FieldType<T>, G> {
    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl<G: FieldGraph> MarkedList<(), G::FieldType<()>, G> {
    #[inline]
    /// Adds a label to the [MarkedList], does nothing if it is already present
    pub fn add(&mut self, label: G::Label) {
        if self.list.set(label, ()) {
            self.len += 1;
        }
    }
}

impl<T, G: FieldGraph> MarkedList<T, G::FieldType<T>, G> {
    /// Try to remove `label` from the [`MarkedList`], returning if the label was found
    /// and thus removed.
    pub fn try_remove(&mut self, label: G::Label) -> bool {
        let removed = self.list.take(label).is_some();
        if removed {
            self.len -= 1;
        }
        removed
    }
}

impl<T, F, G> MarkedList<T, F, G>
where
    F: PeriodicField<T, G>,
    G::Label: PeriodicLabel,
    G: FieldGraph<FieldType<T> = F> + PeriodicGraph,
{
    /// Try to remove the label from the [MarkedList], returns the [NodeLabelMatch::DifferentPatch]
    /// if the label was not found, [NodeLabelMatch::DifferentPatch] if it was found but in a
    /// different patch, and [NodeLabelMatch::Equal] if it was found in the same patch and is
    /// removed from the marked list.
    pub fn try_remove_periodic(&mut self, label: G::Label) -> PeriodicLabelMatch {
        let labelmatch = PeriodicField::has(&self.list, label);
        if matches!(labelmatch, PeriodicLabelMatch::Equal) {
            self.list.take(label);
            self.len -= 1;
        }
        labelmatch
    }
}
