//! This module contains everything to do with constructing, manipulating and analysing
//! a `Triangulation`, meaning a construction of `Triangle`s glued together at their edges.

mod dynamic_triangulation;
mod fixed_triangulation;
pub mod full_triangulation;
mod unique_id;

pub use dynamic_triangulation::DynTriangulation;
pub use fixed_triangulation::FixedTriangulation;
pub use full_triangulation::FullTriangulation;
pub use unique_id::TriangulationID;

mod triangle;

use crate::collections::Label as TypedLabel;
pub use triangle::{Orientation, Triangle};

type Label = TypedLabel<Triangle>;

/// General Triangulation trait
pub trait Triangulation<'a> {
    /// Return iterator over the labels in the triangulation
    fn labels(&'a self) -> Box<dyn Iterator<Item = Label> + 'a>;

    /// Return the triangle at `label`
    fn get(&self, label: Label) -> &Triangle;

    /// Return the currect size of the triangulation
    fn size(&self) -> usize;

    /// Return the maximum size the triangulation can become
    fn max_size(&self) -> usize;
}
