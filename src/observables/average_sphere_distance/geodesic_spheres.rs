//! Handling of geodesic sphere pairs as helper structures for computing the average sphere distance

use std::{cmp::Ordering, iter, ops::Index};

use rand::Rng;

use crate::graph::breadth_first_search::traverse_nodes::TraverseNodeDistance;
use crate::graph::{breadth_first_search::IterNeighbours, IterableGraph};

/// A geodesic sphere as a list of labels of nodes in the sphere
///
/// Implemented as a wrapper of a boxed slice
#[derive(Debug, Clone)]
pub struct GeodesicSphere<L>(Box<[L]>);

/// A pair of geodesic spheres
pub type GeodesicSpherePair<L> = (GeodesicSphere<L>, GeodesicSphere<L>);

impl<L> Index<usize> for GeodesicSphere<L> {
    type Output = L;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<L> GeodesicSphere<L> {
    pub(super) fn from_vec(vec: Vec<L>) -> Self {
        Self(vec.into_boxed_slice())
    }
}

impl<L> GeodesicSphere<L> {
    /// Returns the volume or number of nodes of the geodesic sphere
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether the sphere has no elements
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns an iterator of the nodes in the sphere
    pub fn iter(&self) -> std::slice::Iter<'_, L> {
        self.0.iter()
    }
}

impl<L: PartialEq> GeodesicSphere<L> {
    /// Returns whether the geodesics sphere contains a certain label `label`
    pub fn contains(&self, label: &L) -> bool {
        self.0.contains(label)
    }
}

/// Returns a list of labels that label the nodes a distance `radius` from the given `origin`
pub fn get_geodesic_sphere<'a, I, G>(
    graph: &'a G,
    origin: G::Label,
    radius: usize,
) -> GeodesicSphere<G::Label>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I>,
{
    // TODO: Make better prediction for vector size based on Haussdorf dimension measurement
    let expected_length = 10 * radius;
    let mut sphere = Vec::with_capacity(expected_length); // List of nodes in sphere around origin

    // Find origin sphere
    for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
        match node.distance.cmp(&radius) {
            Ordering::Less => continue,
            Ordering::Equal => sphere.push(node.label),
            Ordering::Greater => break,
        }
    }

    GeodesicSphere::from_vec(sphere)
}

/// Returns a [`GeodesicSpherePair`] of two [`GeodesicSphere`] centered at `origin` with different
/// radii.
#[allow(dead_code)]
pub fn get_two_geodesic_spheres<'a, I, G>(
    graph: &'a G,
    origin: G::Label,
    radii: (usize, usize),
) -> GeodesicSpherePair<G::Label>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I>,
{
    // Sort radii such that first is always smaller
    let (radii, flipped) = match radii.0.cmp(&radii.1) {
        Ordering::Less => (radii, false),
        Ordering::Greater => ((radii.1, radii.0), true),
        Ordering::Equal => {
            // If equal simply use the single geodesic sphere finder
            let sphere = get_geodesic_sphere(graph, origin, radii.0);
            return (sphere.clone(), sphere);
        }
    };

    // OPTIMIZABLE: Make prediction for vector size based on Haussdorf dimension measurement, and pre-allocate
    let mut inner_sphere = Vec::new();
    let mut outer_sphere = Vec::new();

    // Find origin inner sphere
    let mut bfs = graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin);
    for node in &mut bfs {
        match node.distance.cmp(&radii.0) {
            Ordering::Less => continue,
            Ordering::Equal => inner_sphere.push(node.label),
            Ordering::Greater => {
                // Also include the node if radii.1 == radii.0 + 1
                if node.distance == radii.1 {
                    outer_sphere.push(node.label);
                }
                break;
            }
        }
    }
    // Find outer sphere
    for node in bfs {
        match node.distance.cmp(&radii.1) {
            Ordering::Less => continue,
            Ordering::Equal => outer_sphere.push(node.label),
            Ordering::Greater => break,
        }
    }

    let inner_sphere = GeodesicSphere::from_vec(inner_sphere);
    let outer_sphere = GeodesicSphere::from_vec(outer_sphere);
    if flipped {
        (outer_sphere, inner_sphere)
    } else {
        (inner_sphere, outer_sphere)
    }
}

/// A collection of [`GeodesicSphere`]s used to store all geodesic spheres around a single
/// origin with different radii.
pub struct GeodesicSpheres<L>(pub Box<[GeodesicSphere<L>]>);

/// Returns [`GeodesicSpheres`] around `origin` with radius 0 up to and including `max_radius`.
pub fn construct_geodesic_spheres<'a, I, G>(
    graph: &'a G,
    origin: G::Label,
    max_radius: usize,
) -> GeodesicSpheres<G::Label>
where
    I: IterNeighbours<G>,
    G: IterableGraph<'a, I>,
{
    let mut spheres: Vec<Vec<G::Label>> =
        iter::repeat_with(Vec::new).take(max_radius + 1).collect();
    for node in graph.iter_breadth_first::<TraverseNodeDistance<_>>(origin) {
        if node.distance > max_radius {
            break;
        }
        spheres[node.distance].push(node.label);
    }
    GeodesicSpheres(
        spheres
            .into_iter()
            .map(GeodesicSphere::from_vec)
            .collect::<Vec<GeodesicSphere<G::Label>>>()
            .into_boxed_slice(),
    )
}

impl<L: Copy> GeodesicSpheres<L> {
    /// Returns a [`GeodesicSphere`] of the given `radius`.
    pub fn get_sphere(&self, radius: usize) -> &GeodesicSphere<L> {
        &self.0[radius]
    }

    /// Sample a node at the sphere at given `radius`
    pub fn sample_node<R: Rng + ?Sized>(&self, radius: usize, rng: &mut R) -> L {
        let sphere = self.get_sphere(radius);
        sphere[rng.gen_range(0..sphere.len())]
    }
}
