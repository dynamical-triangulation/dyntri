//! Module containing a buffered writer to write 1D data to a HDF5 [Dataset]

use hdf5::{Dataset, H5Type};
use log::{log_enabled, warn};
use ndarray::s;

/// The default size of the buffer of [BufWriter] in bytes
pub const DEFAULT_BUFFER_SIZE: usize = 1_048_576; // in bytes

/// A buffered writer to write 1D data to a HDF5 [Dataset].
///
/// New data can be added through [`BufWriter::push()`] such that the buffer is flushed
/// when the capacity is reached, the buffer goes out of scope,
/// or manually with [`BufWriter::flush()`].
pub struct BufWriter<'a, T: H5Type> {
    /// The HDF5 dataset that the [`BufWriter`] is writing to
    pub dataset: &'a Dataset,
    dataset_index: usize,
    buffer: Vec<T>,
    capacity: usize,
}

impl<'a, T: H5Type> BufWriter<'a, T> {
    /// Create a new [BufWriter] with a default buffer size of `DEFAULT_BUFFER_SIZE`
    /// (at time of writing this is 1MiB, but do check the constant if you want to be certain)
    pub fn new(dataset: &'a Dataset) -> Self {
        let type_size = std::mem::size_of::<T>();
        let buffer_size = DEFAULT_BUFFER_SIZE / type_size;
        Self::with_capacity(dataset, buffer_size)
    }

    /// Create a new [BufWriter] to  `dataset` with a given `buffer_size`.
    ///
    /// If `dataset` is chunked, it is the most efficient to make `buffer_size` a multiple of
    /// the _chunk size_. Such that only full chunks have to be written to at a time.
    pub fn with_capacity(dataset: &'a Dataset, buffer_size: usize) -> Self {
        let buffer = Vec::with_capacity(buffer_size);
        Self {
            dataset,
            buffer,
            dataset_index: 0,
            capacity: buffer_size,
        }
    }

    /// Returns the index of the next value to be pushed to the dataset,
    /// i.e. the amount of values that have been pushed to the [BufWriter]
    pub fn index(&self) -> usize {
        self.dataset_index + self.buffer.len()
    }

    /// Push a new value to the buffer, flushing to the [Dataset] if the capacity if filled
    pub fn push(&mut self, value: T) -> hdf5::Result<()> {
        self.buffer.push(value);
        if self.buffer.len() >= self.capacity {
            self.flush()?;
        }
        Ok(())
    }

    /// Flush the buffer by writing the buffer to the [Dataset], clearing the buffer
    pub fn flush(&mut self) -> hdf5::Result<()> {
        let istart = self.dataset_index;
        let iend = istart + self.buffer.len();
        self.dataset.write_slice(&self.buffer, s![istart..iend])?;
        // Clear buffer and setup index for next write
        self.buffer = Vec::with_capacity(self.capacity);
        self.dataset_index = iend;
        Ok(())
    }
}

impl<T: H5Type> Drop for BufWriter<'_, T> {
    fn drop(&mut self) {
        if log_enabled!(log::Level::Warn) {
            let size_written = self.index();
            let &[size_dataset] = &self.dataset.shape()[..] else {
                panic!("The dataset was not 1D");
            };
            if size_written != size_dataset {
                warn!("Written {size_written} values to buffer, while size of dataset is {size_dataset}");
            }
        }
        self.flush()
            .expect("Buffer could not be written to dataset.");
    }
}
