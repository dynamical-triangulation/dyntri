//! The `collections` module contains all custom collection types
//! used in the Dynamical Triangulation simulations.
//! Most notably: [`LabelSet`] and [`LabelledList`]

// #![warn(missing_docs)]

pub mod label;
pub mod label_set;
pub mod labelled_list;
pub mod labelled_array;

// Make central collection data structures directly visibly in collections module
pub use label::Label;
pub use label_set::LabelSet;
pub use labelled_list::LabelledList;
pub use labelled_array::LabelledArray;